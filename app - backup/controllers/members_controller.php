<?php

App::import('Vendor','highwindsAPI',array('file'=>'highwindsAPI.php'));
App::import('Vendor','storageNinjaAPI',array('file'=>'apiStorageNinja.php'));
App::import('Vendor','firstDataAPI',array('file'=>'firstDataAPI.php'));
//App::import('Vendor','MailChimp',array('file'=>'apiMailchimp.php'));

class MembersController extends AppController {
	
	
	var $name = 'Members';
	var $helpers = array('Html', 'Form');
	
	var $paginate = array('limit' => 10);
	
	var $close_success = false;
	
	var $yesNo = array('Y' => 'Yes',
	                   'N' => 'No');
	
	var $closeReasons = array('billing failure' => 'Billing Failure',
	                   		  'fraud' => 'Fraud',
	                   		  'user cancel' => "User Cancellation",
	                   		  'other' => "Other");

	
	function address($id) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('The member you are trying to edit does not exist.', true), 'error');
			$this->redirect(array('action'=>'search'));
		}
		
		if (!empty($this->data)) {
			if ($this->Member->save($this->data)) {
				$this->logActivity($this->data['Member']['id'], 'compliance', 'members-change-address', 'success', '', '');
				$this->Session->setFlash(__('The members data was saved successfully', true), 'flash_success');
				$this->redirect(array('action' => 'view', $id));
			} 
			else {
				$this->Session->setFlash(__('The members data could not be updated. Please, try again.', true), 'error');
			}
		}
		
		if (empty($this->data)) {
			$this->data = $this->Member->read(null, $id);
		}
		
		$this->pageTitle = "eManager - Change Address";
		
		
		// PCI COMPLIANCE LOG
		// $member_id, $log_type, $log_reason, $value_type, $old, $new
		$this->logActivity($id, 'compliance', 'members-change-address', 'view', '', '');
	}
	
	function beforeFilter() {
		
		parent::beforeFilter(); 
		$this->Auth->allow('job_mailchimp_subscribe');
		
		$this->Session->write('ActivePage','members');

	}
	
	/**
	 * Transfers user to blacklist interface
	 *
	 * @param unknown_type $ip
	 */
	function blacklist_ip($ip,$reason) {
		
		$this->loadModel("Blacklist");
		$check = $this->Blacklist->find('count', array('conditions' => array('IP' => $ip)));
		
		if (!$check) {		
			
			if (!empty($ip)) {
				
				if ($this->Blacklist->blacklistIP($ip, $reason)) {
					$this->logActivity($member['Member']['id'], 'compliance', 'members-blacklist-ip', 'success', '', '');
					$this->Session->setFlash(__('This IP was successfullly blacklisted', true), 'flash_success');
				}
				else {
					$this->logActivity($member['Member']['id'], 'compliance', 'members-blacklist-ip', 'fail', '', '');
					$this->Session->setFlash(__('There was an error blacklisting this IP address', true), 'error');
				}
			}
			else {
				$this->Session->setFlash(__('Please provide an IP address to blacklist.', true), 'error');
			}
			
			$this->Session->write('Blacklist.IP', $ip);
			$this->redirect($this->referer());
				
			
		}
		else {
			$this->Session->setFlash(__('The IP address has already been blacklisted', true), 'error');
			$this->redirect(array('action' => 'report_ip', $ip));
		}
		
		
		// PCI COMPLIANCE LOG
		$this->logActivity($member['Member']['id'], 'compliance', 'members-blacklist-ip', 'view', '', '');
		
	}
	
	/**
	 * Function closes account on given date provided from close or direct cancel from site
	 *
	 * @param unknown_type $id
	 * @param unknown_type $expire_date
	 */
	function cancel($id, $expire_date) {
		
		if (!$id && $expire_date) {
			$this->Session->setFlash(__('Not enough information provided to cancel account.', true), 'error');
			$this->redirect($this->referer());
		}
		
		// perform account close process
		$this->process_account_close($id, $expire_date, 'fraud', false);
		
		if ($this->close_success) { $this->redirect(array('action'=>'view',$id)); }
		else { $this->redirect($this->referer()); }
				
		// PCI COMPLIANCE LOG
		$this->logActivity($member['Member']['id'], 'compliance', 'members-cancel', 'view', '', '');		
	}
	
	function change_plan($id, $new_plan = null) {
		
		// make pre-emptive check to see if 
		if (!$id && empty($new_plan)) {
			$this->Session->setFlash(__('Please select a customer whose plan you are trying to change.', true), 'error');
			$this->redirect(array('action'=>'search'));
		}
	
		$this->loadModel("Plan");
		
		// if this is the post back, change plans in hw and save the data
		if (!empty($new_plan)) {
					
			// get member details
			$myMember = $this->Member->getMember($id);
			
			// get new plan details
			$this->Plan->recursive = 0;
			$plan = $this->Plan->find('first',array('conditions' => array('id' => $new_plan)));
		
			// change the plan in highwinds.
			$hw = new highwindsAPI();
			$hw->changeUserPlan($myMember['Member']['login_username'], $plan['Plan']['server_group_id'], $plan['Plan']['virtual_server_id']);
			
			if ($hw->success) {
				
				$myMember['Member']['plan_id'] = $new_plan;
				
				if ($this->Member->save($myMember)) {
					$this->Session->setFlash(__('The members plan was changed successfully', true), 'flash_success');
					$this->redirect(array('action' => 'view', $id));
				} 
				else {
					$this->Session->setFlash(__('The members plan was changed in Highwinds but could not be saved. Please, update manually.', true), 'error');
				}
			}
			else {
				$this->Session->setFlash(__('The members plan was not changed.', true), 'error');
				//$this->redirect(array('action' => 'view', $id));
			}
		}
		
		// get member information
		$member = $this->Member->read(null, $id);
		
		// if this is a world pay customer, the plan cannot be changed.
		if ($member['Member']['pay_type'] == 'WP') {
			$this->Session->setFlash(__('WorldPay Customers cannot change plans.  They must cancel and re-subscribe to change their plans.', true), 'error');
			$this->redirect($this->referer());
		}
				
		// get active plans
		$this->Plan->recursive = 0; 
		$plans = $this->Plan->find('all',array('conditions' => array('Plan.id >' => 50)));
		
		// get coupon for customer
		if (!empty($member['Member']['coupon_id'])) {
			$this->loadModel("Coupon");
			$coupon = $this->Coupon->find('first',array('conditions' => array('Coupon.id >' =>$member['Member']['coupon_id'])));
			$percentDiscount = $coupon['Coupon']['discount'];
		}
		else 
			$percentDiscount = 0;
		
		// set data for display
		$this->set(compact('member', 'plans', 'percentDiscount'));
		
		$this->pageTitle = "eManager - Change Customer Plan";
		
		// PCI COMPLIANCE LOG
		// $member_id, $log_type, $log_reason, $value_type, $old, $new
		$this->logActivity($member['Member']['id'], 'compliance', 'members-change-plan', 'success', '', '');
	}
	
	function close($id) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('The account you are trying to close does not exist.', true), 'error');
			$this->redirect(array('action'=>'search'));
		}
		
		// capture customer account id
		if (!empty($id)) { $this->Session->write('Member.id',$id); }
		
		if (!empty($this->data)) {

			// retrieve member information
			$member = $this->Member->read(null, $id);				
			$this->process_account_close($id, $this->data['Member']['my_expire_date'], $this->data['Member']['reason']);
			
			// redirect to wp close form if necessary
			/*
if ($this->close_success && $member['Member']['pay_type'] == 'WP') {								
				$this->redirect(array('action'=>'wp-close'));
			}
*/

			if ($this->close_success) {
				$this->redirect(array('action'=>'view',$id));
			}
			
		}
		else {
			$this->data = $this->Member->read(null, $id);
			
		}
		
		$this->set('reasons',$this->closeReasons);
		$this->pageTitle = "eManager - Close Account";
	}	
	
	//TODO:  Remove hardcoded 50 plan id for free accounts
	function close_free_by_ip($ip) {
		$all_close = true;
		$expire_date = date('Y-m-d');
		$members = $this->Member->find('all', array('conditions' => array('Member.ip' => $ip, 'Member.plan_id' => 50)));
		
		foreach ($members as $member) {
			
			if ($member['Member']['date_expire'] == '0000-00-00 00:00:00') {
				$member['Member']['date_expire'] = $expire_date;
				$member['Member']['date_cancel'] = $expire_date;
				
				$hw = new highwindsAPI();
				$hw->setCloseDate($member['Member']['login_username'], $expire_date);
	
				if (!$hw->success) {
					$error_msg = "The following account could not be closed in highwinds. Follow up to ensure account is closed properly.  \n\n". $member['Member']['login_username'] . "  \nExpire: $expire_date \n\nHW Error: $hw->errorMessage";
					parent::send_support_email("HW Account Close Error", $error_msg);
					//$this->redirect($this->referer());
				}
				
				if (!$this->Member->save($member)) {
					$all_close = false;
				}
				
				// log transaction in cancels table
				$this->record_account_cancel($member['Member']['id'], $expire_date, $expire_date, 'free account fraud', $member['Member']['plan_id']);
				
				
			}
		}
		
		if ($all_close) {
			$this->Session->setFlash(__('All accounts were closed successfully', true), 'flash_success');
			$this->redirect($this->referer());
		}
		else {
			$this->Session->setFlash(__('Not all accounts were closed in our systems.', true), 'error');
			$this->redirect($this->referer());
		}
		
		
		// PCI COMPLIANCE LOG
		// $member_id, $log_type, $log_reason, $value_type, $old, $new
		$this->logActivity($member['Member']['id'], 'compliance', 'members-close-free-by-ip', 'success', '', '');
	}
	
	/**
	 * Enter description here...
	 *
	 */
	// create the account, log charge, prompt for payment details				
	function create_account() {
		
		if (!empty($this->data)) {
			
			// make sure an account does not exist with this email address
			$member = $this->Member->find('first', array('conditions' => array('email' => $this->data['Member']['email'])));
			
			if ($member) {
				$this->Session->setFlash(__('An account with this email address already exists. Please, try again.', true), 'error');
				$this->redirect(array('action'=>'view', $member['Member']['id']));
			}
			
			if ($this->data['Member']['free_admin_account'] == "Y") {
				$user_prefix = 'ngdcomp';
			}
			else {
				$user_prefix = 'ngd';			
			}
			
			$clear_password =  rand(1000,999999999);		
			
			$this->data['Member']['date_reg'] = date('Y-m-d H:i:s');
			$this->data['Member']['login_username'] = $user_prefix.rand(1000000,999999999);
			$this->data['Member']['login_password'] = md5($clear_password);
			$this->data['Member']['status'] = 'active';
			
			// get plan details
			$this->loadModel("Plan");
			$this->Plan->recursive = -1;
			$plan = $this->Plan->find('first', array('conditions' => array('Plan.id' => $this->data['Member']['plan_id'])));
							
			// create account in highwinds
			//TODO: Enable this segment for go live
			$hw = new highwindsAPI();
			$hw->insertUser($this->data['Member']['login_username'], $clear_password, $this->data['Member']['email'], $this->data['Member']['name'], $this->data['Member']['city'], $plan['Plan']['server_group_id'], $plan['Plan']['virtual_server_id']);
			
			
			if ($hw->success) {
				$customer = $hw->getUser($this->data['Member']['login_username']);
				$this->data['Member']['hw_cust_id'] = $customer[2];
				
				// save member data in ngd
				$this->Member->create();
				if ($this->Member->save($this->data)) {
					$this->Session->setFlash(__('The Customer account has been created', true), 'flash_success');
					
					parent::session_clean_member_id();
					parent::session_write_member_id($this->Member->id);
					
					// send email notifying them of their account information.
					$this->send_new_user_email($this->data['Member']['name'],$this->data['Member']['email'],$this->data['Member']['login_username'],$clear_password);
				
					$this->redirect('view/'.$this->Member->id);
					
				} else {
					$this->Session->setFlash(__('The account could not be created. Please, try again.', true), 'error');
				}
			}
			else { $this->Session->setFlash(__("There was an error creating the highwinds account.<br/>{$hw->errorMessage}", true), 'error'); }
			
			
		}
		
		$this->pageTitle = "eManager - Create New Account";
		
		$plans = $this->Member->Plan->find('list', array('conditions' => array('Plan.id >=' => 50)));
		$this->set(compact('plans'));
		
		
		// PCI COMPLIANCE LOG
		// $member_id, $log_type, $log_reason, $value_type, $old, $new
		$this->logActivity($member['Member']['id'], 'compliance', 'members-create-account', 'success', '', '');
	}
	
	/**
	 * create account in highwinds
	 *
	 */
	function create_hw_account($id) {
		
		if (!empty($this->data)) {
			
			// make sure an account does not exist with this email address
			$this->Member->recursive = -1;
			$member = $this->Member->find('first', array('conditions' => array('id' => $this->data['Member']['id'])));
			$member['Member']['login_password'] = md5($this->data['Member']['login_password']);
			
			// get plan details
			$this->loadModel("Plan");
			$this->Plan->recursive = -1;
			$plan = $this->Plan->find('first', array('conditions' => array('Plan.id' => $member['Member']['plan_id'])));
							
			// create account in highwinds
			$hw = new highwindsAPI();
			$hw->insertUser($member['Member']['login_username'], $this->data['Member']['login_password'], $member['Member']['email'], $member['Member']['name'], $member['Member']['city'], $plan['Plan']['server_group_id'], $plan['Plan']['virtual_server_id']);
			
			if ($hw->success) {
				$customer = $hw->getUser($member['Member']['login_username']);
				$member['Member']['hw_cust_id'] = $customer[2];
				
				// save member data in ngd
				if ($this->Member->save($member)) {
				
					$this->logActivity($member['Member']['id'], 'maintenance', 'hw account create', 'success', '', '');
				
					$this->Session->setFlash(__('The Customer account has been created', true), 'flash_success');
					$this->redirect('highwinds/'.$this->data['Member']['id']);
					
				} 
				else { $this->Session->setFlash(__('could not save new password and hw id. Please, try again.', true), 'error'); }
			
			}
			else {
				$this->Session->setFlash(__('The account could not be created in highwinds. Please, try again.', true), 'error'); 
				$this->Session->setFlash(__($hw->errorMessage, true), 'hw_error'); 
			}
			
			
		}
		
		if (empty($this->data)) {
			$this->Member->recursive = 0;
			$this->data = $this->Member->read(null, $id);
		}
		
		$this->pageTitle = "eManager - Create HW Account";
		
		// PCI COMPLIANCE LOG
		// $member_id, $log_type, $log_reason, $value_type, $old, $new
		$this->logActivity($member['Member']['id'], 'compliance', 'members-create-account-hw', 'success', '', '');
	}
	
	function create_storageninja_account($id) {
		
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('The member you are trying to edit does not exist.', true), 'error');
			$this->redirect(array('action'=>'search'));
		}
			
		if (!empty($this->data)) {
			
			// see if customer already has storageNinja account
			$member = $this->Member->read(null, $this->data['Member']['id']);
			
			if (empty($member)) {
				$this->Session->setFlash(__('This is not a valid customer account.', true), 'error');
				$this->redirect(array('action'=>'search'));
			}
			
			if (!empty($member['Member']['storageninja_id'])) {
				$this->Session->setFlash(__('This member already has a StorageNinja Account.', true), 'error');
				$this->redirect(array('action'=>'view',$this->data['Member']['id']));
			}
			
			$sn = new storageNinjaAPI();
			
			if (empty($member['Member']['state'])) { $member['Member']['state'] = 'state';}
			
			$snResult = $sn->addUser($member['Member']['email'], $this->data['Member']['sn_pwd'], $member['Member']['name'], $member['Member']['address'], $member['Member']['address2'], $member['Member']['city'], $member['Member']['state'], $member['Member']['postal'], $member['Member']['country']);
						
			if (!$sn->success) {
				$this->Session->setFlash(__("Could not create account at this time.<br/>{$sn->errorMessage}", true), 'error');
			}
			else {
			
				$member['Member']['storageninja_id'] = $snResult[1];
				
				if ($this->Member->save($member)) {
					$this->Session->setFlash(__('StorageNinja account created', true), 'flash_success');
					$this->redirect(array('action'=>'view',$this->data['Member']['id']));
				}
				else {
					$this->Session->setFlash(__('Failed to save StorageNinja id ('.$snResult[1].'), update manually.', true), 'error');
					$this->redirect(array('action'=>'view',$this->data['Member']['id']));
				}
			}

			
		}
		
		if (empty($this->data)) {
			$this->data = $this->Member->read(null, $id);
		}
		
		$this->Session->write('Member.id',$id);			
		$this->pageTitle = "eManager - Member Details";
		$this->pageTitle = "eManager - NGD SN Integration";
		
		// PCI COMPLIANCE LOG
		// $member_id, $log_type, $log_reason, $value_type, $old, $new
		$this->logActivity($member['Member']['id'], 'compliance', 'members-create-account-storageninja', 'success', '', '');
	}
	
	function edit($id) {
		
		//$this->validateUserAccess('support');
	
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('The member you are trying to edit does not exist.', true), 'error');
			$this->redirect(array('action'=>'search'));
		}
		
		if (!empty($this->data)) {
			$this->data['Member']['login_password'] = md5($this->data['Member']['login_password']);
			if ($this->Member->save($this->data)) {
				$this->Session->setFlash(__('The members data was saved successfully', true), 'flash_success');
				$this->redirect(array('action'=>'view',$this->data['Member']['id']));

			} else {
				$this->Session->setFlash(__('The members data could not be updated. Please, try again.', true), 'error');
			}
		}
		
		if (empty($this->data)) {
			$this->Member->recursive = 0;
			$this->data = $this->Member->read(null, $id);
		}
		
		$this->Session->write('Member.id',$id);			
		$this->pageTitle = "eManager - Member Details";
		
		
		// PCI COMPLIANCE LOG
		// $member_id, $log_type, $log_reason, $value_type, $old, $new
		$this->logActivity($this->data['Member']['id'], 'compliance', 'members-hw-usage-custom', 'success', '', '');
	}
	
	function editbilling($id) {
		
		//$this->validateUserAccess('admin');
	
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('The member you are trying to edit does not exist.', true), 'error');
			$this->redirect(array('action'=>'search'));
		}
		
		if (!empty($this->data)) {
			
			print_r($this->data);
			
			
			if ($this->Member->save($this->data)) {
				$this->Session->setFlash(__('The members data was saved successfully', true), 'flash_success');
			} else {
				$this->Session->setFlash(__('The members data could not be updated. Please, try again.', true), 'error');
			}
			
		}
		
		if (empty($this->data)) {
			$this->data = $this->Member->read(null, $id);
		}
		
		
		$this->pageTitle = "eManager - Member Details";
	}
	
	function edit_coupon($id) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('The member you are trying to edit does not exist.', true), 'error');
			$this->redirect(array('action'=>'search'));
		}
		
		if (!empty($this->data)) {
		
			$this->Member->recursive = 0;
			$member = $this->Member->read(null, $this->data['Member']['id']);
			
			if ($this->Member->save($this->data)) {
		
				// log the coupon change
				$this->logActivity($member['Member']['id'], 'account', 'coupon change', 'coupon_id', $member['Member']['coupon_id'], $this->data['Member']['coupon_id']);
			
				$this->Session->setFlash(__('The members data was saved successfully', true), 'flash_success');
				$this->redirect(array('action' => 'view', $id));
				
				
			} else {
				$this->Session->setFlash(__('The members data could not be updated. Please, try again.', true), 'error');
			}
		}
		
		if (empty($this->data)) {
			$this->data = $this->Member->read(null, $id);
		}
		
		$this->loadModel("Coupon");
		$this->set('coupons', $this->Coupon->find('list', array('fields' => array('Coupon.id', 'Coupon.coupon_desc'))));
		
		$this->pageTitle = $this->data['Member']['login_username'] . " - Coupon Update";
		
		// PCI COMPLIANCE LOG
		// $member_id, $log_type, $log_reason, $value_type, $old, $new
		$this->logActivity($member['Member']['id'], 'compliance', 'members-edit_coupon', 'success', '', '');
		
	}
	
	function edit_marketing($id) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('The member you are trying to edit does not exist.', true), 'error');
			$this->redirect(array('action'=>'search'));
		}
		
		if (!empty($this->data)) {
			if ($this->Member->save($this->data)) {
				$this->Session->setFlash(__('The members data was saved successfully', true), 'flash_success');
				$this->redirect(array('action' => 'view', $id));
				
				
			} else {
				$this->Session->setFlash(__('The members data could not be updated. Please, try again.', true), 'error');
			}
		}
		
		if (empty($this->data)) {
			$this->recursive = 1;
			$this->data = $this->Member->read(null, $id);
			print_r($this->data['Member']);
		}
		
		$this->set('yesNo', $this->yesNo);		
		$this->pageTitle = $this->data['Member']['login_username'] . " - Marketing Prefrences";
	}
	
	function edit_pay_type($id) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('The member you are trying to edit does not exist.', true), 'error');
			$this->redirect(array('action'=>'search'));
		}
		
		if (!empty($this->data)) {
			if ($this->Member->save($this->data)) {
				$this->Session->setFlash(__('The members data was saved successfully', true), 'flash_success');
				$this->redirect(array('action' => 'view', $id));
				
				
			} else {
				$this->Session->setFlash(__('The members data could not be updated. Please, try again.', true), 'error');
			}
		}
		
		if (empty($this->data)) {
			$this->data = $this->Member->read(null, $id);
		}
		
		$this->pageTitle = "eManager - Update Pay Type";
	}
	
	function edit_status($id) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('The member you are trying to edit does not exist.', true), 'error');
			$this->redirect(array('action'=>'search'));
		}
		
		if (!empty($this->data)) {
			if ($this->Member->save($this->data)) {
				$this->Session->setFlash(__('The members data was saved successfully', true), 'flash_success');
				$this->redirect(array('action' => 'view', $id));
				
				
			} else {
				$this->Session->setFlash(__('The members data could not be updated. Please, try again.', true), 'error');
			}
		}
		
		if (empty($this->data)) {
			$this->data = $this->Member->read(null, $id);
		}
		
		$this->pageTitle = "eManager - Change Address";
	}
	
	function fail_rebill($member_id) {
		if (!$member_id && empty($this->data)) {
			$this->Session->setFlash(__('The account you are trying to force fail does not exist.', true), 'error');
			$this->redirect(array('action'=>'search'));
		}
		
		// capture customer account id
		if (!empty($member_id)) { $this->Session->write('Member.id',$member_id); }
		
		if ($this->Member->forceAdminRebillFailure($member_id)) {
			$this->Session->setFlash(__('This account will fail during the next rebill cycle.', true), 'flash_success');			
		} 
		else {
			$this->Session->setFlash(__('The fail process failed to complete.', true), 'error');			
		}
		
		// redirect to user
		$this->redirect($this->referer());
		
		
	}
	
	function highwinds($id) {
		
		$this->Member->recursive = -1;
		$member = $this->Member->read(null, $id);
		$hw = new highwindsAPI();
		
		// get data from usenet server
		$hw_results = $hw->getUser($member['Member']['login_username']);
		
		//print_r($hw_results);
		$usage['status'] = $hw->translateAccountStatusId($hw_results[3]);
		
		$usage['used'] = round($hw_results[23]/1000000000,1);
		$usage['total'] = round($hw_results[24]/1000000000,1);		
		$usage['remaining'] = $usage['total'] - $usage['used'];
		$usage['close_date'] = $hw_results[20];
		$usage['open_date'] = $hw_results[22];
		$usage['cycle_date'] = $hw->getCycleDate($member['Member']['login_username']);
		
		$usage['cust_group'] = $hw->getCustomerGroupName($hw_results[4]);
		
		
		$this->pageTitle = "Highwinds Maintenance - {$member['Member']['login_username']}";
		$this->set(compact('usage','member'));
		
		$this->Session->write('ActivePage','highwinds');

		// capture customer account id
		$this->Session->write('Member.id',$id);
		
		// PCI COMPLIANCE LOG
		// $member_id, $log_type, $log_reason, $value_type, $old, $new
		$this->logActivity($member['Member']['id'], 'compliance', 'members-hw-data', 'view', '', '');
	}
	
	function hw_activate($id) {
		// check for valid data
		if (!$id) {
			$this->Session->setFlash(__('The member you are trying to edit does not exist.', true), 'error');
			$this->redirect($this->referer());
		}
		
		$member = $this->Member->read(null, $id);
		$hw = new highwindsAPI();
		$hw->activateHWAccount($member['Member']['login_username']);
		
		if ($hw->success) {
			$this->Session->setFlash(__('The account was successfully activated.', true), 'flash_success');
		}
		else {
			$this->Session->setFlash(__('The customers account was not activated. ' . $hw->errorMessage, true), 'error');
		}
		
		$this->redirect($this->referer());
		
	}
	
	function hw_close_account($id) {
		
		if (!$id) {
			$this->Session->setFlash(__('The member you are trying to edit does not exist.', true), 'error');
			$this->redirect($this->referer());
		}
		
		$member = $this->Member->read(null, $id);
		
		$this->loadModel('HwWeatherman');
		if ($this->HwWeatherman->close($member['Member']['login_username'], date("Y-m-d"))) {
			$this->Session->setFlash(__('Successfully closed account in HW only.', true), 'flash_success');
		}
		else {
			
			$msgLog = "Could not close account in HW || ".$this->HwWeatherman->errorMessage();
	   		$this->log($msgLog, 'debug');
		
			$this->Session->setFlash(__('Could not close account.', true), 'error');
		}
		
		$this->redirect($this->referer());

	}
	
	function hw_date_range($id) {
		
		$this->Member->unBindModel(array('hasMany' => array('*'), 'hasOne' => array('*'), 'belongsTo' => array('*')));
		
		// check for valid data
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('The member you are trying to edit does not exist.', true), 'error');
			$this->redirect(array('action'=>'search'));
		}
		
		if (empty($this->data)) {
			$this->data = $this->Member->getMember($id);
		}

		
		$this->Session->write('Member.id',$id);
		$this->Session->write('ActivePage','highwinds');
		$this->pageTitle = "HW Date Range";
		
		// PCI COMPLIANCE LOG
		// $member_id, $log_type, $log_reason, $value_type, $old, $new
		$this->logActivity($id, 'compliance', 'members-hw-date-range', 'view', '', '');
	}
	
	function hw_recycle($id) {
		
		$member = $this->Member->read(null, $id);
		$hw = new highwindsAPI();
		
		if ($hw->recycleUser($member['Member']['id'])){
			$this->Session->setFlash(__('The account was successfully recycled.', true), 'flash_success');
			$this->redirect($this->referer());
		
		}
		else {
			$this->Session->setFlash(__('The customers account was not recycled. ' . $hw->errorMessage, true), 'error');
			$this->redirect($this->referer());

		
		}
		
		// get data from usenet server
	}
	
	function hw_suspend_account($id) {
		
		// make sure id is provided to reset the account
		if (!$id) {
			$this->Session->setFlash(__('Not enough information provided to suspend account.', true), 'error');
			$this->redirect($this->referer());
		}
		
		// retrieve member information
		$this->Member->recursive = 0;
		$member = $this->Member->getMember($id);
		
		// make sure we have a valid member
		if (!$member) {
			$this->Session->setFlash(__('This is not a valid account.', true), 'error');
			$this->redirect($this->referer());
		}
		
		// suspend the account in highwinds
		$this->loadModel('HwWeatherman');
		if (!$this->HwWeatherman->suspendAccount($member['Member']['login_username'])) {
			
			// PCI COMPLIANCE LOG
			$this->logActivity($member['Member']['id'], 'compliance', 'hw-suspend-account', 'fail', '', '');
			$this->Session->setFlash(__('Failed to suspend account in HW.', true), 'error');
			
		}
		else {
		
			// PCI COMPLIANCE LOG
			$this->logActivity($member['Member']['id'], 'compliance', 'hw-suspend-account', 'success', '', '');
			$this->Session->setFlash(__('Account suspended in HW.', true), 'flash_success');
		}
		
		// redirect accordingly
		$this->redirect($this->referer());
	}
	
	function hw_get_usage($id) {
		
		$this->layout = 'empty';
		
		// check for valid data
		if (empty($this->data)) {
			$this->Session->setFlash(__('The member you are trying to edit does not exist.', true), 'error');
			$this->redirect(array('action'=>'search'));
		}

		
	}
	
	function hw_usage_custom($id) {
	
		// check for valid data
		if (empty($this->data)) {
			$this->Session->setFlash(__('The member you are trying to edit does not exist.', true), 'error');
			$this->redirect(array('action'=>'search'));
		}
		
		// if form was submitted, retrieve data from highwinds.
		
		$this->Member->unBindModel(array('hasMany' => array('*'), 'hasOne' => array('*'), 'belongsTo' => array('*')));
		$this->Member->bindModel(array('hasMany' => array('HwData')));
		
		$this->Member->recursive = 0;
		$member = $this->Member->read(null, $this->data['Member']['id']);
		
		$username = $member['Member']['login_username'];
		$usage_date = $this->data['Member']['hw_start_date'];
		$usage_stop = $this->data['Member']['hw_stop_date'];
		$range = array('start' => $usage_date, 'stop' => $usage_stop);
			
		$count = 0;
		$bytes = "";
		$total = 0;
		
		$hw = new highwindsAPI();
		
		while ($usage_date <= $usage_stop) {
		
			$this->Member->HwData->recursive = 0;
			//$exists = false; //$this->HwData->find('first', array('conditions'=>array('member_id' => $member['Member']['id'], 'usage_date' => $usage_date, 'data_type'=>'daily')));
			$exists = $this->Member->HwData->find('first', array('conditions'=>array('member_id' => $member['Member']['id'], 'usage_date' => $usage_date, 'data_type'=>'daily')));
			
			if (empty($exists)) {
				$bytes_used = $hw->getUsageByDay($username,$usage_date, false);
				
				echo "$usage_date: $bytes_used<br/>";
				if ($usage_date != date("Y-m-d") && $usage_date != date("Y-m-d", mktime(0,0,0,date('m'),date('d')-1,date('Y')))) {
					
					$this->Member->HwData->insertDailyRecord($member['Member']['id'], $member['Member']['hw_cust_id'], $usage_date, $bytes_used);
					
				}
			}
			else { $bytes_used = $exists['HwData']['total_bytes']; }
			
			
			if ($bytes_used > 0) {
				
				$bytes[$count] = array('date' => $usage_date, 
								'daily_bytes' => round($bytes_used/(1000*1000*1000),2));
			
				$total += $bytes_used;				
			}
			
			$usage_date = strtotime($usage_date);
			$usage_date = date('Y-m-d', mktime(0,0,0,date('m',$usage_date),date('d',$usage_date)+1,date('Y',$usage_date)));
			
			$count++;
		}
		
		$total = round($total/(1000*1000*1000),2);
		$this->set(compact('bytes','total','range'));
		$this->Session->write('ActivePage','highwinds');
		
		// capture customer account id
		$this->Session->write('Member.id',$this->data['Member']['id']);
		
		$this->pageTitle = "HW Custom Usage - {$member['Member']['login_username']}";
		
		// PCI COMPLIANCE LOG
		// $member_id, $log_type, $log_reason, $value_type, $old, $new
		$this->logActivity($member['Member']['id'], 'compliance', 'members-hw-usage-custom', 'view', '', '');
	}
	
	function hw_test() {
		$this->layout = 'empty';
		$this->loadModel('HwWeatherman');
		$member = $this->HwWeatherman->getUser('ngd734626311');
		
		if ($this->HwWeatherman->success()) { echo "good call<br/>"; }
		else { echo "bad call<br/>"; }
		
		print_r($member);
		
		echo "<br/><br/>";
		
		$response = $this->HwWeatherman->addBlock('ngd734626311', 150);
		if ($this->HwWeatherman->success()) { echo "good call<br/>"; }
		else { echo "bad call<br/>"; }
		
		print_r($response);
	}
	
	function hw_unlimited_heavy($member_id) {
	
		// get member
		$member = $this->Member->read(null, $member_id);
		
		if (empty($member)) { $this->redirect($this->referer()); }
	
		// get customer groups
		$this->loadModel("HwCustomerGroup");
		$hw_group_id = $this->HwCustomerGroup->getHeavy();
		
		// update the group in HW
		$this->loadModel('HwWeatherman');
		
		if (!$this->HwWeatherman->updateCustomerGroupId($member['Member']['login_username'], $hw_group_id)) {
			
			// PCI COMPLIANCE LOG
			$this->logActivity($member['Member']['id'], 'compliance', 'hw-unlimited-heavy', 'fail', '', '');
			
			$this->Session->setFlash(__('Failed to update in HW.', true), 'error');
			$this->redirect($this->referer());
		}
		else {
			
			// PCI COMPLIANCE LOG
			$this->logActivity($member['Member']['id'], 'compliance', 'hw-unlimited-heavy', 'success', '', '');
			
			$this->Session->setFlash(__('Customer is now unlimited heavy in HW.', true), 'flash_success');
			$this->redirect($this->referer());
		}
		
	
	}
	
	function index() {
		//list all affiliates
	}
	
	function mark_abuser($member_id) {
		
		if ($this->Member->updateMember($member_id,array('abuse_warning' => 'Y'))) {
			$this->Session->setFlash(__('Account has been marked as a potential abuser', true), 'flash_success');
		}
		else {
			$this->Session->setFlash(__('Failed to flag this account', true), 'error');
		}
		
		$this->redirect($this->referer());
	}
	
	function next_bill_date($id) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('The member you are trying to edit does not exist.', true), 'error');
			$this->redirect(array('action'=>'search'));
		}
		
		if (!empty($this->data)) {
			if ($this->Member->save($this->data)) {
				
				// PCI COMPLIANCE LOG
				$this->logActivity($id, 'compliance', 'members-next-bill-date', 'success', '', '');
			
				$this->Session->setFlash(__('The members data was saved successfully', true), 'flash_success');
				$this->redirect(array('action' => 'view', $id));
				
				
			} else {
			
				// PCI COMPLIANCE LOG
				$this->logActivity($id, 'compliance', 'members-next-bill-date', 'fail', '', '');
			
				$this->Session->setFlash(__('The members data could not be updated. Please, try again.', true), 'error');
			}
		}
		
		if (empty($this->data)) {
			$this->data = $this->Member->read(null, $id);
		}
		
		$this->pageTitle = "eManager - Change Next Bill Date";
		
		// PCI COMPLIANCE LOG
		$this->logActivity($id, 'compliance', 'members-next-bill-date', 'view', '', '');
	}
	
	function password_reset($id) {
		
		//validate user is provided
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('The member you are trying to edit does not exist.', true), 'error');
			$this->redirect(array('action'=>'search'));
		}
		
		// generate password
		$clear_password =  rand(1000,999999999);
		$secure_password =	md5($clear_password);
		$member = $this->Member->read(null, $id);					
			
		// reset password in hw
		$hw = new highwindsAPI();
		$hw->updatePassword($member['Member']['login_username'], $member['Member']['login_password'], $secure_password);
		
		if (!$hw->success) {
			/*
			$this->Email->from    = 'NewsgroupDirect.com <noreply@newsgroupdirect.com>';
			$this->Email->to      = array('NewsgroupDirect.com <webmaster@newsgroupdirect.com>, gary@media-leaf.com');			
			$this->Email->bcc     = array('support@newsgroupdirect.com');
			$this->Email->replyTo = 'support@newsgroupdirect.com';
			$this->Email->subject = 'Password Reset Failed in eManager';
			
			// notify of new coupon creation
			$email_message = "System failed to reset password in Highwind.\n\n  Reason: \n ".$hw->errorMessage;					
			$this->Email->send($email_message);
			*/
			
			$this->Session->setFlash(__('System failed to reset password in Highwinds.', true), 'error');
			$this->redirect($this->referer());
			//Report_Site_Error("User ".$search_results[0]['login_username']." password update failed. Reason:".$hw->errorMessage);
		}
				
		// if hw reset success, reset locally
		$member['Member']['login_password'] = $secure_password;
		
		// all success, notify user of new password via email.
		if (!$this->Member->save($member)) {
			$this->Session->setFlash(__('System failed to reset password locally.  Customer does not have valid password at this time.', true), 'error');
			$this->redirect($this->referer());
		}
		
		$this->Email->from    = 'NewsgroupDirect.com <support@newsgroupdirect.com>';
		$this->Email->to      = $member['Member']['name'] . "<".$member['Member']['email'].">";
		$this->Email->bcc      = array('NewsgroupDirect.com <webmaster@newsgroupdirect.com>');
		$this->Email->replyTo = 'support@newsgroupdirect.com';
		$this->Email->subject = 'Your NewsgroupDirect Password';
		
		// message text
		$email_message = "Dear ". $member['Member']['name'] .",\n\n" .
						 "Our support staff has had to reset your password as they were working on your account.  You will find the new one below.  You can change it anytime by logging into your account settings and clicking 'edit profile'.\n\n" .
						 "   Username:  ". $member['Member']['login_username'] ."\r" .
						 "   Password:  ". $clear_password ."\r\r" .
						 "Once again we thank you for your business.  \n\n" .
						 "Sincerely, \n\n" .
						 "The NewsgroupDirect Team";
			
		$this->Email->send($email_message);
		
		
		// PCI COMPLIANCE LOG
		// $member_id, $log_type, $log_reason, $value_type, $old, $new
		$this->logActivity($member['Member']['id'], 'compliance', 'members-password-reset', 'success', '', '');
		
		$this->Session->setFlash(__("The password has been reset. The new password is:  $clear_password", true), 'flash_success');
		$this->redirect($this->referer());
		
	}
	
	function process_account_close($id, $expire_date, $reason, $logCancel = true) {
		
		// make sure we have valid fields
		if (empty($id)) {
			$this->Session->setFlash(__('Not enough information provided to cancel account.', true), 'error');
			return;//$this->redirect($this->referer());
		}
		
		if (empty($expire_date)) {
			$this->Session->setFlash(__('An expiration date must be provided to cancel account.', true), 'error');
			return;//$this->redirect($this->referer());
		
		}
		
		if (empty($reason)) {
			$this->Session->setFlash(__('A reason must be provided to cancel account.', true), 'error');
			return;//$this->redirect($this->referer());
		
		}
		
		// close the account
		if ($this->Member->closeAccount($id, $expire_date, $reason, $logCancel) === false) { 
		
			if ($this->Member->isSevere()) {
				parent::send_support_email("SN Account Close Error", $this->Member->emailMessages());
			}

			$this->Session->setFlash(__($this->Member->errorMessages(), true), 'error'); 
			$this->logActivity($id, 'compliance', 'process_account_close-promo', 'failure', '', '');
		}
		else { 
			$this->close_success = true;
			$this->Session->setFlash(__('This account was closed successfully', true), 'flash_success'); 
			$this->logActivity($id, 'compliance', 'process_account_close-promo', 'success', '', '');
		}
		
		// return to referring controller
		return;
		$this->redirect($this->referer());
		
		/*
		// retrieve data from members table, confirm exists
		$member = $this->Member->getMember($id);
		
		if (!$member) {
			$this->Session->setFlash(__('This is not a valid account.', true), 'error');
			$this->redirect($this->referer());
		}
		
		// begin save process, as of now, if an account has been closed, we do not override it.
		$member['Member']['date_expire'] = $expire_date;
		$member['Member']['date_cancel'] = date("Y-m-d");
		
		$hw = new highwindsAPI();
		$hw->setCloseDate($member['Member']['login_username'], $expire_date);

		// make sure hw closed properly.  if not notify support, close account.
		if (!$hw->success) {
			$error_msg = "The following account could not be closed in highwinds. Follow up to ensure account is closed properly.  \n\n". $member['Member']['login_username'] . "  \nExpire: $expire_date \n\nHW Error: {$hw->errorMessage}";
			parent::send_support_email("HW Account Close Error", $error_msg);
			$this->redirect($this->referer());
			
		}
		
		// save the fields in the member table
		if ($this->Member->save($member)) {
		
			// log transaction in cancels table
			$this->record_account_cancel($id, date('Y-m-d'), $expire_date, $reason, $member['Member']['plan_id']);
			
			// close the account in StorageNinja if need be.
			
			if (!empty($member['Member']['storageninja_id'])) {
				
				$sn = new storageNinjaAPI();
				$snResult = $sn->cancelUser($member['Member']['storageninja_id'], $expire_date) ;
				
				if (!$sn->success) {
					$error_msg = "The API call to cancel ".$member['Member']['email']." StorageNinja account failed (".$member['Member']['storageninja_id']."):  ".$sn->errorMessage;
					parent::send_support_email("SN Account Close Error", $error_msg);
				}
			} 
			
			
			
			$this->Session->setFlash(__('This account, ' . $member['Member']['email'] . ', was closed successfully', true), 'flash_success');
			
			$this->close_success = true;
			
			//$this->redirect($this->referer());
			
		} else {
			$this->Session->setFlash(__('The account, ' . $member['Member']['email'] . ', was not closed in our systems.', true), 'error');
			$this->redirect($this->referer());
		}
		
		*/
	}

	function promo($id) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('The member you are trying to edit does not exist.', true), 'error');
			$this->redirect(array('action'=>'search'));
		}
		
		if (!empty($this->data)) {
			$this->data["Member"]['is_free_trial'] = strtoupper($this->data["Member"]['is_free_trial']);
			
			if ($this->Member->save($this->data)) {
				
				// PCI COMPLIANCE LOG
				$this->logActivity($member['Member']['id'], 'compliance', 'members-promo', 'success', '', '');
			
				$this->Session->setFlash(__('The members data was saved successfully', true), 'flash_success');
				$this->redirect(array('action' => 'view', $id));
				
				
			} else {
			
				// PCI COMPLIANCE LOG
				$this->logActivity($member['Member']['id'], 'compliance', 'members-promo', 'fail', '', '');
			
				$this->Session->setFlash(__('The members data could not be updated. Please, try again.', true), 'error');
			}
		}
		
		if (empty($this->data)) {
			$this->data = $this->Member->read(null, $id);
		}
		
		$this->pageTitle = "eManager - Change Next Bill Date";
		
		// PCI COMPLIANCE LOG
		$this->logActivity($member['Member']['id'], 'compliance', 'members-promo', 'view', '', '');
	}
	
	function reactivate_account($id) {
		
		// make sure id is provided to reset the account
		if (!$id) {
			$this->Session->setFlash(__('Not enough information provided to cancel account.', true), 'error');
			$this->redirect($this->referer());
		}
		
		// retrieve member information
		$this->Member->recursive = 0;
		$member = $this->Member->find('first', array('conditions' => array('Member.id' => $id)));
		
		// make sure we have a valid member
		if (!$member) {
			$this->Session->setFlash(__('This is not a valid account.', true), 'error');
			$this->redirect($this->referer());
		}
		elseif ($member['Member']['status'] == 'active') {
			$this->Session->setFlash(__('This account is already active.', true), 'error');
			$this->redirect($this->referer());
		}
		
		// Get information about customers plan
		$this->Member->Plan->recursive = 0;
		$plan = $this->Member->Plan->find('first',array('conditions' => array('id'=>$member['Member']['plan_id'])));
						
		// set fields for database update
		$member['Member']['date_expire'] = '0000-00-00 00:00:00';
		$member['Member']['date_cancel'] = '0000-00-00';
		$member['Member']['status'] = 'active';
		
		if ($plan['Plan']['type'] == 'subscription') {
			$member['Member']['next_bill_date'] = date('Y-m-d', mktime(0,0,0,date('m')+$plan['Plan']['duration'],date('d'),date('Y')));
			$member['Member']['is_bill_fail'] = 'Y';
			$member['Member']['recent_failures'] = 5;
			$member['Member']['last_bill_attempt'] = date('Y-m-d');		
		}
		
		// make highwinds calls to activate account
		$hw = new highwindsAPI();
		$hw->updateUser($member['Member']['login_username'], $member['Member']['email'], $member['Member']['city'], 1, $plan['Plan']['server_group_id'], $plan['Plan']['virtual_server_id'], "", "");
		
		if (!$hw->success) {
			$this->Session->setFlash(__('This account, ' . $member['Member']['email'] . ', was failed to reactivate in HW', true), 'error');
			$this->redirect($this->referer());
		}
		
		// save new data
		if ($this->Member->save($member)) {
			$this->Session->setFlash(__('This account, ' . $member['Member']['email'] . ', was reactivated successfully', true), 'flash_success');
			$this->redirect($this->referer());
		} 
		else {
			$this->Session->setFlash(__('The account, ' . $member['Member']['email'] . ', was not reactivated in our systems.', true), 'error');
			$this->redirect($this->referer());
		}
		
		
		
	}
	
	function rebill($id) {
		
		if (empty($id)) {
			$this->Session->setFlash(__('The member you are trying to edit does not exist.', true), 'error');
			$this->redirect($this->referer());
		}
		
		// capture customer account id
		if (!empty($id)) { $this->Session->write('Member.id',$id); }
		
		if ($this->Member->resetRebill($id)) {
			$this->Session->setFlash(__('This account will rebill shortly.', true), 'flash_success');			
		} 
		else {
			$this->Session->setFlash(__('Unable to set up rebill.', true), 'error');			
		}
		
		// redirect to user
		$this->redirect($this->referer());
	
	}
	
	/**
	 * Records the cancels transaction in the cancels table.
	 */	
	private function record_account_cancel($id, $cancel_date, $expire_date, $reason, $plan_id) {
		
		$this->loadModel("Cancel");
		$myCancel = $this->Cancel->create();
		
		$myCancel['Cancel']['member_id'] = $id;
		$myCancel['Cancel']['cancel_date'] = $cancel_date;
		$myCancel['Cancel']['effective_date'] = $expire_date;
		$myCancel['Cancel']['reason'] = $reason;		
		$myCancel['Cancel']['plan_id'] = $plan_id;

		if ($this->Cancel->save($myCancel))
			return true;
		else
			return false;
	}
	
	function search() {
		
		$this->Member->unBindModel(array('hasMany' => array('*'), 'hasOne' => array('*'), 'belongsTo' => array('*')));
		
		//logic
	}
	
	function searchresults() {
	
		// get search term
		if (isset($this->data)) {
			$query=trim($this->data['Member']['searchterm']);
			$this->Session->write('memberSearch',$query);
		}
		else { $query = $this->Session->read('memberSearch'); }
		
		if (empty($query)) {
			$this->Session->setFlash(__('Please provide a search value.', true), 'error');					
			$this->redirect($this->referer());
		}
		
		$this->Member->Behaviors->attach('Containable');
		$this->paginate['contain'] = array();
		$conditions = array('or' => array(
					array("Member.name LIKE"=>"%$query%"),
					array("Member.email LIKE"=>"%$query%"),
					array("Member.address LIKE"=>"%$query%"),
					array("Member.transaction_number LIKE"=>"%$query%"),
					array("Member.id LIKE"=>"%$query%"),
					array("Member.login_username LIKE"=>"%$query%")));
		
		
		$results = $this->paginate(array($conditions));
		
		
		if (count($results) == 1) {
			$this->redirect(array('action' => 'view', $results[0]['Member']['id']));
		}
		else {
			$this->set('members',$results);	
		
			if (!empty($results)) {
				$this->set('message',"<p id='congrats' class='alert'><span class='txt'><span class='icon'></span><strong>Success:</strong> found ".count($this->data['Member']['searchterm'])." result(s) matching '{$this->data['Member']['searchterm']}'</span><a title='Close' class='close'><span class='bg'></span>Close</a></p>");
			}
			else {
				$this->set('message',"<p class='alert'><span class='txt'><span class='icon'></span><strong>Error:</strong> no results found for '{$this->data['Member']['searchterm']}</span><a title='Close' class='close'><span class='bg'></span>Close</a></p>");
			}
		}	
		
		$this->pageTitle = "Customer Search Results";		
	}
	
	function storageninja_create($member_id) {
	
	}
	
	function storageninja_close($member_id) {
		
		if (!$member_id && empty($this->data)) {
			$this->Session->setFlash(__('The member you are trying to close a storageninja account for does not exist.', true), 'error');
			$this->redirect(array('action'=>'search'));
		}
		
		if (!empty($this->data)) {
			
			$sn = new storageNinjaAPI();
			$sn->cancelUser($this->data['Member']['storageninja_id'], $this->data['Member']['my_expire_date']);
			
			if (!$sn->success) {
				$this->Session->setFlash(__("Could not create account at this time.<br/>{$sn->errorMessage}", true), 'error');
			}
			else {
			
				$this->Session->setFlash(__('StorageNinja account closed', true), 'flash_success');
				$this->redirect(array('action'=>'view',$this->data['Member']['id']));
			}
			
		}
		
		if (empty($this->data)) {
			$this->Member->recursive = 0;
			$this->data = $this->Member->read(null, $id);
		}
		
		$this->Session->write('Member.id',$id);			
		$this->pageTitle = "eManager - Member Details";
		
		
		// PCI COMPLIANCE LOG
		// $member_id, $log_type, $log_reason, $value_type, $old, $new
		$this->logActivity($this->data['Member']['id'], 'compliance', 'members-storageninja-close', 'success', '', '');
		
	}
	
	/**
	 * Send a new user email for accounts we have had to create manually
	 *
	 * @param unknown_type $name
	 * @param unknown_type $email
	 * @param unknown_type $username
	 * @param unknown_type $password
	 */
	function send_new_user_email ($name,$email,$username,$password) {
		$this->Email->from    = 'NewsgroupDirect.com <support@newsgroupdirect.com>';
		$this->Email->to      = "$name <$email>";
		$this->Email->bcc      = array('NewsgroupDirect.com <webmaster@newsgroupdirect.com>');
		$this->Email->replyTo = 'support@newsgroupdirect.com';
		$this->Email->subject = 'Your NewsgroupDirect Membership Information';
		
		// message text
		$email_message = "Dear $name,\n\n" .
						 "We want to thank you for choosing NewsgroupDirect as your usenet provider. We will be here to help you every step of the way. You can always go to our website and click on Support if you need assistance.  \n" .
						 "First, you should login to My Account on our website and download the free version of Newsrover.  \n\n" .
						 "Your account information follows. The login will work for both our website and the news server. Feel free to change your password at any time. \n\n" .
						 "   Username:  $username \n\n" .
						 "   Password: $password \n\n" .
						 "Our news server information is as follows: \n" .
						 "   US Server: news.newsgroupdirect.com Ports 119 & 443 \n" .
						 "   US Secure Server: news-ssl.newsgroupdirect.com Ports 563 & 80 \n" .
						 "   Europe Server: news-eu.newsgroupdirect.com Ports 119 & 443 \n" .
						 "   Europe Secure: news-eu-ssl.newsgroupdirect.com Ports 563 & 80 \n\n" . 
						 "Once again we thank you for your business.  \n\n" .
						 "Sincerely, \n\n" .
						 "The NewsgroupDirect Team";
			
		$this->Email->send($email_message);
	}
		
	function view($id) {
		if (!$id) {
			$this->Session->setFlash(__('This is not a valid member.', true));
			$this->redirect(array('action'=>'search'));
		}
		
		$this->loadModel("Newsgroupdirect");
		$member = $this->Newsgroupdirect->getMember($id);
		//print_r($member);
		
		// get chi
		$chi = $this->Member->getChi($member['Member']['id']);
		$this->set('chi', $chi);
		
		// capture customer account id
		$this->Session->write('Member.id',$id);
		
		// set variable for use by view
		$this->set(compact('member'));
		
		$this->pageTitle = "eManager - NGD Customer " . $member['Member']['login_username'];
		
		
		// PCI COMPLIANCE LOG
		$this->Member->MLog->logActivity($member['Member']['id'], 'compliance', 'members-view', 'view', '', '', $_SERVER['REMOTE_ADDR'], $this->system_user['id']);
		
	}
	
	function usage($period = 'days', $length = '12', $id) {
		
		$this->Member->unbindModelAll();
		$this->Member->bindModel(array('hasMany' => array('HwData')));
		
		$member = $this->Member->read(null, $id);
		$username = $member['Member']['login_username'];
		
		$hw = new highwindsAPI();
		$usage['available'] = $hw->getRemainingUsage($username);
		$usage['current_date'] = date('Y-m-d');
		$usage['history_date'] = date('Y-m-d', mktime(0,0,0,date('m'),date('d')-10,date('Y')));
		
		$i = 0;
		
		while ($i < $length) {
		
			if ($period == 'days') {
				
				// get daily usage
				$usage_date = date('Y-m-d', mktime(0,0,0,date('m'),date('d')-$i,date('Y')));
				
				// check to see if it is in database, then pull from highwinds
				$this->Member->HwData->recursive = 0;
				$exists = $this->Member->HwData->find('first', array('conditions'=>array('member_id' => $id, 'usage_date' => $usage_date, 'data_type'=>'daily')));
				
				if (empty($exists)) {
					$bytes_used = $hw->getUsageByDay($username,$usage_date, false);
					
					
					if ($usage_date != date("Y-m-d") && $usage_date != date("Y-m-d", mktime(0,0,0,date('m'),date('d')-1,date('Y')))) {
						$hwd = $this->Member->HwData->create();
						$hwd['HwData']['member_id'] = $id;
						$hwd['HwData']['data_type'] = 'daily';
						$hwd['HwData']['hw_cust_id'] = $member['Member']['hw_cust_id'];
						$hwd['HwData']['total_bytes'] = $bytes_used;
						$hwd['HwData']['usage_date'] = $usage_date;
						
						$this->Member->HwData->save($hwd);
					}
				}
				else {
					$bytes_used = $exists['HwData']['total_bytes'];
				}
			}
			elseif ($period = 'months') {
				
				// check to see if it is in database, then pull from highwinds
				$usage_date =  date('F Y', mktime(0,0,0,date('m')-$i,date('d'),date('Y')));
				$usage_month = date('Y-m-d', mktime(0,0,0,date('m')-$i,1,date('Y')));
				
				$this->Member->HwData->recursive = 0;
				$exists = $this->Member->HwData->find('first', array('conditions'=>array('member_id' => $id, 'usage_date' => $usage_month, 'data_type'=>'monthly')));
				
				if (empty($exists)) {
					$bytes_used = $hw->getUsageByMonth($username, $usage_month, false);
				
					if ($usage_month != date("Y-m-d", mktime(0,0,0,date('m'),1,date('Y')))) {
						$hwd = $this->Member->HwData->create();
						$hwd['HwData']['member_id'] = $id;
						$hwd['HwData']['data_type'] = 'monthly';
						$hwd['HwData']['hw_cust_id'] = $member['Member']['hw_cust_id'];
						$hwd['HwData']['total_bytes'] = $bytes_used;
						$hwd['HwData']['usage_date'] = $usage_month;
						
						$this->Member->HwData->save($hwd);
					}
				}
				else {
					$bytes_used = $exists['HwData']['total_bytes'];
				}
			}
			
			// prep data for output
			$bytes[$i] = array('date' => $usage_date, 
							'usage' => round($bytes_used/(1000*1000*1000),2));
		
						
			
			$i++;
		}
		
		//print_r($this->data);
		$this->set('period', $period);
		$this->set('length', $length);
		$this->set(compact('member'));
		$this->set(compact('bytes'));
		
		// capture customer account id
		$this->Session->write('ActivePage','highwinds');
		$this->Session->write('Member.id',$id);
		
		
		
		// PCI COMPLIANCE LOG
		$this->Member->MLog->logActivity($member['Member']['id'], 'compliance', 'members-usage', 'view', '', '', $_SERVER['REMOTE_ADDR'], $this->system_user['id']);
		
	}

	/**
	 * Reporting feature that shows all free accounts.
	 *
	 */
	function report_free() {
		if (!$this->is_admin_user()) {
			$this->Session->setFlash(__('You do not have the correct privileges to view this information.', true), 'error');			
			$this->redirect(array('action'=>'search'));
		}
		
		$this->pageTitle = "eManager - Free Accounts Report";
		
		$number_days = 10;
		
		$this->paginate = array('limit' => 20,
								'order' => array('date_reg' => 'DESC'));
		
		$this->set('members', $this->paginate(array('Member.plan_id' => 50, 
													"Member.status LIKE" => "active", 
													"Member.date_expire =" => '0000-00-00 00:00:00',
													'to_days(now())-to_days(Member.date_reg) <=' => $number_days)));
													
		parent::session_clean_member_id();
		
		
		// PCI COMPLIANCE LOG
		$this->Member->MLog->logActivity($member['Member']['id'], 'compliance', 'report-free', 'view', '', '', $_SERVER['REMOTE_ADDR'], $this->system_user['id']);
		
	}
	
	/**
	 * Reporting feature that shows all free accounts.
	 *
	 */
	function report_free_by_ip() {
		if (!$this->is_admin_user()) {
			$this->Session->setFlash(__('You do not have the correct privileges to view this information.', true), 'error');			
			$this->redirect(array('action'=>'search'));
		}
		
		$this->pageTitle = "eManager - Free Accounts Report by IP";
		
		$number_days = 10;

		$this->Member->recursive = -1;
		$this->paginate = array('fields' => array('ip','COUNT(*) as active_free_accounts'),
								'limit' => 20,
								'group' => array('ip'),
								'order' => array('COUNT(*)' => 'DESC'));
		
		$members = $this->paginate(array('Member.plan_id' => 50, 
				 						 "Member.status" => "active", 
				 						 "Member.date_expire" => '0000-00-00 00:00:00'));
		
		$this->set('members',$members);
		parent::session_clean_member_id();
		
		
		// PCI COMPLIANCE LOG
		$this->Member->MLog->logActivity($member['Member']['id'], 'compliance', 'report-free-by-ip', 'view', '', '', $_SERVER['REMOTE_ADDR'], $this->system_user['id']);
	}
	
	/**
	 * reporting feature that shows all free accounts for an IP
	 *
	 * @param unknown_type $ip
	 */
	function report_ip($ip) {
		if (!$this->is_admin_user()) {
			$this->Session->setFlash(__('You do not have the correct privileges to view this information.', true), 'error');			
			$this->redirect(array('action'=>'search'));
		}
		
		$this->paginate = array('limit' => 20,
								'order' => array('date_reg' => 'DESC'));
		
		$this->set('members', $this->paginate(array('Member.plan_id' => 50, 'Member.ip' => $ip)));
		
		$this->Session->del('Member.ip');			
		$this->Session->Write('Member.ip', $ip);
		$this->pageTitle = "Free Account Report for $ip";
		
		parent::session_clean_member_id();
		
		
		// PCI COMPLIANCE LOG
		$this->Member->MLog->logActivity($member['Member']['id'], 'compliance', 'report-ip', 'view', '', '', $_SERVER['REMOTE_ADDR'], $this->system_user['id']);
	}
	
	/**
	 * Reporting feature that shows all free accounts.
	 *
	 */
	function report_recent_cancels() {
		
		//ini_set('memory_limit','256M'); 
		
		if (!$this->is_admin_user()) {
			$this->Session->setFlash(__('You do not have the correct privileges to view this information.', true), 'error');			
			$this->redirect(array('action'=>'search'));
		}
		
		$this->pageTitle = "eManager - Recent Cancellations";
		
		$number_days = 3;


		//$this->Member->unbindModelAll();

		$this->Member->recursive = 0;
		$this->paginate = array('limit' => 20,
								'order' => array('date_cancel' => 'DESC'));
		
		$this->set('members', $this->paginate(array('Member.plan_id <>' => 50, 
													'to_days(now())-to_days(Member.date_cancel) <=' => $number_days)));
		
		$this->set('dayslength',$number_days);
		
		parent::session_clean_member_id();
		
		// PCI COMPLIANCE LOG
		$this->Member->MLog->logActivity($member['Member']['id'], 'compliance', 'report-recent-cancels', 'view', '', '', $_SERVER['REMOTE_ADDR'], $this->system_user['id']);
	}
	
	// Reporting feature that shows all new accounts.
	function report_recent_sales() {

		if (!$this->is_admin_user()) {
			$this->Session->setFlash(__('You do not have the correct privileges to view this information.', true), 'error');			
			$this->redirect(array('action' => 'search'));
		}
		
		$this->pageTitle = "eManager - List of Recent Sales";
		
		$number_days = 10;
		/*
		$this->paginate = array(
				'limit' => 25,
				'order' => array('date_reg' => 'DESC')
				);
		
		$this->set('members', $this->paginate(array(
										'Member.status' => 'active'
										)));

		*/
		$this->paginate = array(
        		'conditions' => array('Member.status' => 'active'),
        		'fields' => array('CCTransaction.*','Member.*'),
        		'limit' => 10,
        		'order' => array('date_reg' => 'DESC'),
        		'joins' => array(
        					'type' => 'INNER JOIN',
        					'table' => 'cc_transactions',
        					'alias' => 'CCTransaction',
        					'conditions' => 'ON CCTransaction.member_id=Member.id'

        		)
    		);
    		$this->set('members', $this->paginate('Member'));
		
		$this->set('dayslength', $number_days);
		
		parent::session_clean_member_id();
	}

	function void($trans_id=null) {

		if (!$trans_id) {
			$this->Session->setFlash(__('Invalid CcTransaction.', true));
			$this->redirect(array('controller'=>'cc_transactions','action'=>'index'));
		}

		$cim = new firstDataAPI(true);

		$this->loadModel("CcTransaction");

		$this->loadModel("PaymentDetail");

		$old_order = $this->CcTransaction->find('all',array('conditions'=>array('order_nbr'=>$trans_id)));

		$old_cc_trans                  = $old_order[0]['CcTransaction'];

		$pd = $this->PaymentDetail->find('first', array('conditions' => array('PaymentDetail.member_id' => $old_cc_trans['member_id'])));

		// get everything set up for being put in the table
		unset($old_cc_trans['id']);
		$old_cc_trans['trans_purpose'] = 'refund';
		$old_cc_trans['amount']        = abs($old_cc_trans['amount'])*-1;
		$old_cc_trans['trans_type']    = 'refund';
		$old_cc_trans['trans_date']    = date('Y-m-d H:i:s');

		$member_details = $old_order[0]['Member'];
			// process the refund
			$cim->processRefund($pd['PaymentDetail']['cc_card_nbr'],parent::right($pd['PaymentDetail']['cc_card_exp'],2),parent::right(parent::left($pd['PaymentDetail']['cc_card_exp'],4),2),$old_cc_trans['amount'],$old_cc_trans['order_nbr']);

			if( $cim->success ) {
				$old_cc_trans['approval_code'] = $cim->approvalCode;
				$old_cc_trans['ref_code'] 	   = $cim->refNumber;
				$old_cc_trans['approved'] 	   = $cim->approved;
				$old_cc_trans['error'] 		   = $cim->friendlyMessage;
				$old_cc_trans['full_error']    = $cim->fullError;
				$old_cc_trans['message']       = $cim->returnMessage;
				$old_cc_trans['avs']           = $cim->avs;

				$this->set('response','The payment was successfully voided');

				$this->Member->save();

				$this->CcTransaction->create();
				$this->CCTransaction->save($cc_details);

			} else {
				$this->set('response','The action failed for this reason: <pre>' . var_dump($cim));
			}
	}
	
	function report_invalid_next_bill() {
		
		if (!$this->is_admin_user()) {
			$this->Session->setFlash(__('You do not have the correct privileges to view this information.', true), 'error');			
			$this->redirect(array('action'=>'search'));
		}
	
	
		$this->pageTitle = "eManager - Invalid Next Bill Dates";

		$this->Member->recursive=0	;
		$this->paginate = array('limit' => 15,
								'order' => array('next_bill_date' => 'ASC'));
				
		$members = $this->paginate(array('Member.next_bill_date <' => date('Y-m-d', mktime(0,0,0,date('m'),date('d')+1,date('Y'))),
										"Member.status" => "active", 										
										"Member.free_admin_acct" => 'N',
										"Member.pay_type <>" => 'WP',
										"Member.plan_id <>" => '50',
										"Plan.type <>" => "block",
										"curdate() <>" => 'Member.date_cancel',
										"Member.date_expire" => '0000-00-00 00:00:00'));
		
		//pr($members);
		
		$this->set(compact('members'));
		
		parent::session_clean_member_id();
		
		// PCI COMPLIANCE LOG
		// $member_id, $log_type, $log_reason, $value_type, $old, $new
		$this->logActivity($member['Member']['id'], 'compliance', 'report-invalid-next-bill', 'success', '', '');
	}
	
	function report_free_trial_conversion_rate() {
	
		if (!$this->is_admin_user()) {
			$this->Session->setFlash(__('You do not have the correct privileges to view this information.', true), 'error');			
			$this->redirect(array('action'=>'search'));
		}
	
	
		$this->pageTitle = "NGD Free Trial Conversion Rate";
		
		$closes14 = $this->Member->find('count', array('conditions' => array("to_days(date_expire)-to_days(date_reg) <=" => 14,
																		   "left(date_reg,10) >=" => '2010-01-01',
																		   "left(date_reg,10) <" => '2010-07-08',
																		   "plan_id" => array(51,52,53,54,55,70,72))));

		$closes7 = $this->Member->find('count', array('conditions' => array("to_days(date_expire)-to_days(date_reg) <=" => 7,
																		   "left(date_reg,10) >=" => '2010-07-08',
																		   "plan_id" => array(51,52,53,54,55,70,72))));
		
		$opens = $this->Member->find('count', array('conditions' => array("left(date_reg,10) >=" => '2010-01-01',
																		   "plan_id" => array(51,52,53,54,55,70,72))));
		
		$closes = $closes14 + $closes7;							
		$this->set(compact('opens','closes'));
		
		parent::session_clean_member_id();
		
		// PCI COMPLIANCE LOG
		// $member_id, $log_type, $log_reason, $value_type, $old, $new
		$this->logActivity($member['Member']['id'], 'compliance', 'report-free-trial-conv-rate', 'success', '', '');
	
	}
	
	function report_bad_address() {
	
		$this->pageTitle = "eManager - Bad Address Manager";

		$this->Member->recursive=0	;
		$this->paginate = array('limit' => 30, 
								'order' => 'country desc',
								'fields' => array('id', 'name','address','city','state','postal','country','left(date_reg,10) as date_reg'));
				
		$members = $this->paginate(array('pay_type' => 'CC',
										 'free_admin_acct' => 'N',
										 'plan_id <>' => 50,
										 'Member.status' => array('active','block_used'),
										 'auth_cust_id <>' => '',
										 'OR' => array(
									                  "Member.postal" => '',
							 				   			"Member.address" => '',
							 				   			 "Member.country" => '',
							 				   			 "Member.state" => '')));
		
		$this->set(compact('members'));
		
		parent::session_clean_member_id();
		
		// PCI COMPLIANCE LOG
		// $member_id, $log_type, $log_reason, $value_type, $old, $new
		$this->logActivity($member['Member']['id'], 'compliance', 'report-bad-address', 'success', '', '');
	}
	
	/**
	
	 */
	function report_bad_hw_cust_id() {
	
		$this->pageTitle = "Missing HW Id's - NGD";

		$this->Member->recursive=0	;
		$this->paginate = array('limit' => 30, 
								'order' => 'date_reg desc');
				
		$members = $this->paginate(array('hw_cust_id' => '',
										 'Member.status' => array('block-used', 'active') ));
		
		$this->set(compact('members'));
		
		parent::session_clean_member_id();
		
		// PCI COMPLIANCE LOG
		// $member_id, $log_type, $log_reason, $value_type, $old, $new
		$this->logActivity($member['Member']['id'], 'compliance', 'report-bad-hw-cust-id', 'success', '', '');
	
	}
	
	function report_forecasted_rebills($days = 30) {
		
		$curdate = date("Y-m-d");
		$xy = strtotime(date("Y-m-d"));
		$future = date('Y-m-d', mktime(0,0,0,date('m',$xy),date('d',$xy)+$days,date('Y',$xy)));
		
		$rebills = $this->Member->query("CALL sp_ngd_001_rebills_forecast('$curdate', '$future')");
		
		$this->set('rebills', $rebills);
		$this->set('days', $days);
	}
	
	function active_member_lookup($user, $email) {
		
		$getMember = $this->Member->find('first', array(
	        	'conditions' => array(
						'Member.login_username' => $user,
						'Member.email' => urldecode($email))
	        	));

		if(!empty($getMember)){
			echo $getMember['Member']['status'];
		} else {
			echo "false";
		}	

		$this->autoRender = false;  
		
	}
	
	// Mailchimp Email List Management Functions
	function job_mailchimp_subscribe(){
		
		$mc = new MailChimp;
		
		// Find members that have requested marketing and haven't already been added
		$members = $this->Member->find('all', array('conditions' => 
														array('Member.receive_email' => 'X',
															'Member.mailchimp_optin_sent' => 'N',
															'Member.plan_id !=' => '50',
															'Member.status' => 'active'),
													'limit' => 10
															));
							
		var_dump($members);								
		foreach ($members as $member) {
			
			// echo member data
			echo $member['Member']['email'] . " ";
			
			// Add member to mailchimp
			
			
			// Update member record
			
			
			
		}
		
		$this->autoRender = false;  													
	}
	
	
	function job_mailchimp_tempupdate2(){
		
		$MailChimp = new MailChimp('8445bd028b95f73dbc192cb1b4c9ec35-us1');
		echo Configure::version();
		echo "Timestamp: " . date("h:i:s") . "<br/>";
		
		
		// Find members that have requested marketing and haven't already been added

		
		$members = $this->Member->find('all', array('conditions' => 
														array('Member.receive_email' => 'Y',
															'Member.mailchimp_optin_sent' => 'N',
															'Member.plan_id !=' => '50',
															'Member.status' => 'active'),
													'limit' => 10
															));
					
		echo "still here";			
		//$log = $this->Model->getDataSource()->getLog(false, false);
		//debug($log);
		
		//var_dump($members);		
		if(!empty($members)){						
				foreach ($members as $member) {
					
					// echo member data
					echo $member['Member']['email'] . " ";
					
					// See if member exists in mailchimp
					/*
						$result = $MailChimp->call('lists/member-info', array(
		                	'id'	=> '231009',
							'email'	=> array($member['Member']['email'])
							));
					*/
		            echo "<br/>";
					//var_dump($result);
					
					// Update member record
					
					
					echo "<br/><br/><br/>";
				}
		} else {
			echo "no members found";
		}
		
		$this->autoRender = false;  													
	}
	
	
	
	
}
?>