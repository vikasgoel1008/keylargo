<?php
class RisksController extends AppController {

	var $name = 'Risks';
	var $helpers = array('Html', 'Form');
	
	var $paginate  = array('limit' => 20, 'order' => array('risk_score' => 'DESC'));
		
	function beforeFilter() {
	    parent::beforeFilter(); 
	    
	}
	
	function index($date = null) {
		
		if (empty($date)) { 
		
			if (date('Y-m-d H:i:s') > date("Y-m-d H:i:s",mktime(4,0,0,date("m"),date("d"),date("Y")))) {
				$date = date("Y-m-d",mktime(0,0,0,date("m"),date("d")-1,date("Y"))); 
			}
			else {
				$date = date("Y-m-d",mktime(0,0,0,date("m"),date("d")-2,date("Y"))); 
			}
		}
	
		$this->Risk->recursive = 0;
		$risks = $this->paginate(array('reporting_date' => $date));
		
		$this->pageTitle = "Risk Manager";
		$this->set('risks', $risks);
		$this->set('process_date', $date);
		
		parent::session_clean_member_id();
		
	}
	
	/*
	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid MLog.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('memberLog', $this->MLog->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->MLog->create();
			if ($this->MLog->save($this->data)) {
				$this->Session->setFlash(__('The MLog has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The MLog could not be saved. Please, try again.', true));
			}
		}
		$members = $this->MLog->Member->find('list');
		$this->set(compact('members'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid MLog', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->MLog->save($this->data)) {
				$this->Session->setFlash(__('The MLog has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The MLog could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->MLog->read(null, $id);
		}
		$members = $this->MLog->Member->find('list');
		$this->set(compact('members'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for MLog', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->MLog->del($id)) {
			$this->Session->setFlash(__('MLog deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}
	*/
}
?>