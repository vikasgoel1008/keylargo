<?php
class NgdCreditsController extends AppController {

	var $name = 'NgdCredits';
	var $helpers = array('Html', 'Form');

	function beforeFilter() {
	    parent::beforeFilter(); 
	    
	    $this->Session->write('ActivePage','credits');
	}

	function add($member_id = null) {
		if (!empty($this->data)) {
			$this->NgdCredit->create();
			
			$this->data['NgdCredit']['date_added'] = date("Y-m-d H:i:s");
			
			if ($this->NgdCredit->save($this->data)) {
				$this->Session->setFlash(__('The credit was saved', true), 'flash_success');
				$this->redirect(array('action'=>'index', $this->data['NgdCredit']['member_id']));
			} else {
				$this->Session->setFlash(__('The credit could not be saved. Please, try again.', true), 'error');
			}
		}
		
		if (!empty($member_id)) {
			$this->LoadModel('Member');
			$this->set('member', $this->Member->getMember($member_id));
		}
		
		$this->Session->write('ActivePage','addOption');
	}
	
	function forfeit_credit($credit_id) {
		if (!$credit_id) {
			$this->Session->setFlash(__('Invalid Credit.', true));
			$this->redirect($this->referer());
		}
		
		// forfeit the credit
		if ($this->NgdCredit->forfeitCredit($credit_id)) { $this->Session->setFlash(__('This credit was forfeited.', true), 'flash_success'); }
		else { $this->Session->setFlash(__('Failed to forfeit credit.', true), 'error'); }

		$this->redirect($this->referer());

	}
	
	function index($member_id) {
		$this->NgdCredit->recursive = 0;
		
		$this->paginate = array('limit' => 20,
								'order' => array('NgdCredit.date_added' => 'DESC'));
		
		if (empty($member_id)) {
			$this->Session->setFlash(__('You need to select a customer before you can view the logs.', true), 'error');
			$this->redirect($this->referrer());
		}
		
		$this->LoadModel('Member');
		$member = $this->Member->getMember($member_id);
		
		$this->pageTitle = "{$member['Member']['login_username']} Credits";
		$this->Session->write('Member.id',$member_id);
		
		//echo $this->Session->read("Member.id");
		$credits = $this->paginate(array('member_id' => $member_id));
		
		$this->Session->write('ActivePage','credits');
		$this->set('credits', $credits);
		$this->set('member', $member);
	}
	
	
	function view($id) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid NgdCredit.', true));
			$this->redirect($this->referer());
		}
		
		// get the log record
		$NgdCredit = $this->NgdCredit->read(null, $id);
		$this->Session->write("Member.id", $NgdCredit['NgdCredit']['member_id']);
		
		// get information on plans if this is a plan change
		if ($NgdCredit['NgdCredit']['log_type'] == 'plan change') {
		
			$this->LoadModel('Plan');
			$this->Plan->recursive = 0;
			$this->set("oldplan", $this->Plan->find('first', array('conditions' => array('Plan.id' => $NgdCredit['NgdCredit']['old_value']))));
			$this->set("newplan", $this->Plan->find('first', array('conditions' => array('Plan.id' => $NgdCredit['NgdCredit']['new_value']))));
		}
		
		// get Member Information
		$this->LoadModel('Member');
		$member = $this->Member->getMember($NgdCredit['NgdCredit']['member_id']);
		
		$this->set('member',$member);
		$this->set('credit', $NgdCredit);
		$this->pageTitle = "View Log Entry";
		
	}

	/*
	function add() {
		if (!empty($this->data)) {
			$this->NgdCredit->create();
			if ($this->NgdCredit->save($this->data)) {
				$this->Session->setFlash(__('The NgdCredit has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The NgdCredit could not be saved. Please, try again.', true));
			}
		}
		$members = $this->NgdCredit->Member->find('list');
		$this->set(compact('members'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid NgdCredit', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->NgdCredit->save($this->data)) {
				$this->Session->setFlash(__('The NgdCredit has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The NgdCredit could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->NgdCredit->read(null, $id);
		}
		$members = $this->NgdCredit->Member->find('list');
		$this->set(compact('members'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for NgdCredit', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->NgdCredit->del($id)) {
			$this->Session->setFlash(__('NgdCredit deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}
	*/
}
?>