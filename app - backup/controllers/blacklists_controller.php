<?php
class BlacklistsController extends AppController {

	var $name = 'Blacklists';
	var $helpers = array('Html', 'Form');

	function beforeFilter() {
		parent::beforeFilter(); 
    }

	function index() {
		$this->Blacklist->recursive = 0;
		$this->set('blacklists', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid Blacklist.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('blacklist', $this->Blacklist->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Blacklist->create();
			if ($this->Blacklist->save($this->data)) {
				$this->Session->setFlash(__('The Blacklist has been saved', true));
				$this->redirect(array('action'=>'view', $this->Blacklist->id));
			} else {
				$this->Session->setFlash(__('The Blacklist could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Blacklist', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->Blacklist->save($this->data)) {
				$this->Session->setFlash(__('The Blacklist has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Blacklist could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Blacklist->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Blacklist', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Blacklist->del($id)) {
			$this->Session->setFlash(__('Blacklist deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}

}
?>