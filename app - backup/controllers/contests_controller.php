<?php
class ContestsController extends AppController {

	var $name = 'Contests';
	var $helpers = array('Html', 'Form');

	
	var $paginate = array('Contest'=> array('limit'=>10) ,'Contestants' => array('limit'=>10)); 
	

	function beforeFilter() {
		parent::beforeFilter(); 
   	 	$this->Session->write('ActivePage','transactions');
			
	}
	
	
	function index() {
		$this->Contest->recursive = 0;
		$this->paginate = array('fields' => array('COUNT(Contestant.id) as entries',
		                                                                     'Contest.id',
		                                                                     'Contest.name',
		                                                                     'Contest.effect_date',
		                                                                     'Contest.expire_date'),
			                                            'order' => array('Contest.expire_date DESC'),
			                                            'group' => array('Contest.id','Contest.name', 'Contest.effect_date', 'Contest.expire_date'));								
		
		$this->set('contests', $this->paginate('Contestant'));
		
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid Contest.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('contest', $this->Contest->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Contest->create();
			if ($this->Contest->save($this->data)) {
				$this->Session->setFlash(__('The Contest has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Contest could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$this->is_admin_user()) {
			$this->Session->setFlash(__('You do not have the correct privileges to view this information.', true), 'error');			
			$this->redirect(array('action'=>'view', $id));
		}

		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Contest', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->Contest->save($this->data)) {
				$this->Session->setFlash(__('The Contest has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Contest could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Contest->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$this->is_admin_user()) {
			$this->Session->setFlash(__('You do not have the correct privileges to view this information.', true), 'error');			
			$this->redirect(array('action'=>'view', $id));
		}
		
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Contest', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Contest->del($id)) {
			$this->Session->setFlash(__('Contest deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}
	
	function get_winner($id) {
		if (!$this->is_admin_user()) {
			$this->Session->setFlash(__('You do not have the correct privileges to view this information.', true), 'error');			
			$this->redirect(array('action'=>'view', $id));
		}

		if (!$id) {
			$this->Session->setFlash(__('You must select a valid contest', true), 'error');
			$this->redirect(array('action'=>'index'));
		}
		
		$contest = $this->Contest->find('first', array('conditions' => array('id' => $id, 'CURDATE() >' => 'date_expire')));
		
		// see if contest is closed or a winner has already been selected		
		if (empty($contest)) {
			$this->Session->setFlash(__('This contest is still active', true), 'error');
			$this->redirect(array('action'=>'view', $id));
		
		}
		elseif (!empty($contest['Contest']['contestant_id'])) {			
			$this->Session->setFlash(__('The winner for this contest has already been determined', true), 'error');
			$this->redirect(array('action'=>'view', $id));
		}
		
		
		// determine the winner
		$winner = $this->Contest->Contestant->find('first',array('conditions' => array('Contestant.contest_id' => $id),
																 'order' => 'rand()',
																 'limit' => 1));
					
		// set the value of the winner and save.
		$contest['Contest']['contestant_id'] = $winner['Contestant']['id'];		
		if ($this->Contest->save($contest)) {
			$this->Session->setFlash(__('The winnner is contestant number:  ' . $winner['Contestant']['reg_number'] . ' - '. $winner['Contestant']['name'], true), 'flash_success');
			$this->redirect(array('action'=>'view', $id));
		} else {
			$this->Session->setFlash(__('The winner could not be determined at this time.', true), 'error');
			$this->redirect(array('action'=>'view', $id));
		}
	}

}
?>