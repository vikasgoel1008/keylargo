<?php

class DailyStatsController extends AppController {

	 function daily_stats() {
		$this->pageTitle = "eManager - Daily Trends - 6 Months";
		
		$days = $this->DailyStat->find('all', array( 	'order' 	=> array('date ASC'),
														'conditions' => array('to_days(now())-to_days(date) <=' => 180)));
		
		$count=0;
		$activeSubSeries = "";
		$activeBlockSeries = "";
		$activeFreeSeries = "";
		$activePayingSeries = "";
		$dateSeries = "";
		
		foreach ($days as $day) {
			$activeSubs=$day['DailyStat']['active_subs']-$day['DailyStat']['active_free_subs']-$day['DailyStat']['active_blocks'];
			$payingSubs=$day['DailyStat']['active_subs']-$day['DailyStat']['active_free_subs'];
			$dateSeries .= '<value xid=\''.$count.'\'>'.$day['DailyStat']['date'].'</value>';
			$activeSubSeries .= '<value xid=\''.$count.'\'>'.$activeSubs.'</value>';
			$activeBlockSeries .= '<value xid=\''.$count.'\'>'.$day['DailyStat']['active_blocks'].'</value>';
			$activeFreeSeries .= '<value xid=\''.$count.'\'>'.$day['DailyStat']['active_free_subs'].'</value>';
			$activePayingSeries .= '<value xid=\''.$count.'\'>'.$payingSubs.'</value>';
			$count++;
		}
		
		$this->set('seriesXML',$dateSeries);
		$this->set('valuesXML',"<graph gid='1'>$activeSubSeries</graph><graph gid='2'>$activeBlockSeries</graph><graph gid='3'>0</graph><graph gid='4'>$activePayingSeries</graph>");
	}
	
	function daily_stats_30_day() {
		$this->pageTitle = "eManager - Daily Trends - 6 Months";
		
		$days = $this->DailyStat->find('all', array( 	'order' 	=> array('date ASC'),
														'conditions' => array('to_days(now())-to_days(date) <=' => 30)));
		
		$count=0;
		$activeSubSeries = "";
		$activeBlockSeries = "";
		$activeFreeSeries = "";
		$activePayingSeries = "";
		$dateSeries = "";
		
		foreach ($days as $day) {
			$activeSubs=$day['DailyStat']['active_subs']-$day['DailyStat']['active_free_subs']-$day['DailyStat']['active_blocks'];
			$payingSubs=$day['DailyStat']['active_subs']-$day['DailyStat']['active_free_subs'];
			$dateSeries .= '<value xid=\''.$day['DailyStat']['date'].'\'>'.$day['DailyStat']['date'].'</value>';
			$activeSubSeries .= '<value xid=\''.$day['DailyStat']['date'].'\'>'.$activeSubs.'</value>';
			$activeBlockSeries .= '<value xid=\''.$day['DailyStat']['date'].'\'>'.$day['DailyStat']['active_blocks'].'</value>';
			$activeFreeSeries .= '<value xid=\''.$day['DailyStat']['date'].'\'>'.$day['DailyStat']['active_free_subs'].'</value>';
			$activePayingSeries .= '<value xid=\''.$day['DailyStat']['date'].'\'>'.$payingSubs.'</value>';
			$count++;
		}
		
		$this->set('seriesXML',$dateSeries);
		$this->set('valuesXML',"<graph gid='1'>$activeSubSeries</graph><graph gid='2'>$activeBlockSeries</graph><graph gid='3'>0</graph><graph gid='4'>$activePayingSeries</graph>");
	}

	function report_graph_cancels_by_day () {
		
		$numberDays = 30;
		$this->pageTitle = "eManager - $numberDays Day Cancels";
		
		$days = $this->DailyStat->find('all', array( 	'order' 	=> array('date ASC'),
														'conditions' => array('to_days(now())-to_days(date) <=' => $numberDays)));
		
		$count=0;
		$cancelSeries = "";
		$dateSeries = "";
		
		foreach ($days as $day) {
			$dateSeries .= '<value xid=\''.$count.'\'>'.$day['DailyStat']['date'].'</value>';
			$cancelSeries .= '<value xid=\''.$count.'\'>'.$day['DailyStat']['cancels_today'].'</value>';
			$count++;
		}
		
		$this->set('seriesXML',$dateSeries);
		$this->set('valuesXML',"<graph gid='1'>$cancelSeries</graph>");
		$this->set('numDays',$numberDays);
		
	}
	
}