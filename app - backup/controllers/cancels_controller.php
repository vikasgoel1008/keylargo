<?php
class CancelsController extends AppController {

	var $name = 'Cancels';
	var $helpers = array('Html', 'Form');


	function beforeFilter() {
		parent::beforeFilter(); 
    }

	function index() {
		$this->Cancel->recursive = 0;
		$this->set('cancels', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid Cancel.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('cancel', $this->Cancel->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Cancel->create();
			if ($this->Cancel->save($this->data)) {
				$this->Session->setFlash(__('The Cancel has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Cancel could not be saved. Please, try again.', true));
			}
		}
		$members = $this->Cancel->Member->find('list');
		$plans = $this->Cancel->Plan->find('list');
		$this->set(compact('members', 'plans'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Cancel', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->Cancel->save($this->data)) {
				$this->Session->setFlash(__('The Cancel has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Cancel could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Cancel->read(null, $id);
		}
		$members = $this->Cancel->Member->find('list');
		$plans = $this->Cancel->Plan->find('list');
		$this->set(compact('members','plans'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Cancel', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Cancel->del($id)) {
			$this->Session->setFlash(__('Cancel deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}
	
	function report_graph_30_day_cancels() {
		$this->pageTitle = "eManager - Cancels by Type - 30 Days";
		
		$cancels = $this->Cancel->find('all', array('fields'	=> array('cancel_date', 
																		 'reason', 
																		 'count(*) nbr_cancels'),
		                                     			   	'order' 	=> array('cancel_date ASC', 'reason'),
														 	'group'	 	=> array('cancel_date ASC', 'reason'),
															'conditions' => array('to_days(now())-to_days(cancel_date) <=' => 31)));
		//print_r($cancels);
		
        $rcd_date = "";
        $userCancelSeries = "";        
        $freeAccountFraundSeries = "";
		$billFailSeries = "";
		$defaultSeries = "";
		$dateSeries = "";
		foreach ($cancels as $cancel) {
            
            if ($cancel['Cancel']['cancel_date'] != $rcd_date) {
                $dateSeries .= '<value xid=\''.$cancel['Cancel']['cancel_date'].'\'>'.$cancel['Cancel']['cancel_date'].'</value>';
                $rcd_date = $cancel['Cancel']['cancel_date'];
            }
            
            switch ($cancel['Cancel']['reason']) {
                case "user cancel":
                    $userCancelSeries .= '<value xid=\''.$cancel['Cancel']['cancel_date'].'\'>'.$cancel[0]['nbr_cancels'].'</value>';
                    break;
                    
                case "free account fraud":
                    $freeAccountFraundSeries .= '<value xid=\''.$cancel['Cancel']['cancel_date'].'\'>'.$cancel[0]['nbr_cancels'].'</value>';
                    break;
                    
                case "billing failure":
                    $billFailSeries .= '<value xid=\''.$cancel['Cancel']['cancel_date'].'\'>'.$cancel[0]['nbr_cancels'].'</value>';
                    break;
                    
                default:
                    $defaultSeries .= '<value xid=\''.$cancel['Cancel']['cancel_date'].'\'>'.$cancel[0]['nbr_cancels'].'</value>';
                    break;

                
            }
            
		  
		}
		
		$this->set('seriesXML',$dateSeries);
		$this->set('valuesXML',"<graph gid='1'>$userCancelSeries</graph><graph gid='2'>$freeAccountFraundSeries</graph><graph gid='3'>$billFailSeries</graph><graph gid='4'>$defaultSeries</graph>");
		
		
		parent::session_clean_member_id();
	}

}
?>