<?php
class PlansController extends AppController {

	var $name = 'Plans';
	var $helpers = array('Html', 'Form');
	var $plan_status = array('active'	=> 'Active', 
						     'current' 	=> 'Current', 
							 'inactive'	=> 'Inactive');

	var $plan_types = array('subscription'	=> 'Subscription', 
						     'block' 	=> 'Block');
							 
	var $yesNo = array('Y' => 'Yes',
	                   'N' => 'No');

	function active() {
		$this->Plan->recursive = 0;
		
		$this->paginate = array('limit' => 15,
								'order' => array('id' => 'ASC'));
		
		$this->set('plans', $this->paginate(array('status' => 'current')));
		
		$this->pageTitle = "NGD Active Plan Listing";
		$this->Session->write('ActivePage','activePlans');

	}

	function index() {
		$this->Plan->recursive = 0;
		$this->paginate = array('limit' => 20,
								'order' => array('id' => 'DESC'));
		
		$this->set('plans', $this->paginate());
		$this->Session->write('ActivePage','allPlans');
		
	}

	function view($id = null) {
		$this->Plan->recursive = 0;
		
		if (!$id) {
			$this->Session->setFlash(__('Invalid Plan.', true), 'error');
			$this->redirect(array('action'=>'index'));
		}
		
		$this->Plan->recursive = -1;
		$this->set('plan', $this->Plan->read(null, $id));
	}

	function add() {
		if (!$this->is_admin_user()) {
			$this->Session->setFlash(__('You cannot modify this information.', true), 'error');			
			$this->redirect(array('action'=>'active'));
		}
		
		if (!empty($this->data)) {
			$this->Plan->create();
			if ($this->Plan->save($this->data)) {
				$this->Session->setFlash(__('The Plan has been saved', true), 'flash_success');
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Plan could not be saved. Please, try again.', true), 'error');
			}
		}
		
		$this->set('status', $this->plan_status);
		$this->Session->write('ActivePage','addPlan');
	}

	function edit($id = null) {
		if (!$this->is_admin_user()) {
			$this->Session->setFlash(__('You cannot modify this information.', true), 'error');			
			$this->redirect(array('action'=>'view', $id));
		}
		
		$this->Plan->recursive = 0;
		
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Plan', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->Plan->save($this->data)) {
				$this->Session->setFlash(__('The Plan has been saved', true), 'flash_success');
				$this->redirect(array('action'=>'view', $this->data['Plan']['id']));
			} else {
				$this->Session->setFlash(__('The Plan could not be saved. Please, try again.', true), 'error');
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Plan->read(null, $id);
		}
		//print_r($this->data);
		$this->set('yesNo', $this->yesNo);
		$this->set('status', $this->plan_status);
	}
	
	function edit_status($id = null) {
		if (!$this->is_admin_user()) {
			$this->Session->setFlash(__('You cannot modify this information.', true), 'error');			
			$this->redirect(array('action'=>'view', $id));
		}
	
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Plan', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->Plan->save($this->data)) {
				$this->Session->setFlash(__('The status has been changed', true), 'flash_success');
				$this->redirect(array('action'=>'view', $this->data['Plan']['id']));
			} else {
				$this->Session->setFlash(__('We could not change the status on this plan.', true), 'error');
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Plan->read(null, $id);
		}
		
		$this->set('yesNo', $this->yesNo);
		$this->set('status', $this->plan_status);
	}


	function delete($id = null) {
		if (!$this->is_admin_user()) {
			$this->Session->setFlash(__('You cannot modify this information.', true), 'error');			
			$this->redirect(array('action'=>'view', $id));
		}
		
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Plan', true), 'error');
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Plan->del($id)) {
			$this->Session->setFlash(__('Plan deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}
	
	function promotion_start($plan_id) {
		
		
		ini_set('memory_limit','256M'); 
		
		if (!$this->is_admin_user()) {
			$this->Session->setFlash(__('You cannot modify this information.', true), 'error');			
			$this->redirect(array('action'=>'view', $id));
		}
	
		if (!$plan_id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Plan', true), 'error');
			$this->redirect($this->referer());
		}
		if (!empty($this->data)) {
			
			// get the plan data for reference purposes.
			$plan = $this->Plan->read(null, $this->data['Plan']['id']);
			
			// enable promotion flag and save off original price.
			$this->data['Plan']['promotion'] = 'Y';
			$this->data['Plan']['curr_3_price'] = $plan['Plan']['curr_1_price'];	
			
			
			if ($this->Plan->save($this->data)) {
				$this->Session->setFlash(__('The plan has been updated for this promotion', true), 'flash_success');
				
				$xy = $this->Session->read('thisrefer');
				$this->redirect($xy);
			} else {
				$this->Session->setFlash(__('Oops! No promo for you buddy.', true), 'error');
			}
		}
		if (empty($this->data)) {
			$xy = $this->referer();
			$this->Session->write('thisrefer', $xy);
			$this->recursive = -1;
			$this->data = $this->Plan->read(null, $plan_id);
		}
		
		$this->set('yesNo', $this->yesNo);
		$this->set('status', $this->plan_status);
		$this->pageTitle = "Promotion Updates";
      	
	}
	
	function promotion_stop($plan_id) {
		
		ini_set('memory_limit','256M'); 
		
		if (!$this->is_admin_user()) {
			$this->Session->setFlash(__('You cannot modify this information.', true), 'error');			
			$this->redirect(array('action'=>'view', $plan_id));
		}
	
		if (!$plan_id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Plan', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			
			// get the plan data for reference purposes.
			$plan = $this->Plan->read(null, $this->data['Plan']['id']);
			
			// enable promotion flag and save off original price.
			$this->data['Plan']['promotion'] = 'N';
			$this->data['Plan']['curr_3_price'] = $plan['Plan']['curr_1_price'];	
			
			
			if ($this->Plan->save($this->data)) {
				$this->Session->setFlash(__('The promotion has been stopped', true), 'flash_success');
				
				$xy = $this->Session->read('thisrefer');
				$this->redirect($xy);
			} else {
				$this->Session->setFlash(__('Oops! No promo for you buddy.', true), 'error');
			}
		}
		if (empty($this->data)) {
			$xy = $this->referer();
			$this->Session->write('thisrefer', $xy);
			$this->recursive = -1;
			$this->data = $this->Plan->read(null, $plan_id);
		}
		
		$this->set('yesNo', $this->yesNo);
		$this->set('status', $this->plan_status);
		$this->pageTitle = "Promotion Updates";
      	
	}

}
?>