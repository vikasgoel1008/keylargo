<?php
class FreeAccountAuthsController extends AppController {

	var $name = 'FreeAccountAuths';
	var $helpers = array('Html', 'Form');

	function beforeFilter() {
	    parent::beforeFilter(); 
	    
	}

	function index() {
		$this->FreeAccountAuth->recursive = 0;
		
		$this->paginate = array('limit' => 20,
								'order' => array('FreeAccountAuth.id' => 'DESC'));
		
		parent::session_clean_member_id();
		$this->set('freeAccountAuths', $this->paginate());
	}
	
	function bulk_create() {
	
		if (!empty($this->data)) {
			
			$count = 0;
			$records = $this->data['FreeAccountAuth']['records'];
			
			//echo "records $records";
			
			while ($count < $records ) {
				
				$this->data['FreeAccountAuth']['auth_code'] = sha1('ngdFreeAccountInvite'.date('Y-m-d-H-i-s'));
				$this->FreeAccountAuth->create();				
				$this->FreeAccountAuth->save($this->data);
				$count++;				
				$this->my_sleep(1);

			}
			
			$this->Session->setFlash(__('Invites created', true), 'flash_success');
			$this->redirect(array('action'=>'index'));
		
		}
		
		//$auth_code = sha1($username.date('Y-m-d-H-i-s').$clean['email'].$_SERVER['HTTP_X_FORWARDED_FOR']);
				
	
	}
	
	
	
	function my_sleep($seconds)
	{
	    $start = microtime(true);
	    for ($i = 1; $i <= $seconds; $i ++) {
	        @time_sleep_until($start + $i);
	    }
	} 
	
	function send_invite($id) {
		
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Please select an invite', true));
			$this->redirect(array('action'=>'index'));
		}
		
		if (!empty($this->data)) {
		
			$gaSource = '?utm_source=ngd-invite-email&utm_medium=email&utm_campaign=outbound-customer-mail';

			$text = $this->_generateInviteTextEmail($this->data['FreeAccountAuth']['name'], $this->data['FreeAccountAuth']['invited_by'], $this->data['FreeAccountAuth']['free_months'], $this->data['FreeAccountAuth']['auth_code'], $gaSource);
			$html = $this->_generateInviteHtmlEmail($this->data['FreeAccountAuth']['name'], $this->data['FreeAccountAuth']['invited_by'], $this->data['FreeAccountAuth']['free_months'], $this->data['FreeAccountAuth']['auth_code'], $gaSource);
				
			$sendFail = false;
			
			try {

				// send the invite
				App::import('Vendor','Postmark',array('file'=>'Postmark.php'));

				$msg = new Mail_Postmark();
				$msg->to($this->data['FreeAccountAuth']['email'], $this->data['FreeAccountAuth']['name']);
			    $msg->subject('Your NewsgroupDirect Invitation');
			    $msg->messagePlain($text);
			    $msg->messageHtml($html);
			    
			    //echo $html;
			    $msg->send();
		    }
		    catch (Exception $e) {
		    	$sendFail = true;
		    	$errorMsg = $e->getMessage();		    	
		    }
			
			if (!$sendFail && $this->FreeAccountAuth->save($this->data)) {
			
				$this->Session->setFlash(__('The invite has been sent', true), 'flash_success');
				$this->redirect(array('action'=>'index'));
			} 
			else {
				if (!empty($errorMsg)) { $myError = "The Invite failed to be sent <br/>$errorMsg"; }	
				else {$myError = "The invite could not be saved";}
				
				$this->Session->setFlash(__($myError, true), 'error');			
			}
		}
		
		
		if (empty($this->data)) {
			$this->data = $this->FreeAccountAuth->read(null, $id);
		}

		
		
	}
	
	function _generateInviteTextEmail($name, $invited_by, $free_months, $auth_code, $analyticsSource) {
		
		$text  = "Hi $name,\n\n";
		$text .= "You have been invited by $invited_by to try out our services, free, for $free_months months. \n\n";
		$text .= 'Just visit the address below to create your free account\n';
		$text .= "http://www.newsgroupdirect.com/invite/$analyticsSource&invite=$auth_code \n\n";
		$text .= $this->_generateTextEmailFooter($analyticsSource);
		
		return $text;
	
	
	}
	
	function _generateInviteHtmlEmail($name, $invited_by, $free_months, $auth_code, $analyticsSource) {
	
		$html  = $this->_generateHTMLEmailHeader('NewsgroupDirect Invite');
	
		$html .= '<p style="font-family: Arial, MS Trebuchet, sans-serif; font-size: 12px; margin: 1em 0px;">
									Hi '.$name.',
								</p>
								<p style="font-family: Arial, MS Trebuchet, sans-serif; font-size: 12px; margin: 1em 0px;">
									You have been invited by '.$invited_by.' to try out our services, free, for '.$free_months.' months.  <br/>Just visit the address below to get your free account.
								</p>										
							</div>
						</td>
					</tr>';
		$html .= '<tr><td style="border-top:1px dashed #A3B8C5; border-bottom:1px dashed #A3B8C5; border-right:0px solid #A3B8C5; border-left:0px solid #A3B8C5; background-color:#FFFFCC; padding: 10px 20px 10px 20px;" align="left">';
		
		$html .= '
							<p style="font-family: Arial, MS Trebuchet, sans-serif; font-size: 12px; color: #E50E07; margin: 1em 0px;">
								<strong><a href="http://www.newsgroupdirect.com/invite/'.$analyticsSource.'&invite='.$auth_code.'">Create Your Free Account</a></strong>
							</p>	
						</td>
					</tr>';
		
		$html .= $this->_generateHTMLEmailFooter($analyticsSource);
		
		return $html;

	
	}
	

	
	function create_tiny($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid FreeAccountAuth.', true));
			$this->redirect(array('action'=>'index'));
		}
		
		$accAuth =  $this->FreeAccountAuth->read(null, $id);
		$url = "http://www.newsgroupdirect.com/invite/?utm_source=ngd-invite-email&utm_medium=email&utm_campaign=outbound-customer-mail&invite={$accAuth['FreeAccountAuth']['auth_code']}";
		$this->set('url',$url);
	}
	
	/*
	function add() {
		if (!empty($this->data)) {
			$this->FreeAccountAuth->create();
			if ($this->FreeAccountAuth->save($this->data)) {
				$this->Session->setFlash(__('The FreeAccountAuth has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The FreeAccountAuth could not be saved. Please, try again.', true));
			}
		}
		$members = $this->FreeAccountAuth->Member->find('list');
		$this->set(compact('members'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid FreeAccountAuth', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->FreeAccountAuth->save($this->data)) {
				$this->Session->setFlash(__('The FreeAccountAuth has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The FreeAccountAuth could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->FreeAccountAuth->read(null, $id);
		}
		$members = $this->FreeAccountAuth->Member->find('list');
		$this->set(compact('members'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for FreeAccountAuth', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->FreeAccountAuth->del($id)) {
			$this->Session->setFlash(__('FreeAccountAuth deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}
	*/
}
?>