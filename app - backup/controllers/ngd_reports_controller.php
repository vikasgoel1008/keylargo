<?php
class NgdReportsController extends AppController {

	var $name = 'NgdReports';
	var $helpers = array('Html', 'Form');

	function beforeFilter() {
	    parent::beforeFilter(); 
	}

	function index() {
		$this->NgdReport->recursive = 0;
		$this->set('NgdReports', $this->paginate());
		$this->Session->write('ActivePage','allOptions');
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid NgdReport.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('NgdReport', $this->NgdReport->read(null, $id));
	}
	
	function billings() {
	
		// get most recent reporting date
		$report_date = $this->NgdReport->getRecentReportDate('avg_billings','overall');
			
		// set up pagination
		$this->paginate = array('limit' => 20);
		
		$conds = array("report" => 'avg_billings', 'data_date' => $report_date);
		
		$reports = $this->paginate($conds);
		$this->set('reports', $reports);
		$this->set('report_date', $report_date);
	}
	
	function chi($type = 'member') {
	
		// get report type
		$sub = '';
		if ($type = 'member') {
			$sub = 'member_chi';
		}
		
		// get most recent reporting date
		$report_date = $this->NgdReport->getRecentReportDate('chi', $sub);
		
		// bind models for member/plan information
		$this->NgdReport->bindModel(array('belongsTo' => array("Member","Plan")),false);
		
		// set up pagination
		$this->paginate = array('limit' => 20, 
								'order' => 'value desc',
								'contain' => array("Member","Plan"));
		
		$conds = array("report" => 'chi', 'data_date' => $report_date);
		if (!empty($sub)) {
			$conds['subkey'] = $sub;
		}
		
		$reports = $this->paginate($conds);
		$this->set('reports', $reports);
		$this->set('report_date', $report_date);
		
		
	}
	
	function churn() {
	
	}
	
	function ltv() {
	
		// get most recent reporting date
		$report_date = $this->NgdReport->getRecentReportDate('ltv','overall');
			
		// set up pagination
		$this->paginate = array('limit' => 20);
		
		$conds = array("report" => 'ltv', 'data_date' => $report_date);
		
		$reports = $this->paginate($conds);
		$this->set('reports', $reports);
		$this->set('report_date', $report_date);
	}
}
?>