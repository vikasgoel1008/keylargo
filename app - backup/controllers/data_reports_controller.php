<?php
class DataReportsController extends AppController {

	var $name = 'DataReports';
	var $helpers = array('Html', 'Form');

	function beforeFilter() {
	    parent::beforeFilter(); 
	    $this->DataReport->Behaviors->attach('Containable');
		
	}
	
	function usage_by_account_type($days = 30) {
		
		//$this->layout = 'empty';
		//$this->HwData->unbindModelAll();
		
		$xy = strtotime(date("Y-m-d"));
		$date1 = date('Y-m-d H:i:s', mktime(0,0,0,date('m',$xy),date('d',$xy)-$days,date('Y',$xy)));
		
		$conditions = array('reporting_date between ? and ?' => array($date1,date("Y-m-d")));
		$results = $this->DataReport->find('all', array('conditions'=>$conditions,
														'order' => array('reporting_date' => 'asc')));
		
		$this->set('bytes',$results);
		$this->set('days',$days);
		parent::session_clean_member_id();
	}
	
	function report_average_bandwidth($days = 90) {
		
		$results = $this->DataReport->query("CALL sp_ngd_008_average_bandwidth(".Sanitize::escape($days).");");
		
		$this->set('days',$days);
		$this->set('bytes',$results);
		parent::session_clean_member_id();
	}
	
	function report_daily_bandwidth_usage($days = 30, $member_id = null) {
	
		if (!empty($member_id)) {
			$this->redirect(array('controller' => 'hw_datas', 'action' => 'report_daily_bandwidth_usage', $days, $member_id));
		}
		
		$xy = strtotime(date("Y-m-d"));
		$date1 = date('Y-m-d H:i:s', mktime(0,0,0,date('m',$xy),date('d',$xy)-$days,date('Y',$xy)));
		
		$fields = array('reporting_date', 'bw_total_gb', 'bw_moving_7_day_gb', 'bw_moving_28_day_gb');
		$conds = array('reporting_date between ? and ?' => array($date1,date("Y-m-d")));
		
		$results = $this->DataReport->find('all', array('conditions'=>$conds, 'fields' => $fields,
														'order' => array('reporting_date' => 'asc')));
		
		$this->set('days',$days);
		$this->set('bytes',$results);
		//$this->set('chartMin',$chartMin-1500);
		//$this->set('chartMax',$chartMax+1500);
		//print_r($results);
		
		// capture customer account id
		parent::session_clean_member_id();
	
	}
	
	
}