<?php
class GroupsController extends AppController {

	var $name = 'Groups';
	var $helpers = array('Html', 'Form');

	function beforeFilter() {
	    parent::beforeFilter(); 
	    //$this->Auth->allowedActions = array('*');
	   
	    
	    //	echo $this->system_user['group_id']; 
	    /*
	    if ($this->system_user['group_id'] <> 1){
	    	$this->Session->setFlash(__('You do not have access to view this page.', true));
            $this->redirect('/login');
	    }
		  */  
	    $this->validateUserAccess('admin');
    	
	}
	
	function index() {
		
		
		$this->Group->recursive = 0;
		
		$this->set('groups', $this->paginate());
				
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid Group.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('group', $this->Group->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Group->create();
			
			print_r($this->Acl->Aro);
			
			if ($this->Group->save($this->data)) {
				
				//$this->Acl->Aco->create(array('parent_id' => null, 'alias' => $this->data['Group']['name']));
   				//$this->Acl->Aco->save();
   				/*
				$aro =& $this->Acl->Aro;
	
				//Here's all of our group info in an array we can iterate through
				$groups = array(0 => array(	'model'    => 'Group',
											'foreign_key'    => $this->Group->id,
											'alias' => $this->data['Group']['name']	));
				
				//Iterate and create ARO groups
				foreach($groups as $data)
				{
					//Remember to call create() when saving in loops...
					$aro->create();
					
					//Save data
					$aro->save($data);
				}
			   				
				
				*/
				
				$this->Session->setFlash(__('The Group has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Group could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Group', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->Group->save($this->data)) {
				$this->Session->setFlash(__('The Group has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Group could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Group->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Group', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Group->del($id)) {
			$this->Session->setFlash(__('Group deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}

}
?>