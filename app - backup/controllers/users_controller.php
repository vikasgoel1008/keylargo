<?php
class UsersController extends AppController {

	var $name = 'Users';
	var $helpers = array('Html', 'Form');
	var $uses = array('User','Group');
		
	function beforeFilter() {
	    parent::beforeFilter(); 
	    $this->Auth->allowedActions = array('login','logout');

	}
	
	function login() {
	    //Auth Magic
	    //$this->redirect(array('controller'=>'pages', 'action'=>'display'));
	    //echo $this->referer();
	    $this->pageTitle = "eManager - User Login";
	}
	 
	function logout() {
	    $this->Session->del('Auth.User');
        $this->redirect(array('controller'=>'users', 'action'=>'login'));	
	}
		
	
	function initGroups() {
		$this->validateUserAccess('admin');
		
	    // Admins Security (1)
    	// Allow admins to everything
	    $group =& $this->User->Group;
		$group->id = 1;     
	    $this->Acl->allow($group, 'controllers');
	 
    	
    	// Support Security (2)
    	$group =& $this->User->Group;
		$group->id = 2;
	    $this->Acl->deny($group, 'controllers');
	    $this->Acl->allow($group, 'controllers/Members');	    
	    $this->Acl->deny($group, 'controllers/Groups');
    	//parent::initGroups($group);	
    	
    }
	
	function index() {
				
		$this->User->recursive = 0;
		$this->set('users', $this->paginate());
		$this->set(compact('error_message'));
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid User.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('user', $this->User->read(null, $id));
	}

	function add() {
		if (!$this->validateUserAccess('admin')) {
			$this->Session->setFlash(__('You do not have access to add new users.', true),'error');
			$this->redirect(array('action'=>'index'));
		}
		
		if (!empty($this->data)) {
			$this->User->create();
			if ($this->User->save($this->data)) {
								
				$this->Session->setFlash(__('The User has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The User could not be saved. Please, try again.', true));
			}
		}
		$groups = $this->User->Group->find('list');
		$this->set(compact('groups'));
	}

	function edit($id = null) {
		$this->validateUserAccess('admin');
		
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid User', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->User->save($this->data)) {
				$this->Session->setFlash(__('The User has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The User could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->User->read(null, $id);
		}
		$groups = $this->User->Group->find('list');
		$this->set(compact('groups'));
	}

	function delete($id = null) {
		$this->validateUserAccess('admin');
		
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for User', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->User->del($id)) {
			$this->Session->setFlash(__('User deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}
	
	
	

}
?>