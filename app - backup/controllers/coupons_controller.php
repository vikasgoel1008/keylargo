<?php
class CouponsController extends AppController {

	var $name = 'Coupons';
	var $helpers = array('Html', 'Form');
	
	var $account_options = array('all'			=>'All Accounts', 
						    	 'all_subs' 	=> 'Only Subscription Accounts', 
								 'monthly_subs'	=> 'Only Monthly Subs', 
								 'all_blocks' 	=> "Only Block Accounts", 
								 'unlimited_only' 	=> "Monthly Unlimited Accounts", 
								 'yearly_unlimited' 	=> "Yearly Unlimited Accounts" );
	
	var $coupon_types = array('percent'			=> 'Percent Off', 
						      'dollar_off' 		=> 'Dollars Off', 
							  'dollar_set_amt' 	=> 'Special Pricing Amount');
	
	var $edu_required = array('Y'			=> 'Edu Required', 
						      'N' 		=> 'Edu Not Required');
	
	var $pre_reqs = array('no_prereq' 			=> "No Pre-Requisites",
						  'active_account'		=> 'Active NGD Account', 
						  'active_subscription' => 'Active Subscription Account', 
						  'active_block'		=> 'Active Block Account', 
						  'closed_subscription'	=> "Closed Subscription", 
						  'block_used' 			=> "Closed Block Account", 
						  'closed_unlimited' 	=> "Only Closed Unlimited Accounts", 
						  'free_account' 		=> "Free Accounts Only" , 
						  'closed' 		=> "Closed Accounts Only" );
	
	
	function beforeFilter() {
		parent::beforeFilter(); 
   	 	$this->Session->write('ActivePage','transactions');
			
	}
	
	function active() {
		$this->Coupon->recursive = 0;
		
		$this->paginate = array('limit' => 15,
								'order' => array('expire_date' => 'ASC'));
		
		$this->set('coupons', $this->paginate(array('to_days(Coupon.expire_date)-to_days(now()) >=' => 0)));
		
		$this->pageTitle = "NGD Active Coupon Listing";
		$this->Session->write('ActivePage','activeCoupon');

	}
	
	function add() {
		if (!empty($this->data)) {
			$this->Coupon->create();
			
			// convert affiliate field to affiliate_id
			$this->data['Coupon']['affiliate_id'] = $this->data['Coupon']['affiliate'];
			
			if ($this->Coupon->save($this->data)) {
				$this->Session->setFlash(__('The Coupon has been saved', true), 'flash_success');
				
				$this->Email->from    = 'NewsgroupDirect.com <support@newsgroupdirect.com>';
				//$this->Email->to      = "$name <$email>";
				$this->Email->bcc      = array('NewsgroupDirect.com <webmaster@newsgroupdirect.com>, jimlastinger@gmail.com');
				$this->Email->replyTo = 'support@newsgroupdirect.com';
				$this->Email->subject = 'New Coupon Created for NGD';
				
				// notify of new coupon creation
				$email_message = $this->system_user['username']." created coupon, ". $this->Coupon->id ." for NGD.  If this was intended, please disregard this notice.";					
				$this->Email->send($email_message);
				
				
				$this->redirect(array('action'=>'view',$this->Coupon->id));
			} else {
				$this->Session->setFlash(__('The Coupon could not be saved. Please, try again.', true), 'error');
			}
		}
		
		$this->set('options', $this->account_options);
		$this->set('types', $this->coupon_types);
		$this->set('edu_required', $this->edu_required);
		$this->set('pre_reqs', $this->pre_reqs);
								  
		$this->Session->write('ActivePage','addCoupon');

	}

	function edit($id = null) {
		if (!$this->is_admin_user()) {
			$this->Session->setFlash(__('You do not have the correct privileges to view this information.', true), 'error');			
			$this->redirect(array('action'=>'view', $id));
		}
		
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Coupon', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->Coupon->save($this->data)) {
				$this->Session->setFlash(__('The Coupon has been saved', true), 'flash_success');
				$this->redirect(array('action'=>'view',$id));
			} else {
				$this->Session->setFlash(__('The Coupon could not be saved. Please, try again.', true), 'error');
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Coupon->read(null, $id);
		}
		
		// get active plans
		$affiliates = $this->Coupon->Affiliate->find('list', array('conditions' => array('status' => 'active')));
		$this->set(compact('affiliates'));
		
		$this->set('options', $this->account_options);
		$this->set('types', $this->coupon_types);
		$this->set('edu_required', $this->edu_required);
		$this->set('pre_reqs', $this->pre_reqs);
		

	}

	function delete($id = null) {
		if (!$this->is_admin_user()) {
			$this->Session->setFlash(__('You do not have the correct privileges to view this information.', true), 'error');			
			$this->redirect(array('action'=>'view', $id));
		}
		
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Coupon', true), 'error');
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Coupon->del($id)) {
			$this->Session->setFlash(__('Coupon deleted', true), 'flash_success');
			
			$this->Email->from    = 'NewsgroupDirect.com <support@newsgroupdirect.com>';
			//$this->Email->to      = "$name <$email>";
			$this->Email->bcc      = array('NewsgroupDirect.com <webmaster@newsgroupdirect.com>, jimlastinger@gmail.com');
			$this->Email->replyTo = 'support@newsgroupdirect.com';
			$this->Email->subject = 'Coupon Deleted for NGD';
			
			// notify coupon deleted
			$email_message = "Coupon, ". $this->Coupon->id ." was deleted for today.  If this was intended, please disregard this notice.";					
			$this->Email->send($email_message);
			
			
			$this->redirect(array('action'=>'index'));
		}
	}

	function index() {
		$this->Coupon->recursive = 0;
		
		$this->paginate = array('limit' => 15,
								'order' => array('expire_date' => 'DESC'));
		
		$this->set('coupons', $this->paginate());
		
		$this->pageTitle = "NGD Coupon Listing";
		$this->Session->write('ActivePage','allCoupon');

	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid Coupon.', true));
			$this->redirect(array('action'=>'index'));
		}
		
		$coupon = $this->Coupon->read(null, $id);
		//print_r($coupon);
		$this->set('coupon',$coupon);
		
		$this->pageTitle = "NGD View Coupon";
		$this->Session->write('ActivePage','');
	}

}
?>