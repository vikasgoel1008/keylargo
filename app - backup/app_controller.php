<?php
App::import('Sanitize');

class AppController extends Controller {
	var $helpers = array('Html','Form','Ajax','Javascript');
	
	var $components = array('Auth','Email'); //'Acl', 
	var $system_user = "";
	
	var $admin_group = 1;
	
	
    function beforeFilter() {
        //Configure AuthComponent
        $this->Auth->autoRedirect = true;
		$this->Auth->flashElement = "error";
        $this->Auth->loginAction = '/login';
		$this->Auth->loginError = "The email address / password combination submitted is invalid";
        $this->Auth->authError = "Please sign in.";
		$this->Auth->allow('active_member_lookup');
        $this->Auth->deny('*');
        
		if ($this->Session->check('Auth.User')) {
			$systemUser = $this->Session->read('Auth.User');
			$this->system_user = $systemUser;
			$this->set(compact('systemUser'));
		}
		
		if ($this->is_admin_user()) {			
			$this->set('ngdAdminUser', true);
		}
		else {			
			$this->set('ngdAdminUser', false);
		}


		//$this->logActivity($member_id, $log_type, $log_reason, $value_type, $old, $new);

    }
		

	
	
    function validateUserAccess($required_access) {
     	
    	$this->Group->recursive = 0;		
    	$getGroup =  $this->Group->find('all', array('conditions' => array('name' => $required_access),
    	   										  'fields'     => array('id')));
	    if ($this->system_user['group_id'] <> $getGroup[0]['Group']['id']){
	    	return false;
	    	//$this->Session->setFlash(__('You do not have access to view this page.', true));
            //$this->redirect('/login');
            //exit();
	    }
	    
	    return true;
    }
    
    function is_admin_user() {
    
    	if (isset($this->system_user['group_id'])) {
    		//echo $this->system_user['group_id'];
    		
	    	if ($this->system_user['group_id'] == $this->admin_group) {
		    	return true;
		    }
		    
		    return false;
	    }
	    else {
	    	return false;
	    }

    }
    
	function session_clean_member_id () {
		$this->Session->del('Member.id');
	}
	
	function session_write_member_id ($id) {
		$this->Session->write('Member.id', $id);
	}
	
	/**
	 * 	returns the rightmost N characters of a string
	 */
	function right($input,$length){
	
		if($length>(strlen($input))){
			$length=strlen($input);
		}
		return substr($input,strlen($input)-$length,$length);
	}
	
	/**
	 * 	returns the leftmost N characters of a string
	 */
	function left($input,$length){
	
		if($length>(strlen($input))){
			$length=strlen($input);
		}	
		return substr($input,0,$length);
	}
	
	function send_support_email($subject, $message) {
		$this->Email->from    = '<no-reply@newsgroupdirect.com>';
		$this->Email->to      = "<support@newsgroupdirect.com>";
		$this->Email->subject = $subject;
			
		$this->Email->send($message);
	}
	
	
	function amcharts_buildXMLstring ($data, $type) {
		 $counter = 0;
		 $xmlString = ''; 
		
		 foreach($data as $key => $value) {
		   $xmlString .= '<value xid=\''.$counter.'\'>'.$value[$type].'</value>';
		   $counter++;
		 } 
		
		 return $xmlString;
	}
	
	function amcharts_buildXMLstring_mod ($data, $type) {
		 $counter = 0;
		 $xmlString = ''; 
		
		 foreach($data as $key => $value) {
		   $xmlString .= '<value xid=\''.$counter.'\'>'.$value[0][$type].'</value>';
		   $counter++;
		 } 
		
		 return $xmlString;
	}
	
	function logActivity($member_id, $log_type, $log_reason, $value_type, $old, $new) {
		
		$this->LoadModel("MLog");
		$this->MLog->logActivity($member_id, $log_type, $log_reason, $value_type, $old, $new, $_SERVER['REMOTE_ADDR'], $this->system_user['id']);
		
	}
	
	function _generateHTMLEmailHeader($title) {
		$html_open = "<body bgcolor='#D5E7F1' style='font-family: Arial, MS Trebuchet, sans-serif; padding: 5px 5px 5px 5px; background: #F1F1F1'>";
		$html_open .= "<center><div style='margin: 0; padding: 10px; background: #F1F1F1'><table width='635' border='0' cellpadding='0' cellspacing='0'>";
		$html_open .= "<tr style='margin: 0; padding: 0; height: 60px'>";
		$html_open .= "<td style='margin: 0; padding: 0; height: 60px'><img src='http://newsgroupdirect.com/images/layout/logo.gif' height='50' /></td>";
		$html_open .= "</tr><tr>";
		$html_open .= "<td style='border-top:0px solid #A3B8C5; border-right:0px solid #A3B8C5; border-left:0px solid #A3B8C5; background-color:#FEFEFE; padding: 10px 20px 10px 20px;' align='left'>";
		$html_open .= "<div style='margin-top: 5px;'>";
		$html_open .= "<h1 style='font-family: Arial, MS Trebuchet, sans-serif; font-size: 24px;margin: 0 0 10px;padding: 0; color: #000; background-color:#FFF;'>$title</h1>";
		
		return $html_open;

	}
	
	function _generateHTMLEmailFooter($gaSourceExt) {
	
		$html_source  = "<tr>";
		$html_source .= "<td style='border-bottom:0px solid #A3B8C5; border-right:0px solid #A3B8C5; border-left:0px solid #A3B8C5; background-color:#FEFEFE; padding: 10px 20px 10px 20px;' align='left'>";
		$html_source .= "<p style='font-family: Arial, MS Trebuchet, sans-serif; font-size: 12px; margin: 1em 0px;'>";
		$html_source .= "<br/><br/>Thank you for choosing NewsgroupDirect as your usenet provider.<br/><br/>";	
		$html_source .= "Sincerely,<br/>";
		$html_source .= "<strong>The NewsgroupDirect Team</strong><br/><br/>";
		$html_source .= "<img src='http://newsgroupdirect.com/images/layout/logo.gif' height='30' />";
		$html_source .= "</p></td></tr>";
		$html_source .= "<tr>";
		$html_source .= "<td style='border-top:1px dashed #A3B8C5; border-bottom:1px dashed #A3B8C5; border-right:0px solid #A3B8C5; border-left:0px solid #A3B8C5; background-color:#CCFFF4; padding: 10px 20px 10px 20px;' align='left'>";
		$html_source .= "<p style='font-family: Arial, MS Trebuchet, sans-serif; font-size: 16px; color: #E50E07; margin: 1em 0px;font-weight: bold;'>Need Help?</p>";
		$html_source .= "<p style='font-family: Arial, MS Trebuchet, sans-serif; font-size: 12px; margin: 1em 0px;'>";
		$html_source .= "We're ready to help! Visit our <a href='http://support.newsgroupdirect.com'>Helpdesk</a> or ";
		$html_source .= "<a href='http://www.newsgroupdirect.com/help/contact.php$gaSourceExt'>Send Us An Email</a>.";
		$html_source .= "</p></td></tr>";
		$html_source .= "<tr>";
		$html_source .= "<td style='border-top:0px solid #A3B8C5; border-right:0px solid #A3B8C5; border-left:0px solid #A3B8C5; background-color:#FEFEFE; padding: 10px 20px 10px 20px;' align='left'>";
		$html_source .= "<div style='margin-top: 5px;'>";
		$html_source .= "<p style='font-family: Arial, MS Trebuchet, sans-serif; font-size: 12px; color: #E50E07; margin: 1em 0px;'>";
		$html_source .= "<strong>We also think you might like some of our other products</strong></p>";
	
		$html_source .= "<center>";
		$html_source .= "<table border='0' cellspacing='5' cellpadding='5' style='text-align: left; border: 0px solid #ddd'>";
		
		$html_source .= "<tr><td><img src='http://shrinkingapp.com/css/images/shrinking-logo-trans.png' width='190'/></td>";
		$html_source .= "<td><p style='font-family: Arial, MS Trebuchet, sans-serif; font-size: 12px; margin: 1em 0px;'>";
		$html_source .= "<strong><a href='http://shrinkingapp.com/$gaSourceExt' style='font-family: Arial, MS Trebuchet, sans-serif; color: #0c4976; font-size: 12px; text-decoration: underline; border: none;'>Shrinking</a></strong><br/>";
		$html_source .= "<strong>Easy To Use Weight-Loss Tracker</strong><br/>";
		$html_source .= "Track your weight, weight-loss goals, diet, and more with Shrinking. Shrinking gives you the power to take control of your weight-loss.";
		$html_source .= "</p></td></tr>";
		
		$html_source .= "<tr><td><img src='http://storageninja.com/img/logo.gif' width='190'/></td>";
		$html_source .= "<td><p style='font-family: Arial, MS Trebuchet, sans-serif; font-size: 12px; margin: 1em 0px;'>";
		$html_source .= "<strong><a href='http://storageninja.com/$gaSourceExt' style='font-family: Arial, MS Trebuchet, sans-serif; color: #0c4976; font-size: 12px; text-decoration: underline; border: none;'>StorageNinja</a></strong><br/>";
		$html_source .= "<strong>Simple Online Storage</strong><br/>";
		$html_source .= "Secure, reliable online storage and backup for your most critical files. Never worry about losing a file again.";
		$html_source .= "</p></td></tr>";
		
		$html_source .= "<tr><td><img src='http://sparkcdn.com/images/logo.gif' width='190'/></td>";
		$html_source .= "<td><p style='font-family: Arial, MS Trebuchet, sans-serif; font-size: 12px; margin: 1em 0px;'>";
		$html_source .= "<strong><a href='http://sparkcdn.com/$gaSourceExt' style='font-family: Arial, MS Trebuchet, sans-serif; color: #0c4976; font-size: 12px; text-decoration: underline; border: none;'>SparkCDN</a></strong><br/>";
		$html_source .= "<strong>Content Delivery For Small Business</strong><br/>";
		$html_source .= "Live streaming, progressive streaming, content distribution, and more. Enhance your visitor's experience by powering your website's media with our world-class delivery network.";
		$html_source .= "</p></td></tr>";
		
		$html_source .= "</table></center></div></td></tr></table>";
		$html_source .= "<p style='font-family: Arial, MS Trebuchet, sans-serif;font-size: 11px; color: #666; padding: 0px 0px 0px 0; text-align: center;'>";
		$html_source .= "<a href='http://newsgroupdirect.com/$gaSourceExt' style='font-family: Arial, MS Trebuchet, sans-serif; color: #0c4976; text-decoration: underline; border: none;'>Home</a>&nbsp;|&nbsp;";
		$html_source .= "<a href='http://newsgroupdirect.com/login.php$gaSourceExt' style='font-family: Arial, MS Trebuchet, sans-serif; color: #0c4976; text-decoration: underline; border: none;'>Login</a>&nbsp;|&nbsp;";
		$html_source .= "<a href='http://www.newsgroupdirect.com/help/contact.php$gaSourceExt' style='font-family: Arial, MS Trebuchet, sans-serif; color: #0c4976; text-decoration: underline; border: none;'>Contact Us</a>&nbsp;|&nbsp;";
		$html_source .= "<a href='http://support.newsgroupdirect.com' style='font-family: Arial, MS Trebuchet, sans-serif; color: #0c4976; text-decoration: underline; border: none;'>Support</a></p>";
		$html_source .= "<p style='font-family: Arial, MS Trebuchet, sans-serif;font-size: 11px; color: #666; padding: 0px 20px 5px 20px; text-align: center; width: 635px'>";
		$html_source .= "All text and design is Copyright 2004-".date('Y')." NewsgroupDirect.com. All rights reserved.";
		$html_source .= "</p></div></center></body>";
		
		return $html_source;
	
	}
	
	function _generateTextEmailFooter($gaSourceExt) {
		
		$text_source  = 'Thank you for choosing NewsgroupDirect as your usenet provider.\n\n';
		$text_source .= 'Sincerely,\n';
		$text_source .= 'The NewsgroupDirect Team\n\n\n\n';
		$text_source .= '---------------------------------------------------------------------------------------------------- \n\n';
		$text_source .= 'NEED HELP?\n';
		$text_source .= "We're ready to help! Visit our Helpdesk or Send Us An Email.\n";
		$text_source .= 'Helpdesk:  http://support.newsgroupdirect.com \n';
		$text_source .= "Send Email:  http://www.newsgroupdirect.com/help/contact.php$gaSourceExt \n\n";
		$text_source .= '---------------------------------------------------------------------------------------------------- \n\n';
		$text_source .= 'We also think you might like some of our other products \n\n';
		$text_source .= 'Shrinking \n';
		$text_source .= 'Easy To Use Weight-Loss Tracker \n';
		$text_source .= 'Track your weight, weight-loss goals, diet, and more with Shrinking. Shrinking gives you the power to take control of your weight-loss.\n';
		$text_source .= "http://shrinkingapp.com/$gaSourceExt \n\n";
		$text_source .= 'StorageNinja \n';
		$text_source .= 'Simple Online Storage \n';
		$text_source .= 'Secure, reliable online storage and backup for your most critical files. Never worry about losing a file again. \n';
		$text_source .= "http://storageninja.com/$gaSourceExt \n\n";
		$text_source .= 'SparkCDN \n';
		$text_source .= 'Content Delivery For Small Business \n';
		$text_source .= "Live streaming, progressive streaming, content distribution, and more. Enhance your visitor's experience by powering your website's media with our world-class delivery network.\n";
		$text_source .= "http://sparkcdn.com/$gaSourceExt \n\n";
		$text_source .= '----------------------------------------------------------------------------------------------------\n\n';
		$text_source .= "Home - http://newsgroupdirect.com/$gaSourceExt \n";
		$text_source .= "Login - http://newsgroupdirect.com/login.php$gaSourceExt \n";
		$text_source .= "Contact Us - http://www.newsgroupdirect.com/help/contact.php$gaSourceExt \n";
		$text_source .= 'Support - http://support.newsgroupdirect.com \n\n';
		$text_source .= 'All text and design is Copyright 2004-'.date("Y").' NewsgroupDirect.com. All rights reserved.';
		
		return $text_source;
	}
		
}

 
?>
