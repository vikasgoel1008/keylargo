<p id="tip" class="alert">
	<span class="txt"><span class="icon"></span><strong>Tip:</strong> Enter full or partial email addresses, first name, last name, etc.</span>
	<a title="Close" class="close"><span class="bg"></span>Close</a>
</p>
<h2>Edit Group</h2>
<ul class="tabs">
	<li class="active"><a href="#" title="Item1"><span>Group</span></a></li>
</ul>
<?=$form->create('Group');?>
<fieldset>
	<?=$form->input('id',array('type'=>'text','class'=>'medium','label'=>'Group Id'));?>
	<?=$form->input('name',array('type'=>'text','class'=>'medium','label'=>'Group Name'));?>
	<p>&nbsp;</p>
	<?=$form->end(array('label'=>'Submit','class'=>'button submit'));?>
	<p></p>
	<span class="clear"></span>
	<div class="actions">
		<ul>
			<li><?php echo $html->link(__('Delete', true), array('action' => 'delete', $form->value('Group.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $form->value('Group.id'))); ?></li>
			<li><?php echo $html->link(__('List Groups', true), array('action' => 'index'));?></li>
		</ul>
	</div>
</fieldset>
