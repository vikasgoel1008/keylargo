		<h2>Groups Search Results</h2>
		<ul class="tabs">
			<li class="active"><a href="#" title="Item1"><span>Basic View</span></a></li>
			<li><a href="#" title="Item2"><span>Alternate View</span></a></li>
		</ul>
		<div class="box">
			<table cellpadding="0" cellspacing="0">
				<tr>
					<th><?php echo $paginator->sort('id');?></th>
					<th><?php echo $paginator->sort('name');?></th>
					<th><?php echo $paginator->sort('created');?></th>
					<th><?php echo $paginator->sort('modified');?></th>
					<th class="actions"><?php __('Actions');?></th>
				</tr>
				
				<?php foreach ($groups as $group): ?>
					<tr class="highlight">
						<td>
							<?php echo $group['Group']['id']; ?>
						</td>
						<td>
							<?php echo $group['Group']['name']; ?>
						</td>
						<td>
							<?php echo $group['Group']['created']; ?>
						</td>
						<td>
							<?php echo $group['Group']['modified']; ?>
						</td>
						
						<td class="actions">
							<?php echo $html->link(__('View', true), array('action' => 'view', $group['Group']['id'])); ?>
							<?php echo $html->link(__('Edit', true), array('action' => 'edit', $group['Group']['id'])); ?>
							<?php echo $html->link(__('Delete', true), array('action' => 'delete', $group['Group']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $group['Group']['id'])); ?>
						</td>
					</tr>
				<?php endforeach; ?>
			</table>
			
			<div class="paging">
				<p id='pagin'>
				<?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
			 | 	<?php echo $paginator->numbers();?>
				<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class' => 'disabled'));?>
				</p>
			</div>
			
			<div class="actions">
				<ul>
					<li><?php echo $html->link(__('New Group', true), array('action' => 'add')); ?></li>
				</ul>
			</div>
			
		</div>


