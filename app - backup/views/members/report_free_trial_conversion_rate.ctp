<div class="ccTransactions index">

	<h2>Free Trial Conversion Rate</h2>
	
	<?php echo $this->element('members_menu');?>
	
	<div class="box">
		<table cellpadding="0" cellspacing="0">
			<tr class="highlight">	
				<td>Accounts Opened</td>
				<td><?php echo $opens;?></td>		
			</tr>
			
			<tr class="highlight">	
				<td>Accounts Closed in Free Trial</td>
				<td><?php echo $closes;?></td>		
			</tr>
			
			<tr class="highlight">	
				<td>Conversion Rate</td>
				<td><?php echo round(((1-($closes/$opens)) * 100),2);?>%</td>		
			</tr>
		</table>


		<span class="clear"></span>
	</div>	
</div>
