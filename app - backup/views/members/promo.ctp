<h2>Update Bill Status</h2>

<?php echo $this->element('members_menu');?>

<?=$form->create('Member', array('action' => 'promo'));?>
<fieldset>
	<?php echo $form->hidden('id'); ?> 

	<?=$form->input('name',array('type'=>'text','class'=>'small','label'=>'Name','readonly'=>'readonly'));?><br class="hid" />
	<?=$form->input('date_reg',array('type'=>'text','class'=>'small','label'=>'Registration Date','readonly'=>'readonly'));?><br class="hid" />
	<?=$form->input('next_bill_date',array('type'=>'text','class'=>'small','label'=>'Next Bill Date'));?><br class="hid" /><p>Format: YYYY-MM-DD</p>
	<?=$form->input('invites',array('type'=>'text','class'=>'small','label'=>'Invites'));?><br class="hid" />
	<?=$form->input('is_free_trial',array('type'=>'text','class'=>'small','label'=>'Free Trial'));?><br class="hid" /><p>Y / N</p>
	<?=$form->input('free_admin_acct',array('type'=>'text','class'=>'small','label'=>'Free Admin Account'));?><br class="hid" /><p>Y / N</p>
	<?=$form->input('storageninja_id',array('type'=>'text','class'=>'small','label'=>'StorageNinja Id'));?><br class="hid" />
	<p></p>
	<?=$form->end(array('label'=>'Update','class'=>'button submit'));?>
	<span class="clear"></span>
</fieldset>