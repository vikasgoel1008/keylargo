<h2>Update Payment Information</h2>

<?php echo $this->element('members_menu');?>

<?=$form->create('Member', array('action' => 'edit_pay_type'));?>
<fieldset>
	<?php echo $form->hidden('id'); ?> 

	<?=$form->input('pay_type',array('type'=>'text','class'=>'small','label'=>'Pay Type'));?><br class="hid" />
	<?=$form->input('auth_cust_id',array('type'=>'text','class'=>'medium','label'=>'Auth.net Profile Id'));?><br class="hid" />
	<?=$form->input('auth_payment_id',array('type'=>'text','class'=>'medium','label'=>'Auth.net Payment Profile'));?><br class="hid" />
	<?=$form->input('auth_shipping_id',array('type'=>'text','class'=>'medium','label'=>'Auth.net Shipping Profile'));?><br class="hid" />


	<?=$form->input('transaction_number',array('type'=>'text','class'=>'medium','label'=>'WP Transaction Number'));?><br class="hid" />
	<?=$form->input('payment_proc_id',array('type'=>'text','class'=>'medium','label'=>'WP Agreement Number'));?><br class="hid" />
	<p></p>
	<?=$form->end(array('label'=>'Update','class'=>'button submit'));?>
	<span class="clear"></span>
</fieldset>