<div class="ccTransactions index">
	<h2>Change Plan</h2>

	<?php echo $this->element('members_menu');?>

	<div class="box">
		<p>&nbsp;</p>
		<table cellpadding="0" cellspacing="0">

			<?php if ($this->data['Member']['last_upgrade_date'] > date('Y-m-d', mktime(0,0,0,date('m'),date('d')-30,date('Y')))) : ?>
			<tr class="highlight"><td colspan="3"><span class="notgood">This customers account has already been changed once in the last 30 days.</span></td></tr>
			<?php endif;?>
			
			<tr class="highlight">
				<td>Plan</td>
				<td>Standard Rate</td>
				<td>Discounted Rate</td>
			</tr>

			<?php $myPlan = false; ?>		
			<?php foreach($plans as $plan): ?>		
			<tr class="highlight">
				<td>
					<?php
						if ($member['Member']['plan_id'] == $plan['Plan']['id']) {
							$myPlan = true;
							echo $plan['Plan']['name'];
						}
						else {
							echo $html->link($plan['Plan']['name'],"change_plan/".$member['Member']['id']."/".$plan['Plan']['id']);
						}
					?>
				</td>
				<td><?php echo $plan['Plan']['curr_1_price']; ?></td>
				<td>
					<?php 
						$myCost = $plan['Plan']['curr_1_price'] * (1-$percentDiscount);
						echo $myCost; 
					?>
				</td>
				
			</tr>
			<?php endforeach;?>

			<?php if (!$myPlan) : ?>
			<tr class="highlight"><td colspan="3"><span class="notgood">This customer is currently on a plan that has been discontinued.</span></td></tr>
			<?php endif;?>
			
		</table>
		
		<p id="pagin">
			&nbsp;
		</p>
		
		<span class="clear">&nbsp;</span>
	</div>
</div>
	