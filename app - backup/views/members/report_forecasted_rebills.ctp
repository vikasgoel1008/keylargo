<script type="text/javascript" src="https://www.google.com/jsapi"></script>

 <script type="text/javascript">
   google.load('visualization', '1', {packages: ['corechart']});
 </script>


<script type="text/javascript">
  google.load("visualization", "1", {packages:["corechart"]});
  google.setOnLoadCallback(drawChart);
  function drawChart() {
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Date');
		data.addColumn('number', 'Rebill Revenues');
		data.addRows([
		<?php foreach ($rebills as $bill) : ?> <?php if (!empty($bill)): ?>
        ['<?php echo $bill['t_daily_forecasts']['next_bill_date'];?>', <?php echo $bill[0]['revenues'];?>],
		<?php endif; ?>	<?php endforeach; ?>
		]);
		
		var options = {
			title: 'Rebills Forecast',
          	chartArea:{left:75,top:75,width:"90%",height:"60%"},
           	id3D: true,
           	hAxis:{slantedText:true},
           	yAxis:{title: 'Sales in $$'},
           	series: [{color: 'red'},{color: 'blue'}, {color: 'silver'}, ],
           	
           	
		};
    var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
    chart.draw(data, options);
  }
</script>



<div class="ccTransactions index">
	<h2><?php echo $days;?> Day Rebill Forecast</h2>
	<?php echo $this->element('members_menu');?>

	<div class="box">
		
		<table cellpadding="0" cellspacing="0">
		
			<tr>
				<td colspan="2">
					 <div id="chart_div"  style="align: center; width: 700px; height: 500px;"></div>
				</td>
			</tr>
		</table>
        <span class="clear"></span>
		
	
	</div>
</div>
	
