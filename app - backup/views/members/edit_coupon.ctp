<h2>Update Coupon</h2>

<?php echo $this->element('members_menu');?>

<?=$form->create('Member', array('action' => 'edit_coupon'));?>
<fieldset>
	<?php echo $form->hidden('id'); ?> 

	<?=$form->input('coupon_id',array('type'=>'text','class'=>'medium','label'=>'Coupon', 'empty' => true));?><br class="hid" />
	<?=$form->input('promo_remain',array('type'=>'text','class'=>'medium','label'=>'Coupon Length'));?><br class="hid" />
	<p></p>
	<?=$form->end(array('label'=>'Update','class'=>'button submit'));?>
	<span class="clear"></span>
</fieldset>