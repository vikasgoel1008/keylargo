
<h2>Customer Info</h2>

<?php echo $this->element('members_menu');?>

<div class="box">
	<p>&nbsp;</p>
	<table>
		<tr class="highlight">
			<td colspan="2">
				<?php echo $html->link('Highwinds', 'highwinds/'. $member['Member']['id']);?> | 
				
				<?php if ($period=='days') :?>
				<?php echo $html->link('Monthly Usage', "usage/months/12/". $member['Member']['id']);?> | 
				<?php elseif ($period=='months'): ?>
				<?php echo $html->link('Daily Usage', "usage/days/12/". $member['Member']['id']);?> | 
				<?php endif; ?>
				<?php echo $html->link('Custom Usage', 'hw_date_range/'. $member['Member']['id']);?>  
			</td>
		</tr>
		
		<tr class="highlight"><td colspan="2"><?php echo $member['Member']['name']; ?> | <em><?php echo $member['Member']['email']; ?></em> | <?php echo $member['Member']['login_username']; ?></td></tr>
		
		<?php if ($member['Member']['is_free_trial'] == "Y"): ?>
		<tr class="highlight"><td colspan="2"><span class="notgood">This customer is currently within their free trial period.</span></td></tr>
		<?php endif; ?>
		
		<tr><td colspan="3"</td></tr>
		
		<tr><td colspan="2" align="center">Last <?php echo $length;?> <?php if ($period=='days') {echo "Days";} elseif ($period=='months') {echo "Months";}?> Usage</td></tr>
		
		<?php foreach ($bytes as $byte) :?>
			<tr>
				<td><?php echo $byte['date']; ?></td>
				<td><?php echo $byte['usage']; ?></td>
			</tr>
		<?php endforeach; ?>
		
		
	</table>
	
	
	<span class="clear"></span>

</div>
