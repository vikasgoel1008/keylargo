<div class="Members search">
	<h2>Customer Search Results</h2>
		
	<?php //echo $this->element('members_menu');?>
	<?php $paginator->options(array('url' => $this->passedArgs));?>
	
	<div class="box">
		<p>&nbsp;</p>
		<table cellpadding="0" cellspacing="0">
			<tr class="highlight">	
				<td colspan="5"><?php  echo $paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)));?></td>
			</tr>
			
			<tr>
				<th><?php echo $paginator->sort('email');?></th>
				<th><?php echo $paginator->sort('name');?></th>
				<th><?php echo $paginator->sort('status');?></th>
				<th><?php echo $paginator->sort('username');?></th>

				<?php if ($ngdAdminUser) : ?>						                    
				<th>Actions</th>
				<?php endif; ?>
			</tr>
			
			<?php foreach ($members as $member): ?>
				<tr class="highlight">
					<td><?php echo $html->link($member['Member']['email'],"/members/view/".$member['Member']['id']); ?></td>
					<td><?=$member['Member']['name'];?></td>
					<td><?=$member['Member']['status'];?></td>
					<td><?=$member['Member']['login_username'];?></td>
					
					<?php if ($ngdAdminUser) : ?>						                    
					<td class="action">
						<?php echo $html->link("Close Account","/members/close/".$member['Member']['id']); ?>
					</td>
					<?php endif; ?>
				</tr>
			<?php endforeach; ?>
				
		</table>

		<p id="pagin">
			<?php echo $paginator->first();?>
		 	<?php echo $paginator->prev('< previous', "", "", "");?>
		 	<?php //echo $paginator->prev() ?> 
			<?php echo $paginator->numbers();?>  
		 	<?php //echo $paginator->next() ?>
			<?php //echo $paginator->counter(); ?>
			<?php echo $paginator->next('next >', "", "", "");?>
			<?php echo $paginator->last();?>
		<p/>

		<span class="clear"></span>
	</div>
</div>

