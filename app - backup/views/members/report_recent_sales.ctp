<div class="ccTransactions index">
	<h2>Recent Sales Last <?php echo $dayslength;?> Days</h2>
	
	<?php echo $this->element('members_menu');?>
	<?php $paginator->options(array('url' => $this->passedArgs));?>
	
	<div class="box">
		<table cellpadding="0" cellspacing="0">
			<tr class="highlight">	
				<td colspan="5"><?php  echo $paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)));?></td>
			</tr>
			<tr class="highlight">	
				<td><?php echo $paginator->sort('date_reg');?></td>
				<td><?php echo $paginator->sort('name');?></td>		
				<td><?php echo $paginator->sort('email');?></td>
				<td><?php echo $paginator->sort('plan_id');?></td>
				<td>Void/Refund</td>
			</tr>
			<?php foreach ($members as $member): ?>
				<tr  class="highlight">
					<td>
						<?php echo $member['Member']['date_reg']; ?>
					</td>
					<td>
						<?php echo $member['Member']['name']; ?>
					</td>
					<td>
						<?php echo $html->link($member['Member']['email'], 'view/'.$member['Member']['id']); ?>
					</td>
					<td>
						<?php echo $member['Plan']['name']; ?>
					</td>
					<td style="padding: 10px">
					  <form action="/members/void" method="POST">
						<input type="hidden" name="transaction_id" value="<?php echo $member['CCTransaction']['order_nbr']; ?>">
						<input type="hidden" name="amount" value="<?php echo $member['CCTransaction']['amount'];?>">
						<input type="hidden" name="auth_cust_id" value="<?php echo $member['Member']['auth_cust_id']; ?>">
						<input type="hidden" name="auth_payment_id" value="<?php echo $member['Member']['auth_payment_id']; ?>">
						<input type="hidden" name="auth_shipping_id" value="<?php echo $member['Member']['auth_shipping_id']; ?>">
						<input type="hidden" name="member_id" value="<?php echo $member['Member']['id']; ?>">
						<input type="submit" value="Void/Refund Order" style="height: auto">
					  </form>
					</td>
				</tr>
			<?php endforeach; ?>
		</table>


		<p id="pagin">
			<?php echo $paginator->first();?>
		 	<?php echo $paginator->prev('< previous', "", "", "");?>
		 	<?php //echo $paginator->prev() ?> 
			<?php echo $paginator->numbers();?>  
		 	<?php //echo $paginator->next() ?>
			<?php //echo $paginator->counter(); ?>
			<?php echo $paginator->next('next >', "", "", "");?>
			<?php echo $paginator->last();?>
		 	
		</p>

		<span class="clear"></span>
	</div>	
</div>