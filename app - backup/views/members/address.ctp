<h2>Update Member Address</h2>

<?php echo $this->element('members_menu');?>

<?=$form->create('Member', array('action' => 'address'));?>
<fieldset>
	<?php echo $form->hidden('id'); ?> 

	<?=$form->input('address',array('type'=>'text','class'=>'small','label'=>'Address'));?><br class="hid" />
	<?=$form->input('address2',array('type'=>'text','class'=>'small','label'=>'Address 2'));?><br class="hid" />
	<?=$form->input('city',array('type'=>'text','class'=>'tiny','label'=>'City'));?><br class="hid" />
	<?=$form->input('state',array('type'=>'text','class'=>'tiny','label'=>'State'));?><br class="hid" />
	<?=$form->input('postal',array('type'=>'text','class'=>'tiny','label'=>'Postal'));?><br class="hid" />
	<?=$form->input('country',array('type'=>'text','class'=>'tiny','label'=>'Country'));?><br class="hid" />
	<p></p>
	<?=$form->end(array('label'=>'Update','class'=>'button submit'));?>
	<span class="clear"></span>
</fieldset>