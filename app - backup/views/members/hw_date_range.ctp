
<h2>HW Date Range Entry</h2>

<?php echo $this->element('members_menu');?>

	
	<?=$form->create('Member', array('action' => 'hw_usage_custom'));?>
	
	<fieldset>
		<br/>
		<h3>The report may take some time to generate if you select to broad a range.</h3>
	
		<?php echo $form->hidden('id'); ?> 
		<?=$form->input('hw_start_date',array('type'=>'text',
											'class'=>'small',
											'label'=>'Start Date'));?><br class="hid" /><p>Format: YYYY-MM-DD</p>
		<?=$form->input('hw_stop_date',array('type'=>'text',
											'class'=>'small',
											'label'=>'Stop Date'));?><br class="hid" /><p>Format: YYYY-MM-DD</p>
										
		<p></p>
		<?=$form->end(array('label'=>'View Usage','class'=>'button submit'));?>
		<span class="clear"></span>
	</fieldset>
	

	<span class="clear"></span>
