<h2>Promote Plan</h2>

<?php echo $this->element('plans_menu');?>

<?=$form->create('Plan', array('action' => 'promotion_stop'));?>
<fieldset>
	<?php echo $form->hidden('id'); ?> 

	<?php echo $form->input('name',array('type'=>'text','class'=>'medium','label'=>'Plan', 'readonly' => 'readonly'));?><br class="hid" />
	<?php echo $form->input('coupon_allowed',array('type'=>'text','class'=>'tiny','label'=>'Are Coupons Allowed?'));?><br class="hid" /><br/>
	<p>Y / N</p>
	<?php echo $form->input('curr_1_price',array('type'=>'text','class'=>'tiny','label'=>'What is the Regular Price?'));?><br class="hid" /><br/>
	<p></p>
	
	<?=$form->end(array('label'=>'Stop Promotion','class'=>'button submit'));?>
	<span class="clear"></span>
</fieldset>