<h2>Update Plan</h2>

<?php echo $this->element('plans_menu');?>

<?=$form->create('Plan', array('action' => 'edit_status'));?>
<fieldset>
	<?php echo $form->hidden('id'); ?> 

	<?php echo $form->input('name',array('type'=>'text','class'=>'medium','label'=>'Plan'));?><br class="hid" />
	<?php echo $form->input('status',array('options'=>$status,'type'=>'select','class'=>'medium','label'=>'Status', 'empty' => true));?><br class="hid" />
	<p></p>
	
	<?=$form->end(array('label'=>'Update','class'=>'button submit'));?>
	<span class="clear"></span>
</fieldset>