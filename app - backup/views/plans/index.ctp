

<div class="plans index">

	<h2>Plans</h2>
	
	<?php echo $this->element('plans_menu');?>
	<?php $paginator->options(array('url' => $this->passedArgs));?>
	
	<div class="box">
		<p>&nbsp;</p>
	
		<table cellpadding="0" cellspacing="0">
			<tr class="highlight">
				<td colspan="6"><?php  echo $paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)));?></td>
			</tr>
			<tr class="highlight">
				<td><?php echo $paginator->sort('plan_name');?></td>
				<td><?php echo $paginator->sort('type');?></td>
				<td><?php echo $paginator->sort('curr_1_price');?></td>
				<td><?php echo $paginator->sort('coupon_allowed');?></td>
				<td><?php echo $paginator->sort('duration');?></td>
				<td><?php echo $paginator->sort('status');?></td>

			</tr>
		
			<?php foreach ($plans as $plan): ?>
			<tr>
				<td>
					<?php echo $html->link(__($plan['Plan']['name'], true), array('action' => 'view', $plan['Plan']['id'])); ?>
				</td>
				<td>
					<?php echo $plan['Plan']['type']; ?>
				</td>
				<td>
					<?php echo $plan['Plan']['curr_1_price']; ?>
				</td>
				<td>
					<?php echo $plan['Plan']['coupon_allowed']; ?>
				</td>
				<td>
					<?php echo $plan['Plan']['duration']; ?>
				</td>
				<td>
					<?php echo $html->link(__($plan['Plan']['status'], true), array('action' => 'edit_status', $plan['Plan']['id'])); ?>
				</td>
			</tr>
			<?php endforeach; ?>
			
		</table>
		
		<p id="pagin">
			<?php echo $paginator->first();?>
		 	<?php echo $paginator->prev('< previous', "", "", "");?>
		 	<?php //echo $paginator->prev() ?> 
			<?php echo $paginator->numbers();?>  
		 	<?php //echo $paginator->next() ?>
			<?php //echo $paginator->counter(); ?>
			<?php echo $paginator->next('next >', "", "", "");?>
			<?php echo $paginator->last();?>
		 	
		<p/>
		
		<span class="clear"></span>
	
	</div>
</div>

