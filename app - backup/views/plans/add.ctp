<div class="plans form">
<?php echo $form->create('Plan');?>
	<fieldset>
 		<legend><?php __('Add Plan');?></legend>
	<?php
		echo $form->input('plan_id');
		echo $form->input('plan_name');
		echo $form->input('type');
		echo $form->input('allowance');
		echo $form->input('unlimited');
		echo $form->input('curr_1_price');
		echo $form->input('curr_2_price');
		echo $form->input('curr_3_price');
		echo $form->input('allot_type');
		echo $form->input('duration');
		echo $form->input('bandwidth');
		echo $form->input('connections');
		echo $form->input('ssl');
		echo $form->input('ssl_comp');
		echo $form->input('1_month_comp');
		echo $form->input('3_month_comp');
		echo $form->input('6_month_comp');
		echo $form->input('12_month_comp');
		echo $form->input('server_group_id');
		echo $form->input('virtual_server_id');
		echo $form->input('server2_group_id');
		echo $form->input('virtual_server2_id');
		echo $form->input('valid_recycle');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('List Plans', true), array('action' => 'index'));?></li>
	</ul>
</div>
