<h2>Update Plan</h2>

<?php echo $this->element('plans_menu');?>

<?=$form->create('Plan');?>
<fieldset>
	<?php echo $form->hidden('id'); ?> 

	<?php echo $form->input('name',array('type'=>'text','class'=>'medium','label'=>'Plan'));?><br class="hid" />
	<?php echo $form->input('status',array('options'=>$status,'type'=>'select','class'=>'medium','label'=>'Status', 'empty' => true));?><br class="hid" />
	<?php echo $form->input('free_trial',array('options'=>$yesNo,'type'=>'select','class'=>'small','label'=>'Free Trial?', 'empty' => true));?><br class="hid" />
	<?php echo $form->input('type',array('type'=>'text','class'=>'medium','label'=>'Type'));?><br class="hid" />
	<?php echo $form->input('allowance',array('type'=>'text','class'=>'medium','label'=>'Allowance'));?><br class="hid" />
	<?php echo $form->input('unlimited',array('options'=>$yesNo,'type'=>'select','label'=>'Unlimited?', 'empty' => true));?><br class="hid" />
	<?php echo $form->input('curr_1_price',array('type'=>'text','class'=>'medium','label'=>'USD Price'));?><br class="hid" />
	<?php echo $form->input('curr_2_price',array('type'=>'text','class'=>'medium','label'=>'GBP Price'));?><br class="hid" />
	<?php echo $form->input('curr_3_price',array('type'=>'text','class'=>'medium','label'=>'CUR 3 Price'));?><br class="hid" />
	<?php echo $form->input('allot_type',array('type'=>'text','class'=>'medium','label'=>'Allotment Type'));?><br class="hid" />
	<?php echo $form->input('duration',array('type'=>'text','class'=>'medium','label'=>'Duration'));?><br class="hid" />
	<?php echo $form->input('bandwidth',array('type'=>'text','class'=>'medium','label'=>'Bandwidth Amount'));?><br class="hid" />
	<?php echo $form->input('connections',array('type'=>'text','class'=>'medium','label'=>'Nbr Connections'));?><br class="hid" />
	<?php echo $form->input('ssl',array('type'=>'text','class'=>'medium','label'=>'SSL Enabled'));?><br class="hid" />
	<?php echo $form->input('ssl_comp',array('type'=>'text','class'=>'medium','label'=>'SSL Comp'));?><br class="hid" />
	<?php echo $form->input('1_month_comp',array('type'=>'text','class'=>'medium','label'=>'1 Month Comp'));?><br class="hid" />
	<?php echo $form->input('3_month_comp',array('type'=>'text','class'=>'medium','label'=>'3 Month Comp'));?><br class="hid" />
	<?php echo $form->input('6_month_comp',array('type'=>'text','class'=>'medium','label'=>'6 Month Comp'));?><br class="hid" />
	<?php echo $form->input('12_month_comp',array('type'=>'text','class'=>'medium','label'=>'12 Month Comp'));?><br class="hid" />
	<?php echo $form->input('server_group_id',array('type'=>'text','class'=>'medium','label'=>'Server Group Id'));?><br class="hid" />
	<?php echo $form->input('virtual_server_id',array('type'=>'text','class'=>'medium','label'=>'Virtual Server Id'));?><br class="hid" />
	<?php echo $form->input('server2_group_id',array('type'=>'text','class'=>'medium','label'=>'Server 2 Group Id'));?><br class="hid" />
	<?php echo $form->input('virtual_server2_id',array('type'=>'text','class'=>'medium','label'=>'Virtual Server 2 Id'));?><br class="hid" />
	<?php echo $form->input('valid_recycle',array('options'=>$yesNo,'type'=>'select','class'=>'medium','label'=>'Recyclable Plan?', 'empty' => true));?><br class="hid" />
	
	<?php echo $form->input('coupon_allowed',array('options'=>$yesNo,'type'=>'select','class'=>'small','label'=>'Coupon Allowed', 'empty' => true));?><br class="hid" />
	<p></p>
	
	<?=$form->end(array('label'=>'Update','class'=>'button submit'));?>
	<span class="clear"></span>
</fieldset>