<div class="plans view">
	<h2><?php echo $plan['Plan']['name'] . ' ('.$plan['Plan']['id'].')'; ?></h2>

	<?php echo $this->element('plans_menu');?>

	<div class="box">
		<p>&nbsp;</p>
		<table cellpadding="0" cellspacing="0">
			<tr class="highlight"><td colspan="3"><?php echo $html->link('Edit Plan Information', 'edit/'. $plan['Plan']['id']);?></td></tr>
			<tr>
				<td>Plan:</td>
				<td><?php echo $plan['Plan']['name']; ?></td>			
				<td></td>
			</tr>			
			<tr>
				<td>Status:</td>
				<td><?php echo $html->link($plan['Plan']['status'], 'edit_status/'. $plan['Plan']['id']);?></td>			
				<td></td>
			</tr>			
			<tr>
				<td>Free Trial:</td>
				<td><?php echo $plan['Plan']['free_trial'];?></td>			
				<td></td>
			</tr>			
			<tr class="highlight">
				<td>Type:</td>	
				<td><?php echo $plan['Plan']['type']; ?></td>			
				<td></td>
			</tr>
			<tr>
				<td>Allowance</td>
				<td><?php echo $plan['Plan']['allowance']; ?></td>			
				<td></td>
			</tr>			
			<tr class="highlight">
				<td>Unlimited:</td>	
				<td><?php echo $plan['Plan']['unlimited']; ?></td>			
				<td></td>
			</tr>

			<tr>
				<td>USD Price</td>
				<td><?php echo $plan['Plan']['curr_1_price']; ?></td>			
				<td></td>
			</tr>			
			<tr class="highlight">
				<td>GBP Price:</td>	
				<td><?php echo $plan['Plan']['curr_2_price']; ?></td>			
				<td></td>
			</tr>		
			<tr class="highlight">
				<td>Allotment Type:</td>	
				<td><?php echo $plan['Plan']['allot_type']; ?></td>			
				<td></td>
			</tr>
			<tr>
				<td>Plan Duration:</td>
				<td><?php echo $plan['Plan']['duration']; ?></td>			
				<td></td>
			</tr>			
			<tr class="highlight">
				<td>Bandwidth:</td>	
				<td><?php echo $plan['Plan']['bandwidth']; ?></td>			
				<td></td>
			</tr>

			<tr>
				<td>Connections</td>
				<td><?php echo $plan['Plan']['connections']; ?></td>			
				<td></td>
			</tr>			
			<tr class="highlight">
				<td>SSL:</td>	
				<td><?php echo $plan['Plan']['ssl']; ?></td>			
				<td></td>
			</tr>
			<tr>
				<td>SSL Comp:</td>
				<td><?php echo $plan['Plan']['ssl_comp']; ?></td>			
				<td></td>
			</tr>			
			<tr class="highlight">
				<td>1 Month Comp:</td>	
				<td><?php echo $plan['Plan']['1_month_comp']; ?></td>			
				<td></td>
			</tr>
			<tr>
				<td>3 Month Comp:</td>
				<td><?php echo $plan['Plan']['3_month_comp']; ?></td>			
				<td></td>
			</tr>			
			<tr class="highlight">
				<td>6 Month Comp:</td>	
				<td><?php echo $plan['Plan']['6_month_comp']; ?></td>			
				<td></td>
			</tr>

			<tr>
				<td>12 Month Comp</td>
				<td><?php echo $plan['Plan']['12_month_comp']; ?></td>			
				<td></td>
			</tr>			
			<tr class="highlight">
				<td>Server Group Id:</td>	
				<td><?php echo $plan['Plan']['server_group_id']; ?></td>			
				<td></td>
			</tr>
			<tr>
				<td>Virtual Server Id</td>
				<td><?php echo $plan['Plan']['virtual_server_id']; ?></td>			
				<td></td>
			</tr>			
			<tr class="highlight">
				<td>Server2 Group Id:</td>	
				<td><?php echo $plan['Plan']['server2_group_id']; ?></td>			
				<td></td>
			</tr>
			<tr>
				<td>Virtual Server2 Id</td>
				<td><?php echo $plan['Plan']['virtual_server2_id']; ?></td>			
				<td></td>
			</tr>			
			<tr class="highlight">
				<td>Valid Recycle:</td>	
				<td><?php echo $plan['Plan']['valid_recycle']; ?></td>			
				<td></td>
			</tr>
		
		
		</table>
		
		<p id="pagin">
			&nbsp;
		</p>
		
		<span class="clear">&nbsp;</span>
	</div>
</div>
	