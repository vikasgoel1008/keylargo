<ul class="tabs">
	<li <?php if ($session->read('ActivePage') == 'addSlaReport') echo "class='active'" ?>><?php echo $html->link($html->tag('span', 'Add SLA Data'), '/sla_reports/add/', array('escape'=>false, 'title'=>"Add SLA Data")); ?></li>
	<li <?php if ($session->read('ActivePage') == 'slaDailyStats') echo "class='active'" ?>><?php echo $html->link($html->tag('span', 'SLA Daily Chart'), '/sla_reports/report_daily_sla_stats', array('escape'=>false, 'title'=>"Daily Chart")); ?></li>
	<li <?php if ($session->read('ActivePage') == 'slaWeeklyStats') echo "class='active'" ?>><?php echo $html->link($html->tag('span', 'SLA Weekly Chart'), '/sla_reports/report_weekly_sla_stats', array('escape'=>false, 'title'=>"Weekly Chart")); ?></li>
	
</ul>


