<ul class="tabs">
	<li <?php if ($session->read('ActivePage') == 'addCoupon') echo "class='active'" ?>><?php echo $html->link($html->tag('span', 'Add Coupons'), '/coupons/add/', array('escape'=>false, 'title'=>"Add Coupon")); ?></li>
	<li <?php if ($session->read('ActivePage') == 'allCoupon') echo "class='active'" ?>><?php echo $html->link($html->tag('span', 'All Coupons'), '/coupons/index/', array('escape'=>false, 'title'=>"All Coupons")); ?></li>
	<li <?php if ($session->read('ActivePage') == 'activeCoupon') echo "class='active'" ?>><?php echo $html->link($html->tag('span', 'Active Coupons'), '/coupons/active/', array('escape'=>false, 'title'=>"Active Coupons")); ?></li>
	
</ul>


