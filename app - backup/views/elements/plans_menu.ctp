<ul class="tabs">
	
	<li><?php echo $html->link($html->tag('span', 'Add Block'), '/ngd_options/plans_display/plans_add_block', array('escape'=>false, 'title'=>"Block Display")); ?></li>
	<li><?php echo $html->link($html->tag('span', 'Upgrade'), '/ngd_options//plans_display/plans_upgrade', array('escape'=>false, 'title'=>"Upgrade Display")); ?></li>
	<li><?php echo $html->link($html->tag('span', 'Signup'), '/ngd_options//plans_display/signup', array('escape'=>false, 'title'=>"Signup Display")); ?></li>
	
	<?php if ($ngdAdminUser) : ?>						                    
	<li <?php if ($session->read('ActivePage') == 'addPlan') echo "class='active'" ?>><?php echo $html->link($html->tag('span', 'Add Plan'), '/plans/add/', array('escape'=>false, 'title'=>"Add Plan")); ?></li>
	<?php endif; ?>
	
	<li <?php if ($session->read('ActivePage') == 'activePlans') echo "class='active'" ?>><?php echo $html->link($html->tag('span', 'Active Plans'), '/plans/active/', array('escape'=>false, 'title'=>"Active Plans")); ?></li>
	
	
	
	<?php if (!$session->read('ActivePage') == 'planDisplay') : ?>
		<li <?php if ($session->read('ActivePage') == 'allPlans') echo "class='active'" ?>><?php echo $html->link($html->tag('span', 'All Plans'), '/plans/index/', array('escape'=>false, 'title'=>"All Plans")); ?></li>
	<?php endif;?>
	
</ul>


