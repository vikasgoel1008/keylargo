<?php if ($session->check('Member.id')) :?>
<ul class="tabs">
	<li <?php if ($session->read('ActivePage') == 'members') echo "class='active'" ?>><?php echo $html->link($html->tag('span', 'Account'), '/members/view/' . $session->read('Member.id'), array('escape'=>false, 'title'=>"Member")); ?></li>
	<li <?php if ($session->read('ActivePage') == 'highwinds') echo "class='active'" ?>><?php echo $html->link($html->tag('span', 'HW'), '/members/highwinds/' . $session->read('Member.id'), array('escape'=>false, 'title'=>"Highwinds")); ?></li>
	<li <?php if ($session->read('ActivePage') == 'transactions') echo "class='active'" ?>><?php echo $html->link($html->tag('span', 'Billing'), '/cc_transactions/index/' . $session->read('Member.id'), array('escape'=>false, 'title'=>"Transactions")); ?></li>
	<li <?php if ($session->read('ActivePage') == 'notes') echo "class='active'" ?>><?php echo $html->link($html->tag('span', 'Notes'), '/notes/index/' . $session->read('Member.id'), array('escape'=>false, 'title'=>"Notes")); ?></li>
	<li <?php if ($session->read('ActivePage') == 'credits') echo "class='active'" ?>><?php echo $html->link($html->tag('span', 'Credits'), '/ngd_credits/index/' . $session->read('Member.id'), array('escape'=>false, 'title'=>"Credits")); ?></li>
	<li <?php if ($session->read('ActivePage') == 'logs') echo "class='active'" ?>><?php echo $html->link($html->tag('span', 'Logs'), '/m_logs/index/' . $session->read('Member.id'), array('escape'=>false, 'title'=>"Logs")); ?></li>
</ul>
<?php endif; ?> 


