<ul class="tabs">
	<li <?php if ($session->read('ActivePage') == 'addDeal') echo "class='active'" ?>><?php echo $html->link($html->tag('span', 'Add Deals'), '/deals/add/', array('escape'=>false, 'title'=>"Add Deal")); ?></li>
	<li <?php if ($session->read('ActivePage') == 'allDeals') echo "class='active'" ?>><?php echo $html->link($html->tag('span', 'All Deals'), '/deals/', array('escape'=>false, 'title'=>"All Deals")); ?></li>
	<li <?php if ($session->read('ActivePage') == 'activeDeals') echo "class='active'" ?>><?php echo $html->link($html->tag('span', 'Active Deals'), '/deals/active', array('escape'=>false, 'title'=>"Active Deals")); ?></li>
	
</ul>


