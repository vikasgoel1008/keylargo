<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><?=$title_for_layout;?></title>
	<meta http-equiv="content-language" content="en" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon"/>
    <?=$html->css('styles');?>
	<!--[if lte IE 6]>
	<?=$html->css('styles_ie6');?>
	<![endif]-->
    <?=$javascript->link('jquery-1.2.6.min');?>
    <?=$javascript->link('ui');?>
</head>

<body>

<div id="wrap">
	<div id="header">
		<h1><a class="bg" href="#" title="Home"></a>Manager 1.0</h1>
		<p id="status">Logged is as <?php if (isset($systemUser)) { echo $systemUser['username']; } else echo "Guest"; ?>, <a href="/users/logout" title="Sign Out?">Sign Out?</a></p>
		<h2><span>Welcome to the NewsgroupDirect Dashboard</span></h2>
		<p id="description">Choose an option from the menu on the left.</p>
	</div>