<h2>Insert New SLA Record</h2>

<?php echo $this->element('sla_menu');?>

<?=$form->create('SlaReport');?>
<fieldset>
	
	<?=$form->input('data_date',array('type'=>'text','class'=>'small','label'=>'Record Date (yyyy-mm-dd)'));?><br class="hid" />
	<?=$form->input('goal',array('type'=>'text','class'=>'small','label'=>'Goal'));?><br class="hid" />
	<?=$form->input('failures',array('type'=>'text','class'=>'small','label'=>'Failed to Meet Goal'));?><br class="hid" />
	<?=$form->input('success',array('type'=>'text','class'=>'small','label'=>'Met Goal'));?><br class="hid" />
	
	<p></p>
	<?=$form->end(array('label'=>'Insert SLA Data','class'=>'button submit'));?>
	<span class="clear"></span>
</fieldset>