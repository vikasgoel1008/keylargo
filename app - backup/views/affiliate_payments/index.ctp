<div class="affiliates index">
	<h2>Affiliate Payments</h2>
	
	<?php echo $this->element('affiliates_menu');?>
	<?php $paginator->options(array('url' => $this->passedArgs));?>
	
	<div class="box">
		<p>&nbsp;</p>
		<table cellpadding="0" cellspacing="0">
			
			<tr class="highlight">	
				<td colspan="7"><?php  echo $paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)));?></td>
			</tr>
			
			<tr class="highlight">
				<td><?php echo $paginator->sort('pay_date');?></td>
				<td><?php echo $paginator->sort('affiliate_id');?></td>
				<td><?php echo $paginator->sort('current_earnings_amt');?></td>
				<td><?php echo $paginator->sort('pay_forward_amt');?></td>
				<td><?php echo $paginator->sort('check_amt');?></td>
				<td>Paid</td>
				<td>Actions</td>
				
			</tr>
			
			<?php foreach ($payments as $affiliate): ?>
			
			<tr class="highlight">
				<td><?php echo $affiliate['AffiliatePayment']['pay_date']; ?></td>
				<td><?php echo $html->link($affiliate['AffiliatePayment']['affiliate_id'], array('controller'=>'affiliates', 'action' => 'view', $affiliate['AffiliatePayment']['affiliate_id'])); ?></td>
				<td><?php echo $affiliate['AffiliatePayment']['current_earnings_amt']; ?></td>
				<td><?php echo $affiliate['AffiliatePayment']['pay_forward_amt']; ?></td>
				<td><?php echo $affiliate['AffiliatePayment']['check_amt']; ?></td>
				<td><?php if (!empty($affiliate['AffiliatePayment']['pay_type'])){ echo "Y"; } else { echo "N";} ?></td>
				<td><?php echo $html->link("View", array('action' => 'view', $affiliate['AffiliatePayment']['id'])); ?></td>
				
			</tr>
			
			<?php endforeach; ?>
		</table>

		<p id="pagin">
			<?php echo $paginator->first();?>
		 	<?php echo $paginator->prev('< previous', "", "", "");?>
		 	<?php //echo $paginator->prev() ?> 
			<?php echo $paginator->numbers();?>  
		 	<?php //echo $paginator->next() ?>
			<?php //echo $paginator->counter(); ?>
			<?php echo $paginator->next('next >', "", "", "");?>
			<?php echo $paginator->last();?>
		<p/>

		<span class="clear"></span>
	</div>
</div>

