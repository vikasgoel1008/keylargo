<div class="affiliates view">
<h2>Aff Pay Details</h2>
<?php echo $this->element('affiliates_menu');?>
	
<div class="box">
	<p>&nbsp;</p>

	<table>
		<tr class="highlight"><td>Affiliate:</td><td>
		<?php echo $html->link($ap['AffiliatePayment']['affiliate_id'] . ' | ' . $ap['Affiliate']['name'], array('controller' =>'affiliates', 'action' => 'view', $ap['AffiliatePayment']['affiliate_id'])); ?>
		
		</td></tr>
		<tr class="highlight"><td>Pay Date:</td><td><?php echo $ap['AffiliatePayment']['pay_date']; ?></td></tr>
		<tr class="highlight"><td>Pay Forward:</td><td><?php echo $ap['AffiliatePayment']['pay_forward_amt']; ?></td></tr>
		<tr class="highlight"><td>Current Earnings:</td><td><?php echo $ap['AffiliatePayment']['current_earnings_amt']; ?></td></tr>
		<tr class="highlight"><td>Check Amount:</td><td><?php echo $ap['AffiliatePayment']['check_amt']; ?></td></tr>
		<tr class="highlight"><td>Rewards:</td><td><?php echo $ap['AffiliatePayment']['aff_rewards']; ?></td></tr>

		<tr class="highlight"><td colspan="2">&nbsp;</td></tr>
		<tr class="highlight">
			<td>Pay Type:</td>
			<td>
				<?php if (!empty($ap['AffiliatePayment']['pay_type'])) : ?>
				<?php echo $ap['AffiliatePayment']['pay_type']; ?>
				<?php else : ?>
				<?php echo $html->link("Pay Me", array('action' => 'pay', $ap['AffiliatePayment']['id'])); ?>
				<?php endif;?>
			
			</td>
		</tr>
		<tr class="highlight"><td>Check #:</td><td><?php echo $ap['AffiliatePayment']['check_nbr']; ?></td></tr>
		<tr class="highlight"><td>Tracking #:</td><td><?php echo $ap['AffiliatePayment']['tracking_nbr']; ?></td></tr>
		
		
		
		
	</table>
	<p id="pagin">
			&nbsp;
		</p>
		
		<span class="clear">&nbsp;</span>

</div>	
</div>
	