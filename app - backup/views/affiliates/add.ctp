<div class="affiliate form">
	<h2>Add Affiliate</h2>

	<?php echo $this->element('affiliates_menu');?>

	<?=$form->create('Affiliate');?>
	<fieldset>
		<?=$form->input('affiliate_id',array('type'=>'text','class'=>'small','label'=>'Affiliate Id'));?><br class="hid" />
		<?=$form->input('name',array('type'=>'text','class'=>'small','label'=>'Name'));?><br class="hid" />
		<?=$form->input('email_address',array('type'=>'text','class'=>'large','label'=>'Email'));?><br class="hid" />
		<?=$form->input('login_password',array('type'=>'text','class'=>'large','label'=>'Password'));?><br class="hid" />
		<?=$form->input('url',array('type'=>'text','class'=>'large','label'=>'Website',));?><br class="hid" />
		<?=$form->input('address1',array('type'=>'text','class'=>'large','label'=>'Address1'));?><br class="hid" />
		<?=$form->input('address2',array('type'=>'text','class'=>'large','label'=>'Address2'));?><br class="hid" />
		<?=$form->input('city',array('type'=>'text','class'=>'tiny','label'=>'City'));?><br class="hid" />
		<?=$form->input('state',array('type'=>'text','class'=>'tiny','label'=>'State'));?><br class="hid" />
		<?=$form->input('postal',array('type'=>'text','class'=>'tiny','label'=>'Postal'));?><br class="hid" />
		<?=$form->input('country',array('type'=>'text','class'=>'tiny','label'=>'Country'));?><br class="hid" />
		<?=$form->input('status',array('type'=>'text','class'=>'tiny','label'=>'Status'));?><br class="hid" />
		<?=$form->input('reg_date',array('type'=>'text','class'=>'tiny','label'=>'Registration Date'));?><br class="hid" /><p>Format: YYYY-MM-DD</p>
		<?=$form->input('close_date',array('type'=>'text','class'=>'tiny','label'=>'Close Date'));?><br class="hid" /><p>Format: YYYY-MM-DD</p>
		<?=$form->input('commission_rate',array('type'=>'text','class'=>'tiny','label'=>'Commission Rate'));?><br class="hid" />
		<?=$form->input('max_sale_amount',array('type'=>'text','class'=>'tiny','label'=>'Max Sale Amount'));?><br class="hid" />
		<?=$form->input('tax_id',array('type'=>'text','class'=>'tiny','label'=>'Tax ID'));?><br class="hid" />
		<?=$form->input('paypal_address',array('type'=>'text','class'=>'tiny','label'=>'Paypal Address'));?><br class="hid" />
		<?=$form->input('comments',array('type'=>'textarea','class'=>'large','label'=>'Comments'));?><br class="hid" />

		
		<p></p>
		<?=$form->end(array('label'=>'Add Affiliate','class'=>'button submit'));?>
		<span class="clear"></span>
	</fieldset>
</div>
