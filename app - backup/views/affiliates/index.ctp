<div class="affiliates index">
	<h2>Affiliates</h2>
	
	<?php echo $this->element('affiliates_menu');?>
	<?php $paginator->options(array('url' => $this->passedArgs));?>
	
	<div class="box">
		<p>&nbsp;</p>
		<table cellpadding="0" cellspacing="0">
			<tr class="highlight">	
				<td colspan="6"><?php  echo $paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)));?></td>
			</tr>
			
			<tr class="highlight">
				<td><?php echo $paginator->sort('affiliate_id');?></td>
				<td><?php echo $paginator->sort('name');?></td>
				<td><?php echo $paginator->sort('email_address');?></td>
				<td><?php echo $paginator->sort('status');?></td>
				<td><?php echo $paginator->sort('url');?></td>
				<td><?php echo $paginator->sort('reg_date');?></td>
			</tr>
			
			<?php foreach ($affiliates as $affiliate): ?>
			
			<tr class="highlight">
				<td><?php echo $html->link($affiliate['Affiliate']['affiliate_id'], array('action' => 'view', $affiliate['Affiliate']['affiliate_id'])); ?></td>
				<td><?php echo $affiliate['Affiliate']['name']; ?></td>
				<td><?php echo $affiliate['Affiliate']['email_address']; ?></td>
				<td><?php echo $affiliate['Affiliate']['status']; ?></td>
				<td><?php echo $affiliate['Affiliate']['url']; ?></td>
				<td><?php echo $affiliate['Affiliate']['reg_date']; ?></td>
			</tr>
			
			<?php endforeach; ?>
		</table>

		<p id="pagin">
			<?php echo $paginator->first();?>
		 	<?php echo $paginator->prev('< previous', "", "", "");?>
		 	<?php //echo $paginator->prev() ?> 
			<?php echo $paginator->numbers();?>  
		 	<?php //echo $paginator->next() ?>
			<?php //echo $paginator->counter(); ?>
			<?php echo $paginator->next('next >', "", "", "");?>
			<?php echo $paginator->last();?>
		<p/>

		<span class="clear"></span>
	</div>
</div>

