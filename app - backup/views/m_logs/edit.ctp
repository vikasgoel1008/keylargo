<div class="memberLogs form">
<?php echo $form->create('MemberLog');?>
	<fieldset>
 		<legend><?php __('Edit MemberLog');?></legend>
	<?php
		echo $form->input('id');
		echo $form->input('member_id');
		echo $form->input('log_date_time');
		echo $form->input('log_type');
		echo $form->input('log_reason');
		echo $form->input('value_type');
		echo $form->input('old_value');
		echo $form->input('new_value');
		echo $form->input('requesting_ip');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Delete', true), array('action' => 'delete', $form->value('MemberLog.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $form->value('MemberLog.id'))); ?></li>
		<li><?php echo $html->link(__('List MemberLogs', true), array('action' => 'index'));?></li>
	</ul>
</div>
