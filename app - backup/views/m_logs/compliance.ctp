<div class="ccTransactions index">
	<h2>Customer Logs</h2>
	
	<?php echo $this->element('members_menu');?>
	<?php $paginator->options(array('url' => $this->passedArgs));?>
	
	<div class="box">
		<p>&nbsp;</p>
		<table cellpadding="0" cellspacing="0">
		
			<tr class="highlight"><td colspan="6"><?php echo $html->link('View Activity Logs', array('action' => 'index', $member['Member']['id']));?> | <?php echo $html->link('View Bandwidth Logs', array('action' => 'bandwidth', $member['Member']['id']));?></td></tr>
			<tr class="highlight"><td colspan="6"><?php echo $member['Member']['name']; ?> | <em><?php echo $member['Member']['email']; ?></em> | <?php echo $member['Member']['login_username']; ?></td></tr>
			
			<tr class="highlight">	
				<td colspan="6"><?php  echo $paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)));?></td>
			</tr>
			<tr>
				<td><?php echo $paginator->sort('log_date_time');?></td>
				<td><?php echo $paginator->sort('log_type');?></td>
				<td><?php echo $paginator->sort('log_reason');?></td>
				<td><?php echo $paginator->sort('value_type');?></td>
				<td><?php echo $paginator->sort('new_value');?></td>
				<td><?php echo $paginator->sort('requesting_ip');?></td>
			</tr>
			
			<?php foreach ($memberLogs as $memberLog): ?>
			
			<tr  class="highlight">
				<td><?php echo $html->link($memberLog['MLog']['log_date_time'], array('action' => 'view', $memberLog['MLog']['id']));?></td>
				<td><?php echo $memberLog['MLog']['log_type']; ?></td>
				<td><?php echo $memberLog['MLog']['log_reason']; ?></td>
				<td><?php echo $memberLog['MLog']['value_type']; ?></td>
				<td><?php echo $memberLog['MLog']['new_value']; ?></td>
				<td><?php echo $memberLog['MLog']['requesting_ip']; ?></td>
			</tr>
			
			<?php endforeach; ?>
		</table>

		<p id="pagin">
			<?php echo $paginator->first();?>
		 	<?php echo $paginator->prev('< previous', "", "", "");?>
		 	<?php //echo $paginator->prev() ?> 
			<?php echo $paginator->numbers();?>  
		 	<?php //echo $paginator->next() ?>
			<?php //echo $paginator->counter(); ?>
			<?php echo $paginator->next('next >', "", "", "");?>
			<?php echo $paginator->last();?>
		<p/>

		<span class="clear"></span>
	</div>
</div>




