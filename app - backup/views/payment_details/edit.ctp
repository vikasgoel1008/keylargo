<h2>Payment Details</h2>

<?php echo $this->element('members_menu');?>

<?=$form->create('PaymentDetail');?>
<fieldset>
	<?php echo $form->hidden('cc_id'); ?> 

	<?=$form->input('cc_card_type',array('type'=>'select',
										 'class'=>'small',
										 'label'=>'Card Type',
										 'options' => array('Visa' => 'Visa',
	             											'MasterCard'=>'MasterCard',
															'American Express' => 'AMEX',
															'Discover' => 'Discover' ),
										 'selected' => $this->data["PaymentDetail"]['cc_card_type']));?><br class="hid" />
															
	<?=$form->input('cc_last_4',array('type'=>'text','class'=>'small','label'=>'CC Last 4',));?><br class="hid" />
	<?=$form->input('cc_card_exp',array('type'=>'text','class'=>'tiny','label'=>'Expiration Date'));?><br class="hid" />
	<p></p>
	<?=$form->end(array('label'=>'Update','class'=>'button submit'));?>
	<span class="clear"></span>
</fieldset>