<div class="paymentDetails index">
<h2><?php __('PaymentDetails');?></h2>
<p>
<?php
echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>
<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('cc_id');?></th>
	<th><?php echo $paginator->sort('member_id');?></th>
	<th><?php echo $paginator->sort('cc_card_type');?></th>
	<th><?php echo $paginator->sort('cc_last_4');?></th>
	<th><?php echo $paginator->sort('cc_card_exp');?></th>
	<th><?php echo $paginator->sort('record_type');?></th>
	<th><?php echo $paginator->sort('cc_change_date');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
$i = 0;
foreach ($paymentDetails as $paymentDetail):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td>
			<?php echo $paymentDetail['PaymentDetail']['cc_id']; ?>
		</td>
		<td>
			<?php echo $paymentDetail['PaymentDetail']['member_id']; ?>
		</td>
		<td>
			<?php echo $paymentDetail['PaymentDetail']['cc_card_type']; ?>
		</td>
		<td>
			<?php echo $paymentDetail['PaymentDetail']['cc_last_4']; ?>
		</td>
		<td>
			<?php echo $paymentDetail['PaymentDetail']['cc_card_exp']; ?>
		</td>
		<td>
			<?php echo $paymentDetail['PaymentDetail']['record_type']; ?>
		</td>
		<td>
			<?php echo $paymentDetail['PaymentDetail']['cc_change_date']; ?>
		</td>
		<td class="actions">
			<?php echo $html->link(__('View', true), array('action' => 'view', $paymentDetail['PaymentDetail']['id'])); ?>
			<?php echo $html->link(__('Edit', true), array('action' => 'edit', $paymentDetail['PaymentDetail']['id'])); ?>
			<?php echo $html->link(__('Delete', true), array('action' => 'delete', $paymentDetail['PaymentDetail']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $paymentDetail['PaymentDetail']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>
<div class="paging">
	<?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class' => 'disabled'));?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('New PaymentDetail', true), array('action' => 'add')); ?></li>
	</ul>
</div>
