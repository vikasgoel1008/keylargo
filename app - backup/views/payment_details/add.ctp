<div class="paymentDetails form">
<?php echo $form->create('PaymentDetail');?>
	<fieldset>
 		<legend><?php __('Add PaymentDetail');?></legend>
	<?php
		echo $form->input('cc_id');
		echo $form->input('member_id');
		echo $form->input('cc_card_type');
		echo $form->input('cc_last_4');
		echo $form->input('cc_card_exp');
		echo $form->input('record_type');
		echo $form->input('cc_change_date');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('List PaymentDetails', true), array('action' => 'index'));?></li>
	</ul>
</div>
