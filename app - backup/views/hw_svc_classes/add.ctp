<div class="coupons form">
	<h2>Add New Service Rate</h2>

	<?php // echo $this->element('coupons_menu');?>

	<?=$form->create('HwSvcClass');?>
	<fieldset>
		<?=$form->input('service_class_id',array('options'=>$svc_classes, 'type'=>'select','class'=>'small','label'=>'Select Service Class','value'=>$service_class_id));?><br class="hid" />
		<?=$form->input('start_time',array('type'=>'text','class'=>'tiny','label'=>'Service Start Time'));?><br class="hid" /><p>Format: HH:MM (24Hr)</p>
		<?=$form->input('end_time',array('type'=>'text','class'=>'tiny','label'=>'Service Stop Time'));?><br class="hid" /><p>Format: HH:MM (24Hr)</p>	
		<?=$form->input('ylw_threshold',array('type'=>'text','class'=>'tiny','label'=>'Yellow Zone GB'));?><br class="hid" />
		<?=$form->input('ylw_rate_min',array('type'=>'text','class'=>'tiny','label'=>'Yellow Zone Min Rate'));?><br class="hid" />
		<?=$form->input('ylw_rate_max',array('type'=>'text','class'=>'tiny','label'=>'Yellow Zone Max Rate'));?><br class="hid" />
		<?=$form->input('red_threshold',array('type'=>'text','class'=>'tiny','label'=>'Red Zone GB'));?><br class="hid" />
		<?=$form->input('red_rate_min',array('type'=>'text','class'=>'tiny','label'=>'Red Zone Min Rate'));?><br class="hid" />
		<?=$form->input('red_rate_max',array('type'=>'text','class'=>'tiny','label'=>'Red Zone Max Rate'));?><br class="hid" />
		
		<p></p>
		<?=$form->end(array('label'=>'Submit Service Rate Hour','class'=>'button submit'));?>
		<span class="clear"></span>
	</fieldset>
</div>
