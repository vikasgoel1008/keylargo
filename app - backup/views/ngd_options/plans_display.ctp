<div class="ngd options index">

	<?php if ($key_name == 'plans_add_block') : ?>
	<h2>Add Block Display</h2>
	<?php elseif ($key_name == 'plans_upgrade') : ?>
	<h2>Upgrade Display</h2>
	<?php elseif ($key_name == 'signup') : ?>
	<h2>Signup Display</h2>
	<?php endif;?>
		
	<?php echo $this->element('plans_menu');?>
	<?php $paginator->options(array('url' => $this->passedArgs));?>
	
	<div class="box">
		<p>&nbsp;</p>
		<table cellpadding="0" cellspacing="0">
		
			<?php if ($key_name == 'signup') : ?>
			
			<tr class="highlight">	
				<td colspan="5">
					<?php if (!isset($this->params['pass'][1])) : ?>
						Monthly Plans | <?php echo $html->link('Block Plans', array('action' => 'plans_display', 'signup', 'blocks')); ?>
					<?php elseif ($this->params['pass'][1] == 'monthly') : ?>
						Monthly Plans | <?php echo $html->link('Block Plans', array('action' => 'plans_display', 'signup', 'blocks')); ?>
					<?php elseif ($this->params['pass'][1] == 'blocks') : ?>
						<?php echo $html->link('Monthly Plans', array('action' => 'plans_display', 'signup', 'monthly')); ?> | Block Plans
					<?php endif; ?>
					
				</td>
			</tr>
			
			<?php endif; ?>
		
			<tr class="highlight">
				<td><?php echo $paginator->sort('id');?></td>
				<td><?php echo $paginator->sort('key_name');?></td>
				<td><?php echo $paginator->sort('subkey');?></td>
				<td><?php echo $paginator->sort('value');?></td>
			</tr>
			
			<?php foreach ($ngdOptions as $ngdOption): ?>
			
			<tr class="highlight">
				<td><?php echo $html->link('Modify Option', array('action' => 'edit_plan_option', $ngdOption['NgdOption']['id'])); ?></td>
				<td><?php echo $ngdOption['NgdOption']['key_name']; ?></td>
				<td><?php echo $ngdOption['NgdOption']['subkey']; ?></td>
				<td>
					<?php echo "({$ngdOption['NgdOption']['value']}) " . $plans[$ngdOption['NgdOption']['value']]; ?>

				</td>
			</tr>
			
			<?php endforeach; ?>
			<tr class="highlight">	
				<td colspan="5"><?php  echo $paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)));?></td>
			</tr>
			
			
		</table>

		<p id="pagin">
			<?php echo $paginator->first();?>
		 	<?php echo $paginator->prev('< previous', "", "", "");?>
		 	<?php //echo $paginator->prev() ?> 
			<?php echo $paginator->numbers();?>  
		 	<?php //echo $paginator->next() ?>
			<?php //echo $paginator->counter(); ?>
			<?php echo $paginator->next('next >', "", "", "");?>
			<?php echo $paginator->last();?>
		<p/>

		<span class="clear"></span>
	</div>
</div>

