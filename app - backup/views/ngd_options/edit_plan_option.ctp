<div class="options form">
	<h2>Edit Plan Displays Options</h2>

	<?php echo $this->element('options_menu');?>

	<?=$form->create('NgdOption', array('action'=>'edit_plan_option'));?>
	<fieldset>
		<?=$form->input('id',array('type'=>'hidden'));?><br class="hid" />
		<?=$form->input('key_name',array('type'=>'text','class'=>'medium','label'=>'Key Name', 'readonly'=>'readonly'));?><br class="hid" />
		<?=$form->input('subkey',array('type'=>'text','class'=>'medium','label'=>'Display Position', 'readonly'=>'readonly'));?><br class="hid" />	
		<?=$form->input('plan_id',array('type'=>'text','class'=>'medium','label'=>'Plan Id', 'value'=>$this->data['NgdOption']['value']));?><br class="hid" />	
		<p></p>
		<?=$form->end(array('label'=>'Edit','class'=>'button submit'));?>
		<span class="clear"></span>
	</fieldset>
</div>
