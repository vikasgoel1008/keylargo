<div class="ngdOptions form">
	<h2>Add New Option</h2>

	<?php echo $this->element('options_menu');?>

	<?=$form->create('NgdOption');?>
	<fieldset>
		<?=$form->input('name',array('type'=>'text','class'=>'medium','label'=>'Option Name'));?><br class="hid" />
		<?=$form->input('key_name',array('type'=>'text','class'=>'medium','label'=>'Key Name'));?><br class="hid" />
		<?=$form->input('subkey',array('type'=>'text','class'=>'medium','label'=>'Sub Key'));?><br class="hid" />	
		<?=$form->input('value',array('type'=>'text','class'=>'medium','label'=>'Value'));?><br class="hid" />
		<p></p>
		<?=$form->end(array('label'=>'Submit Option','class'=>'button submit'));?>
		<span class="clear"></span>
	</fieldset>
</div>
