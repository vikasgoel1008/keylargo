<div class="ngd options index">
	<h2>Options</h2>
	
	<?php echo $this->element('options_menu');?>
	<?php $paginator->options(array('url' => $this->passedArgs));?>
	
	<div class="box">
		<p>&nbsp;</p>
		<table cellpadding="0" cellspacing="0">
			<tr class="highlight">	
				<td colspan="5"><?php  echo $paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)));?></td>
			</tr>
			
			<tr class="highlight">
				<td><?php echo $paginator->sort('id');?></td>
				<td><?php echo $paginator->sort('name');?></td>
				<td><?php echo $paginator->sort('key_name');?></td>
				<td><?php echo $paginator->sort('subkey');?></td>
			</tr>
			
			<?php foreach ($ngdOptions as $ngdOption): ?>
			
			<tr class="highlight">
				<td><?php echo $html->link('View Option', array('action' => 'view', $ngdOption['NgdOption']['id'])); ?></td>
				<td><?php echo $ngdOption['NgdOption']['name']; ?></td>
				<td><?php echo $ngdOption['NgdOption']['key_name']; ?></td>
				<td><?php echo $ngdOption['NgdOption']['subkey']; ?></td>
			</tr>
			
			<?php endforeach; ?>
		</table>

		<p id="pagin">
			<?php echo $paginator->first();?>
		 	<?php echo $paginator->prev('< previous', "", "", "");?>
		 	<?php //echo $paginator->prev() ?> 
			<?php echo $paginator->numbers();?>  
		 	<?php //echo $paginator->next() ?>
			<?php //echo $paginator->counter(); ?>
			<?php echo $paginator->next('next >', "", "", "");?>
			<?php echo $paginator->last();?>
		<p/>

		<span class="clear"></span>
	</div>
</div>

