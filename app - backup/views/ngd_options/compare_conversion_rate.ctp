<div class="options form">
	<h2>Compare Conversion Rate</h2>

	<?php echo $this->element('options_menu');?>

	<?=$form->create('NgdOption', array('action' => 'compare_conversion_rate'));?>
	<fieldset>
		<?=$form->input('id',array('type'=>'hidden','class'=>'medium','label'=>'Option Name'));?><br class="hid" />
		<?=$form->input('subkey',array('type'=>'text','class'=>'medium','label'=>'Sub Key', 'readonly' => 'readonly'));?><br class="hid" />	
		<?=$form->input('value',array('type'=>'text','class'=>'medium','label'=>'Old Rate', 'readonly' => 'readonly'));?><br class="hid" />
		<?=$form->input('newRate',array('type'=>'text','class'=>'medium','label'=>'New Rate'));?><br class="hid" />
		<p></p>
		<?=$form->end(array('label'=>'Compare','class'=>'button submit'));?>
		<span class="clear"></span>
	</fieldset>
	
	<?php if (!empty($plans)) : ?>
	<div class="box">
		<p>&nbsp;</p>
		<table cellpadding="0" cellspacing="0">
						
			<tr class="highlight">
				<td>Plan</td>
				<td>Current Price</td>
				<td>New Price</td>
				<td>Difference</td>
			</tr>
			
			<?php foreach ($plans as $plan): ?>
			<?php 
				$old = round($plan['Plan']['curr_1_price'] * $this->data['NgdOption']['value'],2);
				$new = $old;
				
				if (isset($newRate) && $newRate > 0) {
					$new = round($plan['Plan']['curr_1_price'] * $newRate,2);
				}
			?>
			
			<tr class="highlight">
				<td><?php echo $html->link($plan['Plan']['name'], array('controller'=>'plans', 'action' => 'view', $plan['Plan']['id'])); ?></td>
				<td><?php echo $old; ?></td>
				<td><?php echo $new; ?></td>
				<td><?php echo $new-$old; ?></td>
			</tr>
			
			<?php endforeach; ?>
		</table>
	</div>
	<?php endif; ?>
</div>
