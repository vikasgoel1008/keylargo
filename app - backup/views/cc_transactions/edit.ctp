<div class="ccTransactions form">
<?php echo $form->create('CcTransaction');?>
	<fieldset>
 		<legend><?php __('Edit CcTransaction');?></legend>
	<?php
		echo $form->input('id');
		echo $form->input('member_id');
		echo $form->input('ip');
		echo $form->input('amount');
		echo $form->input('cc_last_4');
		echo $form->input('expire');
		echo $form->input('order_nbr');
		echo $form->input('approval_code');
		echo $form->input('ref_code');
		echo $form->input('approved');
		echo $form->input('error');
		echo $form->input('full_error');
		echo $form->input('message');
		echo $form->input('avs');
		echo $form->input('trans_date');
		echo $form->input('trans_type');
		echo $form->input('trans_purpose');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Delete', true), array('action' => 'delete', $form->value('CcTransaction.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $form->value('CcTransaction.id'))); ?></li>
		<li><?php echo $html->link(__('List CcTransactions', true), array('action' => 'index'));?></li>
		<li><?php echo $html->link(__('List Members', true), array('controller' => 'members', 'action' => 'index')); ?> </li>
		<li><?php echo $html->link(__('New Member', true), array('controller' => 'members', 'action' => 'add')); ?> </li>
	</ul>
</div>
