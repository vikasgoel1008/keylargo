<div class="ccTransactions search">
	<h2>Transaction Search Results</h2>
	
	<?php //echo $this->element('members_menu');?>
	<?php $paginator->options(array('url' => $this->passedArgs));?>
	
	<div class="box">
		<p>&nbsp;</p>
		<table cellpadding="0" cellspacing="0">
			<tr class="highlight">	
				<td colspan="5"><?php  echo $paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)));?></td>
			</tr>
			
			<tr>
				<td><?php echo $paginator->sort('order_nbr');?></td>
				<td><?php echo $paginator->sort('cc_last_4');?></td>
				<td><?php echo $paginator->sort('ref_code');?></td>
				<td><?php echo $paginator->sort('trans_date');?></td>
				<td><?php echo $paginator->sort('amount');?></td>
			</tr>
			
			<?php foreach ($ccTransactions as $ccTransaction): ?>
			
			<tr  class="highlight">
				<td><?php echo $html->link($ccTransaction['CcTransaction']['order_nbr'], array('action' => 'view', $ccTransaction['CcTransaction']['id'])); ?></td>
				<td><?php echo $ccTransaction['CcTransaction']['cc_last_4']; ?></td>
				<td><?php echo $ccTransaction['CcTransaction']['ref_code']; ?></td>
				<td><?php echo $ccTransaction['CcTransaction']['trans_date']; ?></td>
				<td><?php echo $ccTransaction['CcTransaction']['amount']; ?></td>
			</tr>
			
			<?php endforeach; ?>
		</table>

		<p id="pagin">
			<?php echo $paginator->first();?>
		 	<?php echo $paginator->prev('< previous', "", "", "");?>
		 	<?php //echo $paginator->prev() ?> 
			<?php echo $paginator->numbers();?>  
		 	<?php //echo $paginator->next() ?>
			<?php //echo $paginator->counter(); ?>
			<?php echo $paginator->next('next >', "", "", "");?>
			<?php echo $paginator->last();?>
		<p/>

		<span class="clear"></span>
	</div>
</div>

