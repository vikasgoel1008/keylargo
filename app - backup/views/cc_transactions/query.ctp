<p id="tip" class="alert">
	<span class="txt"><span class="icon"></span><strong>Tip:</strong> Enter full or partial order number, reference number, or the last 4 of cc, etc.</span>
	<a title="Close" class="close"><span class="bg"></span>Close</a>
</p>

<h2>Transaction Search</h2>

<?=$form->create('CcTransaction',array('url'=>'/cc_transactions/search'));?>
<fieldset>
	<?=$form->input('searchterm',array('type'=>'text','class'=>'medium','label'=>'Search Text'));?>
	<p>Any text can be used to search</p>
	<?=$form->end(array('label'=>'Submit','class'=>'button submit'));?>
	<p></p>
	<span class="clear"></span>
</fieldset>
		
		