<h2>Add Customer Transaction</h2>

<?php echo $this->element('members_menu');?>

<?=$form->create('CcTransaction');?>
<fieldset>
	
	<?=$form->input('member_id',array('type'=>'text','class'=>'small','label'=>'Member Id', 'value' => $session->read('Member.id')));?><br class="hid" />
	<?=$form->input('ip',array('type'=>'text','class'=>'small','label'=>'IP Address'));?><br class="hid" />
	<?=$form->input('amount',array('type'=>'text','class'=>'tiny','label'=>'Amount'));?><br class="hid" />
	<?=$form->input('cc_last_4',array('type'=>'text','class'=>'tiny','label'=>'Last 4 CC'));?><br class="hid" />
	<?=$form->input('expire',array('type'=>'text','class'=>'tiny','label'=>'Expiration Date (YYYYMM)'));?><br class="hid" />
	<?=$form->input('order_nbr',array('type'=>'text','class'=>'large','label'=>'Order'));?><br class="hid" />
	<?=$form->input('approval_code',array('type'=>'text','class'=>'large','label'=>'Approval Code'));?><br class="hid" />
	<?=$form->input('ref_code',array('type'=>'text','class'=>'large','label'=>'Reference Code'));?><br class="hid" />
	<?=$form->input('approved',array('type'=>'text','class'=>'large','label'=>'Approved Status'));?><br class="hid" />
	<?=$form->input('error',array('type'=>'text','class'=>'large','label'=>'Error'));?><br class="hid" />
	<?=$form->input('full_error',array('type'=>'text','class'=>'large','label'=>'Full Error Message'));?><br class="hid" />
	<?=$form->input('message',array('type'=>'text','class'=>'large','label'=>'Message'));?><br class="hid" />
	<?=$form->input('avs',array('type'=>'text','class'=>'large','label'=>'Avs'));?><br class="hid" />
	<?=$form->input('trans_date',array('type'=>'text','class'=>'small','label'=>'Transaction Date'));?><br class="hid" />
	<?=$form->input('trans_type',array('type'=>'text','class'=>'small','label'=>'Transaction Type'));?><br class="hid" />
	<?=$form->input('trans_purpose',array('type'=>'text','class'=>'small','label'=>'Reason for Transaction'));?><br class="hid" />
	
	<p></p>
	<?=$form->end(array('label'=>'Add Transaction','class'=>'button submit'));?>
	<span class="clear"></span>
</fieldset>