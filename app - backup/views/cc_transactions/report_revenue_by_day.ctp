<div class="ccTransactions index">
	<h2>Revenue for Last <?php echo $dayslength;?> Days</h2>
	<?php echo $this->element('members_menu');?>

	<div class="box">
		
		<table cellpadding="0" cellspacing="0">
		<tr  class="highlight">
			<td colspan="5"><?php echo $html->link(__('See Revenue Graph for Last 30 Days', true), array('action' => 'report_cancel_graph')); ?></td>
			
		</tr>
		<tr class="highlight">	
			<td>Revenue Date</td>
			<td>Charges</td>
			
		</tr>
		<?php $income = 0;?>
		<?php foreach ($revenues as $revenue): ?>
			<tr  class="highlight">
				
				<td>
					<?php echo $revenue['0']['revenue_date']; ?>
				</td>
				<td>
					<?php echo $revenue['0']['charges']; $income+=$revenue['0']['charges']; ?>
				</td>
				
			</tr>
		<?php endforeach; ?>
		<tr  class="highlight">
			<td>Total Revenue for Period</td>
			<td><strong><?php echo $income; ?></strong></td>
		</tr>
	
		</table>
		
		<span class="clear"></span>
		
	</div>
</div>
	
