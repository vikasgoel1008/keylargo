<div class="contestants index">
<h2><?php __('Contestants');?></h2>
<p>
<?php
echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>
<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('id');?></th>
	<th><?php echo $paginator->sort('contest_id');?></th>
	<th><?php echo $paginator->sort('name');?></th>
	<th><?php echo $paginator->sort('email');?></th>
	<th><?php echo $paginator->sort('twitter');?></th>
	<th><?php echo $paginator->sort('shirt_size');?></th>
	<th><?php echo $paginator->sort('ip');?></th>
	<th><?php echo $paginator->sort('date_submitted');?></th>
	<th><?php echo $paginator->sort('reg_number');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
$i = 0;
foreach ($contestants as $contestant):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td>
			<?php echo $contestant['Contestant']['id']; ?>
		</td>
		<td>
			<?php echo $contestant['Contestant']['contest_id']; ?>
		</td>
		<td>
			<?php echo $contestant['Contestant']['name']; ?>
		</td>
		<td>
			<?php echo $contestant['Contestant']['email']; ?>
		</td>
		<td>
			<?php echo $contestant['Contestant']['twitter']; ?>
		</td>
		<td>
			<?php echo $contestant['Contestant']['shirt_size']; ?>
		</td>
		<td>
			<?php echo $contestant['Contestant']['ip']; ?>
		</td>
		<td>
			<?php echo $contestant['Contestant']['date_submitted']; ?>
		</td>
		<td>
			<?php echo $contestant['Contestant']['reg_number']; ?>
		</td>
		<td class="actions">
			<?php echo $html->link(__('View', true), array('action' => 'view', $contestant['Contestant']['id'])); ?>
			<?php echo $html->link(__('Edit', true), array('action' => 'edit', $contestant['Contestant']['id'])); ?>
			<?php echo $html->link(__('Delete', true), array('action' => 'delete', $contestant['Contestant']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $contestant['Contestant']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>
<div class="paging">
	<?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class' => 'disabled'));?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('New Contestant', true), array('action' => 'add')); ?></li>
	</ul>
</div>
