<div class="contestants form">
<?php echo $form->create('Contestant');?>
	<fieldset>
 		<legend><?php __('Add Contestant');?></legend>
	<?php
		echo $form->input('contest_id');
		echo $form->input('name');
		echo $form->input('email');
		echo $form->input('twitter');
		echo $form->input('shirt_size');
		echo $form->input('ip');
		echo $form->input('date_submitted');
		echo $form->input('reg_number');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('List Contestants', true), array('action' => 'index'));?></li>
	</ul>
</div>
