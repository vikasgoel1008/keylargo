<?php 

$session->flash('auth');

?>
	
<p id="tip" class="alert">
	<span class="txt"><span class="icon"></span><strong>Tip:</strong> Enter full or partial email addresses, first name, last name, etc.</span>
	<a title="Close" class="close"><span class="bg"></span>Close</a>
</p>
<h2>Login to Account Manager</h2>
<ul class="tabs">
	<li class="active"><a href="#" title="Item1"><span>Login</span></a></li>
</ul>
<?=$form->create('User', array('action' => 'login'));?>
<fieldset>
	<?=$form->input('username',array('type'=>'text','class'=>'medium','label'=>'Username'));?>
	<?=$form->input('password',array('type'=>'password','class'=>'medium','label'=>'Password'));?>
	<p>&nbsp;</p>
	<?=$form->end(array('label'=>'Login','class'=>'button submit'));?>
	<p></p>
	<span class="clear"></span>
	
</fieldset>
