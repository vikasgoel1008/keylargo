<div class="coupons form">
	<h2>New Credit</h2>

	<?=$form->create('NgdCredit');?>
	<fieldset>
		<? if (isset($member)) : ?>
		<?=$form->input('member_id',array('class'=>'small','label'=>'Member Id', 'readonly' => 'readonly', 'value' => $member['Member']['id']));?><br class="hid" />
		<? else : ?>
		<?=$form->input('member_id',array('class'=>'small','label'=>'Member Id'));?><br class="hid" />
		<? endif; ?>
		<?=$form->input('orig_credit_amount',array('type'=>'text','class'=>'tiny','label'=>'Credit Amount'));?><br class="hid" />
		<?=$form->input('credit_reason',array('type'=>'text','class'=>'small','label'=>'Reason for Credit'));?><br class="hid" />	
		<?=$form->input('for_tweet',array('options'=>array("N"=>"No","Y"=>"Yes"), 'type'=>'select','class'=>'tiny','label'=>'Credit for Tweet'));?><br class="hid" />
		<p></p>
		<?=$form->end(array('label'=>'Submit Credit','class'=>'button submit'));?>
		<span class="clear"></span>
	</fieldset>
</div>
