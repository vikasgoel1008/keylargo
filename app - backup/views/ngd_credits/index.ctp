<div class="ccTransactions index">
	<h2>Customer Credits</h2>
	
	<?php echo $this->element('members_menu');?>
	<?php $paginator->options(array('url' => $this->passedArgs));?>
	
	<div class="box">
		<p>&nbsp;</p>
		<table cellpadding="0" cellspacing="0">
		
			<tr class="highlight"><td colspan="6"><?php echo $html->link('Add Credit', array('action' => 'add', $member['Member']['id']));?></td></tr>
			<tr class="highlight"><td colspan="6"><?php echo $member['Member']['name']; ?> | <em><?php echo $member['Member']['email']; ?></em> | <?php echo $member['Member']['login_username']; ?></td></tr>
			
			<tr class="highlight">	
				<td colspan="6"><?php  echo $paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)));?></td>
			</tr>
			<tr>
				<td><?php echo $paginator->sort('date_added');?></td>
				<td><?php echo $paginator->sort('orig_credit_amount');?></td>
				<td><?php echo $paginator->sort('credit_amount');?></td>
				<td><?php echo $paginator->sort('credit_used');?></td>
				<td><?php echo $paginator->sort('forfeited');?></td>
				<td><?php echo $paginator->sort('credit_reason');?></td>
			</tr>
			
			<?php foreach ($credits as $credit): ?>
			
			<tr  class="highlight">
				<td><?php echo $html->link($credit['NgdCredit']['date_added'], array('action' => 'view', $credit['NgdCredit']['id']));?></td>
				<td><?php echo $credit['NgdCredit']['orig_credit_amount']; ?></td>
				<td><?php echo $credit['NgdCredit']['credit_amount']; ?></td>
				<td><?php echo $credit['NgdCredit']['credit_used']; ?></td>
				<td>
					<?php if ($credit['NgdCredit']['forfeited'] == 'Y'): ?>
						<?php echo $credit['NgdCredit']['forfeited']; ?>
					<?php elseif ($credit['NgdCredit']['credit_used'] == 'Y'): ?>
						<?php echo $credit['NgdCredit']['forfeited']; ?>
					<?php else : ?>				
						<?php echo $html->link('Forfeit?', array('action' =>'forfeit_credit',$credit['NgdCredit']['id']), null, sprintf(__('Are you sure you want to forfeit this credit %s?', true), $credit['NgdCredit']['id'])); ?>
					<?php endif; ?>
					
				</td>
				<td><?php echo $credit['NgdCredit']['credit_reason']; ?></td>
			</tr>
			
			<?php endforeach; ?>
		</table>

		<p id="pagin">
			<?php echo $paginator->first();?>
		 	<?php echo $paginator->prev('< previous', "", "", "");?>
		 	<?php //echo $paginator->prev() ?> 
			<?php echo $paginator->numbers();?>  
		 	<?php //echo $paginator->next() ?>
			<?php //echo $paginator->counter(); ?>
			<?php echo $paginator->next('next >', "", "", "");?>
			<?php echo $paginator->last();?>
		<p/>

		<span class="clear"></span>
	</div>
</div>




