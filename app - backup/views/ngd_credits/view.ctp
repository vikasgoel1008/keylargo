<div class="ccTransactions index">
	<h2>Log Details</h2>

	<?php echo $this->element('members_menu');?>

	<div class="box">
		<p>&nbsp;</p>
		<table cellpadding="0" cellspacing="0">
			<tr class="highlight"><td colspan="6"><?php echo $member['Member']['name']; ?> | <em><?php echo $member['Member']['email']; ?></em> | <?php echo $member['Member']['login_username']; ?></td></tr>
			
			<tr class="highlight">
				<td>Orig. Credit Amount:</td>	
				<td><?php echo $credit['NgdCredit']['orig_credit_amount']; ?></td>			
				<td></td>
			</tr>
			
			
			<tr class="highlight">
				<td>Credit Amount:</td>	
				<td><?php echo $credit['NgdCredit']['credit_amount']; ?></td>			
				<td></td>
			</tr>
			
			
			<tr class="highlight">
				<td>Credit Added:</td>	
				<td><?php echo $credit['NgdCredit']['date_added']; ?></td>			
				<td></td>
			</tr>
			
			
			<tr class="highlight">
				<td>Credit Used:</td>	
				<td><?php echo $credit['NgdCredit']['credit_used']; ?></td>			
				<td></td>
			</tr>
			
			<tr class="highlight">
				<td>Forfeited Credit:</td>	
				<td><?php echo $credit['NgdCredit']['forfeited']; ?></td>			
				<td></td>
			</tr>
			
			<tr class="highlight">
				<td>Date Used:</td>	
				<td><?php echo $credit['NgdCredit']['date_used']; ?></td>			
				<td></td>
			</tr>
			
			<tr class="highlight">
				<td>Credit Reason:</td>	
				<td><?php echo $credit['NgdCredit']['credit_reason']; ?></td>			
				<td></td>
			</tr>
			
			<tr class="highlight">
				<td>Associated Transaction:</td>	
				<td><?php echo $credit['NgdCredit']['cc_transaction_id']; ?></td>			
				<td></td>
			</tr>
			
			
			
		
		</table>
		
		<p id="pagin">
			&nbsp;
		</p>
		
		<span class="clear">&nbsp;</span>
	</div>
</div>
	
