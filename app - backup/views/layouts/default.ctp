<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><?=$title_for_layout;?></title>
	<meta http-equiv="content-language" content="en" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon"/>
    <?=$html->css('styles');?>
	<!--[if lte IE 6]>
	<?=$html->css('styles_ie6');?>
	<![endif]-->
    <?=$javascript->link('jquery-1.2.6.min');?>
    <?=$javascript->link('ui');?>
    <?=$javascript->link('swfobject'); ?>
    
</head>

<body>

<div id="wrap">
	<div id="header">
		<h1><a class="bg" href="#" title="Home"></a>Manager 1.0</h1>
		<p id="status">Logged is as <?php if (isset($systemUser)) { echo $systemUser['username']; } else echo "Guest"; ?>, <a href="/users/logout" title="Sign Out?">Sign Out?</a></p>
		<h2><span>Welcome to the NewsgroupDirect Dashboard</span></h2>
		<p id="description">Choose an option from the menu on the left.</p>
	</div>	
	
	<div id="sidebar">
		
		<ul class="items">
			<?php if (isset($systemUser)) : ?>	
    
            <li class="active">
            	<a href="#" title="Dashboard">Dashboard</a>
				<ul>
                    <li><a href="/members/search" title="Search Customers">Search Customers</a></li>
                    <li><a href="/cc_transactions/query" title="Search Transactions">Search Transactions</a></li>
            		
            		<?php if ($ngdAdminUser) : ?>						
                    <li><a href="" title="-">------</a></li>
					<li><?php echo $html->link('Sales Dashboard', array('controller' => 'cc_transactions', 'action' => 'report_current_day_revenue'));?></li>
					<li><?php echo $html->link('Risk Management', array('controller' => 'risks', 'action' => 'index'));?></li>
					<li><?php echo $html->link(__('Bandwidth Consumption', true), array('controller' => 'data_reports', 'action' => 'report_daily_bandwidth_usage',180)); ?></li>
					<li><?php echo $html->link(__('60 Day Sales Graph', true), array('controller' => 'cc_transactions', 'action' => 'report_graph_daily_sales',60)); ?></li>
					<?php endif; ?>
				</ul>
            </li>
            
			<li class="subitems">
				<a href="#" title="Support">Support</a>
				<ul>
					<li><a href="/coupons/active" title="Active Coupons">Active Coupons</a></li>
					<li><a href="/coupons/" title="All Coupons">All Coupons</a></li>
					<li><a href="/members/report_bad_address" title="Bad Address">Bad Address Cleanup</a></li>
            		<li><a href="/members/report_invalid_next_bill" title="Bad Bill Dates">Bad Next Bill Date</a></li>
                    
				</ul>
			</li>
			
            <li class="subitems">
				<a href="#" title="Customers">Customers</a>
				<ul>
					<li><a href="/members/search" title="Search Customers">Search Customers</a></li>
					<li><a href="/members/create_account" title="Add Customer">Add Customer</a></li>
				</ul>
			</li>
			
			<?php if ($ngdAdminUser) : ?>			
			<li class="subitems">
				<a href="#" title="Affiliates">Affiliates</a>
				<ul>
					<li><a href="/affiliates/" title="View Affiliate">View Affiliates</a></li>
					<li><a href="/affiliates/add" title="Add Affiliate">Add Affiliate</a></li>
					<li><a href="/affiliate_payments/" title="Add Affiliate">Affiliate Payments</a></li>
					<li><a href="/affiliates/performance_report" title="Performance Reports">Performance Reports</a></li>
					
				</ul>
			</li>			
			<?php endif; ?>
			
			<?php if ($ngdAdminUser) : ?>
			<li class="subitems">
				<a href="#" title="Reports">Sales Reports</a>
				<ul>
					<li><?php echo $html->link(__('Rebill Forecast', true), array('controller' => 'members', 'action' => 'report_forecasted_rebills')); ?></li>
					<li><a href="/cc_transactions/bank_reconciliation" title="Recent Signups">Reconciliation Report</a></li>
					<li><a href="/members/report_free_trial_conversion_rate" title="Free Trial Conversion">Free Trial Conversion Rate</a></li>
					<li><a href="" title="-">------</a></li>
					<li><a href="/cc_transactions/report_graph_30_day_sales" title="30 Day Sales">30 Day Sales Graph</a></li>
					<li><a href="/cc_transactions/report_revenue_by_day" title="Daily Revenue Report">Daily Revenue Report</a></li>
					<li><a href="/cc_transactions/report_graph_monthly_sales" title="Monthly Revenue Chart">Monthly Revenue Chart</a></li>			
				</ul>
			</li>
			<?php endif; ?>
					
			
			<li class="subitems">
				<a href="#" title="Reports">Reports</a>
				<ul>
					<li><a href="/sla_reports/report_daily_sla_stats" title="Daily SLA Report">Daily SLA Report</a></li>
					<li><a href="/sla_reports/report_weekly_sla_stats" title="Weekly SLA Report">Weekly SLA Report</a></li>
					
					<?php if ($ngdAdminUser) : ?>
					<li><a href="" title="-">------</a></li>
					<li><?php echo $html->link(__('Avg Bandwidth', true), array('controller' => 'data_reports', 'action' => 'report_average_bandwidth')); ?></li>
					<li><?php echo $html->link(__('Avg Billings', true), array('controller' => 'ngd_reports', 'action' => 'billings')); ?></li>
					<li><?php echo $html->link(__('Bandwidth Consumption', true), array('controller' => 'data_reports', 'action' => 'report_daily_bandwidth_usage',180)); ?></li>
					<li><?php echo $html->link(__('Bandwidth Breakdown', true), array('controller' => 'data_reports', 'action' => 'usage_by_account_type')); ?></li>
					<li><?php echo $html->link(__('Customer Happiness (CHI)', true), array('controller' => 'ngd_reports', 'action' => 'chi')); ?></li>
					<li><?php echo $html->link(__('Lifetime Value (LTV)', true), array('controller' => 'ngd_reports', 'action' => 'ltv')); ?></li>
					<li><a href="" title="-">------</a></li>
					<li><a href="/members/report_recent_sales" title="Recent Signups">Recent Signups</a></li>
					<!--<li><a href="/members/report_free" title="Recent Signups - Free">Recent Signups - Free</a></li>-->
					<li><a href="/members/report_recent_cancels" title="Cancellations">Recent Cancels</a></li>
					<li><a href="" title="-">------</a></li>
					<li><a href="/daily_stats/daily_stats" title="Growth History">Growth History</a></li>
					<li><a href="/cancels/report_graph_30_day_cancels" title="30 Day Cancels">30 Day Cancels Graph</a></li>
					<li><a href="" title="-">------</a></li>
					<li><a href="/m_logs/report_unique_logins_per_day" title="Daily Unique Login Report">Unique Login</a></li>
					<li><a href="/m_logs/report_unique_logins_pay_type" title="Daily Unique Login Report by Paytype">Unique Login Pay Type</a></li>
					<li><a href="/m_logs/report_unique_logins_pay_vs_free" title="Daily Unique Login Report Pay vs Free">Unique Login Pay vs Free</a></li>
					<li><a href="/m_logs/report_active_logins_daily" title="Active Login Report">Actives Percentage Daily</a></li>
					<li><a href="/m_logs/report_active_logins_weekly" title="Weekly Active Login Report">Actives Percentage Weekly</a></li>
					<!--
					<li><a href="/reports/plans" title="Plan Analysis">Plan Analysis</a></li>
					<li><a href="/reports/affiliates" title="Affiliate Performance">Affiliate Performance</a></li>
					<li><a href="/reports/expiring" title="Expiring Credit Cards">Expiring Credit Cards</a></li>
					<li><a href="/reports/emails" title="Email Lists">Email Lists</a></li> -->
					<?php endif; ?>
			
				</ul>
			</li>
			
			<li class="subitems">
				<a href="#" title="Coupons">Coupons</a>
				<ul>
					<li><a href="/coupons/active" title="Active Coupons">Active Coupons</a></li>
					<li><a href="/coupons/" title="All Coupons">All Coupons</a></li>
				</ul>
			</li>
			
			<li class="subitems">
				<a href="#" title="Coupons">Deals</a>
				<ul>
					<li><a href="/deals/active" title="Active Coupons">Active Deals</a></li>
					<li><a href="/deals/" title="All Coupons">All Deals</a></li>
				</ul>
			</li>
			
			<li class="subitems">
				<a href="#" title="Coupons">Contests</a>
				<ul>
					<li><a href="/contests" title="All Contests">All Contests</a></li>
					<li><a href="/contestants" title="All Contestants">All Contestants</a></li>
				</ul>
			</li>
			<li class="subitems">
				<a href="#" title="Testing">A/B Testing</a>
				<ul>
					<li><?php echo $html->link(__('Active Tests', true), array('controller' => 'landing_pages', 'action' => 'active')); ?></li>
					
					
				</ul>
			</li>
			<li class="subitems">
				<a href="#" title="Invites">Invites</a>
				<ul>
					<li><a href="/free_account_auths" title="All Contests">All Invites</a></li>
					<li><a href="/free_account_auths/bulk_create" title="All Contestants">Create Invite Codes</a></li>
				</ul>
			</li>
			<li class="subitems">
				<a href="#" title="Configuration">Configuration</a>
				<ul>
					<li><a href="/plans" title="Manage Plans">Manage Plans</a></li>
					<li><a href="/blacklists" title="Manage Plans">Manage Blacklists</a></li>
					<li><a href="/ngd_options" title="Manage Options">Manage Options</a></li>
					<li><a href="/quips" title="Manage Quips">Manage Quips</a></li>
					<!--<li><a href="#" title="Manage NNTP Stats">Manage NNTP Stats</a></li>-->
					<li><?php echo $html->link('HW Service Classes', array('controller' => 'hw_svc_classes', 'action' => 'index'));?></li>
					<li><a href="/plans" title="Manage Plans">Manage Plans</a></li>
					
				</ul>
			</li>
			
			<?php if ($ngdAdminUser) : ?>			
			<li class="subitems">
				<a href="#" title="Admin Functions">Admin Functions</a>
				<ul>
					<li><a href="/jobs" title="Jobs">Trigger Job</a></li>
                    <li><a href="/users" title="View Users">View Admins</a></li>
                    <li><a href="/users/add" title="Add User">Add Admin</a></li>
                    <li><a href="" title="-">------</a></li>
					<li><a href="https://www.newsgroupdirect.com/scheduled/job_mailchimp_abuse_updates.php" title="Job Email Marketing Abuse">Email Marketing Abuse Job</a></li>
                    
				</ul>
			</li>
			
			<?php endif; ?>
		
			<?php endif; ?>
			
		</ul>
		<span class="clear"></span>
		
		
		<div id="redbox">
			<h4>Operation: NGD Backoffice</h4>
			<p>
				<big>Copyright 2008</big>
				Script executed in 0.1568s<br />
				30 SQL queries used<br />
				Build: 20080928<br />
				<?=$html->image('cake.power.gif');?>
			</p>

		</div>
	</div>
	
	
	<div id="content">
	
		<?php
		if ($session->check('Message.flash')) {
		  $session->flash();
		}
		?>
				
		<?=$content_for_layout;?>
	</div>
	<div id="footer">
	</div>
</div>

</body>
</html>
