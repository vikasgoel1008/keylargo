<div class="coupons form">
	<h2>Add New Coupon</h2>

	<?php echo $this->element('coupons_menu');?>

	<?=$form->create('Coupon');?>
	<fieldset>
		<?=$form->input('pre_reqs',array('options'=>$pre_reqs, 'type'=>'select','class'=>'tiny','label'=>'Coupon Pre Requirements',));?><br class="hid" />
		<?=$form->input('allowed_accounts',array('options'=>$options, 'type'=>'select','class'=>'medium','label'=>'Coupon Applied To'));?><br class="hid" />
		<?=$form->input('type',array('options'=>$types, 'type'=>'select','class'=>'small','label'=>'Type of Discount'));?><br class="hid" />
		<?=$form->input('edu_required',array('options'=>array("N"=>"No","Y"=>"Yes"), 'type'=>'select','class'=>'tiny','label'=>'Education Only Coupon',));?><br class="hid" />
		<?=$form->input('is_limited',array('options'=>array("N"=>"No","Y"=>"Yes"), 'type'=>'select','class'=>'tiny','label'=>'Is this a limited coupon?',));?><br class="hid" />
		<?=$form->input('remaining',array('type'=>'text','class'=>'tiny','label'=>'Number Coupons Available'));?><br class="hid" />
		<?=$form->input('id',array('type'=>'text','class'=>'small','label'=>'Coupon Code'));?><br class="hid" />	
		<?=$form->input('coupon_desc',array('type'=>'text','class'=>'small','label'=>'Description'));?><br class="hid" />
		<?=$form->input('discount',array('type'=>'text','class'=>'small','label'=>'Discount Value'));?><br class="hid" />
		<?=$form->input('send_date',array('type'=>'text','class'=>'tiny','label'=>'Market Date'));?><br class="hid" /><p>Format: YYYY-MM-DD</p>
		<?=$form->input('effect_date',array('type'=>'text','class'=>'tiny','label'=>'Effective Date'));?><br class="hid" /><p>Format: YYYY-MM-DD</p>
		<?=$form->input('expire_date',array('type'=>'text','class'=>'tiny','label'=>'Expiration Date'));?><br class="hid" /><p>Format: YYYY-MM-DD</p>
		<?=$form->input('prom_allow_mths',array('type'=>'text','class'=>'tiny','label'=>'Length of Coupon'));?><br class="hid" /><p>The number of months the coupon can be applied to an account (1000 = indefinite).</p>
		<?=$form->input('display_text',array('type'=>'text','class'=>'large','label'=>'Text to display to customer'));?><br class="hid" /><p>This is the display text the customer sees when successful.</p>
		<?=$form->input('affiliate',array('type'=>'text','class'=>'tiny','label'=>'Exclusive Coupon Provider'));?><br class="hid" /><p>The affiliate id.</p>
		
		<p></p>
		<?=$form->end(array('label'=>'Submit Coupon','class'=>'button submit'));?>
		<span class="clear"></span>
	</fieldset>
</div>
