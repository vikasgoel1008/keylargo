<div class="coupons index">
	<h2>Coupons</h2>
	
	<?php echo $this->element('coupons_menu');?>
	<?php $paginator->options(array('url' => $this->passedArgs));?>
	
	<div class="box">
		<p>&nbsp;</p>
		<table cellpadding="0" cellspacing="0">
			<tr class="highlight">	
				<td colspan="5"><?php  echo $paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)));?></td>
			</tr>
			
			<tr class="highlight">
				<td><?php echo $paginator->sort('id');?></td>
				<td><?php echo $paginator->sort('coupon_desc');?></td>
				<td><?php echo $paginator->sort('discount');?></td>
				<td><?php echo $paginator->sort('effect_date');?></td>
				<td><?php echo $paginator->sort('expire_date');?></td>
			</tr>
			
			<?php foreach ($coupons as $coupon): ?>
			
			<tr class="highlight">
				<td><?php echo $html->link($coupon['Coupon']['id'], array('action' => 'view', $coupon['Coupon']['id'])); ?></td>
				<td><?php echo $coupon['Coupon']['coupon_desc']; ?></td>
				<td><?php echo $coupon['Coupon']['discount']; ?></td>
				<td><?php echo $coupon['Coupon']['effect_date']; ?></td>
				<td><?php echo $coupon['Coupon']['expire_date']; ?></td>
			</tr>
			
			<?php endforeach; ?>
		</table>

		<p id="pagin">
			<?php echo $paginator->first();?>
		 	<?php echo $paginator->prev('< previous', "", "", "");?>
		 	<?php //echo $paginator->prev() ?> 
			<?php echo $paginator->numbers();?>  
		 	<?php //echo $paginator->next() ?>
			<?php //echo $paginator->counter(); ?>
			<?php echo $paginator->next('next >', "", "", "");?>
			<?php echo $paginator->last();?>
		<p/>

		<span class="clear"></span>
	</div>
</div>

