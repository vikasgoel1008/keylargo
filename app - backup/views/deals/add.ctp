<div class="deals form">
	<h2>Add New Deal</h2>

	<?php echo $this->element('deals_menu');?>

	<?=$form->create('Deal');?>
	<fieldset>
		<?=$form->input('name',array('type'=>'text','class'=>'medium','label'=>'Deal Name'));?><br class="hid" />
		<?=$form->input('start_date',array('type'=>'text','class'=>'medium','label'=>'Start Date'));?><br class="hid" /><p>Format: YYYY-MM-DD HH:ii:ss</p>
		<?=$form->input('expire_date',array('type'=>'text','class'=>'medium','label'=>'Expiration Date'));?><br class="hid" /><p>Format: YYYY-MM-DD HH:ii:ss</p>
		
		<?php if (count($plan) == 1) : ?>
		<?=$form->input('plan_id',array('type'=>'select','class'=>'medium','label'=>"Plan", 'options'=>$plan));?><br class="hid" />
		<?php else : ?>
		<?=$form->input('plan_id',array('type'=>'select','class'=>'medium','label'=>"Plan", 'options'=>$plan, 'empty' => ''));?><br class="hid" />
		<?php endif; ?>
		
		<?=$form->input('bandwidth',array('type'=>'text','class'=>'medium','label'=>'Bandwidth'));?><br class="hid" />
		<?=$form->input('orig_price',array('type'=>'text','class'=>'medium','label'=>'Orig Price'));?><br class="hid" />
		<?=$form->input('sale_price',array('type'=>'text','class'=>'medium','label'=>'Sale Price'));?><br class="hid" />
		
		<?=$form->input('limit',array('type'=>'text','class'=>'medium','label'=>'# Deals Available'));?><br class="hid" />
		
		<?=$form->input('rate_period',array('options'=>array('Monthly' => "Monthly", "Flat Rate" => "Flat Rate"), 'type'=>'select','class'=>'tiny','label'=>'Rate Type', 'empty'=>true));?><br class="hid" />
		<?=$form->input('tb_tuesday',array('options'=>array("N"=>"No","Y"=>"Yes"), 'type'=>'select','class'=>'medium','label'=>'TB Tuesday Deal'));?><br class="hid" />
		<?=$form->input('black_friday',array('options'=>array("N"=>"No","Y"=>"Yes"), 'type'=>'select','class'=>'medium','label'=>'Black Friday Deal'));?><br class="hid" />
		<?=$form->input('deal_of_week',array('options'=>array("N"=>"No","Y"=>"Yes"), 'type'=>'select','class'=>'medium','label'=>'Deal of Week'));?><br class="hid" />
		<?=$form->input('unlimited_usenet',array('options'=>$unlimited, 'type'=>'select','class'=>'medium','label'=>'Unlimited Usenet'));?><br class="hid" />
		<?=$form->input('block_usenet',array('options'=>$block, 'type'=>'select','class'=>'medium','label'=>'Block Usenet'));?><br class="hid" />
		<?=$form->input('counter',array('options'=>array("N"=>"No","Y"=>"Yes"), 'type'=>'select','class'=>'medium','label'=>'Display Counter'));?><br class="hid" />
		
		<?=$form->input('desc',array('type'=>'hidden','class'=>'medium','label'=>'Deal of Week'));?><br class="hid" />
		<?=$form->input('mid_text',array('type'=>'hidden','class'=>'medium','label'=>'Deal of Week'));?><br class="hid" />
		<?=$form->input('bullets',array('type'=>'hidden','class'=>'medium','label'=>'Deal of Week'));?><br class="hid" />
		

		
		<p></p>
		<?=$form->end(array('label'=>'Submit Deal','class'=>'button submit'));?>
		<span class="clear"></span>
	</fieldset>
</div>
