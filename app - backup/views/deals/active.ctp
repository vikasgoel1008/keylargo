<div class="deals index">
	<h2>Deals</h2>
	
	<?php echo $this->element('deals_menu');?>
	<?php $paginator->options(array('url' => $this->passedArgs));?>
	
	<div class="box">
		<p>&nbsp;</p>
		<table cellpadding="0" cellspacing="0">
			<tr class="highlight">	
				<td colspan="7"><?php  echo $paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)));?></td>
			</tr>
			
			<tr class="highlight">
				<td><?php echo $paginator->sort('name');?></td>
				<td><?php echo $paginator->sort('sale_price');?></td>
				<td><?php echo $paginator->sort('start_date');?></td>
				<td><?php echo $paginator->sort('expire_date');?></td>
				<td>Performance</td>
				<td>Deal Flags</td>
				<td>Actions</td>
			</tr>
			
			<?php foreach ($deals as $deal): ?>
			
			<tr class="highlight">
				<td><?php echo $html->link($deal['Deal']['name'], array('action' => 'view', $deal['Deal']['id'])); ?></td>
				<td><?php echo $deal['Deal']['sale_price']; ?></td>
				<td><?php echo $deal['Deal']['start_date']; ?></td>
				<td><?php echo $deal['Deal']['expire_date']; ?></td>
				<td><?php echo $deal['Deal']['limit'] - $deal['Deal']['remaining'] . " / " . $deal['Deal']['limit']; ?></td>
				<td>
					<?php 
						
						$flags = '';
						
						if ($deal['Deal']['tb_tuesday'] == 'Y') { $flags = "TB"; }
							
						if ($deal['Deal']['black_friday'] == 'Y') {
							if (!empty($flags)) { $flags = " | ";} 
							$flags = "BF";
						}
						
						if ($deal['Deal']['deal_of_week'] == 'Y') {
							if (!empty($flags)) { $flags = " | ";} 
							$flags = "DW";
						}
						
						if ($deal['Deal']['unlimited_usenet'] == 'Y') {
							if (!empty($flags)) { $flags = " | ";} 
							$flags = "UU";
						}						
						elseif ($deal['Deal']['unlimited_usenet'] == 'AD') {
							if (!empty($flags)) { $flags = " | ";} 
							$flags = "Adult";
						}					
						elseif ($deal['Deal']['unlimited_usenet'] == 'US') {
							if (!empty($flags)) { $flags = " | ";} 
							$flags = "Free Sucks";
						}					
						elseif ($deal['Deal']['unlimited_usenet'] == 'B') {
							if (!empty($flags)) { $flags = " | ";} 
							$flags = "Best Usenet";
						}
						
						
						if ($deal['Deal']['block_usenet'] == 'Y') {
							if (!empty($flags)) { $flags = " | ";} 
							$flags = "BL";
						}						
						
						echo $flags;
						
					?>
				</td>
				<td><?php echo $html->link('Duplicate', array('action' => 'duplicate', $deal['Deal']['id'])); ?></td>
				
			</tr>
			
			<?php endforeach; ?>
		</table>

		<p id="pagin">
			<?php echo $paginator->first();?>
		 	<?php echo $paginator->prev('< previous', "", "", "");?>
		 	<?php //echo $paginator->prev() ?> 
			<?php echo $paginator->numbers();?>  
		 	<?php //echo $paginator->next() ?>
			<?php //echo $paginator->counter(); ?>
			<?php echo $paginator->next('next >', "", "", "");?>
			<?php echo $paginator->last();?>
		<p/>

		<span class="clear"></span>
	</div>
</div>

