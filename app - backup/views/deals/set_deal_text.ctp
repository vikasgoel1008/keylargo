<div class="deals form">
	<h2>Set Deal Text</h2>

	<?php echo $this->element('deals_menu');?>

	<?=$form->create('Deal', array('action' => 'set_deal_text'));?>
	<fieldset>
		<?=$form->input('id',array('type'=>'hidden','class'=>'medium','label'=>'Description'));?><br class="hid" />
		<?=$form->input('name',array('type'=>'text','class'=>'medium','label'=>'Description', 'readonly' => 'readonly'));?><br class="hid" />
		
		<?=$form->input('desc',array('type'=>'textarea','class'=>'medium','label'=>'Deal Description'));?><br class="hid" />
		<?=$form->input('mid_text',array('type'=>'textarea','class'=>'medium','label'=>'Deal Mid Text'));?><br class="hid" />
		<?=$form->input('bullets',array('type'=>'textarea','class'=>'medium','label'=>'Deal Bullets'));?><br class="hid" />
		<p>Format: Enclose each line in list item tags</p>
		<p></p>
		<?=$form->end(array('label'=>'Submit Deal','class'=>'button submit'));?>
		<span class="clear"></span>
	</fieldset>
</div>
