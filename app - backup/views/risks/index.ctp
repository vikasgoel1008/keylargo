<div class="risks index">
	<h2>Risk Reporting - <?php echo $process_date;?></h2>
	
	<?php $paginator->options(array('url' => $this->passedArgs));?>
	
	<div class="box">
		<p>&nbsp;</p>
		<table cellpadding="0" cellspacing="0">
			
			<tr class="highlight">	
				<td colspan="8">All figures are based on 30 days.</td>
			</tr>
			
			<tr>
				<td><?php echo $paginator->sort('member_id');?></td>
				<td><?php echo $paginator->sort('plan_id');?></td>
				<td><?php echo $paginator->sort('status');?></td>
				<td><?php echo $paginator->sort("Usage (GB)",'30_day_usage');?></td>
				<td><?php echo $paginator->sort("# IPs",'30_day_ips_used');?></td>
				<td><?php echo $paginator->sort("Trunks",'30_day_ip_trunks_used');?></td>
				<td><?php echo $paginator->sort('abuse_warning');?></td>
				<td><?php echo $paginator->sort('risk_score');?></td>
			</tr>
			
			<?php foreach ($risks as $risk): ?>
			
			<tr  class="highlight">
				<td><?php echo $html->link($risk['Member']['login_username'], array('controller' => 'members', 'action' => 'view', $risk['Risk']['member_id'])); ?></td>
				<!--<td><?php echo $html->link($risk['Plan']['name'], array('controller' => 'plans', 'action' => 'view', $risk['Risk']['plan_id'])); ?></td>-->
				<td><?php echo $risk['Plan']['name']; ?></td>
				<td><?php echo $risk['Risk']['status']; ?></td>
				<td><?php echo $risk['Risk']['30_day_usage']; ?></td>
				<td><?php echo $risk['Risk']['30_day_ips_used'];?></td>
				<td><?php echo $risk['Risk']['30_day_ip_trunks_used'];?></td>
				<td>
					<?php if ($risk['Risk']['abuse_warning'] == "N"): ?>
					<?php echo $html->link($risk['Risk']['abuse_warning'], array('controller'=> 'members','action' => 'mark_abuser',$risk['Risk']['member_id']), null, sprintf(__('Do you want to flag this account %s as a potential abuser?', true), $risk['Member']['login_username']));?>
					<?php else : ?>
					<?php echo $risk['Risk']['abuse_warning'];?>
					<?php endif; ?>
				
					
				</td>
				<td><?php echo $risk['Risk']['risk_score']; ?></td>
			</tr>
			
			<?php endforeach; ?>
			
			<tr class="highlight">	
				<td colspan="8"><?php  echo $paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)));?></td>
			</tr>
			
		</table>

		<p id="pagin">
			<?php echo $paginator->first();?>
		 	<?php echo $paginator->prev('< previous', "", "", "");?>
		 	<?php //echo $paginator->prev() ?> 
			<?php echo $paginator->numbers();?>  
		 	<?php //echo $paginator->next() ?>
			<?php //echo $paginator->counter(); ?>
			<?php echo $paginator->next('next >', "", "", "");?>
			<?php echo $paginator->last();?>
		<p/>

		<span class="clear"></span>
	</div>
</div>

