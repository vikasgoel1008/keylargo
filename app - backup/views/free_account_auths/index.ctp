<div class="ccTransactions index">
	<h2>Account Invites</h2>
	
	<?php $paginator->options(array('url' => $this->passedArgs));?>
	
	<div class="box">
		<p>&nbsp;</p>
		<table cellpadding="0" cellspacing="0">
		
			<tr class="highlight"><td colspan="6"><?php echo $html->link('Bulk Create Invites', array('action' => 'bulk_create'));?></td></tr>
			
			<tr class="highlight">	
				<td colspan="6"><?php  echo $paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)));?></td>
			</tr>
			<tr>
				<td><?php echo $paginator->sort('auth_code');?></td>
				<td><?php echo $paginator->sort('validated');?></td>
				<td><?php echo $paginator->sort('plan_id');?></td>
				<td><?php echo $paginator->sort('free_months');?></td>
				<td>Distributed</td>
				<td>Actions</td>
			</tr>
			
			<?php foreach ($freeAccountAuths as $accAuth): ?>
			
			<tr  class="highlight">
				<td><?php echo $html->link($accAuth['FreeAccountAuth']['auth_code'], array('action' => 'view', $accAuth['FreeAccountAuth']['id']));?></td>
				<td><?php echo $accAuth['FreeAccountAuth']['validated']; ?></td>
				<td><?php echo $accAuth['FreeAccountAuth']['plan_id']; ?></td>
				<td><?php echo $accAuth['FreeAccountAuth']['free_months']; ?></td>
				
				<?php if(empty($accAuth['FreeAccountAuth']['email']) && $accAuth['FreeAccountAuth']['validated'] == 'N') : ?>				
				<td>N</td><td><?php echo $html->link("Send Invite", array('action' => 'send_invite', $accAuth['FreeAccountAuth']['id']));?> | <?php echo $html->link("Create TinyURL", array('action' => 'create_tiny', $accAuth['FreeAccountAuth']['id']));?></td>
				<?php else : ?>
				<td>Y</td>
				<td>&nbsp;</td>
				<?php endif; ?>
				
				
			</tr>
			
			<?php endforeach; ?>
		</table>

		<p id="pagin">
			<?php echo $paginator->first();?>
		 	<?php echo $paginator->prev('< previous', "", "", "");?>
		 	<?php //echo $paginator->prev() ?> 
			<?php echo $paginator->numbers();?>  
		 	<?php //echo $paginator->next() ?>
			<?php //echo $paginator->counter(); ?>
			<?php echo $paginator->next('next >', "", "", "");?>
			<?php echo $paginator->last();?>
		<p/>

		<span class="clear"></span>
	</div>
</div>




