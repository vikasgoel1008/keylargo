<h2>Send Invite</h2>

<?php echo $this->element('members_menu');?>

<?=$form->create('FreeAccountAuth', array('action' => 'send_invite'));?>
<fieldset>
	<?php echo $form->hidden('id'); ?> 

	<?=$form->input('name',array('type'=>'text','class'=>'small','label'=>'Name'));?><br class="hid" />
	<?=$form->input('email',array('type'=>'text','class'=>'small','label'=>'Email Address'));?><br class="hid" />
	<?=$form->input('plan_id',array('type'=>'text','class'=>'tiny','label'=>'Plan Id'));?><br class="hid" />
	<?=$form->input('free_months',array('type'=>'text','class'=>'tiny','label'=>'Free Months'));?><br class="hid" />
	<?=$form->input('invited_by',array('type'=>'text','class'=>'small','label'=>'Invited By', 'value'=>'NewsgroupDirect'));?><br class="hid" />
	<?=$form->input('auth_code',array('type'=>'text','class'=>'medium','label'=>'Invite Code', 'readonly'=>'readonly'));?><br class="hid" />
	<p></p>
	<?=$form->end(array('label'=>'Update','class'=>'button submit'));?>
	<span class="clear"></span>
</fieldset>