<div class="contests form">
<?php echo $form->create('Contest');?>
	<fieldset>
 		<legend><?php __('Edit Contest');?></legend>
	<?php
		echo $form->input('id');
		echo $form->input('name');
		echo $form->input('effect_date');
		echo $form->input('expire_date');
		echo $form->input('description');
		echo $form->input('subs_per_day');
		echo $form->input('subs_max');
		echo $form->input('terms');
		echo $form->input('exclude_accounts');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Delete', true), array('action' => 'delete', $form->value('Contest.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $form->value('Contest.id'))); ?></li>
		<li><?php echo $html->link(__('List Contests', true), array('action' => 'index'));?></li>
	</ul>
</div>
