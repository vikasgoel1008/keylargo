
<div class="contests view">
<h2>Contest Details</h2>
<?php echo $this->element('contests_menu');?>
	
<div class="box">
	<p>&nbsp;</p>

	<table>
		<tr class="highlight"><td colspan="2"><?php echo $html->link("View Contestants", array('controller'=>'contestants', 'action' => 'index', $contest['Contest']['id']));?></td></tr>
		<tr class="highlight"><td>Contest:</td><td><?php echo $html->link($contest['Contest']['name'], array('action' => 'edit', $contest['Contest']['id']));?></td></tr>
		<tr class="highlight">
			<td>Winner:</td>
			<td>
				<?php 
					if (!empty($contest['Contest']['contestant_id'])) {
						echo $html->link("View Winner", array('controller'=>'contestants', 'action' => 'view', $contest['Contest']['contestant_id']));
					}
					elseif (date('Y-m-d') > $contest['Contest']['expire_date']) {
						echo $html->link("Get Winner", array('action' => 'get_winner', $contest['Contest']['id']));
					}
					else {
						echo "Contest is Currently Active";
					}
				
				?>
			</td>
		</tr>
		<tr class="highlight"><td>Expires:</td><td><?php echo $contest['Contest']['expire_date']; ?></td></tr>
		<tr class="highlight"><td>Max Entries:</td><td><?php echo $contest['Contest']['subs_per_day']; ?></td></tr>
		<tr class="highlight"><td>Exclusions:</td><td><?php echo $contest['Contest']['exclude_accounts']; ?></td></tr>
		<tr class=""><td>Description:</td><td>&nbsp;<br/><?php echo $contest['Contest']['description']; ?><br/>&nbsp;</td></tr>
		<tr class=""><td>Contest Terms:</td><td>&nbsp;<br/><?php echo $contest['Contest']['terms']; ?><br/>&nbsp;</td></tr>
		
		
	</table>
	<p id="pagin">
			&nbsp;
		</p>
		
		<span class="clear">&nbsp;</span>

</div>	
</div>
	