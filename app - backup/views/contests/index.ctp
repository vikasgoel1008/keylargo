<div class="contests index">
	<h2>Contests</h2>
	
	<?php echo $this->element('contests_menu');?>
	<?php $paginator->options(array('url' => $this->passedArgs));?>
	
	<div class="box">
		<p>&nbsp;</p>
		<table cellpadding="0" cellspacing="0">
			<tr class="highlight">	
				<td colspan="5"><?php  echo $paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)));?></td>
			</tr>
			
			<tr class="highlight">
				<td><?php echo $paginator->sort('name');?></td>
				<td><?php echo $paginator->sort('effect_date');?></td>
				<td><?php echo $paginator->sort('expire_date');?></td>
				<td><?php echo $paginator->sort('entries');?></td>
			</tr>
			
			<?php foreach ($contests as $contest): ?>
			
			<tr class="highlight">
				<td><?php echo $html->link($contest['Contest']['name'], array('action' => 'view', $contest['Contest']['id'])); ?></td>
				<td><?php echo $contest['Contest']['effect_date']; ?></td>
				<td><?php echo $contest['Contest']['expire_date']; ?></td>
				<td><?php echo $contest['0']['entries']; ?></td>
			</tr>
			
			<?php endforeach; ?>
		</table>

		<p id="pagin">
			<?php echo $paginator->first();?>
		 	<?php echo $paginator->prev('< previous', "", "", "");?>
		 	<?php //echo $paginator->prev() ?> 
			<?php echo $paginator->numbers();?>  
		 	<?php //echo $paginator->next() ?>
			<?php //echo $paginator->counter(); ?>
			<?php echo $paginator->next('next >', "", "", "");?>
			<?php echo $paginator->last();?>
		<p/>

		<span class="clear"></span>
	</div>
</div>

