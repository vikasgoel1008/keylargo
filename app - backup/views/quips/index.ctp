<div class="ngd options index">
	<h2>Options</h2>
	
	<?php echo $this->element('quips_menu');?>
	<?php $paginator->options(array('url' => $this->passedArgs));?>
	
	<div class="box">
		<p>&nbsp;</p>
		<table cellpadding="0" cellspacing="0">
			<tr class="highlight">	
				<td colspan="5"><?php  echo $paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)));?></td>
			</tr>
			
			<tr class="highlight">
				<td><?php echo $paginator->sort('id');?></td>
				<td><?php echo $paginator->sort('type');?></td>
				<td><?php echo $paginator->sort('status');?></td>
				<td><?php echo $paginator->sort('date_added');?></td>
				<td>Actions</td>
			</tr>
			
			<?php foreach ($quips as $quip): ?>
			
			<tr class="highlight">
				<td><?php echo $html->link('View Quip', array('action' => 'view', $quip['Quip']['id'])); ?></td>
				<td><?php echo $quip['Quip']['type']; ?></td>
				<td><?php echo $quip['Quip']['status']; ?></td>
				<td><?php echo $quip['Quip']['date_added']; ?></td>
				
				<td class="actions">
					<?php echo $html->link(__('Delete', true), array('action' => 'delete', $quip['Quip']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $quip['Quip']['id'])); ?>
				</td>
			</tr>
			
			<?php endforeach; ?>
		</table>

		<p id="pagin">
			<?php echo $paginator->first();?>
		 	<?php echo $paginator->prev('< previous', "", "", "");?>
		 	<?php //echo $paginator->prev() ?> 
			<?php echo $paginator->numbers();?>  
		 	<?php //echo $paginator->next() ?>
			<?php //echo $paginator->counter(); ?>
			<?php echo $paginator->next('next >', "", "", "");?>
			<?php echo $paginator->last();?>
		<p/>

		<span class="clear"></span>
	</div>
</div>

