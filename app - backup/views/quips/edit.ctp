<div class="options form">
	<h2>Edit Quip</h2>

	<?php echo $this->element('quips_menu');?>

	<?=$form->create('Quip');?>
	<fieldset>
		<?=$form->input('id',array('type'=>'hidden','class'=>'medium','label'=>''));?><br class="hid" />
		<?=$form->input('type',array('type'=>'text','class'=>'medium','label'=>'Quip Type'));?><br class="hid" />
		<?=$form->input('text',array('type'=>'text','class'=>'large','label'=>'Text'));?><br class="hid" />
		<?=$form->input('date_added',array('type'=>'text','class'=>'medium','label'=>'Date Added'));?><br class="hid" />	
		<?=$form->input('status',array('type'=>'text','class'=>'medium','label'=>'Status'));?><br class="hid" />
		<p></p>
		<?=$form->end(array('label'=>'Edit','class'=>'button submit'));?>
		<span class="clear"></span>
	</fieldset>
</div>
