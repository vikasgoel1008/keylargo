<div class="ccTransactions index">
	<h2>Option Details</h2>

	<?php echo $this->element('quips_menu');?>

	<div class="box">
		<p>
			
		</p>
		<table cellpadding="0" cellspacing="0">
			
			<tr class="highlight"><td colspan="3"><?php echo $html->link('Edit Quip', 'edit/'. $quip['Quip']['id']);?></td></tr>
			<tr class="highlight">
				<td>Quip Type:</td>	
				<td><?php echo $quip['Quip']['type']; ?></td>			
				<td></td>
			</tr>
			<tr class="highlight">
				<td>Text:</td>	
				<td><?php echo $quip['Quip']['text']; ?></td>			
				<td></td>
			</tr>
			<tr class="highlight">
				<td>Status:</td>	
				<td><?php echo $quip['Quip']['status']; ?></td>			
				<td></td>
			</tr>
			<tr class="highlight">
				<td>Date Added:</td>	
				<td><?php echo $quip['Quip']['date_added']; ?></td>			
				<td></td>
			</tr>
					
		</table>
		
		<p id="pagin">
			&nbsp;
		</p>
		
		<span class="clear">&nbsp;</span>
	</div>
</div>
	