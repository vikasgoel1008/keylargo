<div class="quips form">
	<h2>Add New Quip</h2>

	<?php echo $this->element('quips_menu');?>

	<?=$form->create('Quip');?>
	<fieldset>
		<?=$form->input('text',array('type'=>'text','class'=>'medium','label'=>'Quip Text'));?><br class="hid" />
		<?=$form->input('type',array('type'=>'text','class'=>'medium','label'=>'Quip Type'));?><br class="hid" />
		<?=$form->input('status',array('type'=>'text','class'=>'medium','label'=>'Status'));?><br class="hid" />	
		<p></p>
		<?=$form->end(array('label'=>'Submit Quip','class'=>'button submit'));?>
		<span class="clear"></span>
	</fieldset>
</div>
