<div class="blacklists index">
	<h2>Blacklisted Terms</h2>
	
	<?php echo $this->element('members_menu');?>
	<?php $paginator->options(array('url' => $this->passedArgs));?>
	
	<div class="box">
		<p>&nbsp;</p>
		<table cellpadding="0" cellspacing="0">
			<tr class="highlight">	
				<td colspan="5"><?php  echo $html->link('Blacklist', array('action' => 'add'));?></td>
			</tr>
			<tr class="highlight">	
				<td colspan="5"><?php  echo $paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)));?></td>
			</tr>
			<tr>
				<td><?php echo $paginator->sort('id');?></td>
				<td><?php echo $paginator->sort('email');?></td>
				<td><?php echo $paginator->sort('domain');?></td>
				<td><?php echo $paginator->sort('IP');?></td>
				<td><?php echo $paginator->sort('card_number');?></td>
			</tr>
			
			<?php foreach ($blacklists as $blacklist): ?>
			
			<tr  class="highlight">
				<td><?php echo $html->link($blacklist['Blacklist']['id'], array('action' => 'view', $blacklist['Blacklist']['id'])); ?></td>
				<td><?php echo $blacklist['Blacklist']['email']; ?></td>
				<td><?php echo $blacklist['Blacklist']['domain'];?></td>
				<td><?php echo $blacklist['Blacklist']['IP']; ?></td>
				<td><?php echo substr($blacklist['Blacklist']['card_number'], strlen($blacklist['Blacklist']['card_number'])-4) ; ?></td>
			</tr>
			
			<?php endforeach; ?>
		</table>

		<p id="pagin">
			<?php echo $paginator->first();?>
		 	<?php echo $paginator->prev('< previous', "", "", "");?>
		 	<?php //echo $paginator->prev() ?> 
			<?php echo $paginator->numbers();?>  
		 	<?php //echo $paginator->next() ?>
			<?php //echo $paginator->counter(); ?>
			<?php echo $paginator->next('next >', "", "", "");?>
			<?php echo $paginator->last();?>
		<p/>

		<span class="clear"></span>
	</div>
</div>

