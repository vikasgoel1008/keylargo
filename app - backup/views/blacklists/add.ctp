<h2>Blacklist Customer</h2>

<?php echo $this->element('members_menu');?>

<?=$form->create('Blacklist');?>
<fieldset>
	
	<?=$form->input('email',array('type'=>'text','class'=>'small','label'=>'Email Address', 'value' => $session->read('Blacklist.email')));?><br class="hid" />
	<?=$form->input('domain',array('type'=>'text','class'=>'tiny','label'=>'Domain', 'value' => $session->read('Blacklist.domain')));?><br class="hid" />
	<?=$form->input('IP',array('type'=>'text','class'=>'tiny','label'=>'IP Address', 'value' => $session->read('Blacklist.IP')));?><br class="hid" />
	<?=$form->input('card_number',array('type'=>'text','class'=>'tiny','label'=>'Credit Card', 'value' => $session->read('Blacklist.card_number')));?><br class="hid" />
	<?=$form->input('reason',array('type'=>'text','class'=>'tiny','label'=>'Blacklist Reason'));?><br class="hid" />
	<p></p>
	<?=$form->end(array('label'=>'Blacklist','class'=>'button submit'));?>
	<span class="clear"></span>
</fieldset>