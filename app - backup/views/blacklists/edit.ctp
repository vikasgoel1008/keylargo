<div class="blacklists form">
<?php echo $form->create('Blacklist');?>
	<fieldset>
 		<legend><?php __('Edit Blacklist');?></legend>
	<?php
		echo $form->input('id');
		echo $form->input('email');
		echo $form->input('IP');
		echo $form->input('card_number');
		echo $form->input('reason');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Delete', true), array('action' => 'delete', $form->value('Blacklist.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $form->value('Blacklist.id'))); ?></li>
		<li><?php echo $html->link(__('List Blacklists', true), array('action' => 'index'));?></li>
	</ul>
</div>
