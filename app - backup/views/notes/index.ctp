<div class="notes index">
	<h2>Customer Notes</h2>
	
	<?php echo $this->element('members_menu');?>
	<?php $paginator->options(array('url' => $this->passedArgs));?>
	
	<div class="box">
		<p>&nbsp;</p>
		<table cellpadding="0" cellspacing="0">
			<tr class="highlight">	
				<td colspan="5"><?php  echo $html->link('Add Note', array('action' => 'add', $member['Member']['id']));?></td>
			</tr>
			<tr class="highlight"><td colspan="5"><?php echo $member['Member']['name']; ?> | <em><?php echo $member['Member']['email']; ?></em> | <?php echo $member['Member']['login_username']; ?></td></tr>
			
			<tr>
				<td><?php echo $paginator->sort('id');?></td>
				<td><?php echo $paginator->sort('subject');?></td>
				<td><?php echo $paginator->sort('date_added');?></td>
			</tr>
			
			<?php foreach ($notes as $note): ?>
			
			<tr  class="highlight">
				<td><?php echo $html->link($note['Note']['id'], array('action' => 'view', $note['Note']['id'])); ?></td>
				<td><?php echo $note['Note']['subject'];?></td>
				<td><?php echo $note['Note']['date_added']; ?></td>
			</tr>
			
			<?php endforeach; ?>
			
			<tr class="highlight">	
				<td colspan="5"><?php  echo $paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)));?></td>
			</tr>
			
		</table>

		<p id="pagin">
			<?php echo $paginator->first();?>
		 	<?php echo $paginator->prev('< previous', "", "", "");?>
		 	<?php //echo $paginator->prev() ?> 
			<?php echo $paginator->numbers();?>  
		 	<?php //echo $paginator->next() ?>
			<?php //echo $paginator->counter(); ?>
			<?php echo $paginator->next('next >', "", "", "");?>
			<?php echo $paginator->last();?>
		<p/>

		<span class="clear"></span>
	</div>
</div>

