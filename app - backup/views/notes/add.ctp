<h2>Add Note</h2>

<?php echo $this->element('members_menu');?>

<?=$form->create('Note');?>
<fieldset>
	
	<?=$form->input('member_id',array('type'=>'text','class'=>'small','label'=>'Member', 'value' => $member['Member']['id'], 'readonly'=>'readonly'));?><br class="hid" />
	<?=$form->input('member',array('type'=>'text','class'=>'small','label'=>'Member', 'value' => $member['Member']['login_username'], 'readonly'=>'readonly'));?><br class="hid" />
	<?=$form->input('subject',array('type'=>'text','class'=>'tiny','label'=>'Note Topic'));?><br class="hid" />
	<?=$form->input('notes',array('type'=>'textarea','class'=>'tiny','label'=>'Comments'));?><br class="hid" />
	<p></p>
	<?=$form->end(array('label'=>'Note','class'=>'button submit'));?>
	<span class="clear"></span>
</fieldset>