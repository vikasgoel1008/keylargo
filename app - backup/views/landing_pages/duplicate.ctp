<div class="landing page form">
	<h2>Add New Landing Page</h2>
	<?php echo $this->element('landings_menu');?>

	<?=$form->create('LandingPage');?>
	<fieldset>
		<?=$form->input('name',array('type'=>'text','class'=>'medium','label'=>'Deal Name', 'readonly' => 'readonly'));?><br class="hid" />
		<?=$form->input('landing_url',array('type'=>'text','class'=>'medium','label'=>'Landing Url', 'readonly' => 'readonly'));?><br class="hid" />

		<?=$form->input('description',array('type'=>'text','class'=>'medium','label'=>'Template Description'));?><br class="hid" />
		<?=$form->input('template',array('type'=>'text','class'=>'medium','label'=>'Template File'));?><br class="hid" />
		
		<?=$form->input('start_date',array('type'=>'text','class'=>'medium','label'=>'Start Date'));?><br class="hid" /><p>Format: YYYY-MM-DD HH:ii:ss</p>
		<?=$form->input('expire_date',array('type'=>'text','class'=>'medium','label'=>'Expiration Date'));?><br class="hid" /><p>Format: YYYY-MM-DD HH:ii:ss</p>
		
		<?=$form->input('deal_id',array('options'=>$deals, 'type'=>'select','class'=>'medium','label'=>'Associated Deal', 'empty'=>'empty'));?><br class="hid" />

		
		<p></p>
		<?=$form->end(array('label'=>'Submit Landing Page','class'=>'button submit'));?>
		<span class="clear"></span>
	</fieldset>
</div>
