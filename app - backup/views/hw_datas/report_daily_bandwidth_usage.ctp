<script type="text/javascript" src="https://www.google.com/jsapi"></script>

 <script type="text/javascript">
   google.load('visualization', '1', {packages: ['corechart']});
 </script>


<script type="text/javascript">
	google.setOnLoadCallback(drawChart);
	
	function drawChart() {
		var data = new google.visualization.DataTable();
		data.addColumn('string', 'Date');
		data.addColumn('number', 'Daily');
		data.addColumn('number', 'Moving 7');
		data.addColumn('number', 'Moving 28');
		data.addRows([
		<?php foreach ($bytes as $byte) : ?> <?php if (!empty($byte)): ?>
        ['<?php echo $byte['date'];?>', <?php echo $byte['usage'];?>, <?php if (isset($byte['moving7'])) {echo $byte['moving7'];}?>, <?php if (isset($byte['moving28'])) {echo $byte['moving28'];}?>],
		<?php endif; ?>	<?php endforeach; ?>
		]);
		
		var options = {
			chartArea:{left:75,top:75,width:"90%",height:"60%"},
           	id3D: true,
           	legend: {position: 'bottom'},
           	hAxis:{slantedText:true},
           	series: [{color: 'red'},{color: 'blue'}, {color: 'silver'}, ],
           	
           	
		};
		
		var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
		chart.draw(data, options);
	}
</script>

<div class="ccTransactions index">
	<h2><?php echo $days;?> Day Bandwidth Report</h2>
	<?php echo $this->element('members_menu');?>

	<div class="box">
		
		<table cellpadding="0" cellspacing="0">
		
			<tr class="highlight">
				<td colspan="2">
					<?php echo $html->link(__('7 Day', true), array('controller' => 'hw_datas', 'action' => 'report_daily_bandwidth_usage',7)); ?> | 
					<?php echo $html->link(__('30 Day', true), array('controller' => 'hw_datas', 'action' => 'report_daily_bandwidth_usage',30)); ?> | 
					<?php echo $html->link(__('180 Day', true), array('controller' => 'hw_datas', 'action' => 'report_daily_bandwidth_usage',180)); ?>
				</td>
			</tr>
			
			<tr>
				<td colspan="2">
					 <div id="chart_div"  style="align: center; width: 700px; height: 500px;"></div>
				</td>
			</tr>
		</table>
				
		<!--
		<div id="flashcontent">
        	<strong>You need to upgrade your Flash Player</strong>         <br/>
        	<?php //echo $seriesXML; ?><br/>   
        	<?php //echo $valuesXML; ?>
       	</div>
       	
		<div align="center">
	        <script type="text/javascript">
	                // <![CDATA[
	                var so = new SWFObject("/amcharts/amcolumn.swf", "amcolumn", "728", "500", "8", "#ffffff");
	                so.addVariable("path", "/amcharts/");
	                so.addVariable("settings_file", escape("/amcharts/report_graph_30_day_sales_settings.xml?<?php echo microtime();?>"));
	                so.addVariable("chart_data", "<chart><series><?php echo $seriesXML; ?></series><graphs><?php echo $valuesXML; ?></graphs></chart>");
	                so.addVariable("preloader_color", "#FFFFFF");
	                so.write("flashcontent");
	                // ]]>
	        </script>
	    
    	</div>    
        -->
       
       
       
        <span class="clear"></span>
		
	
	</div>
</div>
	
