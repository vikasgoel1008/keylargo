<?php $paginator->options(array('url' => $this->passedArgs));?>
	
<div class="ccTransactions index">
	<h2>Member IP's</h2>
	<?php echo $this->element('members_menu');?>	

	<div class="box">
		<p>&nbsp;</p>

		<table cellpadding="0" cellspacing="0">
		
		<tr class="highlight"><td colspan="3"><?php echo $member['Member']['name']; ?> | <em><?php echo $member['Member']['email']; ?></em> | <?php echo $member['Member']['login_username']; ?></td></tr>
		<tr><td colspan="3"</td></tr>
		
		<tr class="highlight">	
			<td><?php echo $paginator->sort('usage_date');?></td>
			<td><?php echo $paginator->sort('ip_address');?></td>
			<td>Geo Location</td>		
		</tr>
		
		<?php foreach ($ips as $ip): ?>
			<tr  class="highlight">
				<td>
					<?php echo $ip['HwData']['usage_date']; ?>
				</td>
				<td>
					<?php echo $ip['HwData']['ip_address']; ?>
				</td>
				<td>
					Coming Soon
				</td>
				
			</tr>
		<?php endforeach; ?>
		</table>
		
	
	<p id="pagin">
			<?php echo $paginator->first();?>
		 	<?php echo $paginator->prev('< previous', "", "", "");?>
		 	<?php //echo $paginator->prev() ?> 
			<?php echo $paginator->numbers();?>  
		 	<?php //echo $paginator->next() ?>
			<?php //echo $paginator->counter(); ?>
			<?php echo $paginator->next('next >', "", "", "");?>
			<?php echo $paginator->last();?>
		 	
		<p/>

	<span class="clear"></span>
</div>
</div>	