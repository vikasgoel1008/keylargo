<?php
class HwSvcClass extends AppModel {

	var $name = 'HwSvcClass';
	
	/**
	 * Overridden paginateCount method
	 */
	function paginateCount($conditions = null, $recursive = 0, $extra = array()) {
		$sql = "SELECT DISTINCT (service_class_id) service_class_id FROM hw_svc_classes";
		$this->recursive = $recursive;
		$results = $this->query($sql);
		return count($results);
	}

	function getRateHoursForClass($service_class) {
		
		return $this->find('all', array('conditions' => array('service_class_id' => $service_class),
										'order' => array('start_time' => 'asc')));
	}
	
	function getServiceRateList() {
		
		return $this->find('list', array('fields' => array("service_class_id", "name"),
										'group' => array("service_class_id", "name")));
	}
	
	function job_update_hw_service_rates() {
	
		$currentTime = date("H:i:s");
		
		echo $currentTime . "<br/><br/>\n\n";
		
		$rates = $this->find('all', array('conditions' => array('start_time <=' => $currentTime, 
																'end_time >=' => $currentTime)));
																
		
		App::import('Vendor','highwindsAPI',array('file'=>'highwindsAPI.php'));
		$hw = new highwindsAPI();
			
		foreach ($rates as $rate) {
			//print_r($rate);
			
			// get yellow threshold & determine yellow zone bps
			$ylw_bytes = $rate["HwSvcClass"]["ylw_threshold"]*1000*1000*1000;
			$ylw_bps = rand($rate["HwSvcClass"]["ylw_rate_min"], $rate["HwSvcClass"]["ylw_rate_max"]);
			
			// get red threshold & determine red zone bps
			$red_bytes = $rate["HwSvcClass"]["red_threshold"]*1000*1000*1000;
			$red_bps = rand($rate["HwSvcClass"]["red_rate_min"], $rate["HwSvcClass"]["red_rate_max"]);
			
			// update in highwinds
			$hw->updateServiceClass($rate["HwSvcClass"]["service_class_id"], $ylw_bytes, $ylw_bps, $red_bytes, $red_bps);
			
			echo "{$rate["HwSvcClass"]["service_class_id"]}, $ylw_bytes, $ylw_bps, $red_bytes, $red_bps<br/>\n\n";
			
			if ($hw->success) {echo "update successful";}
			else { echo "update failed {$hw->errorMessage}";}
			
			echo "<br/><br/>";
		}
		
	}

}
?>