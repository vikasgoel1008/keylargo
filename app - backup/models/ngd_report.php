<?php
class NgdReport extends AppModel {

	var $name = 'NgdReport';

	function getRecentReportDate($report, $type) {
		
		$conds = array('report' => $report, 'subkey' => $type);
		$date = $this->find('first',array('conditions' => $conds,
										  'fields' => array('max(data_date) report_date')));
										  
		return $date[0]['report_date'];
	}
}
?>