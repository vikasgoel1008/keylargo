<?php
class Deal extends AppModel {

	var $name = 'Deal';

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $hasMany = array('SoldDeal','LandingPage');
	var $belongsTo = array("Plan");
	
	function createDeal($data, $new = true) {
	
		if (empty($data['Deal']['name'])) {
			return errArray(301, "Must provide a name for the deal.");
		}
		elseif ($data['Deal']['start_date'] < date("Y-m-d H:i:s")) {
			return errArray(301, "Start date cannot be in the past.");
		}
		elseif ($data['Deal']['expire_date'] < date("Y-m-d H:i:s")) {
			return errArray(301, "Expire date cannot be in the past.");
		}
		elseif (empty($data['Deal']['bandwidth'])) {
			return errArray(301, "The bandwidth must be provided.");
		}
		elseif (empty($data['Deal']['rate_period'])) {
			return errArray(301, "Must provide the rate period.");
		}
		elseif (empty($data['Deal']['plan_id'])) {
			return errArray(301, "Must provide a plan for this deal.");
		}
		elseif (empty($data['Deal']['limit'])) {
			return errArray(301, "Must provide the number of deals available.");
		}
		elseif (empty($data['Deal']['sale_price'])) {
			return errArray(301, "Must provide a sale price.");
		}
		
		// get the plan
		$plan = $this->Plan->getPlan(array('id' => $data["Deal"]['plan_id']));
		if (empty($plan)) {return errArray(301, "Plan provided is not valid."); }
		
		
		
		// set alternate string
		$data['Deal']['alternate'] = $this->getAlternateString();
		$data['Deal']['remaining'] = $data['Deal']['limit'];
		
		
		
		if ($new) { $this->create(); }
		
		if (!$this->save($data)) {
			return errArray(301, "Deal could not be created at this time.");
		}
		else { return $this->id; }
	
	}
	
	function consumeDeal($alternate, $member_id = 0) {
		$deal = $this->getDeal(array('alternate' => $alternate), -1);
		
		if (empty($deal)) { return errArray(300, "could not find deal"); }
		
		// log the sale
		$this->SoldDeal->logSale($deal['Deal']['id'], $member_id);
		
		// reduce remaining count
		if (!$this->updateDeal($deal['Deal']['id'], array('remaining' => $deal['Deal']['remaining']-1))) {
			Report_Site_Error("Could not reduce the remaining deal quantity for deal {$deal['Deal']['id']}.");
			return errArray(301, "failed to update deal");
		}
		
		return true;
	}
	
	private function genAlternateString($string_length = 20) {
		// init characters string parms
		$characters = 'BCDFGHJKLMNPQRSTVWXYZbcdfghjklmnpqrstvwxyz0123456789';
	
		$string = '';
		
		for ($i = 0; $i < $string_length; $i++) {
			$string .= $characters[rand(0, strlen($characters) - 1)];
		}
		
		return $string;
	}
	
	function getAlternateString() {
	
		$new = '';
		$good = false;
		
		while (!$good) {
			
			$new = $this->genAlternateString();
			$test = $this->getDeal(array('alternate' => $new));
			if (empty($test)) { $good = true; }
		}
		
		return $new;
	}

	function getDeal($args, $recursive = 1) {
	
		$this->recursive = $recursive;
		return $this->find("first", array('conditions' => $args));
	}
	
	function getDeals($args, $recursive = 1) {
	
		$this->recursive = $recursive;
		return $this->find("all", array('conditions' => $args, 'order' => array('start_date' => 'asc')));
	}	
	
	function updateDeal($deal_id, $args) {
		
		if (!is_array($args)) {return false;}
		
		// process args to get field list
		$fields = array_keys($args);
		$args['id'] = $deal_id;
		
		// save the data
		if ($this->save($args, false, $fields)) { return true;}
		else {return false;}
	
	}

}
?>