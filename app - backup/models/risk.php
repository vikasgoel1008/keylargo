<?php
class Risk extends AppModel {

	var $name = 'Risk';

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $belongsTo = array('Member', 'Plan');
	
		
	function getRisks($member_id = null, $days = 180) {
		
		$this->unbindModelAll();
		$cond['data_type'] = 'daily';
		//$cond['to_days(now())-to_days(usage_date) <='] = $days;
		$cond['to_days(now())-to_days(reporting_date) between ? and ?'] = array(0,$days-1);
		
	
		if (!empty($member_id)) { $cond['member_id'] = $member_id; }
		
		$risks = $this->find('all', array('fields'	=> array( 'order' => array('reporting_date'), 'conditions' => $cond)));
		
		return $risks;
		
	}
	
}
?>