<h2>Customer Info</h2>

<?php echo $this->element('members_menu');?>

<?=$form->create('Member', array('action' => 'editbilling'));?>
<fieldset>	
	<?php echo $form->hidden('Member.id'); ?> 
	<?php echo $form->hidden('PaymentDetail.id'); ?> 
	<?=$form->input('PaymentDetail.cc_card_nbr',array('type'=>'text','class'=>'small','label'=>'Card Number'));?><br class="hid" />
	<?=$form->input('PaymentDetail.cc_card_exp',array('type'=>'text','class'=>'small','label'=>'Exp Date'));?><br class="hid" />
	<p>Use YYYYMM format</p>
	<?=$form->input('Member.coupon_code',array('type'=>'text','class'=>'tiny','label'=>'Coupon'));?><br class="hid" />
	<?=$coupon_note;?>
	<?=$form->input('Member.plan_id',array('type'=>'text','class'=>'tiny','label'=>'Plan'));?><br class="hid" />
	<p></p>
	<?=$form->end(array('label'=>'Update','class'=>'button submit'));?>
</fieldset>