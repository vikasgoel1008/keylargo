<div class="ccTransactions index">
	<h2>Active Free Accounts by IP</h2>
	<?php echo $this->element('members_menu');?>
	<?php $paginator->options(array('url' => $this->passedArgs));?>

	<div class="box">
		<p>&nbsp;</p>
		<table cellpadding="0" cellspacing="0">
		<tr class="highlight">	
			<td colspan="4"><?php  echo $paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)));?></td>
		</tr>
		<tr class="highlight">	
			<td><?php echo $paginator->sort('ip');?></td>
			<td><?php echo $paginator->sort('active_free_accounts');?></td>
		</tr>
		<?php foreach ($members as $member): ?>
			<tr class="highlight">
				<td><?php echo $html->link($member['Member']['ip'], 'report_ip/'.$member['Member']['ip']); ?></td>
				<td><?php echo $member[0]['active_free_accounts']; ?></td>
			</tr>
		<?php endforeach; ?>
		</table>

		<p id="pagin">
			<?php echo $paginator->first();?>
		 	<?php echo $paginator->prev('< previous', "", "", "");?>
		 	<?php //echo $paginator->prev() ?> 
			<?php echo $paginator->numbers();?>  
		 	<?php //echo $paginator->next() ?>
			<?php //echo $paginator->counter(); ?>
			<?php echo $paginator->next('next >', "", "", "");?>
			<?php echo $paginator->last();?>
		 	
		<p/>
		<span class="clear"></span>
	</div>	
</div>
