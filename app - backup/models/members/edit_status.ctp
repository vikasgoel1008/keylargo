<h2>Update Member Address</h2>

<?php echo $this->element('members_menu');?>

<?=$form->create('Member', array('action' => 'edit_status'));?>
<fieldset>
	<?php echo $form->hidden('id'); ?> 

	<?=$form->input('status',array('type'=>'text','class'=>'small','label'=>'Status'));?><br class="hid" />
	<?=$form->input('date_cancel',array('type'=>'text','class'=>'small','label'=>'Cancel Date'));?><br class="hid" />
	<?=$form->input('date_expire',array('type'=>'text','class'=>'tiny','label'=>'Expiration Date'));?><br class="hid" />
	<p></p>
	<?=$form->end(array('label'=>'Update','class'=>'button submit'));?>
	<span class="clear"></span>
</fieldset>