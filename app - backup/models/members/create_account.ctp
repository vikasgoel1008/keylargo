<h2>Create Customer Account</h2>

<?php echo $this->element('members_menu');?>

<?=$form->create('Member', array('action' => 'create_account'));?>
<fieldset>
	
	<?=$form->input('email',array('type'=>'text','class'=>'small','label'=>'Email Address'));?><br class="hid" />
	<?=$form->input('name',array('type'=>'text','class'=>'small','label'=>'Name'));?><br class="hid" />
	<?=$form->input('address',array('type'=>'text','class'=>'small','label'=>'Address 1'));?><br class="hid" />
	<?=$form->input('address2',array('type'=>'text','class'=>'small','label'=>'Address 2'));?><br class="hid" />
	<?=$form->input('city',array('type'=>'text','class'=>'tiny','label'=>'City'));?><br class="hid" />
	<?=$form->input('state',array('type'=>'text','class'=>'tiny','label'=>'State'));?><br class="hid" />
	<?=$form->input('postal',array('type'=>'text','class'=>'tiny','label'=>'Postal Code'));?><br class="hid" />
	<?=$form->input('plan_id',array('type'=>'text','class'=>'tiny','label'=>'Plan'));?><br class="hid" />

	<?=$form->input('free_admin_acct',array('options'=>array("Y"=>"Y", "N"=>"N"), 'type'=>'select','class'=>'tiny','label'=>'Free Admin Account','empty' => true));?><br class="hid" />
	
	<p></p>
	<?=$form->end(array('label'=>'Create Account','class'=>'button submit'));?>
	<span class="clear"></span>
</fieldset>