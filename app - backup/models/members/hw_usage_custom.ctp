<div class="Members highwinds">
<h2>Custom Usage</h2>

<?php echo $this->element('members_menu');?>

<div class="box">
	<p>&nbsp;</p>
	
	<table>
		<tr class="highlight"><td colspan="2">Usage Report for <?php echo $range['start']; ?> to <?php echo $range['stop']; ?></td></tr>
		<tr class="highlight">
			<td colspan="2">The customer has used <?php echo $total; ?>GB during the range provided.</td>
		</tr>
		
		<?php foreach ($bytes as $byte) :?>
			<tr class="highlight">
				<td><?php echo $byte['date']; ?></td>
				<td><?php echo $byte['daily_bytes']; ?></td>
			</tr>
		<?php endforeach; ?>
		
		
		
	</table>
	
	<p id="pagin">
		&nbsp;
	<p/>

	<span class="clear"></span>
</div>
</div>