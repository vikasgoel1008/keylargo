<div class="landingpage form">
	<h2>Edit Landing Page</h2>


	<?=$form->create('LandingPage', array('action' => 'edit'));?>
	<fieldset>
		<?=$form->input('name',array('type'=>'text','class'=>'medium','label'=>'Campaign'));?><br class="hid" />
		<?=$form->input('description',array('type'=>'text','class'=>'medium','label'=>'Template Description'));?><br class="hid" />
		<?=$form->input('landing_url',array('type'=>'text','class'=>'large','label'=>'Landing Url'));?><br class="hid" /><p>Format: include initial "/" after domain</p>
		<?=$form->input('alternate_url',array('type'=>'text','class'=>'medium','label'=>'Alternate Url'));?><br class="hid" /><p>Format: include initial "/" after domain</p>
		<?=$form->input('start_date',array('type'=>'text','class'=>'medium','label'=>'Start Date'));?><br class="hid" /><p>Format: YYYY-MM-DD HH:ii:ss</p>
		<?=$form->input('expire_date',array('type'=>'text','class'=>'medium','label'=>'Expiration Date'));?><br class="hid" /><p>Format: YYYY-MM-DD HH:ii:ss</p>
		
		<?=$form->input('default',array('options'=>array("N"=>"No","Y"=>"Yes"), 'type'=>'select','class'=>'medium','label'=>'Default Template'));?><br class="hid" />
		<?=$form->input('template',array('type'=>'text','class'=>'large','label'=>"Template"));?><br class="hid" /><p>Format: Name of file in TPL folder</p>
		<?=$form->input('deal_id',array('options'=>$deals, 'type'=>'select','class'=>'medium','label'=>'Unlimited Usenet'));?><br class="hid" />
		<?=$form->input('id',array('type'=>'hidden','class'=>'medium','label'=>'Deal of Week'));?><br class="hid" />
		
		<p></p>
		<?=$form->end(array('label'=>'Edit Landing Page','class'=>'button submit'));?>
		<span class="clear"></span>
	</fieldset>
</div>
