<?php
class Affiliate extends AppModel {

	var $name = 'Affiliate';	
	var $primaryKey = 'affiliate_id';

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $hasMany = array(
		'Member' => array(
			'className' => 'Member',
			'foreignKey' => 'affiliate_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		
		'Coupon' => array(
			'className' => 'Coupon',
			'foreignKey' => 'affiliate_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		
		'AffiliatePayment' => array(
			'className' => 'AffiliatePayment',
			'foreignKey' => 'affiliate_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		
		'AffiliateSalesTotal' => array(
			'className' => 'AffiliateSalesTotal',
			'foreignKey' => 'affiliate_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
	
	
	function closeAffiliate($id) {
		$aff = $this->read('status', $id);
		$aff['Affiliate']['status'] = 'closed';
		
		if ($this->save($aff)) {return true;}
		else {return false;}
		
	}
}
?>