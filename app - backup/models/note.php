<?php
class Note extends AppModel {

	var $name = 'Note';

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $belongsTo = array(
		'Member' => array(
			'className' => 'Member',
			'foreignKey' => 'member_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	
	function addNote($member_id, $notes) {
		
		$note = $this->create("Note");
		$note['Note']['member_id'] = $member_id;
		$note['Note']['notes'] = $notes;
		$note['Note']['date_added'] = date("Y-m-d H:i:s");
		
		if ($this->save($note)) { return true;}
		else { return false; }
		
	}
}
?>