<?php
class Member extends AppModel {

	var $name = 'Member';
	var $errMsg = '';
	var $severe = false;
	var $emailMsg = '';
	var $recursive = -1;

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $belongsTo = array(
		'Coupon',
		'Plan'
	);

	var $hasOne = array(
		'PaymentDetail'
	);

	var $hasMany = array(
		'Cancel',
		'CcTransaction',
		'MLog',
		'NgdCredit',
		'Mailchimp',
		'HwData'
	);
	
	function errorMessages() {
		return $this->errMsg;
	}
	function emailMessages() {
		return $this->emailMsg;
	}
	
	// 
	function closeAccount($member_id, $expire_date, $reason, $log_cancel = true) {
		
		// retrieve data from members table, confirm exists
		$member = $this->getMember($member_id);
		
		if (!$member) { 
			$this->errMsg = 'Invalid account selected';
			return false; 
		}
		
		// begin save process, as of now, if an account has been closed, we do not override it.
		$member['Member']['date_expire'] = $expire_date;
		$member['Member']['date_cancel'] = date("Y-m-d");
		
		
		// load weatherman api model
		$hw = Classregistry::init('HwWeatherman');
		if (!$hw->close($member['Member']['login_username'], $expire_date)) {
			
			$msgLog = "{$member['Member']['login_username']} could not be closed in highwinds || $expire_date || HW Error: " . $hw->errorMessage();
			$this->log($msgLog, 'debug');
			
			$this->errMsg = 'Account failed to close in highwinds';
			return false;
		}
		unset($hw);
	
		// save the fields in the member table
		if ($this->save($member)) {
		
			// log transaction in cancels table
			if ($log_cancel) {
				$this->Cancel->logCancel($member_id, date('Y-m-d'), $expire_date, $reason, $member['Member']['plan_id']);
			}
				
			// close the account in StorageNinja if need be.
			if (!empty($member['Member']['storageninja_id'])) {
				
				App::import('Vendor','storageNinjaAPI',array('file'=>'apiStorageNinja.php'));

				$sn = new storageNinjaAPI();
				$snResult = $sn->cancelUser($member['Member']['storageninja_id'], $expire_date) ;
				
				if (!$sn->success) {
					
					$msgLog = "{$member['Member']['login_username']} could not be closed in storageninja ({$member['Member']['storageninja_id']}) || $expire_date || HW Error: " . $sn->errorMessage;
					$this->log($msgLog, 'debug');
					$this->errMsg = 'Account Closed!!! || StorageNinja Closure Failed!';
					return false;
					
				}
			} 
			
		} else {
			
			$this->errMsg = 'Failed to close account in database.';
			return false;
		}
		
		return true;
	}
	
	function getChi($member_id = null, $base_date = null) {
	
		if (empty($base_date)) {$base_date = date("Y-m-d");}
		$results = $this->query("CALL sp_ngd_003_get_member_chi('$base_date', $member_id)");
		
		return $results[0];
	}
	
	function getMember($member_id) {
		$this->recursive = -1;
		return $this->find('first', array('conditions' => array('id' => $member_id)));
	}
	
	/*
	function getSourceId($twitter_id) {
		$this->recursive = -1;
		$source = $this->find('first', array('conditions' => array('twitter_id' => $twitter_id)));
		return $source['Source']['id'];
	}
	*/
	
	function forceAdminRebillFailure($member_id) {
	
		// look up user information
		$this->recurisve = -1;
		$this->read(null, $member_id);
		
		$email = $this->data['Member']['email'];
		
		$this->set(array('admin_fail_bill' => 'Y', 'receive_email' => "N"));
		
		if ($this->save()) {
			
			$bl = Classregistry::init('Blacklist');
			$bl->blacklistEmail($email, "bandwidth control");
			unset($bl);
			
			return true;
			
		}
		else {return false;}
	}
	
	function isSevere() {
		return $this->severe;
	}
	
	function resetRebill($member_id) {
		// look up user information
		$this->recurisve = -1;
		$this->read(null, $member_id);
		
		$this->set(array('last_bill_attempt' => '0000-00-00'));
		
		if ($this->save()) { return true; }
		else {return false;}
	}
	
	function test() {
		
		//$x_command=urlencode($command);  
		//$x_post_vars="x_username={$this->username}&x_password={$this->password}&x_command={$x_command}&";
		
		// Convert array of parameters to string
		/*
		$post_vars = "";
		if( is_array($params) )
		{
			foreach( $params as $k => $v )
			{
				if( trim($post_vars) != "" )
					$post_vars .= "&{$k}=$v";
				else
					$post_vars = "{$k}=$v";
			}
			
			$post_vars = $x_post_vars.$post_vars;
		}	
		else
			$post_vars = $x_post_vars.$post_vars; 
			
		// configure the post request
		$hw_config = array('timeout' => $timeout,
						   'User-Agent' => $this->agent,
						   'Referer' => $_SERVER["PHP_SELF"]);
			
		*/
			
		App::import('Core', 'HttpSocket');
		
		
		$HttpSocket = new HttpSocket();
		$results = $HttpSocket->post('http://api.newsgroupdirect.com/members/view/19654.xml', '');
		
		return $results;
	}
	
	function updateMember($member_id, $args) {
		
		if (!is_array($args)) {return false;}
		
		// process args to get field list
		$fields = array_keys($args);
		$args['id'] = $member_id;
		
		// save the data
		if ($this->save($args, false, $fields)) { return true;}
		else {return false;}
	
	}
}
?>