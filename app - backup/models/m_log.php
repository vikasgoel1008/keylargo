<?php
class MLog extends AppModel {

	var $name = 'MLog';

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $belongsTo = array(
		'Member' => array(
			'className' => 'Member',
			'foreignKey' => 'member_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	
	function logActivity($member_id, $log_type, $log_reason, $value_type, $old, $new, $ip, $user_id) {
		
		$this->create();
		
		$log['MLog']['member_id'] = $member_id;
		$log['MLog']['log_date_time'] = date("Y-m-d H:i:s");
		$log['MLog']['log_type'] = $log_type;
		$log['MLog']['log_reason'] = $log_reason;
		$log['MLog']['value_type'] = $value_type;
		$log['MLog']['old_value'] = $old;
		$log['MLog']['new_value'] = $new;
		$log['MLog']['requesting_ip'] = $ip;
		$log['MLog']['user_id'] = $user_id;
		
		//print_r($_SERVER);
		
		$this->save($log);
	}

}
?>