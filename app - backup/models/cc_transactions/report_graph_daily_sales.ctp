
 	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
    
    
     <script type="text/javascript">
       google.load('visualization', '1', {packages: ['corechart']});
     </script>
     <script type="text/javascript">
       function drawVisualization() {
         // Some raw data (not necessarily accurate)
         var data = google.visualization.arrayToDataTable([
           ['Date', 'Rebill', 'Block', 'Recycle', 'Free Trial','Signup', 'Free Upgrade', 'Total'],
           <?php foreach ($revenues as $rev):?>
           
           <?php if (!empty($rev)): ?>
           ['<?php echo $rev['date'];?>', <?php echo $rev['rebill'];?>, <?php echo $rev['block'];?>, <?php echo $rev['recycle'];?>, <?php echo $rev['freetrial'];?>, <?php echo $rev['signup'];?>, <?php echo $rev['freeupgrade'];?>,<?php echo $rev['rebill']+$rev['block']+$rev['recycle']+$rev['freetrial']+$rev['signup']+$rev['freeupgrade'];?>],
           
           
           <?php endif; ?>
           <?php endforeach; ?>
         ]);

         var options = {
           chartArea:{left:75,top:75,width:"90%",height:"60%"},
           vAxis: {title: "$ in Sales"},
           hAxis: {title: "Month"},
           isStacked: true,
           id3D: true,
           legend: {position: 'bottom'},
          
          seriesType: "bars",
           series: {6: {type: "line"}}
         };

         var chart = new google.visualization.ComboChart(document.getElementById('chart_div'));
         chart.draw(data, options);         
         
       }
       google.setOnLoadCallback(drawVisualization);
     </script>

<div class="ccTransactions index">
	<h2><?php echo $days;?> Day Sales Report</h2>
	<?php echo $this->element('members_menu');?>

	<div class="box">
		
		<table cellpadding="0" cellspacing="0">
		
			<tr class="highlight">
				<td colspan="2">
					<?php echo $html->link('30 Day', 'report_graph_daily_sales/30');?> | 
					<?php echo $html->link('60 Day', 'report_graph_daily_sales/60');?> | 
					<?php echo $html->link('90 Day', 'report_graph_daily_sales/90');?>
				</td>
			</tr>
			
			<tr>
				<td colspan="2">
					 <div id="chart_div"  style="align: center; width: 1300px; height: 900px;"></div>
				</td>
			</tr>
		</table>
				
		<!--
		<div id="flashcontent">
        	<strong>You need to upgrade your Flash Player</strong>         <br/>
        	<?php //echo $seriesXML; ?><br/>   
        	<?php //echo $valuesXML; ?>
       	</div>
       	
		<div align="center">
	        <script type="text/javascript">
	                // <![CDATA[
	                var so = new SWFObject("/amcharts/amcolumn.swf", "amcolumn", "728", "500", "8", "#ffffff");
	                so.addVariable("path", "/amcharts/");
	                so.addVariable("settings_file", escape("/amcharts/report_graph_30_day_sales_settings.xml?<?php echo microtime();?>"));
	                so.addVariable("chart_data", "<chart><series><?php echo $seriesXML; ?></series><graphs><?php echo $valuesXML; ?></graphs></chart>");
	                so.addVariable("preloader_color", "#FFFFFF");
	                so.write("flashcontent");
	                // ]]>
	        </script>
	    
    	</div>    
        -->
       
       
       
        <span class="clear"></span>
		
	
	</div>
</div>
	
