<div class="ccTransactions index">

<h2>Transaction History</h2>

<?php echo $this->element('members_menu');?>

<p>
<?php
$paginator->options(array('url' => $this->passedArgs));

echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));


?></p>

<div class="box">
<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('order_nbr');?></th>
	<th><?php echo $paginator->sort('trans_date');?></th>
	<th><?php echo $paginator->sort('amount');?></th>
	<th><?php echo $paginator->sort('approved');?></th>
	<th><?php echo $paginator->sort('trans_purpose');?></th>
</tr>
<?php foreach ($ccTransactions as $ccTransaction): ?>
	<tr  class="highlight">
		<td>
			<?php echo $html->link($ccTransaction['CcTransaction']['order_nbr'], array('action' => 'view', $ccTransaction['CcTransaction']['id'])); ?>
		</td>
		<td>
			<?php echo $ccTransaction['CcTransaction']['trans_date']; ?>
		</td>
		<td>
			<?php echo $ccTransaction['CcTransaction']['amount']; ?>
		</td>
		<td>
			<?php echo $ccTransaction['CcTransaction']['approved']; ?>
		</td>
		<td>
			<?php echo $ccTransaction['CcTransaction']['trans_purpose']; ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>

<p id="pagin">
	<?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class' => 'disabled'));?>
<p/>

<span class="clear"></span>
	
</div>

