<h2>Refund Transaction</h2>

<?php echo $this->element('members_menu');?>

<?=$form->create('CcTransaction', array('action' => 'refund'));?>
<fieldset>

	<?=$form->hidden('id');?>		
	<?=$form->input('order_nbr',array('class'=>'medium','label'=>'Order','readonly' => 'readonly'));?><br class="hid" />
	<?=$form->input('amount',array('type'=>'text','class'=>'tiny','label'=>'Amount', 'value' => $session->read('MaxRefund')));?><br class="hid" />
	<?=$form->input('trans_purpose',array('type'=>'text','class'=>'medium','label'=>'Refund Purpose', 'value' => ''));?><br class="hid" />
	
	<p></p>
	<?=$form->end(array('label'=>'Refund Transaction','class'=>'button submit'));?>
	<span class="clear"></span>
</fieldset>