<div class="ccTransactions index">
	<h2>Customer Logs</h2>
	
	<?php echo $this->element('members_menu');?>
	<?php $this->Paginator->options(array('url' => $this->passedArgs));?>
	
	<div class="box">
		<p>&nbsp;</p>
		<table cellpadding="0" cellspacing="0">
		
			<tr class="highlight"><td colspan="6"><?php echo $this->Html->link('View Compliance Logs', array('action' => 'compliance', $member['Member']['id']));?> | <?php echo $this->Html->link('View Bandwidth Logs', array('action' => 'bandwidth', $member['Member']['id']));?></td></tr>
			<tr class="highlight"><td colspan="6"><?php echo $member['Member']['name']; ?> | <em><?php echo $member['Member']['email']; ?></em> | <?php echo $member['Member']['login_username']; ?></td></tr>
			
			<tr class="highlight">	
				<td colspan="6"><?php  echo $this->Paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)));?></td>
			</tr>
			<tr>
				<td><?php echo $this->Paginator->sort('log_date_time');?></td>
				<td><?php echo $this->Paginator->sort('log_type');?></td>
				<td><?php echo $this->Paginator->sort('log_reason');?></td>
				<td><?php echo $this->Paginator->sort('value_type');?></td>
				<td><?php echo $this->Paginator->sort('new_value');?></td>
				<td><?php echo $this->Paginator->sort('requesting_ip');?></td>
			</tr>
			
			<?php foreach ($memberLogs as $memberLog): ?>
			
			<tr  class="highlight">
				<td><?php echo $this->Html->link($memberLog['MLog']['log_date_time'], array('action' => 'view', $memberLog['MLog']['id']));?></td>
				<td><?php echo $memberLog['MLog']['log_type']; ?></td>
				<td><?php echo $memberLog['MLog']['log_reason']; ?></td>
				<td><?php echo $memberLog['MLog']['value_type']; ?></td>
				<td><?php echo $memberLog['MLog']['new_value']; ?></td>
				<td><?php echo $memberLog['MLog']['requesting_ip']; ?></td>
			</tr>
			
			<?php endforeach; ?>
		</table>

		<p id="pagin">
			<?php echo $this->Paginator->first();?>
		 	<?php echo $this->Paginator->prev('< previous', "", "", "");?>
		 	<?php //echo $this->Paginator->prev() ?> 
			<?php echo $this->Paginator->numbers();?>  
		 	<?php //echo $this->Paginator->next() ?>
			<?php //echo $this->Paginator->counter(); ?>
			<?php echo $this->Paginator->next('next >', "", "", "");?>
			<?php echo $this->Paginator->last();?>
		<p/>

		<span class="clear"></span>
	</div>
</div>




