<div class="memberLogs form">
<?php echo $this->Form->create('MemberLog');?>
	<fieldset>
 		<legend><?php __('Add MemberLog');?></legend>
	<?php
		echo $this->Form->input('member_id');
		echo $this->Form->input('log_date_time');
		echo $this->Form->input('log_type');
		echo $this->Form->input('log_reason');
		echo $this->Form->input('value_type');
		echo $this->Form->input('old_value');
		echo $this->Form->input('new_value');
		echo $this->Form->input('requesting_ip');
	?>
	</fieldset>
<?php echo $this->Form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $this->Html->link(__('List MemberLogs', true), array('action' => 'index'));?></li>
	</ul>
</div>
