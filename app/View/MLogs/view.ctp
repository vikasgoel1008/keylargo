<div class="ccTransactions index">
	<h2>Log Details</h2>

	<?php echo $this->element('members_menu');?>

	<div class="box">
		<p>&nbsp;</p>
		<table cellpadding="0" cellspacing="0">
			<tr class="highlight"><td colspan="6"><?php echo $member['Member']['name']; ?> | <em><?php echo $member['Member']['email']; ?></em> | <?php echo $member['Member']['login_username']; ?></td></tr>
			
			<tr class="highlight">
				<td>Log Date Time:</td>	
				<td><?php echo $memberLog['MLog']['log_date_time']; ?></td>			
				<td></td>
			</tr>
			
			
			<tr class="highlight">
				<td>Log Type:</td>	
				<td><?php echo $memberLog['MLog']['log_type']; ?></td>			
				<td></td>
			</tr>
			
			<tr class="highlight">
				<td>Log Reason:</td>	
				<td><?php echo $memberLog['MLog']['log_reason']; ?></td>			
				<td></td>
			</tr>
			
			<tr class="highlight">
				<td>Value Type:</td>	
				<td><?php echo $memberLog['MLog']['value_type']; ?></td>			
				<td></td>
			</tr>
			
			<tr class="highlight">
				<td>Old Value:</td>	
				<?php if (isset($oldplan)): ?>
				<td><?php echo $this->Html->link($oldplan['Plan']['name'] . " (" . $oldplan['Plan']['id'] . ")", array('controller'=>'plans', 'action' => 'view', $oldplan['Plan']['id'])); ?></td>			
				<?php else: ?>
				<td><?php echo $memberLog['MLog']['old_value']; ?></td>			
				<?php endif; ?>
				<td></td>
			</tr>
			
			<tr class="highlight">
				<td>New Value:</td>	
				<?php if (isset($newplan)): ?>
				<td><?php echo $this->Html->link($newplan['Plan']['name'] . " (" . $newplan['Plan']['id'] . ")", array('controller'=>'plans', 'action' => 'view', $newplan['Plan']['id'])); ?></td>			
				<?php else: ?>
				<td><?php echo $memberLog['MLog']['new_value']; ?></td>			
				<?php endif; ?>
				<td></td>
				
			</tr>
			
			<tr class="highlight">
				<td>Requesting IP:</td>	
				<td><?php echo $memberLog['MLog']['requesting_ip']; ?></td>			
				<td></td>
			</tr>
			
			
			
		
		</table>
		
		<p id="pagin">
			&nbsp;
		</p>
		
		<span class="clear">&nbsp;</span>
	</div>
</div>
	
