<div class="ccTransactions index">
	<h2>Active Users by Week</h2>
	<?php echo $this->element('members_menu');?>

	<div class="box">
		
		<div id="flashcontent">
        	<strong>You need to upgrade your Flash Player</strong>         <br/>
        	<?php echo $seriesXML; ?><br/>   
        	<?php echo $valuesXML; ?>
       	</div>
       	
		<div align="center">
	        <script type="text/javascript">
	                // <![CDATA[
	                 var so = new SWFObject("/amcharts/amline.swf", "amline", "728", "500", "8", "#ffffff");
	               	so.addVariable("path", "/amcharts/");
	                so.addVariable("settings_file", escape("/amcharts/report_active_logins_weekly.xml?<?php echo microtime();?>"));
	                so.addVariable("chart_data", "<chart><series><?php echo $seriesXML; ?></series><graphs><?php echo $valuesXML; ?></graphs></chart>");
	                so.addVariable("preloader_color", "#FFFFFF");
	                so.write("flashcontent");
	                // ]]>
	        </script>
	    
    	</div>    
        
        <span class="clear"></span>
		
	
	</div>
</div>
	
