<h2>Insert New SLA Record</h2>

<?php echo $this->element('sla_menu');?>

<?php echo $this->Form->create('SlaReport');?>
<fieldset>
	
	<?php echo $this->Form->input('data_date',array('type'=>'text','class'=>'small','label'=>'Record Date (yyyy-mm-dd)'));?><br class="hid" />
	<?php echo $this->Form->input('goal',array('type'=>'text','class'=>'small','label'=>'Goal'));?><br class="hid" />
	<?php echo $this->Form->input('failures',array('type'=>'text','class'=>'small','label'=>'Failed to Meet Goal'));?><br class="hid" />
	<?php echo $this->Form->input('success',array('type'=>'text','class'=>'small','label'=>'Met Goal'));?><br class="hid" />
	
	<p></p>
	<?php echo $this->Form->end(array('label'=>'Insert SLA Data','class'=>'button submit'));?>
	<span class="clear"></span>
</fieldset>