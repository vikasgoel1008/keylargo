<div class="affiliates index">
	<h2>Affiliate Payments</h2>
	
	<?php echo $this->element('affiliates_menu');?>
	<?php $this->Paginator->options(array('url' => $this->passedArgs));?>
	
	<div class="box">
		<p>&nbsp;</p>
		<table cellpadding="0" cellspacing="0">
			
			<tr class="highlight">	
				<td colspan="7"><?php  echo $this->Paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)));?></td>
			</tr>
			
			<tr class="highlight">
				<td><?php echo $this->Paginator->sort('pay_date');?></td>
				<td><?php echo $this->Paginator->sort('affiliate_id');?></td>
				<td><?php echo $this->Paginator->sort('current_earnings_amt');?></td>
				<td><?php echo $this->Paginator->sort('pay_forward_amt');?></td>
				<td><?php echo $this->Paginator->sort('check_amt');?></td>
				<td>Paid</td>
				<td>Actions</td>
				
			</tr>
			
			<?php foreach ($payments as $affiliate): ?>
			
			<tr class="highlight">
				<td><?php echo $affiliate['AffiliatePayment']['pay_date']; ?></td>
				<td><?php echo $this->Html->link($affiliate['AffiliatePayment']['affiliate_id'], array('controller'=>'affiliates', 'action' => 'view', $affiliate['AffiliatePayment']['affiliate_id'])); ?></td>
				<td><?php echo $affiliate['AffiliatePayment']['current_earnings_amt']; ?></td>
				<td><?php echo $affiliate['AffiliatePayment']['pay_forward_amt']; ?></td>
				<td><?php echo $affiliate['AffiliatePayment']['check_amt']; ?></td>
				<td><?php if (!empty($affiliate['AffiliatePayment']['pay_type'])){ echo "Y"; } else { echo "N";} ?></td>
				<td><?php echo $this->Html->link("View", array('action' => 'view', $affiliate['AffiliatePayment']['id'])); ?></td>
				
			</tr>
			
			<?php endforeach; ?>
		</table>

		<p id="pagin">
			<?php echo $this->Paginator->first();?>
		 	<?php echo $this->Paginator->prev('< previous', "", "", "");?>
		 	<?php //echo $this->Paginator->prev() ?> 
			<?php echo $this->Paginator->numbers();?>  
		 	<?php //echo $this->Paginator->next() ?>
			<?php //echo $this->Paginator->counter(); ?>
			<?php echo $this->Paginator->next('next >', "", "", "");?>
			<?php echo $this->Paginator->last();?>
		<p/>

		<span class="clear"></span>
	</div>
</div>

