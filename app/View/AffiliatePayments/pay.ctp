<div class="coupons form">
	<h2>Pay Affiliate</h2>

	<?php echo $this->element('affiliates_menu');?>

	<?php echo $this->Form->create('AffiliatePayment', array('action' => 'pay'));?>
	<fieldset>
		<?echo $this->Form->input('id',array('type'=>'hidden','class'=>'small','label'=>'Affiliate Id', 'readonly'=>'readonly'));?><br class="hid" />
		<?echo $this->Form->input('check_amt',array('type'=>'text','class'=>'small','label'=>'Payment Amount', 'readonly' => 'readonly'));?><br class="hid" />
		<?php echo $this->Form->input('affiliate_id',array('type'=>'text','class'=>'small','label'=>'Affiliate', 'readonly' => 'readonly'));?><br class="hid" />
		<?php echo $this->Form->input('pay_type',array('type'=>'text','class'=>'large','label'=>'Pay Type'));?><br class="hid" />
		<?php echo $this->Form->input('check_nbr',array('type'=>'text','class'=>'large','label'=>'Check'));?><br class="hid" />
		<?php echo $this->Form->input('tracking_nbr',array('type'=>'text','class'=>'large','label'=>'Tracking Number',));?><br class="hid" />

		
		<p></p>
		<?php echo $this->Form->end(array('label'=>'Pay','class'=>'button submit'));?>
		<span class="clear"></span>
	</fieldset>
</div>
