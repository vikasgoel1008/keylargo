<div class="dailyStats index">
	<h2><?php echo $numDays;?> Day Cancel History</h2>
	<?php echo $this->element('members_menu');?>

	<div class="box">
	
		<div id="flashcontent">
        	<strong>You need to upgrade your Flash Player</strong><br/>
       	</div>
       	
		<div align="center">
	        <script type="text/javascript">
	                // <![CDATA[
	                var so = new SWFObject("/amcharts/amline.swf", "amline", "728", "500", "8", "#ffffff");
	                so.addVariable("path", "/amcharts");
	                so.addVariable("settings_file", escape("/amcharts/report_graph_cancels_by_day.xml?<?php echo microtime();?>"));
	                so.addVariable("chart_data", "<chart><series><?php echo $seriesXML; ?></series><graphs><?php echo $valuesXML; ?></graphs></chart>");
	                so.addVariable("preloader_color", "#FFFFFF");
	                so.write("flashcontent");
	                // ]]>
	        </script>
	    
    	</div>    
        
        <span class="clear">&nbsp;</span>
		
	
	</div>
</div>
	
