<div class="landing page form">
	<h2>Begin New Test</h2>
	<?php echo $this->element('landings_menu');?>

	<?php echo $this->Form->create('LandingPage', array('action' => 'new_test'));?>
	
		<?php echo $this->Form->input('parent',array('type'=>'hidden','value'=>$parent));?><br class="hid" />	
		<?php echo $this->Form->input('default',array('type'=>'hidden','value'=>'N'));?><br class="hid" />	
		<?php echo $this->Form->input('landings',array('type'=>'hidden','value'=>0));?><br class="hid" />
		<?php echo $this->Form->input('conversions',array('type'=>'hidden','value'=>0));?><br class="hid" />
		
		<fieldset>
		<?php echo $this->Form->input('name',array('type'=>'text','class'=>'medium','label'=>'Landing Page Name'));?><br class="hid" />
		
		<?php echo $this->Form->input('deal_id',array('options'=>$deals, 'type'=>'select','class'=>'medium','label'=>'Deal used for Pricing', 'empty'=>'empty'));?><br class="hid" />
		<?php echo $this->Form->input('landing_url',array('type'=>'text','class'=>'medium','label'=>'Landing Url'));?><br class="hid" /><p>Format: include initial "/" after domain</p>
		<?php echo $this->Form->input('alternate_url',array('type'=>'text','class'=>'medium','label'=>'Alternate Url'));?><br class="hid" /><p>Format: include initial "/" after domain</p>
		<?php echo $this->Form->input('start_date',array('type'=>'text','class'=>'medium','label'=>'Start Date', 'value' => date("Y-m-d H:i:s")));?><br class="hid" /><p>Format: YYYY-MM-DD HH:ii:ss</p>
		<?php echo $this->Form->input('expire_date',array('type'=>'text','class'=>'medium','label'=>'Expiration Date'));?><br class="hid" /><p>Format: YYYY-MM-DD HH:ii:ss</p>
		</fieldset>
		
		<h2>Test 1</h2>
		<fieldset>
		<?php echo $this->Form->input('description_1',array('type'=>'text','class'=>'medium','label'=>'Template Description for A Test'));?><br class="hid" />
		<?php echo $this->Form->input('template_1',array('type'=>'text','class'=>'medium','label'=>'Template for A Test'));?><br class="hid" /><p>Format: Name of file in TPL folder</p>
		</fieldset>
		
		<h2>Test 2</h2>
		<fieldset>
		<?php echo $this->Form->input('description_2',array('type'=>'text','class'=>'medium','label'=>'Template Description for B Test'));?><br class="hid" />
		<?php echo $this->Form->input('template_2',array('type'=>'text','class'=>'medium','label'=>'Template for B Test'));?><br class="hid" /><p>Format: Name of file in TPL folder</p>
		<span class="clear"></span>
		<?php echo $this->Form->end(array('label'=>'Submit Test','class'=>'button submit'));?>
		<span class="clear"></span>
		</fieldset>
		
</div>
