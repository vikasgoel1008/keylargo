<div class="landing page form">
	<h2>Add New Landing Page</h2>
	<?php echo $this->element('landings_menu');?>

	<?php echo $this->Form->create('LandingPage');?>
	<fieldset>
		<?php echo $this->Form->input('name',array('type'=>'text','class'=>'medium','label'=>'Landing Page Name'));?><br class="hid" />
		<?php echo $this->Form->input('description',array('type'=>'text','class'=>'medium','label'=>'Template Description'));?><br class="hid" />
		
		<?php echo $this->Form->input('landing_url',array('type'=>'text','class'=>'medium','label'=>'Landing Url'));?><br class="hid" /><p>Format: include initial "/" after domain</p>
		<?php echo $this->Form->input('alternate_url',array('type'=>'text','class'=>'medium','label'=>'Alternate Url'));?><br class="hid" /><p>Format: include initial "/" after domain</p>
		<?php echo $this->Form->input('template',array('type'=>'text','class'=>'medium','label'=>'Template File'));?><br class="hid" /><p>Format: Name of file in TPL folder</p>
		
		<?php echo $this->Form->input('start_date',array('type'=>'text','class'=>'medium','label'=>'Start Date'));?><br class="hid" /><p>Format: YYYY-MM-DD HH:ii:ss</p>
		<?php echo $this->Form->input('expire_date',array('type'=>'text','class'=>'medium','label'=>'Expiration Date'));?><br class="hid" /><p>Format: YYYY-MM-DD HH:ii:ss</p>
		
		<?php echo $this->Form->input('default',array('options'=>array("N"=>"No","Y"=>"Yes"), 'type'=>'select','class'=>'medium','label'=>'Default Template'));?><br class="hid" />
		
		
		<?php echo $this->Form->input('deal_id',array('options'=>$deals, 'type'=>'select','class'=>'medium','label'=>'Deal of Week', 'empty'=>'empty'));?><br class="hid" />

		
		<p></p>
		<?php echo $this->Form->end(array('label'=>'Submit Landing Page','class'=>'button submit'));?>
		<span class="clear"></span>
	</fieldset>
</div>
