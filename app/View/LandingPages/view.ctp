<div class="deals view">
<h2>Landing Page Details</h2>
<?php echo $this->element('landings_menu');?>
	
<div class="box">
	<p>&nbsp;</p>

	<table>
	
		<tr class="highlight">
			<td colspan="3">
				<?php echo $this->Html->link('Edit',  array('action' => 'edit', $lp['LandingPage']['id']));?>
			</td>
		</tr>
		<tr class="highlight"><td>Campaign:</td><td><?php echo $lp['LandingPage']['name'];?></td></tr>
		<tr class="highlight"><td>Default Template:</td><td><?php echo $lp['LandingPage']['default'];?></td></tr>
		<tr class="highlight"><td>Template Description:</td><td><?php echo $lp['LandingPage']['description']; ?></td></tr>
		<tr class="highlight"><td></td><td></td></tr>
		
		<tr class="highlight"><td>Landing Url:</td><td><?php echo $lp['LandingPage']['landing_url']; ?></td></tr>
		<tr class="highlight"><td>Alternate Url:</td><td><?php echo $lp['LandingPage']['alternate_url']; ?></td></tr>
		<tr class="highlight"><td>Template Url:</td><td><?php echo $lp['LandingPage']['template']; ?></td></tr>
		<tr class="highlight"><td>Associated Deal:</td><td><?php echo $lp['Deal']['name']; ?></td></tr>
		<tr class="highlight"><td>Stats:</td><td>Conversions: <?php echo $lp['LandingPage']['conversions']; ?> | Landings: <?php echo $lp['LandingPage']['landings']; ?> | Rate: <?php echo number_format(($lp['LandingPage']['conversions']/$lp['LandingPage']['landings'])*100,2); ?>%</td></tr>		
				
	</table>
	<p id="pagin">
			&nbsp;
		</p>
		
		<span class="clear">&nbsp;</span>

</div>	
</div>
	