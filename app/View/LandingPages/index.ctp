<div class="LandingPages index">
	<h2>Landing Pages (A/B Testing)</h2>
	<?php echo $this->element('landings_menu');?>
	
	<?php $this->Paginator->options(array('url' => $this->passedArgs));?>
	
	<div class="box">
		<p>&nbsp;</p>
		<table cellpadding="0" cellspacing="0">
			<tr class="highlight">	
				<td colspan="7"><?php  echo $this->Paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)));?></td>
			</tr>
			
			<tr class="highlight">
				<td><?php echo $this->Paginator->sort("Campaign",'name');?></td>
				<td><?php echo $this->Paginator->sort("Template Description",'description');?></td>
				<td><?php echo $this->Paginator->sort('landings');?></td>
				<td><?php echo $this->Paginator->sort('conversions');?></td>
				<td><?php echo $this->Paginator->sort('default');?></td>
				<td>Status</td>
				<td>Actions</td>
			</tr>
			
			<?php foreach ($lps as $lp): ?>
			
			<tr class="highlight">
				<td><?php echo $this->Html->link($lp['LandingPage']['name'], array('action' => 'view', $lp['LandingPage']['id'])); ?></td>
				<td><?php echo $lp['LandingPage']['description']; ?></td>
				<td><?php echo $lp['LandingPage']['landings']; ?></td>
				<td><?php echo $lp['LandingPage']['conversions']; ?></td>
				<td><?php echo $lp['LandingPage']['default']; ?></td>
				
				<td>
					
					<?php 	$status=''; 
						 	if ($lp['LandingPage']['start_date'] < date("Y-m-d H:i:s") && $lp['LandingPage']['expire_date'] > date("Y-m-d H:i:s")) { 
						 		$status = 'active';
						 	}
						 	else {$status = 'ended';}
						 	
						 	if ($lp['LandingPage']['in_test'] == 'Y') {
							 	if (!empty($status)) { $status .= ' | ';}
							 	$status.='test';							 	
						 	}
						 	echo $status;
					?>
				</td>
				
				<td>
					
					<?php // see if active ?>
					<?php if ($lp['LandingPage']['start_date'] < date("Y-m-d H:i:s") && $lp['LandingPage']['expire_date'] > date("Y-m-d H:i:s")): ?>
					
						<?php // see if default ?>
						<?php if ($lp['LandingPage']['default'] == 'Y' && $lp['LandingPage']['in_test'] == 'N'): ?>		
							<?php echo $this->Html->link('New Test', array('action' => 'new_test', $lp['LandingPage']['id'])); ?>
						<?php else : ?>
						
						<?php endif; ?>
						
					<?php else : ?>
					
					<?php endif; ?>
									
					
				</td>
				
			</tr>
			
			<?php endforeach; ?>
		</table>

		<p id="pagin">
			<?php echo $this->Paginator->first();?>
		 	<?php echo $this->Paginator->prev('< previous', "", "", "");?>
		 	<?php //echo $this->Paginator->prev() ?> 
			<?php echo $this->Paginator->numbers();?>  
		 	<?php //echo $this->Paginator->next() ?>
			<?php //echo $this->Paginator->counter(); ?>
			<?php echo $this->Paginator->next('next >', "", "", "");?>
			<?php echo $this->Paginator->last();?>
		<p/>

		<span class="clear"></span>
	</div>
</div>

