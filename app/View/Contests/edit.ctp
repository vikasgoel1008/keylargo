<div class="contests form">
<?php echo $this->Form->create('Contest');?>
	<fieldset>
 		<legend><?php __('Edit Contest');?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('effect_date');
		echo $this->Form->input('expire_date');
		echo $this->Form->input('description');
		echo $this->Form->input('subs_per_day');
		echo $this->Form->input('subs_max');
		echo $this->Form->input('terms');
		echo $this->Form->input('exclude_accounts');
	?>
	</fieldset>
<?php echo $this->Form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $this->Form->value('Contest.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $this->Form->value('Contest.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Contests', true), array('action' => 'index'));?></li>
	</ul>
</div>
