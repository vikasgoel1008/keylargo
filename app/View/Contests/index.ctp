<div class="contests index">
	<h2>Contests</h2>
	
	<?php echo $this->element('contests_menu');?>
	<?php $this->Paginator->options(array('url' => $this->passedArgs));?>
	
	<div class="box">
		<p>&nbsp;</p>
		<table cellpadding="0" cellspacing="0">
			<tr class="highlight">	
				<td colspan="5"><?php  echo $this->Paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)));?></td>
			</tr>
			
			<tr class="highlight">
				<td><?php echo $this->Paginator->sort('name');?></td>
				<td><?php echo $this->Paginator->sort('effect_date');?></td>
				<td><?php echo $this->Paginator->sort('expire_date');?></td>
				<td><?php echo $this->Paginator->sort('entries');?></td>
			</tr>
			
			<?php foreach ($contests as $contest): ?>
			
			<tr class="highlight">
				<td><?php echo $this->Html->link($contest['Contest']['name'], array('action' => 'view', $contest['Contest']['id'])); ?></td>
				<td><?php echo $contest['Contest']['effect_date']; ?></td>
				<td><?php echo $contest['Contest']['expire_date']; ?></td>
				<td><?php echo $contest['0']['entries']; ?></td>
			</tr>
			
			<?php endforeach; ?>
		</table>

		<p id="pagin">
			<?php echo $this->Paginator->first();?>
		 	<?php echo $this->Paginator->prev('< previous', "", "", "");?>
		 	<?php //echo $this->Paginator->prev() ?> 
			<?php echo $this->Paginator->numbers();?>  
		 	<?php //echo $this->Paginator->next() ?>
			<?php //echo $this->Paginator->counter(); ?>
			<?php echo $this->Paginator->next('next >', "", "", "");?>
			<?php echo $this->Paginator->last();?>
		<p/>

		<span class="clear"></span>
	</div>
</div>

