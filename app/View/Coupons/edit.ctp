<div class="coupons form">
	<h2>Edit Coupon</h2>

	<?php echo $this->element('coupons_menu');?>

	<?php echo $this->Form->create('Coupon');?>
	<fieldset>
		<?php echo $this->Form->input('allowed_accounts',array('options'=>$options, 'type'=>'select','class'=>'tiny','label'=>'Applicable Accounts'));?><br class="hid" />
		<?php echo $this->Form->input('type',array('options'=>$types, 'type'=>'select','class'=>'small','label'=>'Type of Discount'));?><br class="hid" />
		<?php echo $this->Form->input('edu_required',array('options'=>$edu_required, 'type'=>'select','class'=>'small','label'=>'Education Only Coupon',));?><br class="hid" />
		<?php echo $this->Form->input('pre_reqs',array('options'=>$pre_reqs, 'type'=>'select','class'=>'small','label'=>'Coupon Pre Requirements',));?><br class="hid" />
		<?php echo $this->Form->input('is_limited',array('options'=>array("N"=>"No","Y"=>"Yes"), 'type'=>'select','class'=>'tiny','label'=>'Is this a limited coupon?',));?><br class="hid" />
		<?php echo $this->Form->input('remaining',array('type'=>'text','class'=>'tiny','label'=>'Number Coupons Available'));?><br class="hid" />
		<?php echo $this->Form->input('id',array('type'=>'text','class'=>'small','label'=>'Coupon Code'));?><br class="hid" />	
		<?php echo $this->Form->input('coupon_desc',array('type'=>'text','class'=>'small','label'=>'Description'));?><br class="hid" />
		<?php echo $this->Form->input('discount',array('type'=>'text','class'=>'small','label'=>'Discount Value'));?><br class="hid" />
		<?php echo $this->Form->input('send_date',array('type'=>'text','class'=>'tiny','label'=>'Market Date'));?><br class="hid" /><p>Format: YYYY-MM-DD</p>
		<?php echo $this->Form->input('effect_date',array('type'=>'text','class'=>'tiny','label'=>'Effective Date'));?><br class="hid" /><p>Format: YYYY-MM-DD</p>
		<?php echo $this->Form->input('expire_date',array('type'=>'text','class'=>'tiny','label'=>'Expiration Date'));?><br class="hid" /><p>Format: YYYY-MM-DD</p>
		<?php echo $this->Form->input('prom_allow_mths',array('type'=>'text','class'=>'tiny','label'=>'Length of Coupon'));?><br class="hid" /><p>The number of months the coupon can be applied to an account.</p>
		<?php echo $this->Form->input('display_text',array('type'=>'text','class'=>'large','label'=>'Coupon Customer Text'));?><br class="hid" />
		<?php echo $this->Form->input('affiliate_id',array('type'=>'text','class'=>'tiny','label'=>'Exclusive Affiliate', 'value'=>$this->data['Affiliate']['affiliate_id'], 'empty'=>true));?><br class="hid" /><p>The affiliate id.</p>
		<p></p>
		<?php echo $this->Form->end(array('label'=>'Edit','class'=>'button submit'));?>
		<span class="clear"></span>
	</fieldset>
</div>
