<div class="coupons index">
	<h2>Coupons</h2>
	
	<?php echo $this->element('coupons_menu');?>
	<?php $this->Paginator->options(array('url' => $this->passedArgs));?>
	
	<div class="box">
		<p>&nbsp;</p>
		<table cellpadding="0" cellspacing="0">
			<tr class="highlight">	
				<td colspan="5"><?php  echo $this->Paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)));?></td>
			</tr>
			
			<tr class="highlight">
				<td><?php echo $this->Paginator->sort('id');?></td>
				<td><?php echo $this->Paginator->sort('coupon_desc');?></td>
				<td><?php echo $this->Paginator->sort('discount');?></td>
				<td><?php echo $this->Paginator->sort('effect_date');?></td>
				<td><?php echo $this->Paginator->sort('expire_date');?></td>
			</tr>
			
			<?php foreach ($coupons as $coupon): ?>
			
			<tr class="highlight">
				<td><?php echo $this->Html->link($coupon['Coupon']['id'], array('action' => 'view', $coupon['Coupon']['id'])); ?></td>
				<td><?php echo $coupon['Coupon']['coupon_desc']; ?></td>
				<td><?php echo $coupon['Coupon']['discount']; ?></td>
				<td><?php echo $coupon['Coupon']['effect_date']; ?></td>
				<td><?php echo $coupon['Coupon']['expire_date']; ?></td>
			</tr>
			
			<?php endforeach; ?>
		</table>

		<p id="pagin">
			<?php echo $this->Paginator->first();?>
		 	<?php echo $this->Paginator->prev('< previous', "", "", "");?>
		 	<?php //echo $this->Paginator->prev() ?> 
			<?php echo $this->Paginator->numbers();?>  
		 	<?php //echo $this->Paginator->next() ?>
			<?php //echo $this->Paginator->counter(); ?>
			<?php echo $this->Paginator->next('next >', "", "", "");?>
			<?php echo $this->Paginator->last();?>
		<p/>

		<span class="clear"></span>
	</div>
</div>

