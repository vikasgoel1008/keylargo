<div class="coupons view">
<h2>Coupon Details</h2>
<?php echo $this->element('coupons_menu');?>
	
<div class="box">
	<p>&nbsp;</p>

	<table>
		<tr class="highlight"><td>Coupon:</td><td><?php echo $this->Html->link($coupon['Coupon']['id'], array('action' => 'edit', $coupon['Coupon']['id']));?></td></tr>
		<tr class="highlight"><td>Description:</td><td><?php echo $coupon['Coupon']['coupon_desc']; ?></td></tr>
		<tr class="highlight"><td>Edu Required:</td><td><?php echo $coupon['Coupon']['edu_required']; ?></td></tr>
		<tr class="highlight"><td>Pre Reqs:</td><td><?php echo $coupon['Coupon']['pre_reqs']; ?></td></tr>
		<tr class="highlight"><td>Limited:</td><td><?php echo $coupon['Coupon']['is_limited']; ?> | <?php echo $coupon['Coupon']['remaining']; ?></td></tr>
		<tr class="highlight"><td>Customer Text:</td><td><?php echo $coupon['Coupon']['display_text']; ?></td></tr>
		
		<?php if (!empty($coupon['Affiliate']['affiliate_id'])) : ?>
		<tr class="highlight"><td>Exclusive Affiliate:</td><td><?php echo $coupon['Affiliate']['affiliate_id']; ?></td></tr>		
		<?php endif; ?>
		
		<tr class="highlight"><td>Coupon Type:</td><td><?php echo $coupon['Coupon']['type']; ?></td></tr>
		<tr class="highlight"><td>Discount Value:</td><td><?php echo $coupon['Coupon']['discount']; ?></td></tr>
		<tr class="highlight">
			<td>Promotion Period:</td>
			<td>
				<?php if ($coupon['Coupon']['prom_allow_mths'] < 1000) : ?>
				<?php echo $coupon['Coupon']['prom_allow_mths'] + 1; ?>
				<?php else: ?>
				<?php echo $coupon['Coupon']['prom_allow_mths']; ?>
				<?php endif; ?>
			</td>
		</tr>
		<tr class="highlight"><td>Allowed Accounts:</td><td><?php echo $coupon['Coupon']['allowed_accounts']; ?></td></tr>
		<tr class="highlight"><td></td><td></td></tr>
		<tr class="highlight"><td>Market Date:</td><td><?php echo $coupon['Coupon']['send_date']; ?></td></tr>
		<tr class="highlight"><td>Effective Date:</td><td><?php echo $coupon['Coupon']['effect_date']; ?></td></tr>
		<tr class="highlight"><td>Expiration Date:</td><td><?php echo $coupon['Coupon']['expire_date']; ?></td></tr>
		<tr class="highlight">
			<td colspan="2">
				<?php echo $this->Html->link('Delete Coupon', 'delete/'.$coupon['Coupon']['id'], null, sprintf(__('This will delete this coupon immediately.  \nAre you sure you want to delete coupon %s?', true), $coupon['Coupon']['id'])); ?>
								
			</td>
		</tr>
		
	</table>
	<p id="pagin">
			&nbsp;
		</p>
		
		<span class="clear">&nbsp;</span>

</div>	
</div>
	