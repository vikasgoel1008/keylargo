<script type="text/javascript" src="https://www.google.com/jsapi"></script>

 <script type="text/javascript">
   google.load('visualization', '1', {packages: ['corechart']});
 </script>


<script type="text/javascript">
	google.setOnLoadCallback(drawChart);
	
	function drawChart() {
		var data = new google.visualization.DataTable();
		data.addColumn('string', 'Date');
		data.addColumn('number', 'Total');
		data.addColumn('number', 'Subs');
		data.addColumn('number', 'Blocks');
		data.addRows([
		<?php foreach ($bytes as $byte) : ?> <?php if (!empty($byte)): ?>
        ['<?php echo $byte['dr']['reporting_date'];?>', <?php echo number_format($byte[0]['avg_bw_all_user'],2);?>, <?php if (isset($byte[0]['avg_bw_sub_user'])) {echo number_format($byte[0]['avg_bw_sub_user'],2);}?>, <?php if (isset($byte[0]['avg_bw_block_user'])) {echo number_format($byte[0]['avg_bw_block_user'],2);}?>],
		<?php endif; ?>	<?php endforeach; ?>
		]);
		
		var options = {
			chartArea:{left:75,top:75,width:"90%",height:"60%"},
           	id3D: true,
           	legend: {position: 'bottom'},
           	hAxis:{slantedText:true},
           	series: [{color: 'red'},{color: 'blue'}, {color: 'silver'}, ],
           	
		};
		
		var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
		chart.draw(data, options);
	}
</script>

<div class="ccTransactions index">
	<h2>Average Daily Bandwidth per User - <?php echo $days;?> days</h2>
	<?php echo $this->element('members_menu');?>

	<div class="box">
		
		<table cellpadding="0" cellspacing="0">
		
			<tr class="highlight">
				<td colspan="2">
					<?php echo $this->Html->link(__('7 Day', true), array('controller' => 'data_reports', 'action' => 'report_average_bandwidth',7)); ?> | 
					<?php echo $this->Html->link(__('30 Day', true), array('controller' => 'data_reports', 'action' => 'report_average_bandwidth',30)); ?> | 
					<?php echo $this->Html->link(__('180 Day', true), array('controller' => 'data_reports', 'action' => 'report_average_bandwidth',180)); ?>
				</td>
			</tr>
			
			<tr>
				<td colspan="2">
					 <div id="chart_div"  style="align: center; width: 700px; height: 500px;"></div>
				</td>
			</tr>
		</table>
        <span class="clear"></span>
		
	
	</div>
</div>
	
