<h2>Update Coupon</h2>

<?php echo $this->element('members_menu');?>

<?php echo $this->Form->create('Member', array('action' => 'edit_coupon'));?>
<fieldset>
	<?php echo $this->Form->hidden('id'); ?> 

	<?php echo $this->Form->input('coupon_id',array('type'=>'text','class'=>'medium','label'=>'Coupon', 'empty' => true));?><br class="hid" />
	<?php echo $this->Form->input('promo_remain',array('type'=>'text','class'=>'medium','label'=>'Coupon Length'));?><br class="hid" />
	<p></p>
	<?php echo $this->Form->end(array('label'=>'Update','class'=>'button submit'));?>
	<span class="clear"></span>
</fieldset>