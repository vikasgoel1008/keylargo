<div class="ccTransactions index">
	<h2>Accounts with Bad Addresses</h2>
	<?php echo $this->element('members_menu');?>
	<?php $this->Paginator->options(array('url' => $this->passedArgs));?>

	<div class="box">
		<table cellpadding="0" cellspacing="0">
		<tr class="highlight">	
			<td colspan="7"><?php  echo $this->Paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)));?></td>
		</tr>
		<tr class="highlight">	
			<td><?php echo $this->Paginator->sort('id');?></td>
			<td><?php echo $this->Paginator->sort('address');?></td>			
			<td><?php echo $this->Paginator->sort('city');?></td>
			<td><?php echo $this->Paginator->sort('state');?></td>
			<td><?php echo $this->Paginator->sort('postal');?></td>
			<td><?php echo $this->Paginator->sort('country');?></td>
			<td></td>
			<td><?php echo $this->Paginator->sort('date_reg');?></td>
		</tr>
		<?php foreach ($members as $member): ?>
		<tr class="highlight">
			<td><?php echo $this->Html->link($member['Member']['name'], 'address/'.$member['Member']['id']); ?></td>
			<td><?php echo $member['Member']['address']; ?></td>
			<td><?php echo $member['Member']['city']; ?></td>
			<td><?php echo $member['Member']['state']; ?></td>
			<td><?php echo $member['Member']['postal']; ?></td>
			<td><?php echo $member['Member']['country']; ?></td>
			<td><a href="http://local.yahooapis.com/MapsService/V1/geocode?appid=riTfkszV34HFfprZWBgQCCNtovEfyBUH3_ybSdxbqhvKvjmpDP6ES0foeJ0pPANtmDIQ&street=<?php echo $member['Member']['address']; ?>&city=<?php echo $member['Member']['city']; ?>&state=<?php echo $member['Member']['state']; ?>">API Lookup</a></td>
			<td><?php echo $member[0]['date_reg']; ?></td>
		</tr>
		<?php endforeach; ?>
		</table>

		<p id="pagin">
			<?php echo $this->Paginator->first();?>
		 	<?php echo $this->Paginator->prev('< previous', "", "", "");?>
		 	<?php echo $this->Paginator->numbers();?>  
		 	<?php echo $this->Paginator->next('next >', "", "", "");?>
			<?php echo $this->Paginator->last();?>
		 	
		<p/>

		<span class="clear"></span>
	</div>	
</div>
