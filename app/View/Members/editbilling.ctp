<h2>Customer Info</h2>

<?php echo $this->element('members_menu');?>

<?php echo $this->Form->create('Member', array('action' => 'editbilling'));?>
<fieldset>	
	<?php echo $this->Form->hidden('Member.id'); ?> 
	<?php echo $this->Form->hidden('PaymentDetail.id'); ?> 
	<?php echo $this->Form->input('PaymentDetail.cc_card_nbr',array('type'=>'text','class'=>'small','label'=>'Card Number'));?><br class="hid" />
	<?php echo $this->Form->input('PaymentDetail.cc_card_exp',array('type'=>'text','class'=>'small','label'=>'Exp Date'));?><br class="hid" />
	<p>Use YYYYMM format</p>
	<?php echo $this->Form->input('Member.coupon_code',array('type'=>'text','class'=>'tiny','label'=>'Coupon'));?><br class="hid" />
	<?php echo $coupon_note;?>
	<?php echo $this->Form->input('Member.plan_id',array('type'=>'text','class'=>'tiny','label'=>'Plan'));?><br class="hid" />
	<p></p>
	<?php echo $this->Form->end(array('label'=>'Update','class'=>'button submit'));?>
</fieldset>