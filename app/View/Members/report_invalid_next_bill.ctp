<div class="ccTransactions index">
	<h2>Recent Free Accounts</h2>
	<?php echo $this->element('members_menu');?>
	<?php $this->Paginator->options(array('url' => $this->passedArgs));?>

	<div class="box">
		<table cellpadding="0" cellspacing="0">
		<tr class="highlight">	
			<td colspan="6"><?php  echo $this->Paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)));?></td>
		</tr>
		<tr class="highlight">	
			<td><?php echo $this->Paginator->sort('id');?></td>
			<td><?php echo $this->Paginator->sort('date_reg');?></td>			
			<td><?php echo $this->Paginator->sort('next_bill_date');?></td>
			<td><?php echo $this->Paginator->sort('last_upgrade_date');?></td>
			<td><?php echo $this->Paginator->sort('date_cancel');?></td>
		</tr>
		<?php foreach ($members as $member): ?>
		<tr class="highlight">
			<td><?php echo $this->Html->link($member['Member']['name'], 'view/'.$member['Member']['id']); ?></td>
			<td><?php echo $member['Member']['date_reg']; ?></td>
			<td><?php echo $member['Member']['next_bill_date']; ?></td>
			<td><?php echo $member['Member']['last_upgrade_date']; ?></td>
			<td><?php echo $member['Member']['date_cancel']; ?></td>
			
		</tr>
		<?php endforeach; ?>
		</table>

		<p id="pagin">
			<?php echo $this->Paginator->first();?>
		 	<?php echo $this->Paginator->prev('< previous', "", "", "");?>
		 	<?php echo $this->Paginator->numbers();?>  
		 	<?php echo $this->Paginator->next('next >', "", "", "");?>
			<?php echo $this->Paginator->last();?>
		 	
		<p/>

		<span class="clear"></span>
	</div>	
</div>
