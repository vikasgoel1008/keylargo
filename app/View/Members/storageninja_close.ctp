<h2>Close StorageNinja Account</h2>

<?php echo $this->element('members_menu');?>

<?php echo $this->Form->create('Member', array('action' => 'storageninja_close'));?>
	
<fieldset>
	<br/>
	<h3>The account is scheduled to be closed on <?php echo $this->data['Member']['date_expire']; ?>.</h3>
	
	<?php echo $this->Form->hidden('id'); ?> 
	<?php echo $this->Form->hidden('storageninja_id'); ?> 
	
	<?php if ($this->data['Member']['date_expire'] == '0000-00-00 00:00:00' || $this->data['Member']['pay_type'] == 'WP') {$myCloseDate = date('Y-m-d');} else {$myCloseDate = $this->data['Member']['date_expire'];} ?>
	
	<?php echo $this->Form->input('my_expire_date',array('type'=>'text',
										'class'=>'small',
										'label'=>'Expire Date', 
										'value'=>$myCloseDate));?><br class="hid" /><p>Format: YYYY-MM-DD</p>
										
	<p></p>
	<?php echo $this->Form->end(array('label'=>'Close StorageNinja','class'=>'button submit'));?>
	<span class="clear"></span>
</fieldset>