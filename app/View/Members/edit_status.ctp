<h2>Update Member Address</h2>

<?php echo $this->element('members_menu');?>

<?php echo $this->Form->create('Member', array('action' => 'edit_status'));?>
<fieldset>
	<?php echo $this->Form->hidden('id'); ?> 

	<?php echo $this->Form->input('status',array('type'=>'text','class'=>'small','label'=>'Status'));?><br class="hid" />
	<?php echo $this->Form->input('date_cancel',array('type'=>'text','class'=>'small','label'=>'Cancel Date'));?><br class="hid" />
	<?php echo $this->Form->input('date_expire',array('type'=>'text','class'=>'tiny','label'=>'Expiration Date'));?><br class="hid" />
	<p></p>
	<?php echo $this->Form->end(array('label'=>'Update','class'=>'button submit'));?>
	<span class="clear"></span>
</fieldset>