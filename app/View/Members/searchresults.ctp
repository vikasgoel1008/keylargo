<!--<div class="Members search">
	<h2>Customer Search Results</h2>
		
	<?php //echo $this->element('members_menu');?>
	<?php $this->Paginator->options(array('url' => $this->passedArgs));?>
	
	<div class="box">
		<p>&nbsp;</p>
		<table cellpadding="0" cellspacing="0">
			<tr class="highlight">	
				<td colspan="5"><?php  echo $this->Paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)));?></td>
			</tr>
			
			<tr>
				<th><?php echo $this->Paginator->sort('email');?></th>
				<th><?php echo $this->Paginator->sort('name');?></th>
				<th><?php echo $this->Paginator->sort('status');?></th>
				<th><?php echo $this->Paginator->sort('username');?></th>

				<?php if ($ngdAdminUser) : ?>						                    
				<th>Actions</th>
				<?php endif; ?>
			</tr>
			
			<?php foreach ($members as $member): ?>
				<tr class="highlight">
					<td><?php echo $this->Html->link($member['Member']['email'],"/members/view/".$member['Member']['id']); ?></td>
					<td><?php echo $member['Member']['name'];?></td>
					<td><?php echo $member['Member']['status'];?></td>
					<td><?php echo $member['Member']['login_username'];?></td>
					
					<?php if ($ngdAdminUser) : ?>						                    
					<td class="action">
						<?php echo $this->Html->link("Close Account","/members/close/".$member['Member']['id']); ?>
					</td>
					<?php endif; ?>
				</tr>
			<?php endforeach; ?>
				
		</table>

		<p id="pagin">
			<?php echo $this->Paginator->first();?>
		 	<?php echo $this->Paginator->prev('< previous', "", "", "");?>
		 	<?php //echo $this->Paginator->prev() ?> 
			<?php echo $this->Paginator->numbers();?>  
		 	<?php //echo $this->Paginator->next() ?>
			<?php //echo $this->Paginator->counter(); ?>
			<?php echo $this->Paginator->next('next >', "", "", "");?>
			<?php echo $this->Paginator->last();?>
		<p/>

		<span class="clear"></span>
	</div>
</div>

-->
<script>
	$(document).ready(function(){
		var current_page = $('.current').text();
		$('.current').addClass('active');
		if(current_page==1)
		{
			$('.current').html("<a href='/members/searchresults'>1</a>");	
		}
		else
		{
			$('.current').html("<a href='/members/searchresults/page:"+current_page+"'>"+current_page+"</a>");
		}
		$('.user_name').each(function(){
			$(this).hover(function(){
				$(this).css('text-decoration','underline');
			},
			function(){
				$(this).css('text-decoration','none');
			});
		});
	});
</script>
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN PAGE TITLE & BREADCRUMB-->
		<h3 class="page-title">
		Search Results <small>search results</small>
		</h3>
		<ul class="page-breadcrumb breadcrumb">
			<li class="btn-group">
				<button type="button" class="btn blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
				<span>Actions</span><i class="fa fa-angle-down"></i>
				</button>
				<ul class="dropdown-menu pull-right" role="menu">
					<li>
						<a href="#">Action</a>
					</li>
					<li>
						<a href="#">Another action</a>
					</li>
					<li>
						<a href="#">Something else here</a>
					</li>
					<li class="divider">
					</li>
					<li>
						<a href="#">Separated link</a>
					</li>
				</ul>
			</li>
			<li>
				<i class="fa fa-home"></i>
				<a href="index.html">Home</a>
				<i class="fa fa-angle-right"></i>
			</li>
			<li>
				<a href="#">Extra</a>
				<i class="fa fa-angle-right"></i>
			</li>
			<li>
				<a href="#">Search Results</a>
			</li>
		</ul>
		<!-- END PAGE TITLE & BREADCRUMB-->
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="tabbable tabbable-custom tabbable-full-width">
			<ul class="nav nav-tabs">
				
				<li class="active">
					<a data-toggle="tab" href="#tab_1_5">
					User Search </a>
				</li>
			</ul>
			<div class="tab-content">
				<div id="tab_1_5" class="tab-pane1">
					<div class="row search-form-default">
						<div class="col-md-12">
							<?php echo $this->Form->create('Member',array('url'=>array('controller'=>'members','action'=>'searchresults'),'class'=>'form-inline')); ?>
								<div class="input-group">
									<?php echo $this->Form->input('searchterm',array('class'=>'form-control','placeholder'=>'Search...','value'=>$this->Session->read('memberSearch'),'div'=>array('class'=>'input-cont'),'label'=>false)); ?>
									
									<span class="input-group-btn">
										<?php //echo $this->Form->submit('Search <i class="m-icon-swapright m-icon-white"></i>',array('class'=>'btn green')); ?>
										<button type="submit" class="btn green">
											Search &nbsp; <i class="m-icon-swapright m-icon-white"></i>
										</button>
									</span>
								</div>
							<?php echo $this->Form->end(); ?>
						</div>
					</div>
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-advance table-hover">
							<thead>
								<tr>
									<th>
										 Photo
									</th>
									<th>
										 Email
									</th>
									<th>
										 Name
									</th>
									<th>
										 Username
									</th>
									<th>
										 Joined
									</th>
									
									<th>
										 Status
									</th>
									<th>
										 Actions
									</th>
									
							</tr>
							</thead>
							<tbody>
							<?php foreach($members as $member_data){ ?>
								<tr>
									<td>
										<?php $hash_email = md5( strtolower( trim($member_data['Member']['email'])));
										//echo $this->Html->image('http://www.gravatar.com/avatar/'.$hash_email.'?s=35',array('class'=>'img-responsive','style'=>'width:45px;'));
										echo $this->Html->link($this->Html->image('http://www.gravatar.com/avatar/'.$hash_email.'?s=35',array('class'=>'img-responsive','style'=>'width:45px;')),array('controller'=>'members','action'=>'view',$member_data['Member']['id']),array('class'=>'btn default btn-xs red-stripe','label'=>false,'escape'=>false,'style'=>"background-color:white;border:medium none;"));
										?>
										
									</td>
									<td>
										 <?php //echo $member_data['Member']['email']; ?>
										 <?php echo $this->Html->link($member_data['Member']['email'],array('controller'=>'members','action'=>'view',$member_data['Member']['id']),array('class'=>'btn-xs red-stripe user_name','label'=>false,'escape'=>false,'style'=>"color:black;border:medium none;")); ?>
										 
									</td>
									<td>
										 <?php echo $member_data['Member']['name']; ?>
									</td>
									<td>
										 <?php echo $member_data['Member']['login_username']; ?>
									</td>
									<td>
										 <?php echo $member_data['Member']['date_reg']; ?>
									</td>
									
									<td>
										<?php
										if($member_data['Member']['status']=="active")
										{
											$color="#1569C7";
										}
										elseif($member_data['Member']['status']=="closed")
										{
											$color="#F62817";
										}
										else
										{
											$color="#c0c04e";
										}
										
										?>
										<span class="label label-sm label-success" style="background-color:<?php echo $color;?>">
										<?php echo $member_data['Member']['status']; ?> </span>
									</td>
									<td>
										 <?php echo $this->Html->link('Close Account',array('controller'=>'members','action'=>'close',$member_data['Member']['id'])); ?>
									</td>
									
								</tr>
							<?php } ?>
							</tbody>
						</table>
					</div>
					<?php $paginate = $this->Paginator->numbers(); ?>
					<?php if($paginate){ ?>
					<div class="margin-top-20">
						<ul class="pagination">
						<li>
						<?php 
						   echo $this->Paginator->prev(
						   		'Prev',
						   		array(
						   				'class' => ' previous',
						   		)
						   );?>
						   </li>
							<?php echo $this->Paginator->numbers(array('separator'=>'','tag'=>'li')); ?>
							
							<li>
							<?php 
							   echo $this->Paginator->next(
							   		'Next',
							   		array(
							   				'class' => 'next',
							   		)
							   );
							   ?>
							</li>
						</ul>
					</div>
					<?php } ?>
				</div>
				<!--end tab-pane-->
			</div>
		</div>
	</div>
	<!--end tabbable-->
</div>