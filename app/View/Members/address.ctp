<h2>Update Member Address</h2>

<?php echo $this->element('members_menu');?>

<?php echo $this->Form->create('Member', array('action' => 'address'));?>
<fieldset>
	<?php echo $this->Form->hidden('id'); ?> 

	<?php echo $this->Form->input('address',array('type'=>'text','class'=>'small','label'=>'Address'));?><br class="hid" />
	<?php echo $this->Form->input('address2',array('type'=>'text','class'=>'small','label'=>'Address 2'));?><br class="hid" />
	<?php echo $this->Form->input('city',array('type'=>'text','class'=>'tiny','label'=>'City'));?><br class="hid" />
	<?php echo $this->Form->input('state',array('type'=>'text','class'=>'tiny','label'=>'State'));?><br class="hid" />
	<?php echo $this->Form->input('postal',array('type'=>'text','class'=>'tiny','label'=>'Postal'));?><br class="hid" />
	<?php echo $this->Form->input('country',array('type'=>'text','class'=>'tiny','label'=>'Country'));?><br class="hid" />
	<p></p>
	<?php echo $this->Form->end(array('label'=>'Update','class'=>'button submit'));?>
	<span class="clear"></span>
</fieldset>