<p id="tip" class="alert">
			<span class="txt"><span class="icon"></span><strong>Tip:</strong> Enter full or partial email addresses, first name, last name, etc.</span>
			<a title="Close" class="close"><span class="bg"></span>Close</a>
		</p>
		<h2>Customer Search</h2>
		<ul class="tabs">
			<li class="active"><a href="#" title="Item1"><span>Search</span></a></li>
		</ul>
		<?php echo $this->Form->create('Member',array('url'=>'/members/searchresults'));?>
		<fieldset>
			<?php echo $this->Form->input('searchterm',array('type'=>'text','class'=>'medium','label'=>'Search Text'));?>
			<p>Any text can be used to search</p>
			<?php echo $this->Form->end(array('label'=>'Submit','class'=>'button submit'));?>
			<p></p>
			<span class="clear"></span>
		</fieldset>
		
		<!--<form method="post" action="">
		<?php echo $this->Form->create('Post',array('url'=>'/members/searchresults'));?>
		<fieldset>			
			<label>Search Text</label>
			<input class="medium" name="searchterm" type="text" value="" /><br class="hid" />
			<p>Any text can be used to search</p>
			<a href="#" class="button submit" title="Submit"><span>Submit</span></a>
			<span class="clear"></span>
		</fieldset>-->