<h2>Customer Info</h2>

<?php echo $this->element('members_menu');?>

<?php echo $this->Form->create('Member');?>
<fieldset>
	<?php echo $this->Form->hidden('id'); ?> 

	<?php echo $this->Form->input('name',array('type'=>'text','class'=>'small','label'=>'Name'));?><br class="hid" />
	<?php echo $this->Form->input('email',array('type'=>'text','class'=>'small','label'=>'Email Address'));?><br class="hid" />
	<?php echo $this->Form->input('login_password',array('type'=>'text','class'=>'tiny','label'=>'Password'));?><br class="hid" />
	<?php echo $this->Form->input('status',array('type'=>'text','class'=>'tiny','label'=>'Status'));?><br class="hid" />
	<?php echo $this->Form->input('hw_cust_id',array('type'=>'text','class'=>'tiny','label'=>'HW Customer ID'));?><br class="hid" />
	<?php echo $this->Form->input('date_cancel',array('type'=>'text','class'=>'tiny','label'=>'Cancel Date'));?><br class="hid" />
	<?php echo $this->Form->input('date_expire',array('type'=>'text','class'=>'tiny','label'=>'Expiration Date'));?><br class="hid" />
	<p></p>
	<?php echo $this->Form->end(array('label'=>'Update','class'=>'button submit'));?>
	<span class="clear"></span>
</fieldset>