<h2>Marketing</h2>

<?php echo $this->element('members_menu');?>

<?php echo $this->Form->create('Member', array('action' => 'edit_marketing'));?>
<fieldset>
	<br/>
	<h3>Modify marketing preferences for <?php echo $this->data['Member']['email']; ?>.</h3>
	
	<?php echo $this->Form->hidden('id'); ?> 

	<?php echo $this->Form->input('receive_email',array('options'=>$yesNo,'type'=>'select','class'=>'small','label'=>'Receive Email?'));?><br class="hid" />
	<?php echo $this->Form->input('Mailchimp.mailchimp_web_id',array('type'=>'text','class'=>'small','label'=>'Mailchimp Web Id'));?><br class="hid" />
	<p></p>
	<?php echo $this->Form->end(array('label'=>'Update','class'=>'button submit'));?>
	<span class="clear"></span>
</fieldset>