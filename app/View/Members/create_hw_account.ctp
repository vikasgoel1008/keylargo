<h2>Create Account HW</h2>

<?php echo $this->element('members_menu');?>

<?php echo $this->Form->create('Member', array('action' => 'create_hw_account'));?>
<fieldset>
	
	<?php echo $this->Form->input('id',array('type'=>'hidden','class'=>'small','label'=>'Email Address'));?><br class="hid" />
	<?php echo $this->Form->input('login_password',array('type'=>'text','class'=>'small','label'=>'Password'));?><br class="hid" />
	
	<p></p>
	<?php echo $this->Form->end(array('label'=>'Create HW Account','class'=>'button submit'));?>
	<span class="clear"></span>
</fieldset>