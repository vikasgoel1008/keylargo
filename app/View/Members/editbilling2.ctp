	<?php echo $message;?>
	<h2>Customer Info</h2>
		<ul class="tabs">
			<li><a href="/members/edit/<?php echo $id;?>" title="Item1"><span>Account Info</span></a></li>
			<li class="active"><a href="/members/editbilling/<?php echo $id;?>" title="Item2"><span>Billing</span></a></li>
			<li><a href="/members/editusenet/<?php echo $id;?>" title="Item3"><span>Usenet</span></a></li>
		</ul>
		<?php echo $this->Form->create('Member',array('url'=>'/members/editbilling/'.$id));?>
		<fieldset>
			<?php echo $this->Form->input('cc_card_number',array('type'=>'text','class'=>'small','label'=>'Card Number','value'=>'xxxxxxxxxxxx'.substr($member['PaymentDetail']['cc_card_nbr'],strlen($member['PaymentDetail']['cc_card_nbr'])-4,strlen($member['PaymentDetail']['cc_card_nbr']))));?><br class="hid" />
			<?php echo $this->Form->input('cc_card_exp',array('type'=>'text','class'=>'small','label'=>'Exp Date','value'=>$member['PaymentDetail']['cc_card_exp']));?><br class="hid" />
			<p>Use YYYYMM format</p>
			<?php echo $this->Form->input('coupon_code',array('type'=>'text','class'=>'tiny','label'=>'Coupon','value'=>$member['Member']['coupon_code']));?><br class="hid" />
			<?php echo $coupon_note;?>
			<?php echo $this->Form->input('plan_id',array('type'=>'text','class'=>'tiny','label'=>'Plan','value'=>$member['Member']['plan_id']));?><br class="hid" />
			<p></p>
			<?php echo $this->Form->end(array('label'=>'Update','class'=>'button submit'));?>
		</fieldset>
		<span class="clear"></span>
		<h2>Invoice History</h2>
		<div class="box">
			<table>
				<tr>
					<th class="col1">&nbsp;&nbsp;&nbsp;&nbsp;Date/Time</th>
					<th class="col2">&nbsp;&nbsp;&nbsp;Transaction #</th>
					<th class="col3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Amount</th>
					<th class="col4">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Result</th>
					<th class="col5">&nbsp;&nbsp;&nbsp;&nbsp;Actions</th>
				</tr>
				<?php foreach ($invoices as $invoice): ?>
					<tr class="highlight">
						<td><?php echo $invoice['CcTransactions']['trans_date'];?></td>
						<td><?php echo $invoice['CcTransactions']['order_nbr'];?></td>
						<td>$<?php echo $invoice['CcTransactions']['amount'];?></td>
						<td><?php echo $invoice['CcTransactions']['approved'];?></td>
						<td class="action">
							<ul>
								<li><a class="view" href="#" title="view"></a></li>
								<li><a class="delete" href="#" title="delete"></a></li>
								<li><a class="mark" href="#" title="mark"></a></li>
							</ul>
						</td>
					</tr>
				<?php endforeach; ?>
			</table>
			<p id="pagin">
				&laquo; <a href="#" title="">previous</a> &ndash;
				<a href="#" title="">1</a>
				<a href="#" title="">2</a>
				<a href="#" title="">3</a>
				<a href="#" title="">4</a>
				<a href="#" title="">5</a>
				<a href="#" title="">6</a>
				<a href="#" title="">7</a>
				<a href="#" title="">8</a>
				<a href="#" title="">9</a>
				&ndash; <a href="#" title="">next</a> &raquo;
			</p>
		</div>
