<div class="ccTransactions index">

	<h2>Accounts per IP</h2>
	
	<?php echo $this->element('members_menu');?>
	<?php $this->Paginator->options(array('url' => $this->passedArgs));?>
	
	<div class="box">
		<p>&nbsp;</p>
	
		<table cellpadding="0" cellspacing="0">
			<tr class="highlight">
				<td colspan="4"><?php echo $this->Html->link(__('Blacklist', true), array('action' => 'blacklist_ip', $this->Session->read('Member.ip'), 'free account fraud')); ?></td>				
			</tr>
			<tr class="highlight">
				<td><?php echo $this->Paginator->sort('date_reg');?></td>
				<td><?php echo $this->Paginator->sort('email');?></td>
				<td><?php echo $this->Paginator->sort('ip');?></td>
				<td><?php echo $this->Html->link(__('Close Free Accounts', true), array('action' => 'close_free_by_ip', $this->Session->read('Member.ip'))); ?></td>
			</tr>
		
			<?php foreach ($members as $member): ?>
			<tr  class="highlight">
				<td><?php echo $member['Member']['date_reg']; ?></td>
				<td><?php echo $this->Html->link($member['Member']['email'], 'view/'.$member['Member']['id']); ?></td>
				<td><?php echo $member['Member']['ip']; ?></td>
				<td>
					<?php 
						if (($member['Member']['status'] == 'closed')) {
							echo "Account Closed";	
						}
						elseif ($member['Member']['date_expire'] != '0000-00-00 00:00:00') {
							echo "Account Closed";	
						}
						else {
							echo $this->Html->link('Cancel', 'cancel/'.$member['Member']['id']."/".date("Y-m-d"), null, sprintf(__('Are you sure you want to cancel the account %s?', true), $member['Member']['email']));					
						} 
					?>
				</td>
			</tr>
			<?php endforeach; ?>
			<tr class="highlight">
				<td colspan="4"><?php  echo $this->Paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)));?></td>
			</tr>
			
		</table>
		
		<p id="pagin">
			<?php echo $this->Paginator->first();?>
		 	<?php echo $this->Paginator->prev('< previous', "", "", "");?>
		 	<?php //echo $this->Paginator->prev() ?> 
			<?php echo $this->Paginator->numbers();?>  
		 	<?php //echo $this->Paginator->next() ?>
			<?php //echo $this->Paginator->counter(); ?>
			<?php echo $this->Paginator->next('next >', "", "", "");?>
			<?php echo $this->Paginator->last();?>
		 	
		<p/>
		
		<span class="clear"></span>
	
	</div>
</div>