<h2>Update Bill Status</h2>

<?php echo $this->element('members_menu');?>

<?php echo $this->Form->create('Member', array('action' => 'next_bill_date'));?>
<fieldset>
	<?php echo $this->Form->hidden('id'); ?> 

	<?php echo $this->Form->input('name',array('type'=>'text','class'=>'small','label'=>'Name','readonly'=>'readonly'));?><br class="hid" />
	<?php echo $this->Form->input('date_reg',array('type'=>'text','class'=>'small','label'=>'Registration Date','readonly'=>'readonly'));?><br class="hid" />
	<?php echo $this->Form->input('next_bill_date',array('type'=>'text','class'=>'small','label'=>'Next Bill Date'));?><br class="hid" /><p>Format: YYYY-MM-DD</p>
	<?php echo $this->Form->input('is_bill_fail',array('type'=>'text','class'=>'small','label'=>'Bill Fail'));?><br class="hid" /><p>Y / N</p>
	<?php echo $this->Form->input('recent_failures',array('type'=>'text','class'=>'small','label'=>'Number Failures'));?><br class="hid" /><p>Interger 0-7</p>
	<p></p>
	<?php echo $this->Form->end(array('label'=>'Update','class'=>'button submit'));?>
	<span class="clear"></span>
</fieldset>