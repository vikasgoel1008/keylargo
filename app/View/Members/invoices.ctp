
<h2>Customer Info</h2>

<?php echo $this->element('members_menu');?>

<div class="box">
	<p>&nbsp;</p>

	<table>
		<tr>
			<th class="col1">Date</th>
			<th class="col2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Amount</th>
			<th class="col3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Status</th>
			<th class="col4">&nbsp;&nbsp;&nbsp;&nbsp;Type</th>
			<th class="col4">&nbsp;&nbsp;&nbsp;&nbsp;Reason</th>
			<th class="col5">&nbsp;&nbsp;&nbsp;&nbsp;Actions</th>
		</tr>
		<?php foreach ($invoices as $invoice): ?>
		<tr class="highlight">
			<td><?php echo date("m/d/Y H:i:s",strtotime($invoice['CcTransaction']['trans_date'])); ?></td>
			<td><?php echo $invoice['CcTransaction']['amount'];?></td>
			<td><?php echo $invoice['CcTransaction']['approved'];?></td>
			<td><?php echo $invoice['CcTransaction']['trans_type'];?></td>
			<td><?php echo $invoice['CcTransaction']['trans_purpose'];?></td>
			<td class="action">
				<ul>
					<li><?php echo $this->Html->link("",array('controller'=>'CcTransactions', 'action'=>'view', $invoice['CcTransaction']['id']),array('escape'=>false, 'class'=>'edit','title'=>'view')); ?></li>
							
					<li><a class="delete" href="#" title="delete"></a></li>
					<li><a class="mark" href="#" title="mark"></a></li>
				</ul>
			</td>
		</tr>
		<?php endforeach; ?>

	</table>
	
	
	<span class="clear"></span>
	
	<?php echo $this->Form->create('Member');?>
	<?php echo $this->Form->hidden('Member.id'); ?> 
	<?php echo $this->Form->end();?>	
</div>
