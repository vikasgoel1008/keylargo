<div class="ccTransactions index">
	<h2>Recent Sales Last <?php echo $dayslength;?> Days</h2>
	
	<?php echo $this->element('members_menu');?>
	<?php $this->Paginator->options(array('url' => $this->passedArgs));?>
	
	<div class="box">
		<table cellpadding="0" cellspacing="0">
			<tr class="highlight">	
				<td colspan="5"><?php  echo $this->Paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)));?></td>
			</tr>
			<tr class="highlight">	
				<td><?php echo $this->Paginator->sort('date_reg');?></td>
				<td><?php echo $this->Paginator->sort('name');?></td>		
				<td><?php echo $this->Paginator->sort('email');?></td>
				<td><?php echo $this->Paginator->sort('plan_id');?></td>
				<td>Void/Refund</td>
			</tr>
			<?php foreach ($members as $member): ?>
				<tr  class="highlight">
					<td>
						<?php echo $member['Member']['date_reg']; ?>
					</td>
					<td>
						<?php echo $member['Member']['name']; ?>
					</td>
					<td>
						<?php echo $this->Html->link($member['Member']['email'], 'view/'.$member['Member']['id']); ?>
					</td>
					<td>
						<?php echo $member['Plan']['name']; ?>
					</td>
					<td style="padding: 10px">
					  <form action="/members/void" method="POST">
						<input type="hidden" name="transaction_id" value="<?php echo $member['CCTransaction']['order_nbr']; ?>">
						<input type="hidden" name="amount" value="<?php echo $member['CCTransaction']['amount'];?>">
						<input type="hidden" name="auth_cust_id" value="<?php echo $member['Member']['auth_cust_id']; ?>">
						<input type="hidden" name="auth_payment_id" value="<?php echo $member['Member']['auth_payment_id']; ?>">
						<input type="hidden" name="auth_shipping_id" value="<?php echo $member['Member']['auth_shipping_id']; ?>">
						<input type="hidden" name="member_id" value="<?php echo $member['Member']['id']; ?>">
						<input type="submit" value="Void/Refund Order" style="height: auto">
					  </form>
					</td>
				</tr>
			<?php endforeach; ?>
		</table>


		<p id="pagin">
			<?php echo $this->Paginator->first();?>
		 	<?php echo $this->Paginator->prev('< previous', "", "", "");?>
		 	<?php //echo $this->Paginator->prev() ?> 
			<?php echo $this->Paginator->numbers();?>  
		 	<?php //echo $this->Paginator->next() ?>
			<?php //echo $this->Paginator->counter(); ?>
			<?php echo $this->Paginator->next('next >', "", "", "");?>
			<?php echo $this->Paginator->last();?>
		 	
		</p>

		<span class="clear"></span>
	</div>	
</div>