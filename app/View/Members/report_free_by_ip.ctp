<div class="ccTransactions index">
	<h2>Active Free Accounts by IP</h2>
	<?php echo $this->element('members_menu');?>
	<?php $this->Paginator->options(array('url' => $this->passedArgs));?>

	<div class="box">
		<p>&nbsp;</p>
		<table cellpadding="0" cellspacing="0">
		<tr class="highlight">	
			<td colspan="4"><?php  echo $this->Paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)));?></td>
		</tr>
		<tr class="highlight">	
			<td><?php echo $this->Paginator->sort('ip');?></td>
			<td><?php echo $this->Paginator->sort('active_free_accounts');?></td>
		</tr>
		<?php foreach ($members as $member): ?>
			<tr class="highlight">
				<td><?php echo $this->Html->link($member['Member']['ip'], 'report_ip/'.$member['Member']['ip']); ?></td>
				<td><?php echo $member[0]['active_free_accounts']; ?></td>
			</tr>
		<?php endforeach; ?>
		</table>

		<p id="pagin">
			<?php echo $this->Paginator->first();?>
		 	<?php echo $this->Paginator->prev('< previous', "", "", "");?>
		 	<?php //echo $this->Paginator->prev() ?> 
			<?php echo $this->Paginator->numbers();?>  
		 	<?php //echo $this->Paginator->next() ?>
			<?php //echo $this->Paginator->counter(); ?>
			<?php echo $this->Paginator->next('next >', "", "", "");?>
			<?php echo $this->Paginator->last();?>
		 	
		<p/>
		<span class="clear"></span>
	</div>	
</div>
