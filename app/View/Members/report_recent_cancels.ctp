<?php $this->Paginator->options(array('url' => $this->passedArgs));?>
			
<div class="ccTransactions index">
	<h2>Recent Cancels Last <?php echo $dayslength;?> Days</h2>
	<?php echo $this->element('members_menu');?>	

	<div class="box">
		<p>
			&nbsp;<?php  echo $this->Paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)));?><br/><br/>
		</p>

		<table cellpadding="0" cellspacing="0">
		<tr  class="highlight">
			<td colspan="5"><?php echo $this->Html->link(__('See Cancellations for Last 30 Days', true), array('action' => 'report_cancel_graph')); ?></td>
			
		</tr>
		<tr class="highlight">	
			<td><?php echo $this->Paginator->sort('name');?></td>
			<td><?php echo $this->Paginator->sort('email');?></td>
			<td><?php echo $this->Paginator->sort('plan_id');?></td>
			<td><?php echo $this->Paginator->sort('date_reg');?></td>
			<td><?php echo $this->Paginator->sort('date_cancel');?></td>
			
		</tr>
		<?php foreach ($members as $member): ?>
			<tr  class="highlight">
				<td>
					<?php echo $member['Member']['name']; ?>
				</td>
				<td>
					<?php echo $this->Html->link($member['Member']['email'], 'view/'.$member['Member']['id']); ?>
				</td>
				<td>
					<?php echo $member['Plan']['name']; ?>
				</td>
				<td>
					<?php echo $member['Member']['date_reg']; ?>
				</td>
				<td>
					<?php echo $member['Member']['date_cancel']; ?>
				</td>
				
			</tr>
		<?php endforeach; ?>
		</table>
		
		<p id="pagin">
			<?php //echo $this->Paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
		 	<?php echo $this->Paginator->numbers();?>  	
			<?php //echo $this->Paginator->next(__('next', true).' >>', array(), null, array('class' => 'disabled'));?>
		<p/>
		
		<span class="clear"></span>
		
	</div>
</div>
	
