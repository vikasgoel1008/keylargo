<h2>Create Customer Account</h2>

<?php echo $this->element('members_menu');?>

<?php echo $this->Form->create('Member', array('action' => 'create_account'));?>
<fieldset>
	
	<?php echo $this->Form->input('email',array('type'=>'text','class'=>'small','label'=>'Email Address'));?><br class="hid" />
	<?php echo $this->Form->input('name',array('type'=>'text','class'=>'small','label'=>'Name'));?><br class="hid" />
	<?php echo $this->Form->input('address',array('type'=>'text','class'=>'small','label'=>'Address 1'));?><br class="hid" />
	<?php echo $this->Form->input('address2',array('type'=>'text','class'=>'small','label'=>'Address 2'));?><br class="hid" />
	<?php echo $this->Form->input('city',array('type'=>'text','class'=>'tiny','label'=>'City'));?><br class="hid" />
	<?php echo $this->Form->input('state',array('type'=>'text','class'=>'tiny','label'=>'State'));?><br class="hid" />
	<?php echo $this->Form->input('postal',array('type'=>'text','class'=>'tiny','label'=>'Postal Code'));?><br class="hid" />
	<?php echo $this->Form->input('plan_id',array('type'=>'text','class'=>'tiny','label'=>'Plan'));?><br class="hid" />

	<?php echo $this->Form->input('free_admin_acct',array('options'=>array("Y"=>"Y", "N"=>"N"), 'type'=>'select','class'=>'tiny','label'=>'Free Admin Account','empty' => true));?><br class="hid" />
	
	<p></p>
	<?php echo $this->Form->end(array('label'=>'Create Account','class'=>'button submit'));?>
	<span class="clear"></span>
</fieldset>