<div class="ccTransactions index">
	<h2>Member Details</h2>

	<?php echo $this->element('members_menu');?>

	<div class="box">
		<p>&nbsp;</p>
		<table cellpadding="0" cellspacing="0">
		
			<tr class="highlight">
				<td colspan="3">
					<?php echo $this->Html->link('Edit Member', 'edit/'. $member['Member']['id']);?> |
					<?php echo $this->Html->link('Promo Info', 'promo/'. $member['Member']['id']);?> |
					<?php echo $this->Html->link('Bandwidth', array('controller'=>'hw_datas', 'action'=>'report_daily_bandwidth_usage', 180, $member['Member']['id']));?> | 
					<?php echo $this->Html->link("IP's", array('controller'=>'hw_datas', 'action'=>'member_ips', $member['Member']['id']));?>
				</td>
			</tr>
			
			<tr class="highlight"><td colspan="6"><?php echo $member['Member']['name']; ?> | <em><?php echo $member['Member']['email']; ?></em> | <?php echo $member['Member']['login_username']; ?></td></tr>
			
			<?php if (!empty($chi)): ?>
			<tr class="highlight"><td colspan="6">CHI:  <?php echo $chi['t_chi1']['bw_score'] + $chi['t_chi1']['c_score'] + $chi['t_chi1']['i_score']; ?> </td></tr>
			<?php endif; ?>
		
			<?php if ($member['Member']['is_free_trial'] == "Y"): ?>
			<tr class="highlight"><td colspan="3"><span class="notgood">This customer is currently within their free trial period.</span></td></tr>
			<?php elseif ($member['Member']['free_admin_acct'] == "Y"): ?>
			<tr class="highlight"><td colspan="3"><span class="notgood">This customer has a free admin account.</span></td></tr>
			<?php endif; ?>
			
			<tr><td colspan="3"</td></tr>
			
			<tr class="highlight">
				<td colspan="3">
					<?php echo $this->Html->link('View Bandwidth', array('controller'=>'hw_datas', 'action'=>'report_graph_daily_bw_usage', 180, $member['Member']['id']));?>
				</td>
			</tr>
			
			<tr class="highlight">
				<td>IP:</td>	
				<td><?php echo $this->Html->link($member['Member']['ip'],"report_ip/".$member['Member']['ip']); ; ?></td>			
				<td></td>
			</tr>
			<tr class="highlight">
				<td>Address:</td>

				<td>
					<?php 
						$display = $member['Member']['address'] . " ";
						
						if (!empty($member['Member']['address2'])) {
							$display.= $member['Member']['address2'] . " ";
						}
						
						$display .= $member['Member']['city'] . ", ";
						$display .= $member['Member']['state'] . " ";
						$display .= $member['Member']['postal'] . " ";
						$display .= $member['Member']['country'];
						
						echo $this->Html->link($display,"/members/address/".$member['Member']['id']);
					?>				
				</td>				
				<td></td>
			</tr>
		
			<tr class="highlight"><td colspan="3"></td></tr>
			<tr class="highlight">
				<td>Status:</td>	
				<td>
					<?php 	echo $member['Member']['status'];
							
							if ($member['Member']['admin_fail_bill'] == 'Y') {
								echo " | <span class='notgood'>forced failure</class>"; 
								
							}
							
							elseif ($member['Member']['status'] == 'active') {
								echo " | "; 
								echo $this->Html->link('revoke', 'cancel/'.$member['Member']['id']."/".date("Y-m-d"), null, sprintf(__('This will close the account immediately.  \nAre you sure you want to cancel the account %s?', true), $member['Member']['email']))  . " | ";
								echo $this->Html->link('close', 'close/'.$member['Member']['id'], null, sprintf(__('This will close the account on the date provided on the next page.  \nAre you sure you want to close the account %s?', true), $member['Member']['email']))  . " | ";
								echo $this->Html->link('force fail', 'fail_rebill/'.$member['Member']['id'], null, sprintf(__('This will force the members rebill to fail.  \nAre you sure you want to force fail the account %s?', true), $member['Member']['email']));
								
							}
							
							elseif ($member['Member']['status'] == 'block-used') {
								echo " | "; 
								echo $this->Html->link('reactivate', 'reactivate_account/'.$member['Member']['id'], null, sprintf(__('This will reactivate the customers account, setting the status to active and clearing the expiration and cancel dates.  \n\nAre you sure you want to reset this account %s?', true), $member['Member']['email'])) ;
								
							}
							
							elseif ($member['Member']['status'] == 'closed') {
								echo " | "; 
								echo $this->Html->link('re-activate', 'reactivate_account/'.$member['Member']['id'], null, sprintf(__('This will reactivate the customers account, setting the status to active and clearing the expiration and cancel dates.  \n\nAre you sure you want to reset this account %s?', true), $member['Member']['email'])) ;
								
							}
					?>
				</td>			
				<td></td>
			
			</tr>
			<tr class="highlight">
				<td>Username:</td>				
				<td><?php echo $member['Member']['login_username']; ?> | <?php echo $this->Html->link("reset password","password_reset/".$member['Member']['id']); ?></td>
				<td></td>
			</tr>
			
			<tr class="highlight">
				<td>Plan:</td>	

				<td>
					<?php echo $member['Plan']['name']; ?> (<?php echo $this->Html->link($member['Member']['plan_id'],"/plans/view/".$member['Member']['plan_id']); ?>)
					<?php if ($member['Member']['auto_recycle'] == 'Y') echo " | auto recycle";?> 
					<?php if ($member['Member']['pay_type'] <> "WP") echo " | " . $this->Html->link("change plan","change_plan/".$member['Member']['id']); ?>
				</td>			
				<td></td>
			</tr>
			
			<?php if ($member['Plan']['unlimited'] == 'Y') : ?>
			<tr class="highlight">
				<td>StorageNinja Id:</td>	

				<td>
					<?php if (empty($member['Member']['storageninja_id'])) :?>
					<?php echo $this->Html->link("Create Account","create_storageninja_account/".$member['Member']['id']); ?>
					<?php else: ?>
					<?php echo $member['Member']['storageninja_id']; ?> | 
					<?php echo $this->Html->link("close storageninja", array('controller' => 'members', 'action' => 'storageninja_close', $member['Member']['id'])); ?>
					<?php endif; ?>
				</td>			
				
				<td></td>
			</tr>
			<?php endif; ?>
			
			<tr class="highlight"><td colspan="3"></td></tr>
			<tr class="highlight">
				<td>Pay Type:</td>

				<td>
					<?php
							
							echo $member['Member']['pay_type']; 
							
							//display transaction number if it is set
							if (!empty($member['Member']['transaction_number'])) {
								echo " | " . $this->Html->link($member['Member']['transaction_number'],"edit_pay_type/".$member['Member']['id']); 
							}
							
							if ($member['Member']['pay_type'] == 'WP' && !empty($member['Member']['payment_proc_id'])) {
								echo " | " . $this->Html->link($member['Member']['payment_proc_id'],"edit_pay_type/".$member['Member']['id']); 
							}
							
							
					?> 
				
				</td>				
				<td><?php echo $this->Html->link('update',"edit_pay_type/".$member['Member']['id']); ?></td>
			</tr>
			<tr class="highlight">
				<td>Coupon:</td>
				<td>
					<?php echo $this->Html->link($member['Coupon']['coupon_desc'], array('controller'=>'coupons','action'=>'view', $member['Member']['coupon_id']));?>
					
					<?php if (!empty($member['Coupon']['coupon_desc'])): ?>
	
						| <?php echo $member['Member']['promo_remain']; ?> remain
					
					<?php endif; ?>
				
				</td>
				<td><?php echo $this->Html->link('update',"edit_coupon/".$member['Member']['id']); ?></td>
			</tr>
			<tr class="highlight">
				<td>Marketing:</td>
				<td>
					<?php echo $member['Member']['receive_email']; ?>
					
					<?php if (!empty($member['Mailchimp']['mailchimp_web_id'])) : ?>
					 |  <?php echo $this->Html->link($member['Mailchimp']['mailchimp_web_id'], 'https://us1.admin.mailchimp.com/lists/members/view?id='.$member['Mailchimp']['mailchimp_web_id'], array('target' => '_blank'));?>
					<?php elseif (!empty($member['Mailchimp']['mailchimp_id'])) : ?>
					 |  <?php echo $member['Mailchimp']['mailchimp_id']; ?>
					<?php endif; ?>
					 |  <?php echo $member['Mailchimp']['last_update_date']; ?>
					
				</td>
				<td><?php echo $this->Html->link('update',"edit_marketing/".$member['Member']['id']); ?></td>
			</tr>
			
			
			<tr class="highlight"><td colspan="3"></td></tr>
			<tr class="highlight">
				<td>Registration Date:</td>				
				<td><?php echo $member['Member']['date_reg']; ?></td>
				<td></td>
			</tr>
			<tr class="highlight">
				<td>Upgrade Date:</td>				
				<td><?php echo $member['Member']['last_upgrade_date']; ?></td>
				<td></td>
			</tr>
			<tr class="highlight">
				<td>Next Bill Date:</td>				

				<td>
					<?php echo $member['Member']['next_bill_date'];?>
					<?php if ($member['Member']['is_bill_fail'] == 'Y'): echo " | ";?>
					<span class="notgood">Days Late:  <?php echo $member['Member']['recent_failures'];?>
					<?php endif;?>
				</td>
				<td><?php echo $this->Html->link('update', 'next_bill_date/'.$member['Member']['id']);?></td>
			</tr>
			<tr class="highlight">
				<td>Last Bill Attempt:</td>				
				<td>
					<?php echo $member['Member']['last_bill_attempt']; ?>

					<?php if ($member['Member']['is_bill_fail'] == 'Y' && $member['Member']['status'] == 'active' && $member['Member']['last_bill_attempt'] == date("Y-m-d")): echo " | ";?>
					<?php echo $this->Html->link("Rebill the Account", array('action'=>'rebill', $member['Member']['id']));?>
					
					<?php endif;?>

				</td>
				<td></td>
			</tr>
			<tr class="highlight">
				<td>Cancel Date:</td>				
				<td><?php echo $member['Member']['date_cancel']; ?></td>
				<td></td>
			</tr>
			<tr class="highlight">
				<td>Expiration Date:</td>				
				<td><?php echo $member['Member']['date_expire']; ?></td>
				<td></td>
			</tr>
			
			
		
		</table>
		
		<p id="pagin">
			&nbsp;
		</p>
		
		<span class="clear">&nbsp;</span>
	</div>
</div>
	