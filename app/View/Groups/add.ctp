
	
<p id="tip" class="alert">
	<span class="txt"><span class="icon"></span><strong>Tip:</strong> Enter full or partial email addresses, first name, last name, etc.</span>
	<a title="Close" class="close"><span class="bg"></span>Close</a>
</p>
<h2>Add a Group</h2>
<ul class="tabs">
	<li class="active"><a href="#" title="Item1"><span>Groups</span></a></li>
</ul>
<?php echo $this->Form->create('Group');?>
<fieldset>
	<?php echo $this->Form->input('name',array('type'=>'text','class'=>'medium','label'=>'Group Name'));?>
	<p>&nbsp;</p>
	<?php echo $this->Form->end(array('label'=>'Submit','class'=>'button submit'));?>
	<p></p>
	<span class="clear"></span>
	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('List Groups', true), array('action' => 'index'));?></li>
		</ul>
	</div>
</fieldset>
