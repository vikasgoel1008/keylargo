		<h2>Groups Search Results</h2>
		<ul class="tabs">
			<li class="active"><a href="#" title="Item1"><span>Basic View</span></a></li>
			<li><a href="#" title="Item2"><span>Alternate View</span></a></li>
		</ul>
		<div class="box">
			<table cellpadding="0" cellspacing="0">
				<tr>
					<th><?php echo $this->Paginator->sort('id');?></th>
					<th><?php echo $this->Paginator->sort('name');?></th>
					<th><?php echo $this->Paginator->sort('created');?></th>
					<th><?php echo $this->Paginator->sort('modified');?></th>
					<th class="actions"><?php __('Actions');?></th>
				</tr>
				
				<?php foreach ($groups as $group): ?>
					<tr class="highlight">
						<td>
							<?php echo $group['Group']['id']; ?>
						</td>
						<td>
							<?php echo $group['Group']['name']; ?>
						</td>
						<td>
							<?php echo $group['Group']['created']; ?>
						</td>
						<td>
							<?php echo $group['Group']['modified']; ?>
						</td>
						
						<td class="actions">
							<?php echo $this->Html->link(__('View', true), array('action' => 'view', $group['Group']['id'])); ?>
							<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $group['Group']['id'])); ?>
							<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $group['Group']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $group['Group']['id'])); ?>
						</td>
					</tr>
				<?php endforeach; ?>
			</table>
			
			<div class="paging">
				<p id='pagin'>
				<?php echo $this->Paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
			 | 	<?php echo $this->Paginator->numbers();?>
				<?php echo $this->Paginator->next(__('next', true).' >>', array(), null, array('class' => 'disabled'));?>
				</p>
			</div>
			
			<div class="actions">
				<ul>
					<li><?php echo $this->Html->link(__('New Group', true), array('action' => 'add')); ?></li>
				</ul>
			</div>
			
		</div>


