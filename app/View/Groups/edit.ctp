<p id="tip" class="alert">
	<span class="txt"><span class="icon"></span><strong>Tip:</strong> Enter full or partial email addresses, first name, last name, etc.</span>
	<a title="Close" class="close"><span class="bg"></span>Close</a>
</p>
<h2>Edit Group</h2>
<ul class="tabs">
	<li class="active"><a href="#" title="Item1"><span>Group</span></a></li>
</ul>
<?php echo $this->Form->create('Group');?>
<fieldset>
	<?php echo $this->Form->input('id',array('type'=>'text','class'=>'medium','label'=>'Group Id'));?>
	<?php echo $this->Form->input('name',array('type'=>'text','class'=>'medium','label'=>'Group Name'));?>
	<p>&nbsp;</p>
	<?php echo $this->Form->end(array('label'=>'Submit','class'=>'button submit'));?>
	<p></p>
	<span class="clear"></span>
	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $this->Form->value('Group.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $this->Form->value('Group.id'))); ?></li>
			<li><?php echo $this->Html->link(__('List Groups', true), array('action' => 'index'));?></li>
		</ul>
	</div>
</fieldset>
