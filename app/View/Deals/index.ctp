<div class="deals index">
	<h2>Deals</h2>
	
	<?php echo $this->element('deals_menu');?>
	<?php $this->Paginator->options(array('url' => $this->passedArgs));?>
	
	<div class="box">
		<p>&nbsp;</p>
		<table cellpadding="0" cellspacing="0">
			<tr class="highlight">	
				<td colspan="7"><?php  echo $this->Paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)));?></td>
			</tr>
			
			<tr class="highlight">
				<td><?php echo $this->Paginator->sort('name');?></td>
				<td><?php echo $this->Paginator->sort('sale_price');?></td>
				<td><?php echo $this->Paginator->sort('start_date');?></td>
				<td><?php echo $this->Paginator->sort('expire_date');?></td>
				<td>Performance</td>
				<td>Deal Flags</td>
				<td>Actions</td>
			</tr>
			
			<?php foreach ($deals as $deal): ?>
			
			<tr class="highlight">
				<td><?php echo $this->Html->link($deal['Deal']['name'], array('action' => 'view', $deal['Deal']['id'])); ?></td>
				<td><?php echo $deal['Deal']['sale_price']; ?></td>
				<td><?php echo $deal['Deal']['start_date']; ?></td>
				<td><?php echo $deal['Deal']['expire_date']; ?></td>
				<td><?php echo $deal['Deal']['limit'] - $deal['Deal']['remaining'] . " / " . $deal['Deal']['limit']; ?></td>
				<td>
					<?php 
						
						$flags = '';
						
						if ($deal['Deal']['tb_tuesday'] == 'Y') { $flags = "TB"; }
							
						if ($deal['Deal']['black_friday'] == 'Y') {
							if (!empty($flags)) { $flags = " | ";} 
							$flags = "BF";
						}
						
						if ($deal['Deal']['deal_of_week'] == 'Y') {
							if (!empty($flags)) { $flags = " | ";} 
							$flags = "DW";
						}
						
						if ($deal['Deal']['unlimited_usenet'] == 'Y') {
							if (!empty($flags)) { $flags = " | ";} 
							$flags = "UU";
						}						
						elseif ($deal['Deal']['unlimited_usenet'] == 'AD') {
							if (!empty($flags)) { $flags = " | ";} 
							$flags = "Adult";
						}					
						elseif ($deal['Deal']['unlimited_usenet'] == 'US') {
							if (!empty($flags)) { $flags = " | ";} 
							$flags = "Free Sucks";
						}					
						elseif ($deal['Deal']['unlimited_usenet'] == 'B') {
							if (!empty($flags)) { $flags = " | ";} 
							$flags = "Best Usenet";
						}
						
						if ($deal['Deal']['block_usenet'] == 'Y') {
							if (!empty($flags)) { $flags = " | ";} 
							$flags = "BL";
						}						
						
						echo $flags;						
					?>
				</td>
				<td><?php echo $this->Html->link('Duplicate', array('action' => 'duplicate', $deal['Deal']['id'])); ?></td>
				
			</tr>
			
			<?php endforeach; ?>
		</table>

		<p id="pagin">
			<?php echo $this->Paginator->first();?>
		 	<?php echo $this->Paginator->prev('< previous', "", "", "");?>
		 	<?php //echo $this->Paginator->prev() ?> 
			<?php echo $this->Paginator->numbers();?>  
		 	<?php //echo $this->Paginator->next() ?>
			<?php //echo $this->Paginator->counter(); ?>
			<?php echo $this->Paginator->next('next >', "", "", "");?>
			<?php echo $this->Paginator->last();?>
		<p/>

		<span class="clear"></span>
	</div>
</div>

