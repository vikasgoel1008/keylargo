<div class="deals form">
	<h2>Set Deal Text</h2>

	<?php echo $this->element('deals_menu');?>

	<?php echo $this->Form->create('Deal', array('action' => 'set_deal_text'));?>
	<fieldset>
		<?php echo $this->Form->input('id',array('type'=>'hidden','class'=>'medium','label'=>'Description'));?><br class="hid" />
		<?php echo $this->Form->input('name',array('type'=>'text','class'=>'medium','label'=>'Description', 'readonly' => 'readonly'));?><br class="hid" />
		
		<?php echo $this->Form->input('desc',array('type'=>'textarea','class'=>'medium','label'=>'Deal Description'));?><br class="hid" />
		<?php echo $this->Form->input('mid_text',array('type'=>'textarea','class'=>'medium','label'=>'Deal Mid Text'));?><br class="hid" />
		<?php echo $this->Form->input('bullets',array('type'=>'textarea','class'=>'medium','label'=>'Deal Bullets'));?><br class="hid" />
		<p>Format: Enclose each line in list item tags</p>
		<p></p>
		<?php echo $this->Form->end(array('label'=>'Submit Deal','class'=>'button submit'));?>
		<span class="clear"></span>
	</fieldset>
</div>
