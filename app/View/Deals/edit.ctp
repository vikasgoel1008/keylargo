<div class="deals form">
	<h2>Edit Deal</h2>

	<?php echo $this->element('deals_menu');?>

	<?php echo $this->Form->create('Deal', array('action' => 'edit'));?>
	<fieldset>
		<?php echo $this->Form->input('name',array('type'=>'text','class'=>'medium','label'=>'Description'));?><br class="hid" />
		<?php echo $this->Form->input('start_date',array('type'=>'text','class'=>'medium','label'=>'Start Date'));?><br class="hid" /><p>Format: YYYY-MM-DD HH:ii:ss</p>
		<?php echo $this->Form->input('expire_date',array('type'=>'text','class'=>'medium','label'=>'Expiration Date'));?><br class="hid" /><p>Format: YYYY-MM-DD HH:ii:ss</p>
		
		<?php echo $this->Form->input('plan_id',array('type'=>'select','class'=>'medium','label'=>"Plan", 'options'=>$plan));?><br class="hid" />
		<?php echo $this->Form->input('bandwidth',array('type'=>'text','class'=>'medium','label'=>'Bandwidth'));?><br class="hid" />
		<?php echo $this->Form->input('orig_price',array('type'=>'text','class'=>'medium','label'=>'Original Price'));?><br class="hid" />
		<?php echo $this->Form->input('sale_price',array('type'=>'text','class'=>'medium','label'=>'Sale Price'));?><br class="hid" />
		<?php echo $this->Form->input('limit',array('type'=>'text','class'=>'medium','label'=>'# Deals Available'));?><br class="hid" />
		<?php echo $this->Form->input('remaining',array('type'=>'text','class'=>'medium','label'=>'# Deals Remaining'));?><br class="hid" />
		
		<?php echo $this->Form->input('rate_period',array('options'=>array('Monthly' => "Monthly", "Flat Rate" => "Flat Rate"), 'type'=>'select','class'=>'tiny','label'=>'Rate Type', 'empty'=>true));?><br class="hid" />
		<?php echo $this->Form->input('tb_tuesday',array('options'=>array("N"=>"No","Y"=>"Yes"), 'type'=>'select','class'=>'medium','label'=>'TB Tuesday Deal'));?><br class="hid" />
		<?php echo $this->Form->input('black_friday',array('options'=>array("N"=>"No","Y"=>"Yes"), 'type'=>'select','class'=>'medium','label'=>'Black Friday Deal'));?><br class="hid" />
		<?php echo $this->Form->input('deal_of_week',array('options'=>array("N"=>"No","Y"=>"Yes"), 'type'=>'select','class'=>'medium','label'=>'Deal of Week'));?><br class="hid" />
		<?php echo $this->Form->input('unlimited_usenet',array('options'=>$unlimited, 'type'=>'select','class'=>'medium','label'=>'Unlimited Usenet'));?><br class="hid" />
		<?php echo $this->Form->input('block_usenet',array('options'=>$block, 'type'=>'select','class'=>'medium','label'=>'Block Usenet'));?><br class="hid" />
		<?php echo $this->Form->input('counter',array('options'=>array("N"=>"No","Y"=>"Yes"), 'type'=>'select','class'=>'medium','label'=>'Display Counter'));?><br class="hid" />
		
		<?php echo $this->Form->input('id',array('type'=>'hidden','class'=>'medium','label'=>'Deal of Week'));?><br class="hid" />
		<?php echo $this->Form->input('desc',array('type'=>'hidden','class'=>'medium','label'=>'Deal of Week'));?><br class="hid" />
		<?php echo $this->Form->input('mid_text',array('type'=>'hidden','class'=>'medium','label'=>'Deal of Week'));?><br class="hid" />
		<?php echo $this->Form->input('bullets',array('type'=>'hidden','class'=>'medium','label'=>'Deal of Week'));?><br class="hid" />
		
		
		
		<p></p>
		<?php echo $this->Form->end(array('label'=>'Edit Deal','class'=>'button submit'));?>
		<span class="clear"></span>
	</fieldset>
</div>
