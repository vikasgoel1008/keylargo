 <!--Load the AJAX API-->
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">

  // Load the Visualization API and the piechart package.
  google.load('visualization', '1.0', {'packages':['corechart']});
  
  // Set a callback to run when the Google Visualization API is loaded.
  google.setOnLoadCallback(drawChart);
  
  // Callback that creates and populates a data table, 
  // instantiates the pie chart, passes in the data and
  // draws it.
  function drawChart() {

  // Create the data table.
  var data = new google.visualization.DataTable();
  data.addColumn('string', 'Deal');
  data.addColumn('number', 'Sales');
  data.addColumn('number', 'Revenues');
  data.addRows([
    ['Mushrooms', 3, 4],
    ['Onions', 1, 2],
    ['Olives', 1, 2], 
    ['Zucchini', 1, 2],
    ['Pepperoni', 2, 2]
  ]);

  // Set chart options
  var options = {'title':'How Much Pizza I Ate Last Night',
  				'legend':'left',
				  'is3D':true,
                 'width':400,
                 'height':300};

  // Instantiate and draw our chart, passing in some options.
  var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
  chart.draw(data, options);
}
</script>

<div class="deals index">
	<h2>Deals</h2>
	
	<?php echo $this->element('deals_menu');?>
	
	<div class="box">
		<p>&nbsp;</p>
		
		<table cellpadding="0" cellspacing="0">
			<tr>	
				<td colspan="6">
					<!--Div that will hold the pie chart-->
				    <div id="chart_div" style="align:center;"></div>

				</td>
			</tr>
			
			
		</table>
	    
		<span class="clear"></span>
	</div>
</div>

