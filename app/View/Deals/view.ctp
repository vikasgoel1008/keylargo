<div class="deals view">
<h2>Deal Details</h2>
<?php echo $this->element('deals_menu');?>
	
<div class="box">
	<p>&nbsp;</p>

	<table>
	
		<tr class="highlight">
			<td colspan="3">
				<?php echo $this->Html->link('Edit',  array('action' => 'edit', $deal['Deal']['id']));?> |
				<?php echo $this->Html->link('Performance', 'promo/'. $deal['Deal']['id']);?> 
			</td>
		</tr>
		<tr class="highlight"><td>Deal:</td><td><?php echo $deal['Deal']['name'];?></td></tr>
		<tr class="highlight"><td>Start Date:</td><td><?php echo $deal['Deal']['start_date']; ?></td></tr>
		<tr class="highlight"><td>Expiration Date:</td><td><?php echo $deal['Deal']['expire_date']; ?></td></tr>
		<tr class="highlight"><td></td><td></td></tr>
		
		<tr class="highlight"><td>Pricing:</td><td><?php echo $deal['Deal']['orig_price']; ?> | <?php echo $deal['Deal']['sale_price']; ?></td></tr>
		<tr class="highlight"><td>Remaining:</td><td><?php echo $deal['Deal']['remaining']; ?> | <?php echo $deal['Deal']['limit']; ?></td></tr>
		<tr class="highlight">
			<td>Deal Type:</td>
			<td><?php
				if ($deal['Deal']['tb_tuesday'] == 'Y') { echo "TB"; }
							
						if ($deal['Deal']['black_friday'] == 'Y') {
							if ($deal['Deal']['tb_tuesday'] == 'Y') { echo " | ";} 
							echo "BF";
						}
						
						if ($deal['Deal']['deal_of_week'] == 'Y') {
							if ($deal['Deal']['tb_tuesday'] == 'Y' || $deal['Deal']['black_friday'] == 'Y') { echo " | ";} 
							echo "DW";
						}
						
						if ($deal['Deal']['unlimited_usenet'] == 'Y') {
							if ($deal['Deal']['tb_tuesday'] == 'Y' || $deal['Deal']['black_friday'] == 'Y' || $deal['Deal']['deal_of_week'] == 'Y') { echo " | ";} 
							echo "UU";
						}
				?>
			</td>
		</tr>
		<tr class="highlight"><td>Plan:</td><td><?php echo $deal['Deal']['plan_id']; ?></td></tr>
		<tr class="highlight"><td>Coupon:</td><td><?php echo $deal['Deal']['coupon_id']; ?></td></tr>
		<tr class="highlight"><td></td><td></td></tr>
		
		<tr class="highlight"><td colspan='2'>Customer Text Displays</td></tr>
		<tr class="highlight"><td>Bandwidth:</td><td><?php echo $deal['Deal']['bandwidth']; ?></td></tr>
		<tr class="highlight"><td width="35%">Description:</td><td><?php echo $deal['Deal']['desc']; ?></td></tr>
		<tr class="highlight"><td>Middle Text:</td><td><?php echo $deal['Deal']['mid_text']; ?></td></tr>
		<tr class="highlight"><td>Bullet Points:</td><td><?php echo $deal['Deal']['bullets']; ?></td></tr>
		<tr class="highlight"><td></td><td></td></tr>
		
				
	</table>
	<p id="pagin">
			&nbsp;
		</p>
		
		<span class="clear">&nbsp;</span>

</div>	
</div>
	