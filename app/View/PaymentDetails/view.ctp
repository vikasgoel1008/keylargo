<div class="ccTransactions index">
<h2>Payment Details</h2>

<?php echo $this->element('members_menu');?>

<div class="box">
	<p>&nbsp;</p>

	<table cellpadding="0" cellspacing="0">
			
		<?php if ($paymentDetail['Member']['is_free_trial'] == "Y"): ?>
		<tr class="highlight"><td colspan="2"><span class="notgood">This customer is currently within their free trial period.</span></td></tr>
		<?php endif; ?>
		
		<tr class="highlight"><td colspan="2"><?php echo $this->Html->link('Edit Billing Information', 'edit/'. $paymentDetail['PaymentDetail']['cc_id']);?></td></tr>
		<tr class="highlight"><td colspan="2"><?php echo $member['Member']['name']; ?> | <em><?php echo $member['Member']['email']; ?></em> | <?php echo $member['Member']['login_username']; ?></td></tr>
		
		<?php if($paymentDetail['Member']['is_bill_fail'] == 'Y'): ?>
		<tr class="highlight"><td><span class="notgood">Days Late:</span></td><td><span class="notgood"><?php echo $paymentDetail['Member']['recent_failures']; ?></span></td></tr>
		<?php endif; ?>
		
		<?php if($paymentDetail['Member']['pay_type'] == 'WP'): ?>
		<tr class="highlight"><td colspan="2"><span class="notgood">This is a WorldPay Customer:</span></td></tr>
		<?php endif; ?>
		
		<tr><td colspan="2"</td></tr>
		
		<?php if(empty($paymentDetail['Member']['auth_cust_id'])): ?>
		<tr class="highlight"><td colspan="2"><?php echo $this->Html->link("Create CIM Profile","http://newsgroupdirect.com/scripts/script_update_cim_payment_profile.php?id=".$paymentDetail['Member']['id'], array("target" => "_blank", 'class' => 'notgood'));  ?></td></tr>
		<?php else : ?>
		<tr class="highlight"><td colspan="2"><?php echo $this->Html->link("Update CIM Profile","http://newsgroupdirect.com/scripts/script_update_cim_payment_profile.php?id=".$paymentDetail['Member']['id'], array("target" => "_blank"));  ?></td></tr>
		<?php endif; ?>
		
		<tr class="highlight"><td>Auth Customer ID:</td><td><?php echo $paymentDetail['Member']['auth_cust_id']; ?></td></tr>
		<tr class="highlight"><td>Auth Payment ID:</td><td><?php echo $paymentDetail['Member']['auth_payment_id']; ?></td></tr>
		<tr class="highlight">
			<td>Card Number:</td>
			<td>
				<?php if (!empty($paymentDetail['PaymentDetail']['cc_card_type'])) {echo $paymentDetail['PaymentDetail']['cc_card_type'] . " | ";} ?>
				xxxxxxxxxx<?php echo $paymentDetail['PaymentDetail']['cc_last_4']; ?>
			</td>
		</tr>
		<tr class="highlight">
			<td>Expiration Date:</td>
			<td>
			
				<?php echo substr($paymentDetail['PaymentDetail']['cc_card_exp'],4,2) . '/' . substr($paymentDetail['PaymentDetail']['cc_card_exp'],0,4); ?>
			
			</td>
		</tr>
		<tr class="highlight"><td>Last Changed:</td><td><?php echo $paymentDetail['PaymentDetail']['cc_change_date']; ?></td></tr>
		
		
	</table>
	
	<p id="pagin">
		&nbsp;
	</p>
	
	<span class="clear">&nbsp;</span>
	
</div>		
</div>
	
