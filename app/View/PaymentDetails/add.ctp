<div class="paymentDetails form">
<?php echo $this->Form->create('PaymentDetail');?>
	<fieldset>
 		<legend><?php __('Add PaymentDetail');?></legend>
	<?php
		echo $this->Form->input('cc_id');
		echo $this->Form->input('member_id');
		echo $this->Form->input('cc_card_type');
		echo $this->Form->input('cc_last_4');
		echo $this->Form->input('cc_card_exp');
		echo $this->Form->input('record_type');
		echo $this->Form->input('cc_change_date');
	?>
	</fieldset>
<?php echo $this->Form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $this->Html->link(__('List PaymentDetails', true), array('action' => 'index'));?></li>
	</ul>
</div>
