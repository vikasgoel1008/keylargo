<h2>Blacklist Customer</h2>

<?php echo $this->element('members_menu');?>

<?php echo $this->Form->create('Blacklist');?>
<fieldset>
	
	<?php echo $this->Form->input('email',array('type'=>'text','class'=>'small','label'=>'Email Address', 'value' => $this->Session->read('Blacklist.email')));?><br class="hid" />
	<?php echo $this->Form->input('domain',array('type'=>'text','class'=>'tiny','label'=>'Domain', 'value' => $this->Session->read('Blacklist.domain')));?><br class="hid" />
	<?php echo $this->Form->input('IP',array('type'=>'text','class'=>'tiny','label'=>'IP Address', 'value' => $this->Session->read('Blacklist.IP')));?><br class="hid" />
	<?php echo $this->Form->input('card_number',array('type'=>'text','class'=>'tiny','label'=>'Credit Card', 'value' => $this->Session->read('Blacklist.card_number')));?><br class="hid" />
	<?php echo $this->Form->input('reason',array('type'=>'text','class'=>'tiny','label'=>'Blacklist Reason'));?><br class="hid" />
	<p></p>
	<?php echo $this->Form->end(array('label'=>'Blacklist','class'=>'button submit'));?>
	<span class="clear"></span>
</fieldset>