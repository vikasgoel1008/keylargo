<div class="blacklists form">
<?php echo $this->Form->create('Blacklist');?>
	<fieldset>
 		<legend><?php __('Edit Blacklist');?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('email');
		echo $this->Form->input('IP');
		echo $this->Form->input('card_number');
		echo $this->Form->input('reason');
	?>
	</fieldset>
<?php echo $this->Form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $this->Form->value('Blacklist.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $this->Form->value('Blacklist.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Blacklists', true), array('action' => 'index'));?></li>
	</ul>
</div>
