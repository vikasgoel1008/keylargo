<div class="blacklists index">
	<h2>Blacklisted Terms</h2>
	
	<?php echo $this->element('members_menu');?>
	<?php $this->Paginator->options(array('url' => $this->passedArgs));?>
	
	<div class="box">
		<p>&nbsp;</p>
		<table cellpadding="0" cellspacing="0">
			<tr class="highlight">	
				<td colspan="5"><?php  echo $this->Html->link('Blacklist', array('action' => 'add'));?></td>
			</tr>
			<tr class="highlight">	
				<td colspan="5"><?php  echo $this->Paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)));?></td>
			</tr>
			<tr>
				<td><?php echo $this->Paginator->sort('id');?></td>
				<td><?php echo $this->Paginator->sort('email');?></td>
				<td><?php echo $this->Paginator->sort('domain');?></td>
				<td><?php echo $this->Paginator->sort('IP');?></td>
				<td><?php echo $this->Paginator->sort('card_number');?></td>
			</tr>
			
			<?php foreach ($blacklists as $blacklist): ?>
			
			<tr  class="highlight">
				<td><?php echo $this->Html->link($blacklist['Blacklist']['id'], array('action' => 'view', $blacklist['Blacklist']['id'])); ?></td>
				<td><?php echo $blacklist['Blacklist']['email']; ?></td>
				<td><?php echo $blacklist['Blacklist']['domain'];?></td>
				<td><?php echo $blacklist['Blacklist']['IP']; ?></td>
				<td><?php echo substr($blacklist['Blacklist']['card_number'], strlen($blacklist['Blacklist']['card_number'])-4) ; ?></td>
			</tr>
			
			<?php endforeach; ?>
		</table>

		<p id="pagin">
			<?php echo $this->Paginator->first();?>
		 	<?php echo $this->Paginator->prev('< previous', "", "", "");?>
		 	<?php //echo $this->Paginator->prev() ?> 
			<?php echo $this->Paginator->numbers();?>  
		 	<?php //echo $this->Paginator->next() ?>
			<?php //echo $this->Paginator->counter(); ?>
			<?php echo $this->Paginator->next('next >', "", "", "");?>
			<?php echo $this->Paginator->last();?>
		<p/>

		<span class="clear"></span>
	</div>
</div>

