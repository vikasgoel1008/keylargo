<div class="blacklists view">
<h2><?php  __('Blacklist');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $blacklist['Blacklist']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Email'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $blacklist['Blacklist']['email']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('IP'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $blacklist['Blacklist']['IP']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Card Number'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $blacklist['Blacklist']['card_number']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Reason'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $blacklist['Blacklist']['reason']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<ul>
		<li><?php echo $this->Html->link(__('Edit Blacklist', true), array('action' => 'edit', $blacklist['Blacklist']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Blacklist', true), array('action' => 'delete', $blacklist['Blacklist']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $blacklist['Blacklist']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Blacklists', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Blacklist', true), array('action' => 'add')); ?> </li>
	</ul>
</div>
