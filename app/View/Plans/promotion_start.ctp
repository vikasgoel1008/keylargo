<h2>Promote Plan</h2>

<?php echo $this->element('plans_menu');?>

<?php echo $this->Form->create('Plan', array('action' => 'promotion_start'));?>
<fieldset>
	<?php echo $this->Form->hidden('id'); ?> 

	<?php echo $this->Form->input('name',array('type'=>'text','class'=>'medium','label'=>'Plan', 'readonly' => 'readonly'));?><br class="hid" />
	<?php echo $this->Form->input('coupon_allowed',array('type'=>'text','class'=>'tiny','label'=>'Can Coupons Be Used During Promotion'));?><br class="hid" /><br/>
	<p>Y / N</p>
	<?php echo $this->Form->input('curr_1_price',array('type'=>'text','class'=>'tiny','label'=>'What is the Sales Price?'));?><br class="hid" /><br/>
	<p></p>
	
	<?php echo $this->Form->end(array('label'=>'Begin Promotion','class'=>'button submit'));?>
	<span class="clear"></span>
</fieldset>