<h2>Update Plan</h2>

<?php echo $this->element('plans_menu');?>

<?php echo $this->Form->create('Plan');?>
<fieldset>
	<?php echo $this->Form->hidden('id'); ?> 

	<?php echo $this->Form->input('name',array('type'=>'text','class'=>'medium','label'=>'Plan'));?><br class="hid" />
	<?php echo $this->Form->input('status',array('options'=>$status,'type'=>'select','class'=>'medium','label'=>'Status', 'empty' => true));?><br class="hid" />
	<?php echo $this->Form->input('free_trial',array('options'=>$yesNo,'type'=>'select','class'=>'small','label'=>'Free Trial?', 'empty' => true));?><br class="hid" />
	<?php echo $this->Form->input('type',array('type'=>'text','class'=>'medium','label'=>'Type'));?><br class="hid" />
	<?php echo $this->Form->input('allowance',array('type'=>'text','class'=>'medium','label'=>'Allowance'));?><br class="hid" />
	<?php echo $this->Form->input('unlimited',array('options'=>$yesNo,'type'=>'select','label'=>'Unlimited?', 'empty' => true));?><br class="hid" />
	<?php echo $this->Form->input('curr_1_price',array('type'=>'text','class'=>'medium','label'=>'USD Price'));?><br class="hid" />
	<?php echo $this->Form->input('curr_2_price',array('type'=>'text','class'=>'medium','label'=>'GBP Price'));?><br class="hid" />
	<?php echo $this->Form->input('curr_3_price',array('type'=>'text','class'=>'medium','label'=>'CUR 3 Price'));?><br class="hid" />
	<?php echo $this->Form->input('allot_type',array('type'=>'text','class'=>'medium','label'=>'Allotment Type'));?><br class="hid" />
	<?php echo $this->Form->input('duration',array('type'=>'text','class'=>'medium','label'=>'Duration'));?><br class="hid" />
	<?php echo $this->Form->input('bandwidth',array('type'=>'text','class'=>'medium','label'=>'Bandwidth Amount'));?><br class="hid" />
	<?php echo $this->Form->input('connections',array('type'=>'text','class'=>'medium','label'=>'Nbr Connections'));?><br class="hid" />
	<?php echo $this->Form->input('ssl',array('type'=>'text','class'=>'medium','label'=>'SSL Enabled'));?><br class="hid" />
	<?php echo $this->Form->input('ssl_comp',array('type'=>'text','class'=>'medium','label'=>'SSL Comp'));?><br class="hid" />
	<?php echo $this->Form->input('1_month_comp',array('type'=>'text','class'=>'medium','label'=>'1 Month Comp'));?><br class="hid" />
	<?php echo $this->Form->input('3_month_comp',array('type'=>'text','class'=>'medium','label'=>'3 Month Comp'));?><br class="hid" />
	<?php echo $this->Form->input('6_month_comp',array('type'=>'text','class'=>'medium','label'=>'6 Month Comp'));?><br class="hid" />
	<?php echo $this->Form->input('12_month_comp',array('type'=>'text','class'=>'medium','label'=>'12 Month Comp'));?><br class="hid" />
	<?php echo $this->Form->input('server_group_id',array('type'=>'text','class'=>'medium','label'=>'Server Group Id'));?><br class="hid" />
	<?php echo $this->Form->input('virtual_server_id',array('type'=>'text','class'=>'medium','label'=>'Virtual Server Id'));?><br class="hid" />
	<?php echo $this->Form->input('server2_group_id',array('type'=>'text','class'=>'medium','label'=>'Server 2 Group Id'));?><br class="hid" />
	<?php echo $this->Form->input('virtual_server2_id',array('type'=>'text','class'=>'medium','label'=>'Virtual Server 2 Id'));?><br class="hid" />
	<?php echo $this->Form->input('valid_recycle',array('options'=>$yesNo,'type'=>'select','class'=>'medium','label'=>'Recyclable Plan?', 'empty' => true));?><br class="hid" />
	
	<?php echo $this->Form->input('coupon_allowed',array('options'=>$yesNo,'type'=>'select','class'=>'small','label'=>'Coupon Allowed', 'empty' => true));?><br class="hid" />
	<p></p>
	
	<?php echo $this->Form->end(array('label'=>'Update','class'=>'button submit'));?>
	<span class="clear"></span>
</fieldset>