

<div class="plans index">

	<h2>Plans</h2>
	
	<?php echo $this->element('plans_menu');?>
	<?php $this->Paginator->options(array('url' => $this->passedArgs));?>
	
	<div class="box">
		<p>&nbsp;</p>
	
		<table cellpadding="0" cellspacing="0">
			<tr class="highlight">
				<td colspan="6"><?php  echo $this->Paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)));?></td>
			</tr>
			<tr class="highlight">
				<td><?php echo $this->Paginator->sort('id');?></td>
				<td><?php echo $this->Paginator->sort('plan_name');?></td>
				<td><?php echo $this->Paginator->sort('type');?></td>
				<td><?php echo $this->Paginator->sort('Price', 'curr_1_price');?></td>
				<td><?php echo $this->Paginator->sort('Coupons','coupon_allowed');?></td>
				<td><?php echo $this->Paginator->sort('Promotion','promotion');?></td>

			</tr>
		
			<?php foreach ($plans as $plan): ?>
			<tr>
				<td style="align:left;">
					<?php echo $this->Html->link(__($plan['Plan']['id'], true), array('action' => 'view', $plan['Plan']['id'])); ?>
				</td>
				<td>
					<?php echo $plan['Plan']['name']; ?>
				</td>
				<td>
					<?php echo $plan['Plan']['type']; ?>
				</td>
				<td>
					<?php echo $plan['Plan']['curr_1_price']; ?>
				</td>
				<td>
					<?php echo $plan['Plan']['coupon_allowed']; ?>
				</td>
				<td>
					<?php if ($plan['Plan']['promotion'] == 'Y') : ?>
						<?php echo $this->Html->link(__("On Sale!", true), array('action' => 'promotion_stop', $plan['Plan']['id'])); ?>
					<?php elseif ($plan['Plan']['type'] == 'block') : ?>
						<?php echo $this->Html->link(__("Start a Deal", true), array('action' => 'promotion_start', $plan['Plan']['id'])); ?>

					<?php else : ?>
						-- 
					<?php endif; ?>
				
				</td>
			</tr>
			<?php endforeach; ?>
			
		</table>
		
		<p id="pagin">
			<?php echo $this->Paginator->first();?>
		 	<?php echo $this->Paginator->prev('< previous', "", "", "");?>
		 	<?php //echo $this->Paginator->prev() ?> 
			<?php echo $this->Paginator->numbers();?>  
		 	<?php //echo $this->Paginator->next() ?>
			<?php //echo $this->Paginator->counter(); ?>
			<?php echo $this->Paginator->next('next >', "", "", "");?>
			<?php echo $this->Paginator->last();?>
		 	
		<p/>
		
		<span class="clear"></span>
	
	</div>
</div>

