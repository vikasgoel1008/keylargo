<h2>Update Plan</h2>

<?php echo $this->element('plans_menu');?>

<?php echo $this->Form->create('Plan', array('action' => 'edit_status'));?>
<fieldset>
	<?php echo $this->Form->hidden('id'); ?> 

	<?php echo $this->Form->input('name',array('type'=>'text','class'=>'medium','label'=>'Plan'));?><br class="hid" />
	<?php echo $this->Form->input('status',array('options'=>$status,'type'=>'select','class'=>'medium','label'=>'Status', 'empty' => true));?><br class="hid" />
	<p></p>
	
	<?php echo $this->Form->end(array('label'=>'Update','class'=>'button submit'));?>
	<span class="clear"></span>
</fieldset>