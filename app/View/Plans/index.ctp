

<div class="plans index">

	<h2>Plans</h2>
	
	<?php echo $this->element('plans_menu');?>
	<?php $this->Paginator->options(array('url' => $this->passedArgs));?>
	
	<div class="box">
		<p>&nbsp;</p>
	
		<table cellpadding="0" cellspacing="0">
			<tr class="highlight">
				<td colspan="6"><?php  echo $this->Paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)));?></td>
			</tr>
			<tr class="highlight">
				<td><?php echo $this->Paginator->sort('plan_name');?></td>
				<td><?php echo $this->Paginator->sort('type');?></td>
				<td><?php echo $this->Paginator->sort('curr_1_price');?></td>
				<td><?php echo $this->Paginator->sort('coupon_allowed');?></td>
				<td><?php echo $this->Paginator->sort('duration');?></td>
				<td><?php echo $this->Paginator->sort('status');?></td>

			</tr>
		
			<?php foreach ($plans as $plan): ?>
			<tr>
				<td>
					<?php echo $this->Html->link(__($plan['Plan']['name'], true), array('action' => 'view', $plan['Plan']['id'])); ?>
				</td>
				<td>
					<?php echo $plan['Plan']['type']; ?>
				</td>
				<td>
					<?php echo $plan['Plan']['curr_1_price']; ?>
				</td>
				<td>
					<?php echo $plan['Plan']['coupon_allowed']; ?>
				</td>
				<td>
					<?php echo $plan['Plan']['duration']; ?>
				</td>
				<td>
					<?php echo $this->Html->link(__($plan['Plan']['status'], true), array('action' => 'edit_status', $plan['Plan']['id'])); ?>
				</td>
			</tr>
			<?php endforeach; ?>
			
		</table>
		
		<p id="pagin">
			<?php echo $this->Paginator->first();?>
		 	<?php echo $this->Paginator->prev('< previous', "", "", "");?>
		 	<?php //echo $this->Paginator->prev() ?> 
			<?php echo $this->Paginator->numbers();?>  
		 	<?php //echo $this->Paginator->next() ?>
			<?php //echo $this->Paginator->counter(); ?>
			<?php echo $this->Paginator->next('next >', "", "", "");?>
			<?php echo $this->Paginator->last();?>
		 	
		<p/>
		
		<span class="clear"></span>
	
	</div>
</div>

