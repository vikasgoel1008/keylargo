<div class="plans form">
<?php echo $this->Form->create('Plan');?>
	<fieldset>
 		<legend><?php __('Add Plan');?></legend>
	<?php
		echo $this->Form->input('plan_id');
		echo $this->Form->input('plan_name');
		echo $this->Form->input('type');
		echo $this->Form->input('allowance');
		echo $this->Form->input('unlimited');
		echo $this->Form->input('curr_1_price');
		echo $this->Form->input('curr_2_price');
		echo $this->Form->input('curr_3_price');
		echo $this->Form->input('allot_type');
		echo $this->Form->input('duration');
		echo $this->Form->input('bandwidth');
		echo $this->Form->input('connections');
		echo $this->Form->input('ssl');
		echo $this->Form->input('ssl_comp');
		echo $this->Form->input('1_month_comp');
		echo $this->Form->input('3_month_comp');
		echo $this->Form->input('6_month_comp');
		echo $this->Form->input('12_month_comp');
		echo $this->Form->input('server_group_id');
		echo $this->Form->input('virtual_server_id');
		echo $this->Form->input('server2_group_id');
		echo $this->Form->input('virtual_server2_id');
		echo $this->Form->input('valid_recycle');
	?>
	</fieldset>
<?php echo $this->Form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $this->Html->link(__('List Plans', true), array('action' => 'index'));?></li>
	</ul>
</div>
