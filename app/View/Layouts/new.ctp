<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><?php echo $title_for_layout;?></title>
	<meta http-equiv="content-language" content="en" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon"/>
    <?php echo $this->Html->css('styles');
    
     	echo $this->Html->css('font-awesome.min');
   		//echo $this->Html->css('simple-line-icons.min');
	    echo $this->Html->css('bootstrap.min');
	    echo $this->Html->css('search');
	    //echo $this->Html->css('uniform.default');
	    //echo $this->Html->css('daterangepicker-bs3');
	    //echo $this->Html->css('fullcalendar');
	    //echo $this->Html->css('jqvmap');
	    //echo $this->Html->css('tasks');
	    //echo $this->Html->css('custom');
	    echo $this->Html->css('layout');
	    echo $this->Html->css('default');
	    echo $this->Html->css('components');
	    echo $this->Html->css('plugins');?>
	<!--[if lte IE 6]>
	<?php echo $this->Html->css('styles_ie6');?>
	<![endif]-->
    <?php echo $this->Html->script('jquery-1.2.6.min');
     	//echo $this->Html->script('ui');
     	//echo $this->Html->script('swfobject'); 
    
     	echo $this->Html->script('jquery-1.11.0.min'); 
     	echo $this->Html->script('jquery-migrate-1.2.1.min'); 
     	echo $this->Html->script('jquery-ui-1.10.3.custom.min'); 
    
     	echo $this->Html->script('bootstrap.min'); 
     	//echo $this->Html->script('bootstrap-hover-dropdown.min'); 
     	//echo $this->Html->script('jquery.slimscroll'); 
     	//echo $this->Html->script('jquery.blockui.min'); 
     	//echo $this->Html->script('jquery.cokie.min'); 
     	//echo $this->Html->script('jquery.uniform.min'); 
     	//echo $this->Html->script('jquery.vmap'); 
     	//echo $this->Html->script('jquery.vmap.europe'); 
     	//echo $this->Html->script('jquery.vmap.germany'); 
     	//echo $this->Html->script('jquery.vmap.russia'); 
     	//echo $this->Html->script('jquery.vmap.usa'); 
     	//echo $this->Html->script('jquery.vmap.world'); 
     	//echo $this->Html->script('jquery.vmap.sampledata'); 
     	echo $this->Html->script('jquery.flot.min'); 
     	echo $this->Html->script('jquery.flot.resize'); 
     	echo $this->Html->script('jquery.flot.categories.min'); 
     	echo $this->Html->script('jquery.flot.time.min');
     	echo $this->Html->script('jquery.flot.stack');
     	//echo $this->Html->script('jquery.pulsate.min'); 
     	//echo $this->Html->script('daterangepicker'); 
     	//echo $this->Html->script('moment.min'); 
     	//echo $this->Html->script('fullcalendar.min'); 
     	echo $this->Html->script('jquery.easypiechart.min'); 
     	echo $this->Html->script('jquery.sparkline.min'); 
     	echo $this->Html->script('metronic'); 
    	echo $this->Html->script('layout'); 
    	//echo $this->Html->script('tasks'); 
      	echo $this->Html->script('index');?> 
<script>
jQuery(document).ready(function() {    
   //Metronic.init(); // init metronic core componets
   Layout.init(); // init layout
   //Index.init();   
   //Index.initDashboardDaterange();
   //Index.initJQVMAP(); // init index page's custom scripts
   //Index.initCalendar(); // init index page's custom scripts
   Index.initCharts(); // init index page's custom scripts
   //Index.initChat();
   Index.initMiniCharts();
   //Index.initIntro();
   //Tasks.initDashboardWidget();
});
</script>
    
</head>

<body class="page-header-fixed">

<div class="page-header navbar navbar-fixed-top">
	<!-- BEGIN HEADER INNER -->
	<div class="page-header-inner">
		<!-- BEGIN LOGO -->
		<div class="page-logo">
			<a href="index.html">
			
			<?php echo $this->Html->image('bg_h1_logo.png',array('class'=>'logo-default','alt'=>'logo')); ?>
			</a>
			<div class="menu-toggler sidebar-toggler hide">
				<!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
			</div>
		</div>
		<!-- END LOGO -->
		<!-- BEGIN RESPONSIVE MENU TOGGLER -->
		<div class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
		</div>
		<!-- END RESPONSIVE MENU TOGGLER -->
		<!-- BEGIN TOP NAVIGATION MENU -->
		<div class="top-menu">
			<ul class="nav navbar-nav pull-right">
				

				<!-- BEGIN USER LOGIN DROPDOWN -->
				<li class="dropdown dropdown-user">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
					<?php echo $this->Html->image('profiles/avatar3_small.jpg',array('class'=>'img-circle')); ?>
					<span class="username">
					<?php echo $systemUser['username']; ?> </span>
					<i class="fa fa-angle-down"></i>
					</a>
					<ul class="dropdown-menu">
						<li>
							<?php echo $this->Html->link('<i class="fa fa-user"></i> My Profile',array('controller'=>'members','action'=>'view',$systemUser['id']),array('escape'=>false)); ?>
						</li>
						<li>
							<?php echo $this->Html->link('<i class="fa fa-key"></i> Log Out',array('controller'=>'users','action'=>'logout'),array('escape'=>false)); ?>
							
						</li>
					</ul>
				</li>
				<!-- END USER LOGIN DROPDOWN -->
			</ul>
		</div>
		<!-- END TOP NAVIGATION MENU -->
	</div>
	<!-- END HEADER INNER -->
</div>

<div class="clearfix">
</div>

<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<div class="page-sidebar-wrapper">
		<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
		<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
		<div class="page-sidebar navbar-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->
			<ul class="page-sidebar-menu" data-auto-scroll="false" data-auto-speed="200">
				<!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
				<li class="sidebar-toggler-wrapper">
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
					<div class="sidebar-toggler">
					</div>
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
				</li>
				<!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
				<li class="sidebar-search-wrapper hidden-xs">
					<!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
					<!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
					<!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->
					<?php echo $this->Form->create('Member',array('url'=>array('controller'=>'members','action'=>'searchresults'),'class'=>'sidebar-search')); ?>
						<a href="javascript:;" class="remove">
						</a>
						<div class="input-group">
							<?php echo $this->Form->input('searchterm',array('class'=>'form-control','placeholder'=>'Search Customers...','div'=>false,'label'=>false)); ?>
							<span class="input-group-btn">
							<!-- DOC: value=" ", that is, value with space must be passed to the submit button -->
							<input class="btn submit" type="button" type="button" value=" "/>
							</span>
						</div>
					<?php echo $this->Form->end(); ?>
					<?php echo $this->Form->create('CcTransaction',array('url'=>array('controller'=>'cc_transactions','action'=>'search'),'class'=>'sidebar-search')); ?>
						<a href="javascript:;" class="remove">
						</a>
						<div class="input-group">
							<?php echo $this->Form->input('searchterm',array('class'=>'form-control','placeholder'=>'Search Transactions...','div'=>false,'label'=>false)); ?>
							<span class="input-group-btn">
							<!-- DOC: value=" ", that is, value with space must be passed to the submit button -->
							<input class="btn submit" type="button" type="button" value=" "/>
							</span>
						</div>
					<?php echo $this->Form->end(); ?>
					<!-- END RESPONSIVE QUICK SEARCH FORM -->
				</li>
				<li class="start active ">
					<a href="/">
					<i class="fa fa-home"></i>
					<span class="title">Dashboard</span>
					<span class="selected"></span>
					</a>
				</li>
				
				<li>
					<a href="javascript:;">
					<i class="fa fa-user"></i>
					<span class="title">Reports</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li>
							<a href="#" title="Daily SLA Report"><i class="fa fa-signal"></i>Daily Revenue</a>
						</li>
						<li>
							<a href="#" title="Weekly SLA Report"><i class="fa fa-signal"></i>Monthly Revenue</a>
						</li>
						
						<li>
							<?php echo $this->Html->link(__('Recent signups', true), array('controller' => 'data_reports', 'action' => 'report_average_bandwidth')); ?>
						</li>
						<li>
							<?php echo $this->Html->link(__('Recent Cancels', true), array('controller' => 'ngd_reports', 'action' => 'billings')); ?>
						</li>
						<li>
							<?php echo $this->Html->link(__('Member Growth', true), array('controller' => 'data_reports', 'action' => 'report_daily_bandwidth_usage',180)); ?>
						</li>
					</ul>
				</li>
				
				<li>
					<a href="javascript:;">
					<i class="fa fa-user"></i>
					<span class="title">Affliates</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li>
							<a href="/affiliates/" title="View Affiliate"><i class="fa fa-signal"></i>View Affiliates</a>
						</li>
						
						<li>
							<a href="/affiliate_payments/" title="Add Affiliate"><i class="fa fa-signal"></i>Monthly Payments</a>
						</li>
						
					</ul>
				</li>
				
				<li>
					<a href="javascript:;">
					<i class="fa fa-ticket"></i>
					<span class="title">Coupons</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li>
							<a href="/coupons/active">
							<i class="fa fa-ticket"></i>
							View Active Coupons</a>
						</li>
						<li>
							<a href="/coupons/">
							<i class="fa fa-ticket"></i>
							View All Coupons</a>
						</li>
						<li>
							<a href="/coupons/add">
							<i class="fa fa-ticket"></i>
							Create New Coupon</a>
						</li>
					</ul>
				</li>
				<li>
					<a href="javascript:;">
					<i class="fa fa-money"></i>
					<span class="title">Deals</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li>
							<a href="/deals/active">
							<i class="fa fa-money"></i>
							View Active Deals</a>
						</li>
						<li>
							<a href="/deals/">
							<i class="fa fa-money"></i>
							View All Deals</a>
						</li>
						<li>
							<a href="/deals/add	">
							<i class="fa fa-money"></i>
							Create New Deal</a>
						</li>
					</ul>
				</li>
				<li>
					<a href="javascript:;">
					<i class="fa fa-edit"></i>
					<span class="title">Config</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li>
							<a href="/plans">
							<i class="fa fa-edit"></i>
							Manage Plans</a>
						</li>
						<li>
							<a href="/blacklists">
							<i class="fa fa-edit"></i>
							Manage Blacklists</a>
						</li>
						<li>
							<a href="/ngd_options">
							<i class="fa fa-edit"></i>
							Manage Options</a>
						</li>
						<li>
							<a href="/quips">
							<i class="fa fa-edit"></i>
							Manage Quips</a>
						</li>
					</ul>
				</li>
				<li>
					<a href="javascript:;">
					<i class="fa fa-user"></i>
					<span class="title">Admin</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li>
							<a href="/users">
							<i class="fa fa-user"></i>
							View Admins</a>
						</li>
						<li>
							<a href="/users/add">
							<i class="fa fa-user"></i>
							Create Admin</a>
						</li>
					</ul>
				</li>
			</ul>
			<!-- END SIDEBAR MENU -->
		</div>
	</div>
	
	<div class="page-content-wrapper">
		<div class="page-content">
			<?php echo $content_for_layout;?>
		</div>
	</div>
</div>
<?php echo $this->element('sql_dump'); ?>


</body>
</html>
