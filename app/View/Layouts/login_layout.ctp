<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><?php echo $title_for_layout;?></title>
	<meta http-equiv="content-language" content="en" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon"/>
    <?php echo $this->Html->css('styles');
    
     	echo $this->Html->css('font-awesome.min');
   		echo $this->Html->css('simple-line-icons.min');
	    echo $this->Html->css('bootstrap.min');
	    echo $this->Html->css('uniform.default');
	    
	    echo $this->Html->css('select2');
	    echo $this->Html->css('login-soft');
	    
	    echo $this->Html->css('custom');
	    echo $this->Html->css('layout');
	    echo $this->Html->css('default');
	    echo $this->Html->css('components');
	    echo $this->Html->css('plugins');?>
	<!--[if lte IE 6]>
	<?php echo $this->Html->css('styles_ie6');?>
	<![endif]-->
    <?php echo $this->Html->script('jquery-1.2.6.min');
     	echo $this->Html->script('ui');
     	echo $this->Html->script('swfobject'); 
    
     	echo $this->Html->script('jquery-1.11.0.min'); 
     	echo $this->Html->script('jquery-migrate-1.2.1.min'); 
     	//echo $this->Html->script('jquery-ui-1.10.3.custom.min'); 
    
     	echo $this->Html->script('bootstrap.min'); 
     	echo $this->Html->script('bootstrap-hover-dropdown.min'); 
     	echo $this->Html->script('jquery.slimscroll'); 
     	echo $this->Html->script('jquery.blockui.min'); 
     	echo $this->Html->script('jquery.cokie.min'); 
     	echo $this->Html->script('jquery.uniform.min'); 
     	echo $this->Html->script('metronic'); 
    	echo $this->Html->script('layout'); 
    	echo $this->Html->script('jquery.validate.min');
    	echo $this->Html->script('jquery.backstretch.min');
    	echo $this->Html->script('select2.min');
    	echo $this->Html->script('login-soft'); ?> 
<script>
jQuery(document).ready(function() {    
   Metronic.init();
   Layout.init();
   Login.init();
  
});
</script>
    
</head>
<body class="login">
	<div class="logo">
		<a href="index.html">
		<?php echo $this->Html->image('logo-big.png'); ?>
		</a>
	</div>
	<div class="menu-toggler sidebar-toggler">
	</div>
	<div class="content">
		<?php echo $content_for_layout;?>
	</div>
	<div class="copyright">
		 2014 &copy; Metronic - Admin Dashboard Template.
	</div>
</body>
</html>