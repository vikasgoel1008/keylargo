<script type="text/javascript" src="//use.typekit.net/iyd5wol.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>

<p style="font-size:10em; font-family:mono45-headline; margin-top:25px; color:yellow;">$ <?php echo number_format($revenues[0]['charges'],2); ?></p>

<p style="font-size:8em; font-family:mono45-headline; margin-top:-120px; color:yellow;"><?php echo $signups[0]['signups']; ?></p>
<p style="font-size:2em; font-family:mono45-headline; margin-top:-150px; color:yellow;">Signups</p>

<p style="font-size:8em; font-family:mono45-headline; margin-top:25px; color:yellow;"><?php echo $adds[0]['adds']; ?></p>
<p style="font-size:2em; font-family:mono45-headline; margin-top:-150px; color:yellow;">Block adds</p>

<p style="font-size:1.5em; font-family:mono45-headline; margin-top:10px; color:yellow;"><?php echo date("Y-m-d H:i:s"); ?> CST</p>

<!--
<p style="font-size:12em; font-family: Helvetica; margin-top:25px;"><strong>$ <?php echo number_format($revenues[0]['charges'],2); ?></strong></p>

<p style="font-size:2.5em; font-family: Helvetica; margin-top:-150px;"><?php echo date("Y-m-d H:i:s"); ?> CST</p>

-->