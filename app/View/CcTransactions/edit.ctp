<div class="ccTransactions form">
<?php echo $this->Form->create('CcTransaction');?>
	<fieldset>
 		<legend><?php __('Edit CcTransaction');?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('member_id');
		echo $this->Form->input('ip');
		echo $this->Form->input('amount');
		echo $this->Form->input('cc_last_4');
		echo $this->Form->input('expire');
		echo $this->Form->input('order_nbr');
		echo $this->Form->input('approval_code');
		echo $this->Form->input('ref_code');
		echo $this->Form->input('approved');
		echo $this->Form->input('error');
		echo $this->Form->input('full_error');
		echo $this->Form->input('message');
		echo $this->Form->input('avs');
		echo $this->Form->input('trans_date');
		echo $this->Form->input('trans_type');
		echo $this->Form->input('trans_purpose');
	?>
	</fieldset>
<?php echo $this->Form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $this->Form->value('CcTransaction.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $this->Form->value('CcTransaction.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List CcTransactions', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Members', true), array('controller' => 'members', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Member', true), array('controller' => 'members', 'action' => 'add')); ?> </li>
	</ul>
</div>
