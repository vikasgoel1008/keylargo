<div class="ccTransactions index">

<h2>Transaction History</h2>

<?php echo $this->element('members_menu');?>

<p>
<?php
$this->Paginator->options(array('url' => $this->passedArgs));

echo $this->Paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));


?></p>

<div class="box">
<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $this->Paginator->sort('order_nbr');?></th>
	<th><?php echo $this->Paginator->sort('trans_date');?></th>
	<th><?php echo $this->Paginator->sort('amount');?></th>
	<th><?php echo $this->Paginator->sort('approved');?></th>
	<th><?php echo $this->Paginator->sort('trans_purpose');?></th>
</tr>
<?php foreach ($ccTransactions as $ccTransaction): ?>
	<tr  class="highlight">
		<td>
			<?php echo $this->Html->link($ccTransaction['CcTransaction']['order_nbr'], array('action' => 'view', $ccTransaction['CcTransaction']['id'])); ?>
		</td>
		<td>
			<?php echo $ccTransaction['CcTransaction']['trans_date']; ?>
		</td>
		<td>
			<?php echo $ccTransaction['CcTransaction']['amount']; ?>
		</td>
		<td>
			<?php echo $ccTransaction['CcTransaction']['approved']; ?>
		</td>
		<td>
			<?php echo $ccTransaction['CcTransaction']['trans_purpose']; ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>

<p id="pagin">
	<?php echo $this->Paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $this->Paginator->numbers();?>
	<?php echo $this->Paginator->next(__('next', true).' >>', array(), null, array('class' => 'disabled'));?>
<p/>

<span class="clear"></span>
	
</div>

