<div class="ccTransactions index">
	<h2>Transactions</h2>
	
	<?php echo $this->element('members_menu');?>
	<?php $this->Paginator->options(array('url' => $this->passedArgs));?>
	
	<div class="box">
		<p>&nbsp;</p>
		<table cellpadding="0" cellspacing="0">
			
			<!--
			<tr class="highlight">	
				<td colspan="6"><?php  echo $this->Html->link('Add Transaction', array('action' => 'add'));?></td>
			</tr>
			-->
			
			<?php if (isset($member)) : ?>
			<tr class="highlight"><td colspan="6"><?php echo $member['Member']['name']; ?> | <em><?php echo $member['Member']['email']; ?></em> | <?php echo $member['Member']['login_username']; ?></td></tr>
			
			<tr class="highlight"><td colspan="6">Auth Profiles: | <?php echo $member['Member']['auth_cust_id']; ?> | <?php echo $member['Member']['auth_payment_id']; ?> | 
				<?php echo $this->Html->link("Update CIM Profile","http://newsgroupdirect.com/scripts/script_update_cim_payment_profile.php?id=".$member['Member']['id'], array("target" => "_blank"));  ?>
		
			</td></tr>
			
			<tr class="highlight"><td colspan="6"><?php if (!empty($pd['PaymentDetail']['cc_card_type'])) {echo $pd['PaymentDetail']['cc_card_type'] . " | ";} ?>
				xxxxxxxxxx<?php echo $pd['PaymentDetail']['cc_last_4']; ?> | 
				<?php echo substr($pd['PaymentDetail']['cc_card_exp'],4,2) . '/' . substr($pd['PaymentDetail']['cc_card_exp'],0,4); ?> | 
				<?php echo $pd['PaymentDetail']['cc_change_date']; ?>	
			</td></tr>
			
			<?php endif; ?>
			
			<tr><td colspan="6"></td></tr>
		
			<tr>
				<td><?php echo $this->Paginator->sort('order_nbr');?></td>
				<td><?php echo $this->Paginator->sort('trans_date');?></td>
				<td><?php echo $this->Paginator->sort('plan_id');?></td>
				<td><?php echo $this->Paginator->sort('amount');?></td>
				<td><?php echo $this->Paginator->sort('approved');?></td>
				<td><?php echo $this->Paginator->sort('trans_purpose');?></td>
			</tr>
			
			<?php foreach ($ccTransactions as $ccTransaction): ?>
			
			<tr  class="highlight">
				<td>
					<?php 
						if (!empty($ccTransaction['CcTransaction']['order_nbr'])) {
							echo $this->Html->link($ccTransaction['CcTransaction']['order_nbr'], array('action' => 'view', $ccTransaction['CcTransaction']['id']));
						}
						else {
							echo $this->Html->link("Order Number not Available", array('action' => 'view', $ccTransaction['CcTransaction']['id']));
						}
					?>
				</td>
				<td><?php echo $ccTransaction['CcTransaction']['trans_date']; ?></td>
				<td><?php echo $ccTransaction['Plan']['name']; ?></td>
				<td><?php echo $ccTransaction['CcTransaction']['amount']; ?></td>
				<td><?php echo $ccTransaction['CcTransaction']['approved']; ?></td>
				<td><?php echo $ccTransaction['CcTransaction']['trans_purpose']; ?></td>
			</tr>
			
			<?php endforeach; ?>
			<tr class="highlight">	
				<td colspan="6"><?php  echo $this->Paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)));?></td>
			</tr>
			
		</table>

		<p id="pagin">
			<?php echo $this->Paginator->first();?>
		 	<?php echo $this->Paginator->prev('< previous', "", "", "");?>
		 	<?php //echo $this->Paginator->prev() ?> 
			<?php echo $this->Paginator->numbers();?>  
		 	<?php //echo $this->Paginator->next() ?>
			<?php //echo $this->Paginator->counter(); ?>
			<?php echo $this->Paginator->next('next >', "", "", "");?>
			<?php echo $this->Paginator->last();?>
		<p/>

		<span class="clear"></span>
	</div>
</div>

