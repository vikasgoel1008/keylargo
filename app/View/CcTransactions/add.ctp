<h2>Add Customer Transaction</h2>

<?php echo $this->element('members_menu');?>

<?php echo $this->Form->create('CcTransaction');?>
<fieldset>
	
	<?php echo $this->Form->input('member_id',array('type'=>'text','class'=>'small','label'=>'Member Id', 'value' => $this->Session->read('Member.id')));?><br class="hid" />
	<?php echo $this->Form->input('ip',array('type'=>'text','class'=>'small','label'=>'IP Address'));?><br class="hid" />
	<?php echo $this->Form->input('amount',array('type'=>'text','class'=>'tiny','label'=>'Amount'));?><br class="hid" />
	<?php echo $this->Form->input('cc_last_4',array('type'=>'text','class'=>'tiny','label'=>'Last 4 CC'));?><br class="hid" />
	<?php echo $this->Form->input('expire',array('type'=>'text','class'=>'tiny','label'=>'Expiration Date (YYYYMM)'));?><br class="hid" />
	<?php echo $this->Form->input('order_nbr',array('type'=>'text','class'=>'large','label'=>'Order'));?><br class="hid" />
	<?php echo $this->Form->input('approval_code',array('type'=>'text','class'=>'large','label'=>'Approval Code'));?><br class="hid" />
	<?php echo $this->Form->input('ref_code',array('type'=>'text','class'=>'large','label'=>'Reference Code'));?><br class="hid" />
	<?php echo $this->Form->input('approved',array('type'=>'text','class'=>'large','label'=>'Approved Status'));?><br class="hid" />
	<?php echo $this->Form->input('error',array('type'=>'text','class'=>'large','label'=>'Error'));?><br class="hid" />
	<?php echo $this->Form->input('full_error',array('type'=>'text','class'=>'large','label'=>'Full Error Message'));?><br class="hid" />
	<?php echo $this->Form->input('message',array('type'=>'text','class'=>'large','label'=>'Message'));?><br class="hid" />
	<?php echo $this->Form->input('avs',array('type'=>'text','class'=>'large','label'=>'Avs'));?><br class="hid" />
	<?php echo $this->Form->input('trans_date',array('type'=>'text','class'=>'small','label'=>'Transaction Date'));?><br class="hid" />
	<?php echo $this->Form->input('trans_type',array('type'=>'text','class'=>'small','label'=>'Transaction Type'));?><br class="hid" />
	<?php echo $this->Form->input('trans_purpose',array('type'=>'text','class'=>'small','label'=>'Reason for Transaction'));?><br class="hid" />
	
	<p></p>
	<?php echo $this->Form->end(array('label'=>'Add Transaction','class'=>'button submit'));?>
	<span class="clear"></span>
</fieldset>