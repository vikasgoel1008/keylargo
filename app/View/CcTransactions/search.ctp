<div class="ccTransactions search">
	<h2>Transaction Search Results</h2>
	
	<?php //echo $this->element('members_menu');?>
	<?php $this->Paginator->options(array('url' => $this->passedArgs));?>
	
	<div class="box">
		<p>&nbsp;</p>
		<table cellpadding="0" cellspacing="0">
			<tr class="highlight">	
				<td colspan="5"><?php  echo $this->Paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)));?></td>
			</tr>
			
			<tr>
				<td><?php echo $this->Paginator->sort('order_nbr');?></td>
				<td><?php echo $this->Paginator->sort('cc_last_4');?></td>
				<td><?php echo $this->Paginator->sort('ref_code');?></td>
				<td><?php echo $this->Paginator->sort('trans_date');?></td>
				<td><?php echo $this->Paginator->sort('amount');?></td>
			</tr>
			
			<?php foreach ($ccTransactions as $ccTransaction): ?>
			
			<tr  class="highlight">
				<td><?php echo $this->Html->link($ccTransaction['CcTransaction']['order_nbr'], array('action' => 'view', $ccTransaction['CcTransaction']['id'])); ?></td>
				<td><?php echo $ccTransaction['CcTransaction']['cc_last_4']; ?></td>
				<td><?php echo $ccTransaction['CcTransaction']['ref_code']; ?></td>
				<td><?php echo $ccTransaction['CcTransaction']['trans_date']; ?></td>
				<td><?php echo $ccTransaction['CcTransaction']['amount']; ?></td>
			</tr>
			
			<?php endforeach; ?>
		</table>

		<p id="pagin">
			<?php echo $this->Paginator->first();?>
		 	<?php echo $this->Paginator->prev('< previous', "", "", "");?>
		 	<?php //echo $this->Paginator->prev() ?> 
			<?php echo $this->Paginator->numbers();?>  
		 	<?php //echo $this->Paginator->next() ?>
			<?php //echo $this->Paginator->counter(); ?>
			<?php echo $this->Paginator->next('next >', "", "", "");?>
			<?php echo $this->Paginator->last();?>
		<p/>

		<span class="clear"></span>
	</div>
</div>

