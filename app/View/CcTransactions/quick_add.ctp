<h2>Add Customer Transaction</h2>

<?php echo $this->element('members_menu');?>

<?php echo $this->Form->create('CcTransaction');?>
<fieldset>
	
	<?php echo $this->Form->input('member_id',array('type'=>'text','class'=>'small','label'=>'Member Id', 'value' => $this->Session->read('Member.id')));?><br class="hid" />
	<?php echo $this->Form->input('order_nbr',array('type'=>'text','class'=>'tiny','label'=>'Order'));?><br class="hid" />
	<?php echo $this->Form->input('amount',array('type'=>'text','class'=>'small','label'=>'Amount'));?><br class="hid" />
	<?php echo $this->Form->input('trans_date',array('type'=>'text','class'=>'tiny','label'=>'Date'));?><br class="hid" />
	<?php echo $this->Form->input('trans_type',array('type'=>'text','class'=>'tiny','label'=>'Type'));?><br class="hid" />
	<?php echo $this->Form->input('trans_purpose',array('type'=>'text','class'=>'tiny','label'=>'Purpose'));?><br class="hid" />
	
	<p></p>
	<?php echo $this->Form->end(array('label'=>'Create Account','class'=>'button submit'));?>
	<span class="clear"></span>
</fieldset>