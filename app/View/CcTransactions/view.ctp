<h2>Transactions</h2>

<?php echo $this->element('members_menu');?>

<div class="box">
	<p>&nbsp;</p>

	<table>
		<?php if ($ccTransaction['CcTransaction']['currency'] == 'GBP') :?>
		<?php $curSymbol = "&#163; "; ?>
				
		<tr class="highlight"><td colspan="2"><span class="notgood">This is a WorldPay Transaction</span></td></tr>
		<?php else: $curSymbol = "$"; ?>
		<?php endif; ?>
		
		<?php if ($ccTransaction['Member']['is_free_trial'] == "Y"): ?>
		<tr class="highlight"><td colspan="2"><span class="notgood">This customer is currently within their free trial period.</span></td></tr>
		<?php endif; ?>
		
		<tr class="highlight"><td>Member:</td><td><?php echo $this->Html->link($ccTransaction['Member']['name'], array('controller' => 'members', 'action' => 'view', $ccTransaction['Member']['id'])); ?></td></tr>
		<tr class="highlight"><td>Amount:</td><td><?php echo $curSymbol . $ccTransaction['CcTransaction']['amount']; ?></td></tr>
		<tr class="highlight"><td>Purchased Plan:</td><td><?php echo $ccTransaction['Plan']['name']; ?></td></tr>
		<tr class="highlight"><td>Credit Card:</td><td><?php echo $ccTransaction['CcTransaction']['cc_last_4']; ?></td></tr>
		<tr class="highlight"><td>Expire Date:</td><td><?php echo $ccTransaction['CcTransaction']['expire']; ?></td></tr>
		<tr class="highlight"><td>Transaction Date:</td><td><?php echo $ccTransaction['CcTransaction']['trans_date']; ?></td></tr>
		<tr class="highlight"><td>Type of Transaction:</td><td><?php echo $ccTransaction['CcTransaction']['trans_type']; ?></td></tr>
		<tr class="highlight"><td>Reason for Transaction:</td><td><?php echo $ccTransaction['CcTransaction']['trans_purpose']; ?></td></tr>
		<tr class="highlight">
			<td>Originating IP:</td>
			<td><?php echo $this->Html->link($ccTransaction['CcTransaction']['ip'], "http://www.ip2location.com/".$ccTransaction['CcTransaction']['ip'], array("target" => "_blank")); ?></td>
		</tr>
		<tr class="highlight"><td></td><td></td></tr>
		
		<tr class="highlight"><td>Transaction Status:</td><td><?php echo $ccTransaction['CcTransaction']['approved']; ?></td></tr>
		<tr class="highlight"><td>Order Number:</td><td><?php echo $ccTransaction['CcTransaction']['order_nbr']; ?></td></tr>
		<tr class="highlight"><td>Approval Code:</td><td><?php echo $ccTransaction['CcTransaction']['approval_code']; ?></td></tr>
		<tr class="highlight"><td>Reference Code:</td><td><?php echo $ccTransaction['CcTransaction']['ref_code']; ?></td></tr>
		<tr class="highlight"><td>Message</td><td><?php echo $ccTransaction['CcTransaction']['message']; ?></td></tr>
		<tr class="highlight"><td>Avs:</td><td><?php echo $ccTransaction['CcTransaction']['avs']; ?></td></tr>
		<tr class="highlight"><td>Transaction Error:</td><td><?php echo $ccTransaction['CcTransaction']['error']; ?></td></tr>
		<tr class="highlight"><td>Full Error:</td><td><?php echo $ccTransaction['CcTransaction']['full_error']; ?></td></tr>
		<tr class="highlight"><td></td><td></td></tr>
		<tr class="highlight">
			<td>Charged Amount after Refunds:</td>
			<td><?php if ($ccTransaction['CcTransaction']['approved'] == "APPROVED" ) :?>
					
					<?php echo $curSymbol . $refunds[0][0]['total_charges']; 
					
					/*
					if ($refunds[0][0]['total_charges'] > 0 && $ccTransaction['CcTransaction']['currency'] != 'GBP') {
						echo "&nbsp;&nbsp;" . $this->Html->link(__('refund', true), array('action' => 'refund', $ccTransaction['CcTransaction']['id']));
					}
					
					if ($refunds[0][0]['total_charges'] < $ccTransaction['CcTransaction']['amount']) {
						echo "&nbsp;&nbsp;" . $this->Html->link(__('view refunds', true), array('action' => 'related', $ccTransaction['CcTransaction']['order_nbr']));
					}
					*/
					?>
				<?php else: ?>
					0.00
				<?php endif; ?>
			</td>
		</tr>
		<tr class='highlight']>
			<td>Refund/void order:</td>
			<td>
				<a href="/members/void/<?php echo $ccTransaction['CcTransaction']['order_nbr']; ?>">Refund/void order</a>
			</td>
		</tr>

	</table>
	
	
	<span class="clear"></span>
	
	<span class="clear"></span>
		
</div>
	