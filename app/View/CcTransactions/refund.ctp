<h2>Refund Transaction</h2>

<?php echo $this->element('members_menu');?>

<?php echo $this->Form->create('CcTransaction', array('action' => 'refund'));?>
<fieldset>

	<?php echo $this->Form->hidden('id');?>		
	<?php echo $this->Form->input('order_nbr',array('class'=>'medium','label'=>'Order','readonly' => 'readonly'));?><br class="hid" />
	<?php echo $this->Form->input('amount',array('type'=>'text','class'=>'tiny','label'=>'Amount', 'value' => $this->Session->read('MaxRefund')));?><br class="hid" />
	<?php echo $this->Form->input('trans_purpose',array('type'=>'text','class'=>'medium','label'=>'Refund Purpose', 'value' => ''));?><br class="hid" />
	
	<p></p>
	<?php echo $this->Form->end(array('label'=>'Refund Transaction','class'=>'button submit'));?>
	<span class="clear"></span>
</fieldset>