<div class="risks index">
	<h2>LTV Reporting - <?php echo $report_date; ?></h2>
	
	<?php $this->Paginator->options(array('url' => $this->passedArgs));?>
	
	<div class="box">
		<p>&nbsp;</p>
		<table cellpadding="0" cellspacing="0">
			
			<tr>
				<td><?php echo $this->Paginator->sort('frequency');?></td>
				<td><?php echo $this->Paginator->sort('LTV Type','subkey');?></td>
				<td><?php echo $this->Paginator->sort('value');?></td>
			</tr>
			
			<?php foreach ($reports as $report): ?>
			
			<tr  class="highlight">
				<td><?php echo $report['NgdReport']['frequency']; ?></td>
				<td><?php echo $report['NgdReport']['subkey']; ?></td>
				<td><?php echo $report['NgdReport']['value']; ?></td>
			</tr>
			
			<?php endforeach; ?>
			
			<tr class="highlight">	
				<td colspan="8"><?php  echo $this->Paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)));?></td>
			</tr>
			
		</table>

		<p id="pagin">
			<?php echo $this->Paginator->first();?>
		 	<?php echo $this->Paginator->prev('< previous', "", "", "");?>
		 	<?php //echo $this->Paginator->prev() ?> 
			<?php echo $this->Paginator->numbers();?>  
		 	<?php //echo $this->Paginator->next() ?>
			<?php //echo $this->Paginator->counter(); ?>
			<?php echo $this->Paginator->next('next >', "", "", "");?>
			<?php echo $this->Paginator->last();?>
		<p/>

		<span class="clear"></span>
	</div>
</div>

