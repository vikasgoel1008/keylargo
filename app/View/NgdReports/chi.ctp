<div class="risks index">
	<h2>CHI Reporting - <?php echo $report_date; ?></h2>
	
	<?php $this->Paginator->options(array('url' => $this->passedArgs));?>
	
	<div class="box">
		<p>&nbsp;</p>
		<table cellpadding="0" cellspacing="0">
			
			<tr>
				<td><?php echo $this->Paginator->sort('member_id');?></td>
				<td><?php echo $this->Paginator->sort('plan_id');?></td>
				<td><?php echo $this->Paginator->sort('Usage %','data0');?></td>
				<td><?php echo $this->Paginator->sort('Cancels 30','data1');?></td>
				<td><?php echo $this->Paginator->sort('Cancels 90','data2');?></td>
				<td><?php echo $this->Paginator->sort('Invoices 30','data3');?></td>
				<td><?php echo $this->Paginator->sort('Invoices 90','data4');?></td>
				<td><?php echo $this->Paginator->sort('value');?></td>
			</tr>
			
			<?php foreach ($reports as $report): ?>
			
			<tr  class="highlight">
				<td><?php echo $this->Html->link($report['Member']['login_username'], array('controller' => 'members', 'action' => 'view', $report['NgdReport']['member_id'])); ?></td>
				<td><?php echo $report['Plan']['name']; ?> (<?php echo $report['NgdReport']['plan_id']; ?>)</td>
				<td><?php echo $report['NgdReport']['data0']; ?></td>
				<td><?php echo $report['NgdReport']['data1']; ?></td>
				<td><?php echo $report['NgdReport']['data2']; ?></td>
				<td><?php echo $report['NgdReport']['data3']; ?></td>
				<td><?php echo $report['NgdReport']['data4']; ?></td>
				<td><?php echo $report['NgdReport']['value']; ?></td>
				
			</tr>
			
			<?php endforeach; ?>
			
			<tr class="highlight">	
				<td colspan="8"><?php  echo $this->Paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)));?></td>
			</tr>
			
		</table>

		<p id="pagin">
			<?php echo $this->Paginator->first();?>
		 	<?php echo $this->Paginator->prev('< previous', "", "", "");?>
		 	<?php //echo $this->Paginator->prev() ?> 
			<?php echo $this->Paginator->numbers();?>  
		 	<?php //echo $this->Paginator->next() ?>
			<?php //echo $this->Paginator->counter(); ?>
			<?php echo $this->Paginator->next('next >', "", "", "");?>
			<?php echo $this->Paginator->last();?>
		<p/>

		<span class="clear"></span>
	</div>
</div>

