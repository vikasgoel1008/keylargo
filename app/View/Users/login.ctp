<?php echo $this->Form->create('User', array('action' => 'login','class'=>'login-form'));?>
	<h3 class="form-title">Login to your account</h3>
	<div class="alert alert-danger display-hide">
		<button class="close" data-close="alert"></button>
		<span>
		Enter any username and password. </span>
	</div>
	<div class="form-group">
		<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
		<label class="control-label visible-ie8 visible-ie9">Username</label>
		<div class="input-icon">
			<i class="fa fa-user"></i>
			<?php echo $this->Form->input('username',array('type'=>'text','class'=>'form-control placeholder-no-fix','autocomplete'=>'off','placeholder'=>'Username','label'=>false,'div'=>false));?>
			
		</div>
	</div>
	<div class="form-group">
		<label class="control-label visible-ie8 visible-ie9">Password</label>
		<div class="input-icon">
			<i class="fa fa-lock"></i>
			<?php echo $this->Form->input('password',array('type'=>'password','class'=>'form-control placeholder-no-fix','autocomplete'=>'off','placeholder'=>'Password','label'=>false,'div'=>false));?>
		</div>
	</div>
	<div class="form-actions">
		<button type="submit" class="btn blue pull-right">
		Login <i class="m-icon-swapright m-icon-white"></i>
		</button>
	</div>
	
<?php echo $this->Form->end(); ?>