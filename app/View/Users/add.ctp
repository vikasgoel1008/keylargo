<p id="tip" class="alert">
	<span class="txt"><span class="icon"></span><strong>Tip:</strong> Enter full or partial email addresses, first name, last name, etc.</span>
	<a title="Close" class="close"><span class="bg"></span>Close</a>
</p>
<h2>Add an Admin</h2>
<ul class="tabs">
	<li class="active"><a href="#" title="Item1"><span>Admin Users</span></a></li>
</ul>
<?php echo $this->Form->create('User');?>
<fieldset>
	<?php echo $this->Form->input('username',array('type'=>'text','class'=>'medium','label'=>'Username'));?>
	<?php echo $this->Form->input('password',array('type'=>'text','class'=>'medium','label'=>'Password'));?>
	<?php echo $this->Form->input('group_id',array('type'=>'text','class'=>'medium','label'=>'Group Name'));?>
	<p>&nbsp;</p>
	<?php echo $this->Form->end(array('label'=>'Submit','class'=>'button submit'));?>
	<p></p>
	<span class="clear"></span>
	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('List Users', true), array('action' => 'index'));?></li>
		</ul>
	</div>
</fieldset>
