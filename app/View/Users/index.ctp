<h2>Admin Users</h2>
<ul class="tabs">
	<li class="active"><?php echo $this->Html->link($this->Html->tag('span', 'Users'), array('action' => 'index'), array('escape'=>false, 'title'=>"Item1")); ?></li>
	<li><?php echo $this->Html->link($this->Html->tag('span', 'Add User'), array('action' => 'add'), array('escape'=>false)); ?></li>
</ul>
<div class="box">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('username');?></th>
			<th><?php echo $this->Paginator->sort('password');?></th>
			<th><?php echo $this->Paginator->sort('group_id');?></th>
			<th><?php echo $this->Paginator->sort('created');?></th>
			<th><?php echo $this->Paginator->sort('modified');?></th>
			<th class="actions"><?php __('Actions');?></th>
		</tr>
		
		<?php foreach ($users as $user): ?>
		<tr class="highlight">
			<td>
				<?php echo $user['User']['id']; ?>
			</td>
			<td>
				<?php echo $user['User']['username']; ?>
			</td>
			<td>
				<?php echo $user['User']['password']; ?>
			</td>
			<td>
				<?php echo $user['User']['group_id']; ?>
			</td>
			<td>
				<?php echo $user['User']['created']; ?>
			</td>
			<td>
				<?php echo $user['User']['modified']; ?>
			</td>
			<td class="action">
				<ul>
					<li><?php echo $this->Html->link(__('', true), array('action' => 'edit', $user['User']['id']), array('class'=>'edit')); ?></li>
					<li><a class="add" href="#" title="add"></a></li>
					<li><?php echo $this->Html->link(__('', true), array('action' => 'delete', $user['User']['id']),array('class'=>'delete'), sprintf(__('Are you sure you want to delete # %s?', true), $user['User']['id'])); ?></li>
					<li><a class="mark" href="#" title="mark"></a></li>
				</ul>
			</td>
			
		</tr>
		<?php endforeach; ?>
	</table>
	
	<div class="paging">
		<p id='pagin'>
		<?php echo $this->Paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
		<?php echo $this->Paginator->next(__('next', true).' >>', array(), null, array('class' => 'disabled'));?>
		</p>
	</div>
	
</div>



