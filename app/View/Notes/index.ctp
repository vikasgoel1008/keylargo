<div class="notes index">
	<h2>Customer Notes</h2>
	
	<?php echo $this->element('members_menu');?>
	<?php $this->Paginator->options(array('url' => $this->passedArgs));?>
	
	<div class="box">
		<p>&nbsp;</p>
		<table cellpadding="0" cellspacing="0">
			<tr class="highlight">	
				<td colspan="5"><?php  echo $this->Html->link('Add Note', array('action' => 'add', $member['Member']['id']));?></td>
			</tr>
			<tr class="highlight"><td colspan="5"><?php echo $member['Member']['name']; ?> | <em><?php echo $member['Member']['email']; ?></em> | <?php echo $member['Member']['login_username']; ?></td></tr>
			
			<tr>
				<td><?php echo $this->Paginator->sort('id');?></td>
				<td><?php echo $this->Paginator->sort('subject');?></td>
				<td><?php echo $this->Paginator->sort('date_added');?></td>
			</tr>
			
			<?php foreach ($notes as $note): ?>
			
			<tr  class="highlight">
				<td><?php echo $this->Html->link($note['Note']['id'], array('action' => 'view', $note['Note']['id'])); ?></td>
				<td><?php echo $note['Note']['subject'];?></td>
				<td><?php echo $note['Note']['date_added']; ?></td>
			</tr>
			
			<?php endforeach; ?>
			
			<tr class="highlight">	
				<td colspan="5"><?php  echo $this->Paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)));?></td>
			</tr>
			
		</table>

		<p id="pagin">
			<?php echo $this->Paginator->first();?>
		 	<?php echo $this->Paginator->prev('< previous', "", "", "");?>
		 	<?php //echo $this->Paginator->prev() ?> 
			<?php echo $this->Paginator->numbers();?>  
		 	<?php //echo $this->Paginator->next() ?>
			<?php //echo $this->Paginator->counter(); ?>
			<?php echo $this->Paginator->next('next >', "", "", "");?>
			<?php echo $this->Paginator->last();?>
		<p/>

		<span class="clear"></span>
	</div>
</div>

