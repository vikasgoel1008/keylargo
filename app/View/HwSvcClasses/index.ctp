<div class="hwSvcClasses index">
	<h2>Service Classes</h2>
	
	<?php //echo $this->element('members_menu');?>
	<?php $this->Paginator->options(array('url' => $this->passedArgs));?>
	
	<div class="box">
		<p>&nbsp;</p>
		<table cellpadding="0" cellspacing="0">
		
			<tr class="highlight">	
				<td colspan="3"><?php  echo $this->Paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)));?></td>
			</tr>
			<tr>
				<td><?php echo $this->Paginator->sort('name');?></td>
				<td><?php echo $this->Paginator->sort('service_class_id');?></td>
				<td>Action</td>
			</tr>
			
			<?php foreach ($services as $service): ?>
			
			<tr  class="highlight">
				<td><?php echo $this->Html->link($service['HwSvcClass']['name'], array('action' => 'view', $service['HwSvcClass']['id']));?></td>
				<td><?php echo $service['HwSvcClass']['service_class_id']; ?></td>
				<td><?php echo $this->Html->link('Service Rate Hours', array('action' => 'rate_hours', $service['HwSvcClass']['service_class_id']));?></td>
			</tr>
			
			<?php endforeach; ?>
		</table>

		<p id="pagin">
			<?php echo $this->Paginator->first();?>
		 	<?php echo $this->Paginator->prev('< previous', "", "", "");?>
		 	<?php //echo $this->Paginator->prev() ?> 
			<?php echo $this->Paginator->numbers();?>  
		 	<?php //echo $this->Paginator->next() ?>
			<?php //echo $this->Paginator->counter(); ?>
			<?php echo $this->Paginator->next('next >', "", "", "");?>
			<?php echo $this->Paginator->last();?>
		<p/>

		<span class="clear"></span>
	</div>
</div>




