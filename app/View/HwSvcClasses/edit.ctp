<div class="coupons form">
	<h2>Edit Service Rate</h2>

	<?php // echo $this->element('coupons_menu');?>

	<?php echo $this->Form->create('HwSvcClass');?>
	<fieldset>
		<?php echo $this->Form->input('name',array('class'=>'small','label'=>'Select Service Class','readonly'=>'readonly'));?><br class="hid" />
		<?php echo $this->Form->input('service_class_id',array('class'=>'small','label'=>'Select Service Class','readonly'=>'readonly'));?><br class="hid" />
		<?php echo $this->Form->input('start_time',array('type'=>'text','class'=>'tiny','label'=>'Service Start Time'));?><br class="hid" /><p>Format: HH:MM (24Hr)</p>
		<?php echo $this->Form->input('end_time',array('type'=>'text','class'=>'tiny','label'=>'Service Stop Time'));?><br class="hid" /><p>Format: HH:MM (24Hr)</p>	
		<?php echo $this->Form->input('ylw_threshold',array('type'=>'text','class'=>'tiny','label'=>'Yellow Zone GB'));?><br class="hid" />
		<?php echo $this->Form->input('ylw_rate_min',array('type'=>'text','class'=>'tiny','label'=>'Yellow Zone Min Rate'));?><br class="hid" />
		<?php echo $this->Form->input('ylw_rate_max',array('type'=>'text','class'=>'tiny','label'=>'Yellow Zone Max Rate'));?><br class="hid" />
		<?php echo $this->Form->input('red_threshold',array('type'=>'text','class'=>'tiny','label'=>'Red Zone GB'));?><br class="hid" />
		<?php echo $this->Form->input('red_rate_min',array('type'=>'text','class'=>'tiny','label'=>'Red Zone Min Rate'));?><br class="hid" />
		<?php echo $this->Form->input('red_rate_max',array('type'=>'text','class'=>'tiny','label'=>'Red Zone Max Rate'));?><br class="hid" />
		
		<p></p>
		<?php echo $this->Form->end(array('label'=>'Update Service Rate Hour','class'=>'button submit'));?>
		<span class="clear"></span>
	</fieldset>
</div>
