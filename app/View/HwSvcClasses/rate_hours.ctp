<div class="hwSvcClasses index">
	<h2>Service Class Hours</h2>
	
	<?php //echo $this->element('members_menu');?>
	
	<div class="box">
		<p>&nbsp;</p>
		<table cellpadding="0" cellspacing="0">
		
			<tr class="highlight">	
				<td colspan="9"><?php echo $this->Html->link("Add New $service_class_id Rate", array('action' => 'add', $service_class_id));?></td>
			</tr>
			<tr>
				<td>Service</td>
				<td>Start Time</td>
				<td>End Time</td>
				<td>Ylw GB</td>
				<td>Ylw Min</td>
				<td>Ylw Max</td>
				<td>Red GB</td>
				<td>Red Min</td>
				<td>Red Max</td>
			</tr>
			
			<?php foreach ($services as $service): ?>
			
			<tr  class="highlight">
				<td><?php echo $this->Html->link($service['HwSvcClass']['name'], array('action' => 'edit', $service['HwSvcClass']['id']));?></td>
				<td><?php echo $service['HwSvcClass']['start_time']; ?></td>
				<td><?php echo $service['HwSvcClass']['end_time']; ?></td>
				<td><?php echo $service['HwSvcClass']['ylw_threshold']; ?></td>
				<td><?php echo $service['HwSvcClass']['ylw_rate_min']; ?></td>
				<td><?php echo $service['HwSvcClass']['ylw_rate_max']; ?></td>
				<td><?php echo $service['HwSvcClass']['red_threshold']; ?></td>
				<td><?php echo $service['HwSvcClass']['red_rate_min']; ?></td>
				<td><?php echo $service['HwSvcClass']['red_rate_max']; ?></td>
				
			</tr>
			
			<?php endforeach; ?>
		</table>

		

		<span class="clear"></span>
	</div>
</div>




