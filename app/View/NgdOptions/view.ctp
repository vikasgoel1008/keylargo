<div class="ccTransactions index">
	<h2>Option Details</h2>

	<?php echo $this->element('options_menu');?>

	<div class="box">
		<p>
			
		</p>
		<table cellpadding="0" cellspacing="0">
			
			<tr class="highlight"><td colspan="2"><?php echo $this->Html->link('Edit Option', 'edit/'. $ngdOption['NgdOption']['id']);?></td></tr>
			<tr class="highlight">
				<td>Option Name:</td>	
				<td><?php echo $ngdOption['NgdOption']['name']; ?></td>			
			</tr>
			<tr class="highlight">
				<td>Key Name:</td>	
				<td><?php echo $ngdOption['NgdOption']['key_name']; ?></td>			
			</tr>
			<tr class="highlight">
				<td>Sub Key:</td>	
				<td><?php echo $ngdOption['NgdOption']['subkey']; ?></td>			
			</tr>
			<tr class="highlight">
				<td>Option Value:</td>	
				<td>
					<?php echo $ngdOption['NgdOption']['value']; ?>
					<?php if ($ngdOption['NgdOption']['key_name'] == 'conversion_rate') : ?>
					 | <a href="http://www.xe.com/ucc/convert.cgi?Amount=1.00&From=USD&To=<?php echo $ngdOption['NgdOption']['subkey']; ?>" target="_blank">See Current Rate</a>
					 | <?php echo $this->Html->link('Compare Rate', 'compare_conversion_rate/'. $ngdOption['NgdOption']['id']);?>
					<?php elseif ($ngdOption['NgdOption']['name'] == 'ngd' && $ngdOption['NgdOption']['key_name'] == 'signup_display') :?>
					 | <?php echo $layouts[$ngdOption['NgdOption']['value']];?>
					
					<?php endif;?>
				</td>			
			</tr>
			<?php if ($ngdOption['NgdOption']['key_name'] == 'conversion_rate') : ?>
			<tr class="highlight">
				<td colspan="2"><span class="notgood">
					If you are updating the conversion rate, get the value based off of 1 USD to <?php echo $ngdOption['NgdOption']['subkey']; ?>. </span> 
					
				</td>
			</tr>
			<?php endif;?>
					
		
		</table>
		
		<p id="pagin">
			&nbsp;
		</p>
		
		<span class="clear">&nbsp;</span>
	</div>
</div>
	