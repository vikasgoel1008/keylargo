<div class="options form">
	<h2>Edit Option</h2>

	<?php echo $this->element('options_menu');?>

	<?php echo $this->Form->create('NgdOption');?>
	<fieldset>
		<?php echo $this->Form->input('id',array('type'=>'hidden','class'=>'medium','label'=>'Option Name'));?><br class="hid" />
		<?php echo $this->Form->input('name',array('type'=>'text','class'=>'medium','label'=>'Option Name'));?><br class="hid" />
		<?php echo $this->Form->input('key_name',array('type'=>'text','class'=>'medium','label'=>'Key Name'));?><br class="hid" />
		<?php echo $this->Form->input('subkey',array('type'=>'text','class'=>'medium','label'=>'Sub Key'));?><br class="hid" />	
		
		<?php if ($this->data['NgdOption']['name'] == 'ngd' && $this->data['NgdOption']['key_name'] == 'signup_display') : ?>
		<?php echo $this->Form->input('value',array('type'=>'select','class'=>'medium','label'=>'Value', 'options'=>$layouts));?><br class="hid" />
		
		<?php else : ?>
		<?php echo $this->Form->input('value',array('type'=>'text','class'=>'medium','label'=>'Value'));?><br class="hid" />
		
		<?php endif; ?>
		
		<p></p>
		<?php echo $this->Form->end(array('label'=>'Edit','class'=>'button submit'));?>
		<span class="clear"></span>
	</fieldset>
</div>
