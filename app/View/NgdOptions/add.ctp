<div class="ngdOptions form">
	<h2>Add New Option</h2>

	<?php echo $this->element('options_menu');?>

	<?php echo $this->Form->create('NgdOption');?>
	<fieldset>
		<?php echo $this->Form->input('name',array('type'=>'text','class'=>'medium','label'=>'Option Name'));?><br class="hid" />
		<?php echo $this->Form->input('key_name',array('type'=>'text','class'=>'medium','label'=>'Key Name'));?><br class="hid" />
		<?php echo $this->Form->input('subkey',array('type'=>'text','class'=>'medium','label'=>'Sub Key'));?><br class="hid" />	
		<?php echo $this->Form->input('value',array('type'=>'text','class'=>'medium','label'=>'Value'));?><br class="hid" />
		<p></p>
		<?php echo $this->Form->end(array('label'=>'Submit Option','class'=>'button submit'));?>
		<span class="clear"></span>
	</fieldset>
</div>
