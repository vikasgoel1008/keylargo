<div class="ngd options index">
	<h2>Options</h2>
	
	<?php echo $this->element('options_menu');?>
	<?php $this->Paginator->options(array('url' => $this->passedArgs));?>
	
	<div class="box">
		<p>&nbsp;</p>
		<table cellpadding="0" cellspacing="0">
			<tr class="highlight">	
				<td colspan="5"><?php  echo $this->Paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)));?></td>
			</tr>
			
			<tr class="highlight">
				<td><?php echo $this->Paginator->sort('id');?></td>
				<td><?php echo $this->Paginator->sort('name');?></td>
				<td><?php echo $this->Paginator->sort('key_name');?></td>
				<td><?php echo $this->Paginator->sort('subkey');?></td>
			</tr>
			
			<?php foreach ($ngdOptions as $ngdOption): ?>
			
			<tr class="highlight">
				<td><?php echo $this->Html->link('View Option', array('action' => 'view', $ngdOption['NgdOption']['id'])); ?></td>
				<td><?php echo $ngdOption['NgdOption']['name']; ?></td>
				<td><?php echo $ngdOption['NgdOption']['key_name']; ?></td>
				<td><?php echo $ngdOption['NgdOption']['subkey']; ?></td>
			</tr>
			
			<?php endforeach; ?>
		</table>

		<p id="pagin">
			<?php echo $this->Paginator->first();?>
		 	<?php echo $this->Paginator->prev('< previous', "", "", "");?>
		 	<?php //echo $this->Paginator->prev() ?> 
			<?php echo $this->Paginator->numbers();?>  
		 	<?php //echo $this->Paginator->next() ?>
			<?php //echo $this->Paginator->counter(); ?>
			<?php echo $this->Paginator->next('next >', "", "", "");?>
			<?php echo $this->Paginator->last();?>
		<p/>

		<span class="clear"></span>
	</div>
</div>

