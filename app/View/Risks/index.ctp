<div class="risks index">
	<h2>Risk Reporting - <?php echo $process_date;?></h2>
	
	<?php $this->Paginator->options(array('url' => $this->passedArgs));?>
	
	<div class="box">
		<p>&nbsp;</p>
		<table cellpadding="0" cellspacing="0">
			
			<tr class="highlight">	
				<td colspan="8">All figures are based on 30 days.</td>
			</tr>
			
			<tr>
				<td><?php echo $this->Paginator->sort('member_id');?></td>
				<td><?php echo $this->Paginator->sort('plan_id');?></td>
				<td><?php echo $this->Paginator->sort('status');?></td>
				<td><?php echo $this->Paginator->sort("Usage (GB)",'30_day_usage');?></td>
				<td><?php echo $this->Paginator->sort("# IPs",'30_day_ips_used');?></td>
				<td><?php echo $this->Paginator->sort("Trunks",'30_day_ip_trunks_used');?></td>
				<td><?php echo $this->Paginator->sort('abuse_warning');?></td>
				<td><?php echo $this->Paginator->sort('risk_score');?></td>
			</tr>
			
			<?php foreach ($risks as $risk): ?>
			
			<tr  class="highlight">
				<td><?php echo $this->Html->link($risk['Member']['login_username'], array('controller' => 'members', 'action' => 'view', $risk['Risk']['member_id'])); ?></td>
				<!--<td><?php echo $this->Html->link($risk['Plan']['name'], array('controller' => 'plans', 'action' => 'view', $risk['Risk']['plan_id'])); ?></td>-->
				<td><?php echo $risk['Plan']['name']; ?></td>
				<td><?php echo $risk['Risk']['status']; ?></td>
				<td><?php echo $risk['Risk']['30_day_usage']; ?></td>
				<td><?php echo $risk['Risk']['30_day_ips_used'];?></td>
				<td><?php echo $risk['Risk']['30_day_ip_trunks_used'];?></td>
				<td>
					<?php if ($risk['Risk']['abuse_warning'] == "N"): ?>
					<?php echo $this->Html->link($risk['Risk']['abuse_warning'], array('controller'=> 'members','action' => 'mark_abuser',$risk['Risk']['member_id']), null, sprintf(__('Do you want to flag this account %s as a potential abuser?', true), $risk['Member']['login_username']));?>
					<?php else : ?>
					<?php echo $risk['Risk']['abuse_warning'];?>
					<?php endif; ?>
				
					
				</td>
				<td><?php echo $risk['Risk']['risk_score']; ?></td>
			</tr>
			
			<?php endforeach; ?>
			
			<tr class="highlight">	
				<td colspan="8"><?php  echo $this->Paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)));?></td>
			</tr>
			
		</table>

		<p id="pagin">
			<?php echo $this->Paginator->first();?>
		 	<?php echo $this->Paginator->prev('< previous', "", "", "");?>
		 	<?php //echo $this->Paginator->prev() ?> 
			<?php echo $this->Paginator->numbers();?>  
		 	<?php //echo $this->Paginator->next() ?>
			<?php //echo $this->Paginator->counter(); ?>
			<?php echo $this->Paginator->next('next >', "", "", "");?>
			<?php echo $this->Paginator->last();?>
		<p/>

		<span class="clear"></span>
	</div>
</div>

