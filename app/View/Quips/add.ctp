<div class="quips form">
	<h2>Add New Quip</h2>

	<?php echo $this->element('quips_menu');?>

	<?php echo $this->Form->create('Quip');?>
	<fieldset>
		<?php echo $this->Form->input('text',array('type'=>'text','class'=>'medium','label'=>'Quip Text'));?><br class="hid" />
		<?php echo $this->Form->input('type',array('type'=>'text','class'=>'medium','label'=>'Quip Type'));?><br class="hid" />
		<?php echo $this->Form->input('status',array('type'=>'text','class'=>'medium','label'=>'Status'));?><br class="hid" />	
		<p></p>
		<?php echo $this->Form->end(array('label'=>'Submit Quip','class'=>'button submit'));?>
		<span class="clear"></span>
	</fieldset>
</div>
