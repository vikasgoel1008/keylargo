<div class="ccTransactions index">
	<h2>Account Invites</h2>
	
	<?php $this->Paginator->options(array('url' => $this->passedArgs));?>
	
	<div class="box">
		<p>&nbsp;</p>
		<table cellpadding="0" cellspacing="0">
		
			<tr class="highlight"><td colspan="6"><?php echo $this->Html->link('Bulk Create Invites', array('action' => 'bulk_create'));?></td></tr>
			
			<tr class="highlight">	
				<td colspan="6"><?php  echo $this->Paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)));?></td>
			</tr>
			<tr>
				<td><?php echo $this->Paginator->sort('auth_code');?></td>
				<td><?php echo $this->Paginator->sort('validated');?></td>
				<td><?php echo $this->Paginator->sort('plan_id');?></td>
				<td><?php echo $this->Paginator->sort('free_months');?></td>
				<td>Distributed</td>
				<td>Actions</td>
			</tr>
			
			<?php foreach ($freeAccountAuths as $accAuth): ?>
			
			<tr  class="highlight">
				<td><?php echo $this->Html->link($accAuth['FreeAccountAuth']['auth_code'], array('action' => 'view', $accAuth['FreeAccountAuth']['id']));?></td>
				<td><?php echo $accAuth['FreeAccountAuth']['validated']; ?></td>
				<td><?php echo $accAuth['FreeAccountAuth']['plan_id']; ?></td>
				<td><?php echo $accAuth['FreeAccountAuth']['free_months']; ?></td>
				
				<?php if(empty($accAuth['FreeAccountAuth']['email']) && $accAuth['FreeAccountAuth']['validated'] == 'N') : ?>				
				<td>N</td><td><?php echo $this->Html->link("Send Invite", array('action' => 'send_invite', $accAuth['FreeAccountAuth']['id']));?> | <?php echo $this->Html->link("Create TinyURL", array('action' => 'create_tiny', $accAuth['FreeAccountAuth']['id']));?></td>
				<?php else : ?>
				<td>Y</td>
				<td>&nbsp;</td>
				<?php endif; ?>
				
				
			</tr>
			
			<?php endforeach; ?>
		</table>

		<p id="pagin">
			<?php echo $this->Paginator->first();?>
		 	<?php echo $this->Paginator->prev('< previous', "", "", "");?>
		 	<?php //echo $this->Paginator->prev() ?> 
			<?php echo $this->Paginator->numbers();?>  
		 	<?php //echo $this->Paginator->next() ?>
			<?php //echo $this->Paginator->counter(); ?>
			<?php echo $this->Paginator->next('next >', "", "", "");?>
			<?php echo $this->Paginator->last();?>
		<p/>

		<span class="clear"></span>
	</div>
</div>




