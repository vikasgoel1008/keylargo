<h2>Send Invite</h2>

<?php echo $this->element('members_menu');?>

<?php echo $this->Form->create('FreeAccountAuth', array('action' => 'send_invite'));?>
<fieldset>
	<?php echo $this->Form->hidden('id'); ?> 

	<?php echo $this->Form->input('name',array('type'=>'text','class'=>'small','label'=>'Name'));?><br class="hid" />
	<?php echo $this->Form->input('email',array('type'=>'text','class'=>'small','label'=>'Email Address'));?><br class="hid" />
	<?php echo $this->Form->input('plan_id',array('type'=>'text','class'=>'tiny','label'=>'Plan Id'));?><br class="hid" />
	<?php echo $this->Form->input('free_months',array('type'=>'text','class'=>'tiny','label'=>'Free Months'));?><br class="hid" />
	<?php echo $this->Form->input('invited_by',array('type'=>'text','class'=>'small','label'=>'Invited By', 'value'=>'NewsgroupDirect'));?><br class="hid" />
	<?php echo $this->Form->input('auth_code',array('type'=>'text','class'=>'medium','label'=>'Invite Code', 'readonly'=>'readonly'));?><br class="hid" />
	<p></p>
	<?php echo $this->Form->end(array('label'=>'Update','class'=>'button submit'));?>
	<span class="clear"></span>
</fieldset>