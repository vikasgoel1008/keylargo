<div class="coupons form">
	<h2>New Credit</h2>

	<?php echo $this->Form->create('NgdCredit');?>
	<fieldset>
		<? if (isset($member)) : ?>
		<?php echo $this->Form->input('member_id',array('class'=>'small','label'=>'Member Id', 'readonly' => 'readonly', 'value' => $member['Member']['id']));?><br class="hid" />
		<? else : ?>
		<?php echo $this->Form->input('member_id',array('class'=>'small','label'=>'Member Id'));?><br class="hid" />
		<? endif; ?>
		<?php echo $this->Form->input('orig_credit_amount',array('type'=>'text','class'=>'tiny','label'=>'Credit Amount'));?><br class="hid" />
		<?php echo $this->Form->input('credit_reason',array('type'=>'text','class'=>'small','label'=>'Reason for Credit'));?><br class="hid" />	
		<?php echo $this->Form->input('for_tweet',array('options'=>array("N"=>"No","Y"=>"Yes"), 'type'=>'select','class'=>'tiny','label'=>'Credit for Tweet'));?><br class="hid" />
		<p></p>
		<?php echo $this->Form->end(array('label'=>'Submit Credit','class'=>'button submit'));?>
		<span class="clear"></span>
	</fieldset>
</div>
