<div class="ccTransactions index">
	<h2>Customer Credits</h2>
	
	<?php echo $this->element('members_menu');?>
	<?php $this->Paginator->options(array('url' => $this->passedArgs));?>
	
	<div class="box">
		<p>&nbsp;</p>
		<table cellpadding="0" cellspacing="0">
		
			<tr class="highlight"><td colspan="6"><?php echo $this->Html->link('Add Credit', array('action' => 'add', $member['Member']['id']));?></td></tr>
			<tr class="highlight"><td colspan="6"><?php echo $member['Member']['name']; ?> | <em><?php echo $member['Member']['email']; ?></em> | <?php echo $member['Member']['login_username']; ?></td></tr>
			
			<tr class="highlight">	
				<td colspan="6"><?php  echo $this->Paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)));?></td>
			</tr>
			<tr>
				<td><?php echo $this->Paginator->sort('date_added');?></td>
				<td><?php echo $this->Paginator->sort('orig_credit_amount');?></td>
				<td><?php echo $this->Paginator->sort('credit_amount');?></td>
				<td><?php echo $this->Paginator->sort('credit_used');?></td>
				<td><?php echo $this->Paginator->sort('forfeited');?></td>
				<td><?php echo $this->Paginator->sort('credit_reason');?></td>
			</tr>
			
			<?php foreach ($credits as $credit): ?>
			
			<tr  class="highlight">
				<td><?php echo $this->Html->link($credit['NgdCredit']['date_added'], array('action' => 'view', $credit['NgdCredit']['id']));?></td>
				<td><?php echo $credit['NgdCredit']['orig_credit_amount']; ?></td>
				<td><?php echo $credit['NgdCredit']['credit_amount']; ?></td>
				<td><?php echo $credit['NgdCredit']['credit_used']; ?></td>
				<td>
					<?php if ($credit['NgdCredit']['forfeited'] == 'Y'): ?>
						<?php echo $credit['NgdCredit']['forfeited']; ?>
					<?php elseif ($credit['NgdCredit']['credit_used'] == 'Y'): ?>
						<?php echo $credit['NgdCredit']['forfeited']; ?>
					<?php else : ?>				
						<?php echo $this->Html->link('Forfeit?', array('action' =>'forfeit_credit',$credit['NgdCredit']['id']), null, sprintf(__('Are you sure you want to forfeit this credit %s?', true), $credit['NgdCredit']['id'])); ?>
					<?php endif; ?>
					
				</td>
				<td><?php echo $credit['NgdCredit']['credit_reason']; ?></td>
			</tr>
			
			<?php endforeach; ?>
		</table>

		<p id="pagin">
			<?php echo $this->Paginator->first();?>
		 	<?php echo $this->Paginator->prev('< previous', "", "", "");?>
		 	<?php //echo $this->Paginator->prev() ?> 
			<?php echo $this->Paginator->numbers();?>  
		 	<?php //echo $this->Paginator->next() ?>
			<?php //echo $this->Paginator->counter(); ?>
			<?php echo $this->Paginator->next('next >', "", "", "");?>
			<?php echo $this->Paginator->last();?>
		<p/>

		<span class="clear"></span>
	</div>
</div>




