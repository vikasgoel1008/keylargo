<div class="affiliates index">
	<h2>Affiliates</h2>
	
	<?php echo $this->element('affiliates_menu');?>
	<?php $this->Paginator->options(array('url' => $this->passedArgs));?>
	
	<div class="box">
		<p>&nbsp;</p>
		<table cellpadding="0" cellspacing="0">
			<tr class="highlight">	
				<td colspan="6"><?php  echo $this->Paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)));?></td>
			</tr>
			
			<tr class="highlight">
				<td><?php echo $this->Paginator->sort('affiliate_id');?></td>
				<td><?php echo $this->Paginator->sort('name');?></td>
				<td><?php echo $this->Paginator->sort('email_address');?></td>
				<td><?php echo $this->Paginator->sort('status');?></td>
				<td><?php echo $this->Paginator->sort('url');?></td>
				<td><?php echo $this->Paginator->sort('reg_date');?></td>
			</tr>
			
			<?php foreach ($affiliates as $affiliate): ?>
			
			<tr class="highlight">
				<td><?php echo $this->Html->link($affiliate['Affiliate']['affiliate_id'], array('action' => 'view', $affiliate['Affiliate']['affiliate_id'])); ?></td>
				<td><?php echo $affiliate['Affiliate']['name']; ?></td>
				<td><?php echo $affiliate['Affiliate']['email_address']; ?></td>
				<td><?php echo $affiliate['Affiliate']['status']; ?></td>
				<td><?php echo $affiliate['Affiliate']['url']; ?></td>
				<td><?php echo $affiliate['Affiliate']['reg_date']; ?></td>
			</tr>
			
			<?php endforeach; ?>
		</table>

		<p id="pagin">
			<?php echo $this->Paginator->first();?>
		 	<?php echo $this->Paginator->prev('< previous', "", "", "");?>
		 	<?php //echo $this->Paginator->prev() ?> 
			<?php echo $this->Paginator->numbers();?>  
		 	<?php //echo $this->Paginator->next() ?>
			<?php //echo $this->Paginator->counter(); ?>
			<?php echo $this->Paginator->next('next >', "", "", "");?>
			<?php echo $this->Paginator->last();?>
		<p/>

		<span class="clear"></span>
	</div>
</div>

