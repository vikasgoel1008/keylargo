<div class="affiliates view">
<h2>Affiliate Details</h2>
<?php echo $this->element('affiliates_menu');?>
	
<div class="box">
	<p>&nbsp;</p>

	<table>
		<tr class="highlight">
			<td colspan="2">
				<?php echo $this->Html->link("Edit", array('action' => 'edit', $affiliate['Affiliate']['affiliate_id']));?> |
				<?php echo $this->Html->link("Earnings Summary", array('action' => 'payment_summary', $affiliate['Affiliate']['affiliate_id']));?> | 
				<?php echo $this->Html->link('Close Affiliate', 'close/'.$affiliate['Affiliate']['affiliate_id'], null, sprintf(__('This will close this affiliate immediately.  \nAre you sure you want to close affiliate %s?', true), $affiliate['Affiliate']['id'])); ?>
			</td>
		</tr>
		
		<tr class="highlight"><td>Affiliate:</td><td><?php echo $affiliate['Affiliate']['affiliate_id'];?> - <?php echo $affiliate['Affiliate']['name']; ?></td></tr>
		<tr class="highlight"><td>Email</td><td><?php echo $affiliate['Affiliate']['email_address']; ?></td></tr>
		<tr class="highlight"><td>URL:</td><td><?php echo $this->Html->link($affiliate['Affiliate']['url'], $affiliate['Affiliate']['url'], array('target' => '_blank'));?></td></tr>
		<tr class="highlight"><td>Address:</td>
			<td>
				<?php echo $affiliate['Affiliate']['address1']; ?> 
				<?php if (!empty($affiliate['Affiliate']['address2'])) { echo $affiliate['Affiliate']['address2']; } ?>
				<?php echo $affiliate['Affiliate']['city']; ?>, <?php echo $affiliate['Affiliate']['state']; ?> <?php echo $affiliate['Affiliate']['postal']; ?> 
				<?php echo $affiliate['Affiliate']['country']; ?> 
			</td>
		</tr>
		<tr class="highlight"><td>Affiliate Comments:</td><td><?php echo $affiliate['Affiliate']['comments']; ?></td></tr>
		
		<tr class="highlight"><td colspan="2">&nbsp;</td></tr>
		<tr class="highlight"><td>Status:</td><td><?php echo $affiliate['Affiliate']['status']; ?></td></tr>
		<tr class="highlight"><td>Registration Date:</td><td><?php echo $affiliate['Affiliate']['reg_date']; ?></td></tr>
		<tr class="highlight"><td>Close Date:</td><td><?php echo $affiliate['Affiliate']['close_date']; ?></td></tr>
		<tr class="highlight"><td>Commission Rate:</td><td><?php echo $affiliate['Affiliate']['commission_rate']; ?></td></tr>
		<tr class="highlight"><td>Max Sale Amount:</td><td><?php echo $affiliate['Affiliate']['max_sale_amount']; ?></td></tr>
		<tr class="highlight"><td>Sales Totals 30/60/90:</td><td><?php echo $sales30; ?> / <?php echo $sales60; ?> / <?php echo $sales90; ?></td></tr>
		
	</table>
	<p id="pagin">
			&nbsp;
		</p>
		
		<span class="clear">&nbsp;</span>

</div>	
</div>
	