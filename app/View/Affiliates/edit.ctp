<div class="coupons form">
	<h2>Edit Affiliate</h2>

	<?php echo $this->element('affiliates_menu');?>

	<?php echo $this->Form->create('Affiliate');?>
	<fieldset>
		<?php echo $this->Form->input('affiliate_id',array('type'=>'text','class'=>'small','label'=>'Affiliate Id', 'readonly'=>'readonly'));?><br class="hid" />
		<?php echo $this->Form->input('name',array('type'=>'text','class'=>'small','label'=>'Name'));?><br class="hid" />
		<?php echo $this->Form->input('email_address',array('type'=>'text','class'=>'large','label'=>'Email'));?><br class="hid" />
		<?php echo $this->Form->input('url',array('type'=>'text','class'=>'large','label'=>'Website',));?><br class="hid" />
		<?php echo $this->Form->input('address1',array('type'=>'text','class'=>'large','label'=>'Address1'));?><br class="hid" />
		<?php echo $this->Form->input('address2',array('type'=>'text','class'=>'large','label'=>'Address2'));?><br class="hid" />
		<?php echo $this->Form->input('city',array('type'=>'text','class'=>'tiny','label'=>'City'));?><br class="hid" />
		<?php echo $this->Form->input('state',array('type'=>'text','class'=>'tiny','label'=>'State'));?><br class="hid" />
		<?php echo $this->Form->input('postal',array('type'=>'text','class'=>'tiny','label'=>'Postal'));?><br class="hid" />
		<?php echo $this->Form->input('country',array('type'=>'text','class'=>'tiny','label'=>'Country'));?><br class="hid" />
		<?php echo $this->Form->input('status',array('type'=>'text','class'=>'tiny','label'=>'Status'));?><br class="hid" />
		<?php echo $this->Form->input('close_date',array('type'=>'text','class'=>'tiny','label'=>'Close Date'));?><br class="hid" /><p>Format: YYYY-MM-DD</p>
		<?php echo $this->Form->input('commission_rate',array('type'=>'text','class'=>'tiny','label'=>'Commission Rate'));?><br class="hid" />
		<?php echo $this->Form->input('max_sale_amount',array('type'=>'text','class'=>'tiny','label'=>'Max Sale Amount'));?><br class="hid" />
		<?php echo $this->Form->input('comments',array('type'=>'textarea','class'=>'large','label'=>'Comments'));?><br class="hid" />

		
		<p></p>
		<?php echo $this->Form->end(array('label'=>'Edit','class'=>'button submit'));?>
		<span class="clear"></span>
	</fieldset>
</div>
