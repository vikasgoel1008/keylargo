<div class="ccTransactions index">
	<h2>Various NGD Jobs</h2>

	<div class="box">
		<p>&nbsp;</p>
		<table cellpadding="0" cellspacing="0">
			
			<tr >
				<td><strong>Mailchimp List Update Jobs</strong></td>
			</tr>
			<tr class="highlight">
				<td><?php echo $this->Html->link('Update Block Customers (1000 increments)',"http://newsgroupdirect.com/scheduled/update_block_customers.php?start=0",array('target' => '_blank')); ?></td>
			</tr>
			<tr class="highlight">
				<td><?php echo $this->Html->link('Potential Auto Recycle Customers (1000 increments)',"http://newsgroupdirect.com/scheduled/job_market_potential_auto_recycle.php?start=0",array('target' => '_blank')); ?></td>
			</tr>
			
			
		</table>
		
		<p id="pagin">
			&nbsp;
		</p>
		
		<span class="clear">&nbsp;</span>
	</div>
</div>
	