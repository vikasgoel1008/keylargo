<!--<p id="congrats" class="alert">
	<span class="txt"><span class="icon"></span><strong>Login Success:</strong> Last login was November 10, 2008 at 04:00:00</span>
	<a title="Close" class="close"><span class="bg"></span>Close</a>
</p>
<p id="tip" class="alert">
	<span class="txt"><span class="icon"></span><strong>Tip:</strong> Choose an option from the menu to get started.</span>
	<a title="Close" class="close"><span class="bg"></span>Close</a>
</p>-->

<!-- BEGIN PAGE HEADER-->
<?php $page = $this->Html->url(array('controller'=>'Pages','action'=>'display')); ?>
<meta http-equiv="refresh" content="90;URL='<?php echo $page?>'">
<script>
$(window).load(function(){
	//$("#month_site_activities").css('display','none');
});
$(document).ready(function(){
	$(".daily_revenue").click(function(){
		$("#site_activities").css('display','block');
		$("#month_site_activities").css('display','none');
	});
	$(".monthly_revenue").click(function(){
		$("#site_activities").css('display','none');
		$("#month_site_activities").css('display','block');
		$("#month_site_activities").find('canvas').css('left','18px');
	});
	$(".daily_bandwidth").click(function(){
		$("#site_statistics").css('display','block');
		$("#weekly_site_statistics").css('display','none');
	});
	$(".weekly_bandwidth").click(function(){
		$("#site_statistics").css('display','none');
		$("#weekly_site_statistics").css('display','block');
	});
	var series = [
	{ 
		data : [
  	         	<?php 
  	         	foreach($day_wise_revenue as $rebill_data){	         		
  	         		$d = date_parse_from_format("Y-m-d", $rebill_data['date']);
  	         		?>
                   [gd(<?php echo $d["year"] ?>, <?php echo $d["month"] ?>, <?php echo $d["day"] ?>), <?php echo $rebill_data['rebill'] ?>],
  				<?php } ?>
                   
               ],
               label:"Rebill"
	},
	{
		data : [
		  	         	<?php 
		  	         	foreach($day_wise_revenue as $recycle_data){	         		
		  	         		$d = date_parse_from_format("Y-m-d", $recycle_data['date']);
		  	         		?>
		                   [gd(<?php echo $d["year"] ?>, <?php echo $d["month"] ?>, <?php echo $d["day"] ?>), <?php echo $recycle_data['recycle'] ?>],
		  				<?php } ?>
		                   
		               ],
		               label:"Recycle"
	},
	{
		data : [
		  	         	<?php 
		  	         	foreach($day_wise_revenue as $freetrial_data){	         		
		  	         		$d = date_parse_from_format("Y-m-d", $freetrial_data['date']);
		  	         		?>
		                   [gd(<?php echo $d["year"] ?>, <?php echo $d["month"] ?>, <?php echo $d["day"] ?>), <?php echo $freetrial_data['freetrial'] ?>],
		  				<?php } ?>
		                   
		               ],
		               label:"Free Trial"
	},
	{
		data : [
	         	<?php 
	         	foreach($day_wise_revenue as $block_data){	         		
	         		$d = date_parse_from_format("Y-m-d", $block_data['date']);
	         		?>
                 [gd(<?php echo $d["year"] ?>, <?php echo $d["month"] ?>, <?php echo $d["day"] ?>), <?php echo $block_data['block'] ?>],
				<?php } ?>
                 
             ],
             label:"Block",
             
	},
	{
		data : [
		  	         	<?php 
		  	         	foreach($day_wise_revenue as $signup_data){	         		
		  	         		$d = date_parse_from_format("Y-m-d", $signup_data['date']);
		  	         		?>
		                   [gd(<?php echo $d["year"] ?>, <?php echo $d["month"] ?>, <?php echo $d["day"] ?>), <?php echo $signup_data['signup'] ?>],
		  				<?php } ?>
		                   
		               ],
		               label:"Signup"
	},
	{
		data : [
		  	         	<?php 
		  	         	foreach($day_wise_revenue as $freeupgrade_data){	         		
		  	         		$d = date_parse_from_format("Y-m-d", $freeupgrade_data['date']);
		  	         		?>
		                   [gd(<?php echo $d["year"] ?>, <?php echo $d["month"] ?>, <?php echo $d["day"] ?>), <?php echo $freeupgrade_data['freeupgrade'] ?>],
		  				<?php } ?>
		                   
		               ],
		               label:"Free Upgrade"
	}
	];
	$.plot($("#site_activities"),
	series,
    {
        xaxis: {
			mode: "time",
		    tickSize: [3, "day"],       
		    tickLength: 10,
		    axisLabel: "Date",
		    axisLabelUseCanvas: true,
		    axisLabelFontSizePixels: 12,
		    axisLabelFontFamily: 'Verdana, Arial',
		    axisLabelPadding: 10,
		    color: "black",
		   	
        },
        yaxis: {
            ticks: 5,
            tickDecimals: 0,
            tickColor: "#eee",
            font: {
                lineHeight: 14,
                style: "normal",
                variant: "small-caps",
                color: "#6F7B8A"
            }
        },
        grid: {
            hoverable: true,
            clickable: true,
            tickColor: "#eee",
            borderColor: "#eee",
            borderWidth: 1
        },
        series: {
		    stack: true,
		    bars: {
		        show: true
		    },
		    points: {
              radius: 3,
              show: false,
              fill: true              
          },
		},
		bars: {
		    align: "center",
		    barWidth:24 * 60 * 60 * 550
		}

        
    });


	var month_series = [
          { 
          	data : [
	                    <?php 
	                    foreach($month_revenues as $monthly_rebill_data){	         		
	                     	//$d = date_parse_from_format("Y-m-d", $monthly_rebill_data['date']);
	                     	$rebill_date =  date('Y-m-d', strtotime($monthly_rebill_data['date']));
	                     	$d = date_parse_from_format("Y-m-d", $rebill_date);
	                     	//print_r($d);die;
	                     	?>
	                             [gd(<?php echo $d["year"] ?>, <?php echo $d["month"] ?>, <?php echo $d["day"] ?>), <?php echo $monthly_rebill_data['rebill'] ?>],
	            		<?php } ?>
                             
                  	],
              	    label:"Rebill"
          },
          {
          	data : [
          	  	         	<?php 
          	  	         	foreach($month_revenues as $monthly_recycle_data){	         		
          	  	         		$recycle_date =  date('Y-m-d', strtotime($monthly_recycle_data['date']));
          	  	         		$d = date_parse_from_format("Y-m-d", $recycle_date);
          	  	         		?>
          	                   [gd(<?php echo $d["year"] ?>, <?php echo $d["month"] ?>, <?php echo $d["day"] ?>), <?php echo $monthly_recycle_data['recycle'] ?>],
          	  				<?php } ?>
          	                   
          	               ],
          	               label:"Recycle"
          },
          {
          	data : [
          	  	         	<?php 
          	  	         	foreach($month_revenues as $monthly_freetrial_data){	         		
          	  	         		$freetrial_date =  date('Y-m-d', strtotime($monthly_freetrial_data['date']));
          	  	         		$d = date_parse_from_format("Y-m-d", $freetrial_date);
          	  	         		?>
          	                   [gd(<?php echo $d["year"] ?>, <?php echo $d["month"] ?>, <?php echo $d["day"] ?>), <?php echo $monthly_freetrial_data['freetrial'] ?>],
          	  				<?php } ?>
          	                   
          	               ],
          	               label:"Free Trial"
          },
          {
          	data : [
                   	<?php 
                   	foreach($month_revenues as $monthly_block_data){	         		
                   		$block_date =  date('Y-m-d', strtotime($monthly_block_data['date']));
                   		$d = date_parse_from_format("Y-m-d", $block_date);
                   		?>
                           [gd(<?php echo $d["year"] ?>, <?php echo $d["month"] ?>, <?php echo $d["day"] ?>), <?php echo $monthly_block_data['block'] ?>],
          			<?php } ?>
                           
                       ],
                       label:"Block",
                       
          },
          {
          	data : [
          	  	         	<?php 
          	  	         	foreach($month_revenues as $monthly_signup_data){	         		
          	  	         		$signup_date =  date('Y-m-d', strtotime($monthly_signup_data['date']));
          	  	         		$d = date_parse_from_format("Y-m-d", $signup_date);
          	  	         		?>
          	                   [gd(<?php echo $d["year"] ?>, <?php echo $d["month"] ?>, <?php echo $d["day"] ?>), <?php echo $monthly_signup_data['signup'] ?>],
          	  				<?php } ?>
          	                   
          	               ],
          	               label:"Signup"
          },
          {
          	data : [
          	  	         	<?php 
          	  	         	foreach($month_revenues as $monthly_freeupgrade_data){	         		
          	  	         		$freeupgrade_date =  date('Y-m-d', strtotime($monthly_freeupgrade_data['date']));
          	  	         		$d = date_parse_from_format("Y-m-d", $freeupgrade_date);
          	  	         		?>
          	                   [gd(<?php echo $d["year"] ?>, <?php echo $d["month"] ?>, <?php echo $d["day"] ?>), <?php echo $monthly_freeupgrade_data['freeupgrade'] ?>],
          	  				<?php } ?>
          	                   
          	               ],
          	               label:"Free Upgrade"
          }
          ];

	
	$.plot($("#month_site_activities"),
			month_series,
		    {
		        xaxis: {
					mode: "time",
				    tickSize: [5, "month"],       
				    tickLength: 10,
				    axisLabel: "Date",
				    axisLabelUseCanvas: true,
				    axisLabelFontSizePixels: 12,
				    axisLabelFontFamily: 'Verdana, Arial',
				    axisLabelPadding: 10,
				    color: "black",
				   	
		        },
		        yaxis: {
		            ticks: 5,
		            tickDecimals: 0,
		            tickColor: "#eee",
		            font: {
		                lineHeight: 14,
		                style: "normal",
		                variant: "small-caps",
		                color: "#6F7B8A"
		            }
		        },
		        grid: {
		            hoverable: true,
		            clickable: true,
		            tickColor: "#eee",
		            borderColor: "#eee",
		            borderWidth: 1
		        },
		        series: {
				    stack: true,
				    bars: {
				        show: true
				    },
				    points: {
		              radius: 3,
		              show: false,
		              fill: true              
		          },
				},
				bars: {
				    align: "center",
				    barWidth:24 * 60 * 60 * 5500
				}

		        
		    });
    
	var bandwidth = [
					<?php 
			     	foreach($bandwidth as $band_data){
			     		$d = date_parse_from_format("Y-m-d", $band_data['0']['usage_date']);
			     		$tera_data = round($band_data['0']['charges']*9.0949e-13,2);
			     		?>
			         [gd(<?php echo $d["year"] ?>, <?php echo $d["month"] ?>, <?php echo $d["day"] ?>), <?php echo $tera_data; ?>],
					<?php } ?>
	                
	            ];
	var plot_statistics = $.plot($("#site_statistics"),

            [{
                data: bandwidth,
                lines: {
                    fill: 0.6,
                    lineWidth: 0,
                },
                color: ['#f89f9f']
            }, {
                data: bandwidth,
                points: {
                    show: true,
                    fill: true,
                    radius: 5,
                    fillColor: "#f89f9f",
                    lineWidth: 3
                },
                color: '#fff',
                shadowSize: 0
            }, ],

            {

                xaxis: {
					mode: "time",
				    tickSize: [3, "day"],       
				    tickLength: 0,
				    axisLabel: "Date",
				    axisLabelUseCanvas: true,
				    axisLabelFontSizePixels: 12,
				    axisLabelFontFamily: 'Verdana, Arial',
				    axisLabelPadding: 10,
				    color: "black",
                },
                yaxis: {
                    ticks: 5,
                    tickDecimals: 0,
                    tickColor: "#eee",
                    font: {
                        lineHeight: 14,
                        style: "normal",
                        variant: "small-caps",
                        color: "#6F7B8A"
                    }
                },
                grid: {
                    hoverable: true,
                    clickable: true,
                    tickColor: "#eee",
                    borderColor: "#eee",
                    borderWidth: 1
                }
            });
	var weekly_bandwidth = [
						<?php 
				     	foreach($weekly_bandwidth as $weekly_band_data){
				     		$d = date_parse_from_format("Y-m-d", $weekly_band_data['0']['usage_date']);
				     		$tera_data = round($weekly_band_data['0']['charges']*9.0949e-13,2);
				     		?>
				         [<?php echo $weekly_band_data['0']['usage_date'] ?>, <?php echo $tera_data; ?>],
						<?php } ?>
		                
		            ];
	var plot_statistics = $.plot($("#weekly_site_statistics"),

            [{
                data: weekly_bandwidth,
                lines: {
                    fill: 0.6,
                    lineWidth: 0,
                },
                color: ['#f89f9f']
            }, {
                data: weekly_bandwidth,
                points: {
                    show: true,
                    fill: true,
                    radius: 5,
                    fillColor: "#f89f9f",
                    lineWidth: 3
                },
                color: '#fff',
                shadowSize: 0
            }, ],

            {

                xaxis: {
					mode: "time",
				    tickSize: [3, "day"],       
				    tickLength: 0,
				    axisLabel: "Date",
				    axisLabelUseCanvas: true,
				    axisLabelFontSizePixels: 12,
				    axisLabelFontFamily: 'Verdana, Arial',
				    axisLabelPadding: 10,
				    color: "black",
                },
                yaxis: {
                    ticks: 5,
                    tickDecimals: 0,
                    tickColor: "#eee",
                    font: {
                        lineHeight: 14,
                        style: "normal",
                        variant: "small-caps",
                        color: "#6F7B8A"
                    }
                },
                grid: {
                    hoverable: true,
                    clickable: true,
                    tickColor: "#eee",
                    borderColor: "#eee",
                    borderWidth: 1
                }
            });
});
function gd(year, month, day) {
    return new Date(year, month - 1, day).getTime();
}
</script>
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN PAGE TITLE & BREADCRUMB-->
		<h3 class="page-title">
		Dashboard
		</h3>
		<ul class="page-breadcrumb breadcrumb">
			<li>
				<i class="fa fa-home"></i>
				<a href="index.html">Home</a>
				<i class="fa fa-angle-right"></i>
			</li>
			<li>
				<a href="#">Dashboard</a>
			</li>
		</ul>
		<!-- END PAGE TITLE & BREADCRUMB-->
	</div>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN DASHBOARD STATS -->
<div class="row">
	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<div class="dashboard-stat red-intense">
			<div class="visual">
				<i class="fa fa-dollar"></i>
			</div>
			<div class="details">
				<div class="number">
					 <?php if(empty($revenues['0']['charges'])){ echo "$ 0";} else echo "$ ".$revenues['0']['charges']; ?>
				</div>
				<div class="desc">
					 Today's Revenue
				</div>
			</div>
			<?php echo $this->Html->link('View More<i class="m-icon-swapright m-icon-white"></i>',array('controller'=>'CcTransactions','action'=>'report_graph_daily_sales',60),array('class'=>'more','escape'=>false)) ?>
			
		</div>
	</div>
	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<div class="dashboard-stat blue-madison">
			<div class="visual">
				<i class="fa fa-bar-chart-o"></i>
			</div>
			<div class="details">
				<div class="number">
					 <?php 
					 	if(!empty($signups))
					 	echo $signups['0']['signups'];
					 	else
					 	echo "0";  
					 ?>
				</div>
				<div class="desc">
					 New Customers Today
				</div>
			</div>
			<a class="more" href="#">
			View more <i class="m-icon-swapright m-icon-white"></i>
			</a>
		</div>
	</div>
	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<div class="dashboard-stat green-haze">
			<div class="visual">
				<i class="fa fa-shopping-cart"></i>
			</div>
			<div class="details">
				<div class="number">
					 <?php
					 	if(!empty($adds)) 
					 	echo $adds['0']['adds'];
					 	else
					 	echo "0"; 
					 ?>
				</div>
				<div class="desc">
					 Blocks Added Today
				</div>
			</div>
			<a class="more" href="#">
			View more <i class="m-icon-swapright m-icon-white"></i>
			</a>
		</div>
	</div>
	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<div class="dashboard-stat purple-plum">
			<div class="visual">
				<i class="fa fa-globe"></i>
			</div>
			<div class="details">
				<div class="number">
					 +89%
				</div>
				<div class="desc">
					 Brand Popularity
				</div>
			</div>
			<a class="more" href="#">
			View more <i class="m-icon-swapright m-icon-white"></i>
			</a>
		</div>
	</div>
</div>
<!-- END DASHBOARD STATS -->
<div class="clearfix"></div>
<div class="row">
	<div class="col-md-6 col-sm-6">
		<!-- BEGIN PORTLET-->
		<div class="portlet solid grey-cararra bordered">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-bullhorn"></i>Revenue
				</div>
				<div class="tools">
					<div class="btn-group" data-toggle="buttons">
						<label class="btn grey-steel btn-sm monthly_revenue">
						<input type="radio" name="options" class="toggle" id="option2">Monthly</label>
						<label class="btn grey-steel btn-sm active daily_revenue">
						<input type="radio" name="options" class="toggle" id="option1">Daily</label>
					</div>
				</div>
			</div>
			<div class="portlet-body">
				<div id="site_activities_loading">
					<?php echo $this->Html->image('loading.gif',array('alt'=>'loading')); ?>
				</div>
				<div id="site_activities_content" class="display-none">
					<div id="site_activities" style="height: 300px;min-width:100px;">
					</div>
					<div id="month_site_activities" style="height: 300px;min-width:100px;display:none;">
					</div>
				</div>
				
			</div>
		</div>
		<!-- END PORTLET-->
	</div>
	<div class="col-md-6 col-sm-6">
		<!-- BEGIN PORTLET-->
		<div class="portlet solid bordered grey-cararra">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-bar-chart-o"></i>Bandwidth Usage
				</div>
				<div class="tools">
					<div class="btn-group" data-toggle="buttons">
						<label class="btn grey-steel btn-sm active daily_bandwidth">
						<input type="radio" name="options" class="toggle" id="option1">Daily</label>
						<label class="btn grey-steel btn-sm weekly_bandwidth">
						<input type="radio" name="options" class="toggle" id="option2">Weekly</label>
					</div>
				</div>
			</div>
			<div class="portlet-body">
				<div id="site_statistics_loading">
					<?php echo $this->Html->image('loading.gif',array('alt'=>'loading')); ?>
				</div>
				<div id="site_statistics_content" class="display-none">
					<div id="site_statistics" class="chart" style="min-width:100px;">
					</div>
					<div id="weekly_site_statistics" class="chart" style="min-width:100px;display:none">
					</div>
				</div>
			</div>
		</div>
		<!-- END PORTLET-->
	</div>
	
</div>
<div class="clearfix"></div>
<div class="row ">
	<div class="col-md-6 col-sm-6">

	</div>
</div>
<div class="clearfix"></div>
<div class="row ">
	<div class="col-md-6 col-sm-6">
		<div class="portlet box purple-wisteria">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-calendar"></i>General Stats
				</div>
				<div class="actions">
					<a href="javascript:;" class="btn btn-sm btn-default easy-pie-chart-reload">
					<i class="fa fa-repeat"></i> Reload </a>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row">
					<div class="col-md-4">
						<div class="easy-pie-chart">
							<div class="number transactions" data-percent="55">
								<span>
								+55 </span>
								%
							</div>
							<a class="title" href="#">
							Transactions <i class="m-icon-swapright"></i>
							</a>
						</div>
					</div>
					<div class="margin-bottom-10 visible-sm">
					</div>
					<div class="col-md-4">
						<div class="easy-pie-chart">
							<div class="number visits" data-percent="85">
								<span>
								+85 </span>
								%
							</div>
							<a class="title" href="#">
							New Visits <i class="m-icon-swapright"></i>
							</a>
						</div>
					</div>
					<div class="margin-bottom-10 visible-sm">
					</div>
					<div class="col-md-4">
						<div class="easy-pie-chart">
							<div class="number bounce" data-percent="46">
								<span>
								-46 </span>
								%
							</div>
							<a class="title" href="#">
							Bounce <i class="m-icon-swapright"></i>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-6 col-sm-6">
		<div class="portlet box red-sunglo">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-calendar"></i>Recent Signups
				</div>
			</div>
			<div class="portlet-body" style="height:auto">
				<div class="scroller" style="height: auto" data-always-visible="1" data-rail-visible1="1">
					<div class="row">
						<?php foreach($last_signup_members as $key=>$members) { ?>
						<div class="col-md-6 user-info">
						<?php 
						
							//print_r($members);die;
							$hash_email = md5( strtolower( trim($members['Member']['email'])));
						?>
							<?php echo $this->Html->image('http://www.gravatar.com/avatar/'.$hash_email.'?s=45',array('class'=>'img-responsive')) ?>
							<div class="details">
								<div>
									<?php echo $this->Html->link($members['Member']['name'],array('controller'=>'Members','action'=>'view',$members['Member']['id'])); ?>
									
								</div>
								<div><?php echo $members['Member']['email']; ?></div>
								<div>
									 <?php echo $members['Member']['date_reg']; ?>
								</div>
							</div>
						</div>
						
						<?php
							if($key==1)
							{?>
								</div>
								<div class="row">
							<?php }
						} ?>
						
					</div>
					
				</div>
			</div>
		</div>
	</div>
</div>