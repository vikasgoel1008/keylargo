<div class="contestants index">
<h2><?php __('Contestants');?></h2>
<p>
<?php
echo $this->Paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>
<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $this->Paginator->sort('id');?></th>
	<th><?php echo $this->Paginator->sort('contest_id');?></th>
	<th><?php echo $this->Paginator->sort('name');?></th>
	<th><?php echo $this->Paginator->sort('email');?></th>
	<th><?php echo $this->Paginator->sort('twitter');?></th>
	<th><?php echo $this->Paginator->sort('shirt_size');?></th>
	<th><?php echo $this->Paginator->sort('ip');?></th>
	<th><?php echo $this->Paginator->sort('date_submitted');?></th>
	<th><?php echo $this->Paginator->sort('reg_number');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
$i = 0;
foreach ($contestants as $contestant):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td>
			<?php echo $contestant['Contestant']['id']; ?>
		</td>
		<td>
			<?php echo $contestant['Contestant']['contest_id']; ?>
		</td>
		<td>
			<?php echo $contestant['Contestant']['name']; ?>
		</td>
		<td>
			<?php echo $contestant['Contestant']['email']; ?>
		</td>
		<td>
			<?php echo $contestant['Contestant']['twitter']; ?>
		</td>
		<td>
			<?php echo $contestant['Contestant']['shirt_size']; ?>
		</td>
		<td>
			<?php echo $contestant['Contestant']['ip']; ?>
		</td>
		<td>
			<?php echo $contestant['Contestant']['date_submitted']; ?>
		</td>
		<td>
			<?php echo $contestant['Contestant']['reg_number']; ?>
		</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $contestant['Contestant']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $contestant['Contestant']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $contestant['Contestant']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $contestant['Contestant']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>
<div class="paging">
	<?php echo $this->Paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $this->Paginator->numbers();?>
	<?php echo $this->Paginator->next(__('next', true).' >>', array(), null, array('class' => 'disabled'));?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $this->Html->link(__('New Contestant', true), array('action' => 'add')); ?></li>
	</ul>
</div>
