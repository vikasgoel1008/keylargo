<div class="contestants form">
<?php echo $this->Form->create('Contestant');?>
	<fieldset>
 		<legend><?php __('Add Contestant');?></legend>
	<?php
		echo $this->Form->input('contest_id');
		echo $this->Form->input('name');
		echo $this->Form->input('email');
		echo $this->Form->input('twitter');
		echo $this->Form->input('shirt_size');
		echo $this->Form->input('ip');
		echo $this->Form->input('date_submitted');
		echo $this->Form->input('reg_number');
	?>
	</fieldset>
<?php echo $this->Form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $this->Html->link(__('List Contestants', true), array('action' => 'index'));?></li>
	</ul>
</div>
