<ul class="tabs">
	<li <?php if ($this->Session->read('ActivePage') == 'addSlaReport') echo "class='active'" ?>><?php echo $this->Html->link($this->Html->tag('span', 'Add SLA Data'), '/sla_reports/add/', array('escape'=>false, 'title'=>"Add SLA Data")); ?></li>
	<li <?php if ($this->Session->read('ActivePage') == 'slaDailyStats') echo "class='active'" ?>><?php echo $this->Html->link($this->Html->tag('span', 'SLA Daily Chart'), '/sla_reports/report_daily_sla_stats', array('escape'=>false, 'title'=>"Daily Chart")); ?></li>
	<li <?php if ($this->Session->read('ActivePage') == 'slaWeeklyStats') echo "class='active'" ?>><?php echo $this->Html->link($this->Html->tag('span', 'SLA Weekly Chart'), '/sla_reports/report_weekly_sla_stats', array('escape'=>false, 'title'=>"Weekly Chart")); ?></li>
	
</ul>


