<ul class="tabs">
	
	<li><?php echo $this->Html->link($this->Html->tag('span', 'Add Block'), '/ngd_options/plans_display/plans_add_block', array('escape'=>false, 'title'=>"Block Display")); ?></li>
	<li><?php echo $this->Html->link($this->Html->tag('span', 'Upgrade'), '/ngd_options//plans_display/plans_upgrade', array('escape'=>false, 'title'=>"Upgrade Display")); ?></li>
	<li><?php echo $this->Html->link($this->Html->tag('span', 'Signup'), '/ngd_options//plans_display/signup', array('escape'=>false, 'title'=>"Signup Display")); ?></li>
	
	<?php if ($ngdAdminUser) : ?>						                    
	<li <?php if ($this->Session->read('ActivePage') == 'addPlan') echo "class='active'" ?>><?php echo $this->Html->link($this->Html->tag('span', 'Add Plan'), '/plans/add/', array('escape'=>false, 'title'=>"Add Plan")); ?></li>
	<?php endif; ?>
	
	<li <?php if ($this->Session->read('ActivePage') == 'activePlans') echo "class='active'" ?>><?php echo $this->Html->link($this->Html->tag('span', 'Active Plans'), '/plans/active/', array('escape'=>false, 'title'=>"Active Plans")); ?></li>
	
	
	
	<?php if (!$this->Session->read('ActivePage') == 'planDisplay') : ?>
		<li <?php if ($this->Session->read('ActivePage') == 'allPlans') echo "class='active'" ?>><?php echo $this->Html->link($this->Html->tag('span', 'All Plans'), '/plans/index/', array('escape'=>false, 'title'=>"All Plans")); ?></li>
	<?php endif;?>
	
</ul>


