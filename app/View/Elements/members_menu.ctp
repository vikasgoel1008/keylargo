<?php if ($this->Session->check('Member.id')) :?>
<ul class="tabs">
	<li <?php if ($this->Session->read('ActivePage') == 'members') echo "class='active'" ?>><?php echo $this->Html->link($this->Html->tag('span', 'Account'), '/members/view/' . $this->Session->read('Member.id'), array('escape'=>false, 'title'=>"Member")); ?></li>
	<li <?php if ($this->Session->read('ActivePage') == 'highwinds') echo "class='active'" ?>><?php echo $this->Html->link($this->Html->tag('span', 'HW'), '/members/highwinds/' . $this->Session->read('Member.id'), array('escape'=>false, 'title'=>"Highwinds")); ?></li>
	<li <?php if ($this->Session->read('ActivePage') == 'transactions') echo "class='active'" ?>><?php echo $this->Html->link($this->Html->tag('span', 'Billing'), '/cc_transactions/index/' . $this->Session->read('Member.id'), array('escape'=>false, 'title'=>"Transactions")); ?></li>
	<li <?php if ($this->Session->read('ActivePage') == 'notes') echo "class='active'" ?>><?php echo $this->Html->link($this->Html->tag('span', 'Notes'), '/notes/index/' . $this->Session->read('Member.id'), array('escape'=>false, 'title'=>"Notes")); ?></li>
	<li <?php if ($this->Session->read('ActivePage') == 'credits') echo "class='active'" ?>><?php echo $this->Html->link($this->Html->tag('span', 'Credits'), '/ngd_credits/index/' . $this->Session->read('Member.id'), array('escape'=>false, 'title'=>"Credits")); ?></li>
	<li <?php if ($this->Session->read('ActivePage') == 'logs') echo "class='active'" ?>><?php echo $this->Html->link($this->Html->tag('span', 'Logs'), '/m_logs/index/' . $this->Session->read('Member.id'), array('escape'=>false, 'title'=>"Logs")); ?></li>
</ul>
<?php endif; ?> 


