<ul class="tabs">
	<li <?php if ($this->Session->read('ActivePage') == 'addCoupon') echo "class='active'" ?>><?php echo $this->Html->link($this->Html->tag('span', 'Add Coupons'), '/coupons/add/', array('escape'=>false, 'title'=>"Add Coupon")); ?></li>
	<li <?php if ($this->Session->read('ActivePage') == 'allCoupon') echo "class='active'" ?>><?php echo $this->Html->link($this->Html->tag('span', 'All Coupons'), '/coupons/index/', array('escape'=>false, 'title'=>"All Coupons")); ?></li>
	<li <?php if ($this->Session->read('ActivePage') == 'activeCoupon') echo "class='active'" ?>><?php echo $this->Html->link($this->Html->tag('span', 'Active Coupons'), '/coupons/active/', array('escape'=>false, 'title'=>"Active Coupons")); ?></li>
	
</ul>


