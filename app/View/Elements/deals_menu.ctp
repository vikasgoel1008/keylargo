<ul class="tabs">
	<li <?php if ($this->Session->read('ActivePage') == 'addDeal') echo "class='active'" ?>><?php echo $this->Html->link($this->Html->tag('span', 'Add Deals'), '/deals/add/', array('escape'=>false, 'title'=>"Add Deal")); ?></li>
	<li <?php if ($this->Session->read('ActivePage') == 'allDeals') echo "class='active'" ?>><?php echo $this->Html->link($this->Html->tag('span', 'All Deals'), '/deals/', array('escape'=>false, 'title'=>"All Deals")); ?></li>
	<li <?php if ($this->Session->read('ActivePage') == 'activeDeals') echo "class='active'" ?>><?php echo $this->Html->link($this->Html->tag('span', 'Active Deals'), '/deals/active', array('escape'=>false, 'title'=>"Active Deals")); ?></li>
	
</ul>


