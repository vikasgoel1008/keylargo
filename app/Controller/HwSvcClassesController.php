<?php
class HwSvcClassesController extends AppController {

	var $name = 'HwSvcClasses';
	var $helpers = array('Html', 'Form');

	function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow("job_update_hw_service_rates");
	}


	function add($service_class_id = null) {
		if (!empty($this->data)) {
			$this->HwSvcClass->create();
			if ($this->HwSvcClasses->save($this->data)) {
				$this->Session->setFlash(__('This has been saved', true), 'flash_success');
				$this->redirect(array('action'=>'rate_hours',$this->data['HwSvcClass']['service_class_id']));
			} else {
				$this->Session->setFlash(__('This could not be saved. Please, try again.', true), 'error');
			}
		}
		$svc_classes = $this->HwSvcClass->getServiceRateList();
		$this->set('svc_classes', $svc_classes);
		$this->set('service_class_id', $service_class_id);
	}


	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid HwSvcClass', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->HwSvcClass->save($this->data)) {
				$this->Session->setFlash(__('The HwSvcClass has been saved', true), 'flash_success');
				$this->redirect(array('action'=>'rate_hours', $this->data['HwSvcClass']['service_class_id']));
			} else {
				$this->Session->setFlash(__('The HwSvcClass could not be saved. Please, try again.', true), 'error');
			}
		}
		if (empty($this->data)) {
			$this->data = $this->HwSvcClass->read(null, $id);
		}
	}


	function index() {
		$this->HwSvcClass->recursive = 0;
		$this->paginate = array('limit' => 20,
								'group' => array('service_class_id'));
		$this->set('title_for_layout',"Service Class Manager");
		$this->set('services', $this->paginate());
	}


	function rate_hours($service_class_id) {
		if (!$service_class_id) {
			$this->Session->setFlash(__('Please select a service class', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('services', $this->HwSvcClass->getRateHoursForClass($service_class_id));
		$this->set('service_class_id', $service_class_id);
		$this->set('title_for_layout',"Manage Service Class");

	}


	function job_update_hw_service_rates() {
		$this->layout = 'empty';
		$this->HwSvcClass->job_update_hw_service_rates();
	}
}
?>