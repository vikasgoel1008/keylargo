<?php
class ContestantsController extends AppController {

	var $name = 'Contestants';
	var $helpers = array('Html', 'Form');

	function beforeFilter() {
		parent::beforeFilter();
		$this->Session->write('ActivePage','transactions');
	}


	function index($contest_id=null) {
		$this->Contestant->recursive = 0;
		if (!empty($contest_id)){
			$this->set('contestants', $this->paginate(array('contest_id' => $contest_id)));
		}
		else {
			$this->set('contestants', $this->paginate());
		}
		$this->set('title_for_layout',"NewsgroupDirect Contestants");
	}


	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid Contestant.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('contestant', $this->Contestant->read(null, $id));
	}


	function add() {
		if (!$this->is_admin_user()) {
			$this->Session->setFlash(__('You do not have the correct privileges to view this information.', true), 'error');
			$this->redirect(array('action'=>'view', $id));
		}
		if (!empty($this->data)) {
			$this->Contestant->create();
			if ($this->Contestant->save($this->data)) {
				$this->Session->setFlash(__('The Contestant has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Contestant could not be saved. Please, try again.', true));
			}
		}
		$contests = $this->Contestant->Contest->find('list');
		$this->set(compact('contests'));
	}


	function edit($id = null) {
		if (!$this->is_admin_user()) {
			$this->Session->setFlash(__('You do not have the correct privileges to view this information.', true), 'error');
			$this->redirect(array('action'=>'view', $id));
		}
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Contestant', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->Contestant->save($this->data)) {
				$this->Session->setFlash(__('The Contestant has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Contestant could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Contestant->read(null, $id);
		}
		$contests = $this->Contestant->Contest->find('list');
		$this->set(compact('contests'));
	}


	function delete($id = null) {
		if (!$this->is_admin_user()) {
			$this->Session->setFlash(__('You do not have the correct privileges to view this information.', true), 'error');
			$this->redirect(array('action'=>'view', $id));
		}
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Contestant', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Contestant->del($id)) {
			$this->Session->setFlash(__('Contestant deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}
}
?>