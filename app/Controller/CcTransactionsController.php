<?php
App::import('Vendor','firstDataAPI',array('file'=>'firstDataAPI.php'));
class CcTransactionsController extends AppController {

	var $name = 'CcTransactions';
	var $helpers = array('Html', 'Form');
	var $paginate = array('limit' => 10, 'order' => 'trans_date DESC');

	function beforeFilter() {
		parent::beforeFilter();
		$this->Session->write('ActivePage','transactions');
		$this->CcTransaction->Behaviors->attach('Containable');
	}


	function bank_reconciliation($date = null) {
		if (empty($date)) { $date = date("Y-m-d"); }
		$base = strtotime($date);
		$date1 = date('Y-m-d H:i:s', mktime(22,0,0,date('m',$base),date('d',$base)-2,date('Y',$base)));
		$date2 = date('Y-m-d H:i:s', mktime(22,0,0,date('m',$base),date('d',$base)-1,date('Y',$base)));
		$prev = date('Y-m-d', mktime(22,0,0,date('m',$base),date('d',$base)-1,date('Y',$base)));
		$next = date('Y-m-d', mktime(22,0,0,date('m',$base),date('d',$base)+1,date('Y',$base)));
		if ($date == date("Y-m-d")) { $next = ''; }
		$dates = array('report' => $date, 'start' => $date1, 'end' => $date2, 'prev' => $prev, 'next' => $next);
		$this->CcTransaction->contain();
		$revenues = $this->CcTransaction->find('first', array('conditions' => array('approved' => 'APPROVED', 'trans_date >' => $date1, 'trans_date <=' => $date2, 'amount >' => 0),
															'fields' => array('sum(amount) dollars','count(id) trans')));
		$this->CcTransaction->contain();
		$refunds = $this->CcTransaction->find('first', array('conditions' => array('approved' => 'APPROVED', 'trans_date >' => $date1, 'trans_date <=' => $date2, 'amount <=' => 0),
															'fields' => array('sum(amount) dollars','count(id) trans')));
		$this->set("refunds", $refunds[0]);
		$this->set("revenues", $revenues[0]);
		$this->set("dates", $dates);
	}


	function index($member_id = null) {
		$this->CcTransaction->recursive = 0;
		if (!$member_id) {
			$this->set('ccTransactions', $this->paginate());
		}
		else {
			parent::session_clean_member_id();
			parent::session_write_member_id($member_id);
			$this->set('ccTransactions', $this->paginate(array('member_id' => $member_id)));
			$this->set("member", $this->CcTransaction->Member->getMember($member_id));
			$this->set("pd", $this->CcTransaction->Member->PaymentDetail->find('first', array('conditions' => array('PaymentDetail.member_id' => $member_id))));
		}

		$this->set('title_for_layout',"eManager - Transactions");
		$this->logActivity($member_id, 'compliance', 'transaction-index', 'success', '', '');
	}


	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid CcTransaction.', true));
			$this->redirect(array('action'=>'index'));
		}

		$ccTransaction = $this->CcTransaction->read(null, $id);
		$refunds = $this->CcTransaction->find('all', array('fields'=>array('SUM(amount) as total_charges'),
		                                     			   'group'=>array('order_nbr'),
														   'conditions' => array('order_nbr' => $ccTransaction['CcTransaction']['order_nbr'],'approved' => 'APPROVED'),
		                                     			   'recursive'=>-1));
		$this->Session->write('Transaction.id',$id);
		$this->set(compact('ccTransaction','refunds'));
	}


	function add() {
		if (!empty($this->data)) {
			$this->CcTransaction->create();
			if ($this->CcTransaction->save($this->data)) {
				$this->Session->setFlash(__('The CcTransaction has been saved', true), 'flash_success');
				$this->redirect(array('action'=>'index', $this->data['CcTransaction']['member_id']));
			} else {
				$this->Session->setFlash(__('The CcTransaction could not be saved. Please, try again.', true));
			}
		}
		if ($this->Session->check('RefundSaveFail') )
		$this->data = $this->Session->read('RefundSaveFail');
		$this->set('title_for_layout',"eManager - Add Customer Transaction");
	}


	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid CcTransaction', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->CcTransaction->save($this->data)) {
				$this->Session->setFlash(__('The CcTransaction has been saved', true), 'flash_success');
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The CcTransaction could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->CcTransaction->read(null, $id);
		}
		$members = $this->CcTransaction->Member->find('list');
		$this->set(compact('members'));
	}


	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for CcTransaction', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->CcTransaction->del($id)) {
			$this->Session->setFlash(__('CcTransaction deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}


	function query() {
		$this->set('title_for_layout',"Search Transaction Records");
	}


	function related ($order_nbr) {
		$this->CcTransaction->recursive = 0;
		if (!$order_nbr) {
			$this->Session->setFlash(__('An order number must be provided.', true));
			$this->redirect(array('action'=>'index', $this->Session->read('Member.id')));
		}
		$this->set('ccTransactions', $this->paginate(array('order_nbr' => $order_nbr)));
	}


	function refund ($trans_id) {
		if (!$trans_id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid CcTransaction', true));
			$this->redirect(array('action'=>'index'));
		}
		$myRefundAmount = $this->Session->read('MaxRefund');
		if (empty($myRefundAmount)) {
			$myRefundAmount = "N/A";
		}
		if (!empty($this->data)) {
			$maxRefundAmount = $this->Session->read('MaxRefund');
			if ($this->data['CcTransaction']['amount'] > $maxRefundAmount ) {
				$this->Session->setFlash(__('The refund amount ('.$this->data['CcTransaction']['amount'].'cannot be greater than ' . $maxRefundAmount, true), 'error');
				$this->redirect(array('action'=>'refund',$trans_id));
			}
			if (empty($this->data['CcTransaction']['trans_purpose'])) {
				$this->Session->setFlash(__('You must provide a reason for the refund', true), 'error');
				$this->redirect(array('action'=>'refund',$trans_id));
			}
			$this->loadModel("PaymentDetail");
			$pd = $this->PaymentDetail->find('first', array('conditions' => array('PaymentDetail.member_id' => $this->Session->read('Member.id'))));
			$cc = new firstDataAPI();
			$cc->processRefund($pd['PaymentDetail']['cc_card_nbr'],parent::right($pd['PaymentDetail']['cc_card_exp'],2),parent::right(parent::left($pd['PaymentDetail']['cc_card_exp'],4),2),$this->data['CcTransaction']['amount'],$this->data['CcTransaction']['order_nbr']);
			unset($this->data['CcTransaction']['id']);
			$this->data['CcTransaction']['member_id'] = $this->Session->read('Member.id');
			$this->data['CcTransaction']['amount'] = abs($this->data['CcTransaction']['amount']) * -1;
			$this->data['CcTransaction']['cc_last_4'] = $pd['PaymentDetail']['cc_last_4'];
			$this->data['CcTransaction']['expire'] = $pd['PaymentDetail']['cc_card_exp'];
			$this->data['CcTransaction']['approval_code'] = $cc->approvalCode;
			$this->data['CcTransaction']['ref_code'] = $cc->refNumber;
			$this->data['CcTransaction']['approved'] = $cc->approved;
			$this->data['CcTransaction']['error'] = $cc->friendlyMessage;
			$this->data['CcTransaction']['full_error'] = $cc->fullError;
			$this->data['CcTransaction']['message'] = $cc->returnMessage;
			$this->data['CcTransaction']['avs'] = $cc->avs;
			$this->data['CcTransaction']['trans_date'] = date('Y-m-d H:i:s');
			$this->data['CcTransaction']['trans_type'] = 'refund';
			$this->data['CcTransaction']['currency'] = 'USD';
			$this->CcTransaction->create();
			if ($this->CcTransaction->save($this->data)) {
				if ($cc->success)
				$this->Session->setFlash(__('The refund was successfully processed', true), 'flash_success');
				else
				$this->Session->setFlash(__('The refund was not processed.  See error below.', true), 'error');
				$this->redirect(array('action'=>'view',$this->CcTransaction->id));
			}
			else {
				if ($cc->success) {
					$this->Session->setFlash(__('The refund was successfully processed, but was not saved.', true), 'error');
					$this->Session->write('RefundSaveFail',$this->data);
					$this->redirect(array('action' => 'add'));
				}
				else {
					$this->Session->setFlash(__('The refund was not processed and could not be saved. <br/>' . $cc->friendlyMessage, true), 'error');
					$this->redirect($this->referer());
				}
			}
		}
		if (empty($this->data)) {
			$this->data = $this->CcTransaction->read(null, $trans_id);
		}
		$refunds = $this->CcTransaction->find('all', array('fields'=>array('SUM(amount) as total_charges'),
		                                     			   'group'=>array('order_nbr'),
														   'conditions' => array('order_nbr' => $this->data['CcTransaction']['order_nbr'],'approved' => 'APPROVED'),
		                                     			   'recursive'=>-1));
		$this->Session->delete('MaxRefund');
		$this->Session->write('MaxRefund', $refunds[0][0]['total_charges']);
		$members = $this->CcTransaction->Member->find('list', array('conditions' => array('Member.id' => $this->Session->read('Member.id') )));
		$this->set(compact('members','refunds'));
	}


	function report_current_day_revenue() {
		$this->layout = 'auto_refresh';
		$this->set('title_for_layout',"Today's Snapshot");
		if (!$this->is_admin_user()) {
			$this->Session->setFlash(__('You do not have the correct privileges to view this information.', true), 'error');
			$this->redirect(array('controller'=>'members', 'action'=>'search'));
		}
		$today = date("Y-m-d");
		$revenues = $this->CcTransaction->find('first', array('fields' => array('SUM(amount) as charges', 'left(trans_date,10) as revenue_date'),
		                                     			   	'group' => array('left(trans_date,10)'),
															'conditions' => array('approved' => 'APPROVED',
																				  'left(trans_date,10)' => $today)));
		$signups = $this->CcTransaction->find('first', array('fields' => array('COUNT(amount) as signups', 'left(trans_date,10) as revenue_date'),
		                                     			   	'group' => array('left(trans_date,10)'),
															'conditions' => array('approved' => 'APPROVED',
																				'trans_purpose' => 'signup',
																				'left(trans_date,10)' => $today)));

		$adds = $this->CcTransaction->find('first', array('fields' => array('COUNT(amount) as adds', 'left(trans_date,10) as revenue_date'),
		                                     			   	'group' => array('left(trans_date,10)'),
															'conditions' => array('approved' => 'APPROVED',
																				'trans_purpose' => 'add block',
																				'left(trans_date,10)' => $today)));
		$this->set(compact('revenues'));
		$this->set(compact('signups'));
		$this->set(compact('adds'));
		parent::session_clean_member_id();
	}


	function report_revenue_by_day() {
		if (!$this->is_admin_user()) {
			$this->Session->setFlash(__('You do not have the correct privileges to view this information.', true), 'error');
			$this->redirect(array('controller'=>'members', 'action'=>'search'));
		}
		$this->set('title_for_layout',"eManager - Daily Revenues");
		$number_days = 15;
		$revenues = $this->CcTransaction->find('all', array('fields' => array('SUM(amount) as charges', 'left(trans_date,10) as revenue_date'),
		                                     			   	'order' => array('trans_date DESC'),
														 	'group' => array('left(trans_date,10)'),
															'conditions' => array('approved' => 'APPROVED',
																				  'to_days(CURDATE())-to_days(left(CcTransaction.trans_date,10)) <=' => $number_days)));
		$this->set('dayslength',$number_days);
		$this->set(compact('revenues'));
		parent::session_clean_member_id();
	}


	function report_graph_daily_sales($days = 30) {
		if (!$this->is_admin_user()) {
			$this->Session->setFlash(__('You do not have the correct privileges to view this information.', true), 'error');
			$this->redirect(array('controller'=>'members', 'action'=>'search'));
		}
		$this->set('title_for_layout',"eManager - Sales by Type - $days Days");
		$revenues = $this->CcTransaction->find('all', array('fields'	=> array('left(trans_date,10) sale_date',
																			  	 'CcTransaction.trans_purpose', 
																			  	 'SUM(amount) charges'),
		                                     			   	'order' 	=> array('trans_date ASC', 'trans_purpose'),
														 	'group'	 	=> array('left(trans_date,10) ASC', 'trans_purpose'),
															'conditions' => array('approved' => 'APPROVED',
																				  'trans_type' => 'charge',
																				  'to_days(now())-to_days(trans_date) <' => $days)));
		$rcd_date = "";
		$output[] = '';
		$new = '';
		foreach ($revenues as $sale) {
			if ($sale[0]['sale_date'] != $rcd_date) {
				if (!empty($new)) { $output[] = $new;}
				$new = array('date'=>$sale[0]['sale_date'], 'block'=>0.00, 'rebill'=>0.00, 'recycle'=>0.00, 'freetrial'=>0.00, 'signup'=>0.00,'freeupgrade'=>0.00);
				$rcd_date = $sale[0]['sale_date'];
			}
			switch ($sale['CcTransaction']['trans_purpose']) {
				case "add block":
					$new['block'] = $sale[0]['charges'];
					break;
				case "free trial expire":
					$new['freetrial'] = $sale[0]['charges'];
					break;
				case "free upgrade":
					$new['freeupgrade'] = $sale[0]['charges'];
					break;
				case "rebill":
					$new['rebill'] = $sale[0]['charges'];
					break;
				case "recycle":
					$new['recycle'] = $sale[0]['charges'];
					break;
				case "signup":
					$new['signup'] = $sale[0]['charges'];
					break;
			}
		}
		if (!empty($new)) { $output[] = $new;}
		$this->set('days',$days);
		$this->set('revenues',$output);
		parent::session_clean_member_id();
	}


	function report_graph_monthly_sales($months = null) {
		if (!$this->is_admin_user()) {
			$this->Session->setFlash(__('You do not have the correct privileges to view this information.', true), 'error');
			$this->redirect(array('controller'=>'members', 'action'=>'search'));
		}
		$this->CcTransaction->contain();
		$revenues = $this->CcTransaction->find('all', array('fields'	=> array("CONCAT(monthname(trans_date), ' ', CAST(year(trans_date) AS CHAR)) as sale_date",
																			  	 'CcTransaction.trans_purpose', 
																			  	 'SUM(amount) charges'),
		                                     			   	'order' 	=> array('year(trans_date) ASC', 'month(trans_date) ASC', 'trans_purpose'),
														 	'group'	 	=> array('year(trans_date) DESC', 'monthname(trans_date) DESC', 'trans_purpose'),
															'conditions' => array('approved' => 'APPROVED', 'left(trans_date,7) <>' => '2009-07'  )));
		$rcd_date = "";
		$new = '';
		foreach ($revenues as $sale) {
			if ($sale[0]['sale_date'] != $rcd_date) {
				if (!empty($new)) { $output[] = $new;}
				$new = array('date'=>$sale[0]['sale_date'], 'block'=>0.00, 'rebill'=>0.00, 'recycle'=>0.00, 'freetrial'=>0.00, 'signup'=>0.00,'freeupgrade'=>0.00);
				$rcd_date = $sale[0]['sale_date'];
			}
			switch ($sale['CcTransaction']['trans_purpose']) {
				case "add block":
					$new['block'] = $sale[0]['charges'];
					break;
				case "free trial expire":
					$new['freetrial'] = $sale[0]['charges'];
					break;
				case "free upgrade":
					$new['freeupgrade'] = $sale[0]['charges'];
					break;

				case "rebill":
					$new['rebill'] = $sale[0]['charges'];
					break;
				case "recycle":
					$new['recycle'] = $sale[0]['charges'];
					break;
				case "signup":
					$new['signup'] = $sale[0]['charges'];
					break;
			}
		}
		if (!empty($new)) { $output[] = $new;$this->set('revenues',$output);}
		$this->set('title_for_layout',"eManager - Monthly Revenues Chart");
		parent::session_clean_member_id();
	}


	function search() {
		if (isset($this->data)) {
			$query=trim($this->data['CcTransaction']['searchterm']);
			$this->Session->write('ccSearchTerm',$query);
		}
		else { $query = $this->Session->read('ccSearchTerm'); }
		if (empty($query)) {
			$this->Session->setFlash(__('Please provide a search value.', true), 'error');
			$this->redirect($this->referer());
		}
		$this->paginate['contain'] = array();
		$conditions = array('or' => array( array("CcTransaction.cc_last_4 LIKE"=>"%$query%"),
		array("CcTransaction.order_nbr LIKE"=>"%$query%"),
		array("CcTransaction.ref_code LIKE"=>"%$query%")));
		$trans = $this->paginate(array($conditions));
		$this->set('ccTransactions', $trans);
		$this->set('searchterm', $query);
		$this->set('title_for_layout',"Transaction Search Results");
		$this->Session->delete('Member.id');
	}
}
?>