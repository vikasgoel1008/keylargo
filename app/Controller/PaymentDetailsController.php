<?php
class PaymentDetailsController extends AppController {
	
	var $name = 'PaymentDetails';
	var $helpers = array('Html', 'Form');

	function beforeFilter() {
	    parent::beforeFilter(); 
    	$this->Session->write('ActivePage','billing');
	}
	
	
	function index() {
		$this->PaymentDetail->recursive = 0;
		$this->set('paymentDetails', $this->paginate());
	}
	

	function view($member_id = null) {
		if (!$member_id) {
			$this->Session->setFlash(__('Invalid PaymentDetail.', true));
			$this->redirect(array('action'=>'index'));
		}
		$paymentDetail = $this->PaymentDetail->find('first', array('conditions' => array('PaymentDetail.member_id' => $member_id)));
		$this->set(compact('paymentDetail'));
		$this->LoadModel('Member');
		$this->set('member', $this->Member->getMember($member_id));
		parent::session_clean_member_id();
		parent::session_write_member_id($member_id);
	}
	

	function add() {
		if (!empty($this->data)) {
			$this->PaymentDetail->create();
			if ($this->PaymentDetail->save($this->data)) {
				$this->Session->setFlash(__('The PaymentDetail has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The PaymentDetail could not be saved. Please, try again.', true));
			}
		}
		$members = $this->PaymentDetail->Member->find('list');
		$this->set(compact('members'));
	}
	

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid PaymentDetail', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->PaymentDetail->save($this->data)) {
				$this->Session->setFlash(__('The PaymentDetail has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The PaymentDetail could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->PaymentDetail->read(null, $id);
			parent::session_clean_member_id();
			parent::session_write_member_id($id);
		}
	}
	

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for PaymentDetail', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->PaymentDetail->del($id)) {
			$this->Session->setFlash(__('PaymentDetail deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}
}
?>