<?php
class MLogsController extends AppController {

	var $name = 'MLogs';
	var $helpers = array('Html', 'Form');

	function beforeFilter() {
		parent::beforeFilter();
	}


	function index($member_id) {
		$this->MLog->recursive = 0;
		$this->paginate = array('limit' => 20,
								'order' => array('MLog.id' => 'DESC'));
		if (empty($member_id)) {
			$this->Session->setFlash(__('You need to select a customer before you can view the logs.', true), 'error');
			$this->redirect(array('controller' => 'members', 'action'=>'search'));
		}
		$this->LoadModel('Member');
		$this->Member->recursive=0;
		$member = $this->Member->find('first', array('conditions' => array('Member.id' => $member_id)));
		$this->set('title_for_layout',"Log Manager");
		$this->Session->write('Member.id',$member_id);
		$this->Session->write('ActivePage','logs');
		$this->set('memberLogs', $this->paginate(array('member_id' => $member_id, "NOT" => array('log_type' => array('compliance','bandwidth')))));
		$this->set('member', $member);
	}


	function bandwidth($member_id) {
		$this->MLog->recursive = 0;
		$this->paginate = array('limit' => 20,
								'order' => array('MLog.id' => 'DESC'));
		if (empty($member_id)) {
			$this->Session->setFlash(__('You need to select a customer before you can view the logs.', true), 'error');
			$this->redirect(array('controller' => 'members', 'action'=>'search'));
		}
		$this->LoadModel('Member');
		$this->Member->recursive=0;
		$member = $this->Member->find('first', array('conditions' => array('Member.id' => $member_id)));
		$this->set('title_for_layout',"Log Manager");
		parent::session_clean_member_id();
		parent::session_write_member_id($member_id);
		$this->Session->write('ActivePage','logs');
		$this->set('memberLogs', $this->paginate(array('member_id' => $member_id, 'log_type' => 'bandwidth')));
		$this->set('member', $member);
	}


	function compliance($member_id) {
		$this->MLog->recursive = 0;
		$this->paginate = array('limit' => 20,
								'order' => array('MLog.id' => 'DESC'));
		if (empty($member_id)) {
			$this->Session->setFlash(__('You need to select a customer before you can view the logs.', true), 'error');
			$this->redirect(array('controller' => 'members', 'action'=>'search'));
		}
		$this->LoadModel('Member');
		$this->Member->recursive=0;
		$member = $this->Member->find('first', array('conditions' => array('Member.id' => $member_id)));
		$this->set('title_for_layout',"Log Manager");
		parent::session_clean_member_id();
		parent::session_write_member_id($member_id);
		$this->Session->write('ActivePage','logs');
		$this->set('memberLogs', $this->paginate(array('member_id' => $member_id, 'log_type' => 'compliance')));
		$this->set('member', $member);
	}


	function report_unique_logins_per_day($days = 30) {
		$this->set('title_for_layout',"$days Unique Logins per Day");
		$debug = Configure::read('debug');
		$logins = $this->MLog->find('all', array('fields' => array('left(log_date_time,10) login_date', 'count(DISTINCT member_id) logins'),
		                                     	'order' => array('log_date_time'),
		                                     	'group' => array('left(log_date_time,10)'),
												'conditions' => array('to_days(now())-to_days(left(log_date_time,10)) <=' => $days +1,
														 			  'log_type' => 'login')));
		$count = 0;
		$dateSeries = '';
		$loginSeries = '';
		foreach ($logins as $loginDate) {				
			if ($debug > 0) { print_r($loginDate); echo "<br/>"; }
			$dateSeries .= '<value xid=\''.$count.'\'>'.$loginDate['0']['login_date'].'</value>';
			$loginSeries .= '<value xid=\''.$count.'\'>'.$loginDate['0']['logins'].'</value>';
			$count++;
		}
		$this->Session->deleteete('ActivePage');
		parent::session_clean_member_id();
		$this->set('seriesXML',$dateSeries);
		$this->set('valuesXML',"<graph gid='1'>$loginSeries</graph>");
		$this->set('days',$days);
	}


	function report_unique_logins_pay_type($days = 30) {
		$debug = Configure::read('debug');
		$this->set('title_for_layout',"$days Day Unique Logins per Day by Pay Type");
		$logins = $this->MLog->query("SELECT left(log_date_time,10) login_date, count(DISTINCT member_id) logins, pay_type
										from m_logs, members m
									   WHERE log_type = 'login'
										 AND m.id = member_id
										 AND to_days(now())-to_days(left(log_date_time,10)) <= $days +1
									group by left(log_date_time,10), m.pay_type");
		$count = 0;
		$dateSeries = '';
		$loginSeriesCC = '';
		$loginSeriesWP = '';
		$loginSeriesOther = '';
		$rcd_date = '';
		foreach ($logins as $loginDate) {
			if ($loginDate[0]['login_date'] != $rcd_date) {
				$dateSeries .= '<value xid=\''.$loginDate[0]['login_date'].'\'>'.$loginDate[0]['login_date'].'</value>';
				$rcd_date = $loginDate[0]['login_date'];
			}
			switch (strtolower($loginDate['m']['pay_type'])) {
				case "cc":
					$loginSeriesCC .= '<value xid=\''.$loginDate[0]['login_date'].'\'>'.$loginDate['0']['logins'].'</value>';
					break;
				case "wp":
					$loginSeriesWP .= '<value xid=\''.$loginDate[0]['login_date'].'\'>'.$loginDate['0']['logins'].'</value>';
					break;
				default:
					$loginSeriesOther .= '<value xid=\''.$loginDate[0]['login_date'].'\'>'.$loginDate['0']['logins'].'</value>';
					break;
			}
		}
		$this->Session->deleteete('ActivePage');
		parent::session_clean_member_id();
		$this->set('seriesXML',$dateSeries);
		$this->set('valuesXML',"<graph gid='1'>$loginSeriesCC</graph><graph gid='2'>$loginSeriesWP</graph><graph gid='3'>$loginSeriesOther</graph>");
		$this->set('days',$days);
	}


	function report_unique_logins_pay_vs_free($days = 30) {
		$debug = Configure::read('debug');
		$this->set('title_for_layout',"$days Day Unique Logins - Paid vs Free");
		$logins = $this->MLog->query("SELECT left(log_date_time,10) login_date,
      		 								 COUNT(DISTINCT member_id) logins, 
      		 								 CASE WHEN curr_1_price > 0 THEN 'PAID' ELSE 'FREE' END ngd_pay
      		 							FROM m_logs, members m, plans p
      		 						   WHERE log_type = 'login'
      		 						     AND m.id = member_id
      		 						     AND m.plan_id = p.id
      		 						     AND to_days(now())-to_days(left(log_date_time,10)) <= 31
      		 						   GROUP BY left(log_date_time,10), ngd_pay");
		$count = 0;
		$dateSeries = '';
		$loginSeriesPaid = '';
		$loginSeriesFree = '';
		$rcd_date = '';
		foreach ($logins as $loginDate) {
			if ($loginDate[0]['login_date'] != $rcd_date) {
				$dateSeries .= '<value xid=\''.$loginDate[0]['login_date'].'\'>'.$loginDate[0]['login_date'].'</value>';
				$rcd_date = $loginDate[0]['login_date'];
			}
			switch (strtolower($loginDate[0]['ngd_pay'])) {
				case "paid":
					$loginSeriesPaid .= '<value xid=\''.$loginDate[0]['login_date'].'\'>'.$loginDate['0']['logins'].'</value>';
					break;
				case "free":
					$loginSeriesFree .= '<value xid=\''.$loginDate[0]['login_date'].'\'>'.$loginDate['0']['logins'].'</value>';
					break;
			}
		}
		$this->Session->delete('ActivePage');
		parent::session_clean_member_id();
		$this->set('seriesXML',$dateSeries);
		$this->set('valuesXML',"<graph gid='1'>$loginSeriesPaid</graph><graph gid='2'>$loginSeriesFree</graph>");
		$this->set('days',$days);
	}


	function report_active_logins_daily($days = 90) {
		$dayX = $days + 1;
		$ngdlogins = $this->MLog->query("SELECT left(log_date_time,10) login_date,
      		 								 COUNT(DISTINCT member_id) logins
      		 							FROM m_logs, members m, plans p
      		 						   WHERE log_type = 'login'
      		 						     AND m.id = member_id
      		 						     AND m.plan_id = p.id
      		 						     AND to_days(now())-to_days(left(log_date_time,10)) <= $dayX
      		 						   GROUP BY left(log_date_time,10)
      		 						   ORDER BY left(log_date_time,10)");
		$hwlogins = $this->MLog->query("SELECT left(usage_date,10) login_date,
      		 						     COUNT(*) hw_logins
      		 							FROM hw_datas
      		 						   WHERE total_bytes > 0
      		 						     AND data_type = 'daily'
       		 						     AND to_days(now())-to_days(usage_date) <= $dayX
      		 						   GROUP BY usage_date
      		 						   ORDER BY usage_date");
		$actives = $this->MLog->query("SELECT date login_date,
		                                      active_subs
		                                 FROM daily_stats
		                                WHERE to_days(now())-to_days(date) <= $dayX
      		 						   ORDER BY date");
		$rcd = '';
		$dateSeries = '';
		$activeSeries = '';
		$ngdLoginSeries = '';
		$hwLoginSeries = '';
		$pctNGDSeries = '';
		$pctHWSeries = '';
		$count = 0;
		foreach ($actives as $active) {
			$dateSeries .= '<value xid=\''.$active['daily_stats']['login_date'].'\'>'.$active['daily_stats']['login_date'].'</value>';				
			$rcd[$count]['rcd_date'] = $active['daily_stats']['login_date'];
			$rcd[$count]['active_accts'] = $active['daily_stats']['active_subs'];	
			$count++;
		}
		$count = 0;
		foreach ($ngdlogins as $login) {
			$match = false;
			while (!$match ) {
				if (!array_key_exists($count, $rcd)) {
					$match = true;
					continue;
				}
				if ($rcd[$count]['rcd_date'] == $login[0]['login_date']) {
					$rcd[$count]['ngd_logins'] = $login[0]['logins'];
					$match = true;
				}
				else {$count++;}
			}
		}
		$count = 0;
		foreach ($hwlogins as $login) {
			$match = false;
			while (!$match) {
				if ($rcd[$count]['rcd_date'] == $login[0]['login_date']) {
					$rcd[$count]['hw_logins'] = $login[0]['hw_logins'];
					$match = true;
				}
				else {$count++;}
			}
		}
		foreach ($rcd as $record) {
			$hwPCT = round(($record['hw_logins']/$record['active_accts'])*100,2);
			$ngdPCT = round(($record['ngd_logins']/$record['active_accts'])*100,2);
			$pctHWSeries .= '<value xid=\''.$record['rcd_date'].'\'>'.$hwPCT.'</value>';
			$pctNGDSeries .= '<value xid=\''.$record['rcd_date'].'\'>'.$ngdPCT.'</value>';	
		}

		$this->Session->delete('ActivePage');
		parent::session_clean_member_id();
		$this->set('seriesXML',$dateSeries);
		$this->set('valuesXML',"<graph gid='3'>$pctNGDSeries</graph><graph gid='5'>$pctHWSeries</graph>");
		$this->set('days',$days);
		$this->set('title_for_layout',"User Login Pct - $days Day");
	}


	function report_active_logins_weekly($weeks = 52) {
		$dateStats =getdate();
		$recentSunday = date('Y-m-d', mktime(0,0,0,date('m'),date('d')-($dateStats['wday']),date('Y')));
		$oldestDate = date('Y-m-d', mktime(0,0,0,date('m'),date('d')-($dateStats['wday'])-(7 * $weeks),date('Y')));
		$ngdlogins = $this->MLog->query("SELECT left(log_date_time,10) login_date,
      		 								 COUNT(DISTINCT member_id) logins
      		 							FROM m_logs, members m, plans p
      		 						   WHERE log_type = 'login'
      		 						     AND m.id = member_id
      		 						     AND m.plan_id = p.id
      		 						     AND left(log_date_time,10) >= '$oldestDate'
      		 						     AND left(log_date_time,10) <= '$recentSunday'
      		 						   GROUP BY left(log_date_time,10)
      		 						   ORDER BY left(log_date_time,10)");
		$hwlogins = $this->MLog->query("SELECT left(usage_date,10) login_date,
      		 						     COUNT(*) hw_logins
      		 							FROM hw_datas
      		 						   WHERE total_bytes > 0
      		 						     AND data_type = 'daily'
       		 						     AND usage_date >= '$oldestDate'
      		 						     AND usage_date <= '$recentSunday'
      		 						   GROUP BY usage_date
      		 						   ORDER BY usage_date");
		$actives = $this->MLog->query("SELECT date login_date,
		                                      active_subs
		                                 FROM daily_stats
		                                WHERE date >= '$oldestDate'
      		 						     AND date <= '$recentSunday'
      		 						   ORDER BY date");
		$rcd = '';
		$dateSeries = '';
		$activeSeries = '';
		$ngdLoginSeries = '';
		$hwLoginSeries = '';
		$pctNGDSeries = '';
		$pctHWSeries = '';
		$count = 0;
		foreach ($actives as $active) {
			$rcd[$count]['rcd_date'] = $active['daily_stats']['login_date'];
			$rcd[$count]['active_accts'] = $active['daily_stats']['active_subs'];
			$count++;
		}
		$count = 0;
		foreach ($ngdlogins as $login) {	
			$match = false;
			while (!$match ) {
				if ($rcd[$count]['rcd_date'] == $login[0]['login_date']) {
					$rcd[$count]['ngd_logins'] = $login[0]['logins'];
					$match = true;
				}
				else {$count++;}
			}
		}
		$count = 0;
		foreach ($hwlogins as $login) {	
			$match = false;
			while (!$match) {
				if ($rcd[$count]['rcd_date'] == $login[0]['login_date']) {
					$rcd[$count]['hw_logins'] = $login[0]['hw_logins'];
					$match = true;
				}
				else {$count++;}
			}
		}
		$count = 0;
		$arCount = 0;
		$actives = 0;
		$ngdlogins = 0;
		$hwlogins = 0;
		foreach ($rcd as $record) {
			$actives += $record['active_accts'];
			$ngdlogins  += $record['ngd_logins'];
			$hwlogins  += $record['hw_logins'];	
			if ($count == 0) {
				$numerictime = strtotime($record['rcd_date']);
				$check = getdate($numerictime);
				$count = $check['wday']-1;
			}
			if ($count <> 6) {
				$count++;
			}
			else {
				$avgActive = $actives / 7;
				$avgNgd = $ngdlogins / 7;
				$avgHw = $hwlogins / 7;
				$hwPCT = round(($avgHw/$avgActive)*100,2);
				$ngdPCT = round(($avgNgd/$avgActive)*100,2);
				$dateSeries .= '<value xid=\''.$arCount.'\'>'.$record['rcd_date'].'</value>';
				$pctHWSeries .= '<value xid=\''.$arCount.'\'>'.$hwPCT.'</value>';
				$pctNGDSeries .= '<value xid=\''.$arCount.'\'>'.$ngdPCT.'</value>';
				$count = 0;
				$actives = 0;
				$ngdlogins = 0;
				$hwlogins = 0;
				$arCount++;
			}
		}
		$this->Session->delete('ActivePage');
		parent::session_clean_member_id();
		$this->set('seriesXML',$dateSeries);
		$this->set('valuesXML',"<graph gid='3'>$pctNGDSeries</graph><graph gid='5'>$pctHWSeries</graph>");
		$this->set('days',$weeks);
		$this->set('title_for_layout',"User Login Pct - $weeks Week");
	}


	function array_msort($array, $cols)
	{
		$colarr = array();
		foreach ($cols as $col => $order) {
			$colarr[$col] = array();
			foreach ($array as $k => $row) { $colarr[$col]['_'.$k] = strtolower($row[$col]); }
		}
		$eval = 'array_multisort(';
		foreach ($cols as $col => $order) {
			$eval .= '$colarr[\''.$col.'\'],'.$order.',';
		}
		$eval = substr($eval,0,-1).');';
		eval($eval);
		$ret = array();
		foreach ($colarr as $col => $arr) {
			foreach ($arr as $k => $v) {
				$k = substr($k,1);
				if (!isset($ret[$k])) $ret[$k] = $array[$k];
				$ret[$k][$col] = $array[$k][$col];
			}
		}
		return $ret;
	}


	function view($id) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid MLog.', true));
			$this->redirect($this->referer());
		}
		$MLog = $this->MLog->read(null, $id);
		$this->Session->write("Member.id", $MLog['MLog']['member_id']);
		if ($MLog['MLog']['log_type'] == 'plan change') {
			$this->LoadModel('Plan');
			$this->Plan->recursive = 0;
			$this->set("oldplan", $this->Plan->find('first', array('conditions' => array('Plan.id' => $MLog['MLog']['old_value']))));
			$this->set("newplan", $this->Plan->find('first', array('conditions' => array('Plan.id' => $MLog['MLog']['new_value']))));
		}
		$this->LoadModel('Member');
		$this->Member->recursive = 0;
		$member = $this->Member->find('first', array('conditions' => array('Member.id' => $MLog['MLog']['member_id'])));
		$this->set('member',$member);
		$this->set('memberLog', $MLog);
		$this->set('title_for_layout',"View Log Entry");
	}
}
?>