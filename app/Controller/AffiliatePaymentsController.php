<?php
class AffiliatePaymentsController extends AppController {

	function beforeFilter() {
		parent::beforeFilter();
		$this->Session->write('ActivePage','allAffPayments');
	}


	function index($count = 30) {
		$this->AffiliatePayment->recursive = 0;
		$this->paginate =  array('limit' => $count,
								'order' => array(
										'pay_date' => 'desc',
										'check_amt' => 'desc' 
										));
		$this->set('payments', $this->paginate());
		$this->set('title_for_layout',"NGD Affiliate Payments");
	}


	function ready_to_pay() {
		$this->AffiliatePayment->recursive = 0;
		$ap = $this->AffiliatePayment->find('all', array('conditions' => array()));
	}


	function view($id) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid Payment Id.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->AffiliatePayment->recursive = 0;
		$ap = $this->AffiliatePayment->read(null, $id);
		$this->set('ap',$ap);
		$this->set('title_for_layout',"NGD Affiliate Payment");
		$this->logActivity('', 'compliance', 'affiliate-payment-view', 'success', '', '');
	}


	function add() {
	}


	function pay($id) {
		if (!$this->is_admin_user()) {
			$this->Session->setFlash(__('You do not have the correct privileges to view this information.', true), 'error');
			$this->redirect(array('action'=>'view', $id));
		}
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Affiliate Payment', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->AffiliatePayment->save($this->data)) {
				$this->logActivity('', 'compliance', 'affiliate-payment-pay', 'saved', '', '');
				$this->Session->setFlash(__('The payment has been saved', true), 'flash_success');
				$this->redirect(array('action'=>'view',$id));
			} else {
				$this->logActivity('', 'compliance', 'affiliate-payment-pay', 'failed', '', '');
				$this->Session->setFlash(__('The payment could not be saved. Please, try again.', true), 'error');
			}
		}
		if (empty($this->data)) {
			$this->AffiliatePayment->recursive = 0;
			$this->data = $this->AffiliatePayment->read(null, $id);
		}
		$this->logActivity('', 'compliance', 'affiliate-payment-pay', 'view', '', '');
		$this->set('title_for_layout',"Pay Affiliate");
	}
}
?>
