<?php
class QuipsController extends AppController {
	
	var $name = 'Quips';
	var $helpers = array('Html', 'Form');

	function beforeFilter() {
		parent::beforeFilter();
	}

	function index() {
		$this->Quip->recursive = 0;
		$this->set('quips', $this->paginate());
		$this->Session->write('ActivePage','allQuips');
	}


	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid Quip.', true), 'error');
			$this->redirect(array('action'=>'index'));
		}
		$this->set('quip', $this->Quip->read(null, $id));
	}


	function add() {
		if (!empty($this->data)) {
			$this->Quip->create();
			$this->data['Quip']['date_added'] = date('Y-m-d');
			if ($this->Quip->save($this->data)) {
				$this->Session->setFlash(__('The Quip has been saved', true), 'flash_success');
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Quip could not be saved. Please, try again.', true), 'error');
			}
		}
		$this->Session->write('ActivePage','addQuip');
	}


	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Quip', true), 'error');
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->Quip->save($this->data)) {
				$this->Session->setFlash(__('The Quip has been saved', true), 'flash_success');
				$this->redirect(array('action'=>'view', $id));
			} else {
				$this->Session->setFlash(__('The Quip could not be saved. Please, try again.', true), 'error');
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Quip->read(null, $id);
		}
	}


	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Quip', true), 'error');
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Quip->del($id)) {
			$this->Session->setFlash(__('Quip deleted', true), 'flash_success');
			$this->redirect(array('action'=>'index'));
		}
	}
}
?>