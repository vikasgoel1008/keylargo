<?php
class RisksController extends AppController {

	var $name = 'Risks';
	var $helpers = array('Html', 'Form');

	var $paginate  = array('limit' => 20, 'order' => array('risk_score' => 'DESC'));

	function beforeFilter() {
		parent::beforeFilter();
	}


	function index($date = null) {
		if (empty($date)) {
			if (date('Y-m-d H:i:s') > date("Y-m-d H:i:s",mktime(4,0,0,date("m"),date("d"),date("Y")))) {
				$date = date("Y-m-d",mktime(0,0,0,date("m"),date("d")-1,date("Y")));
			}
			else {
				$date = date("Y-m-d",mktime(0,0,0,date("m"),date("d")-2,date("Y")));
			}
		}
		$this->Risk->recursive = 0;
		$risks = $this->paginate(array('reporting_date' => $date));
		$this->set('title_for_layout',"Risk Manager");
		$this->set('risks', $risks);
		$this->set('process_date', $date);
		parent::session_clean_member_id();
	}
}
?>