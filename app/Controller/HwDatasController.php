<?php
class HwDatasController extends AppController {

	var $name = 'HwDatas';
	var $helpers = array('Html', 'Form');

	function beforeFilter() {
		parent::beforeFilter();
		$this->HwData->Behaviors->attach('Containable');
	}


	function member_ips($member_id) {
		$this->paginate = array('order' => 'usage_date desc', 'fields' => 'ip_address, usage_date', 'limit' => 15);
		$ips = $this->paginate(array('data_type' => 'summary', "HwData.member_id" => $member_id));
		$this->set("member", $this->HwData->Member->getMember($member_id));
		$this->set('ips', $ips);
		$this->set('title_for_layout',"Member Access IP's");
		$this->Session->write('Member.id',$member_id);
	}


	function report_graph_daily_bw_usage($days = 180, $member_id = null) {
		ini_set('memory_limit','256M');
		ini_set('max_execution_time','120');
		$bytes = $this->HwData->graph_daily_bw_usage($member_id, $days);
		$dateSeries = '';
		$dataSeries = '';
		$movingSeries = '';
		$count = 0;
		$weight = array(0=>0,1=>0,2=>0,3=>0,4=>0,5=>0,6=>0);
		foreach ($bytes as $byte) {
			$dateSeries .= '<value xid=\''.$byte['HwData']['usage_date'].'\'>'.$byte['HwData']['usage_date'].'</value>';
			$dataSeries .= '<value xid=\''.$byte['HwData']['usage_date'].'\'>'.round($byte[0]['bytes'],2).'</value>';
			$weight[6] = $weight[5];
			$weight[5] = $weight[4];
			$weight[4] = $weight[3];
			$weight[3] = $weight[2];
			$weight[2] = $weight[1];
			$weight[1] = $weight[0];
			$weight[0] = $byte[0]['bytes'];
			if ($count >= 6) {
				$movingSeries .= '<value xid=\''.$byte['HwData']['usage_date'].'\'>'.round(array_sum($weight)/7,2).'</value>';
			}
			$count++;
		}
		$this->set('seriesXML',$dateSeries);
		$this->set('valuesXML',"<graph gid='1'>$dataSeries</graph><graph gid='2'>$movingSeries</graph>");
		$pageTitle = "Daily Bandwidth Usage";
		if (!empty($memeber_id)) {
			$pageTitle .= " for $member_id";
		}
		$this->set('title_for_layout',$pageTitle);
	}


	function report_daily_bandwidth_usage($days = 180, $member_id = null) {
		if (empty($member_id)) {
			$this->redirect(array('controller' => 'data_reports', 'action' => 'report_daily_bandwidth_usage', $days));
		}
		$bytes = $this->HwData->graph_daily_bw_usage($member_id, $days);
		$output = '';
		$new = '';
		$count = 0;
		$weight = array(0=>0,1=>0,2=>0,3=>0,4=>0,5=>0,6=>0);
		$moving28 = array(0=>0,1=>0,2=>0,3=>0,4=>0,5=>0,6=>0,7=>0,8=>0,9=>0,10=>0,11=>0,12=>0,13=>0,14=>0,15=>0,16=>0,17=>0,18=>0,19=>0,20=>0,21=>0,22=>0,23=>0,24=>0,25=>0,26=>0,27=>0);
		$chartMin = '';
		$chartMax = '';
		foreach ($bytes as $byte) {
			$weight[6] = $weight[5];
			$weight[5] = $weight[4];
			$weight[4] = $weight[3];
			$weight[3] = $weight[2];
			$weight[2] = $weight[1];
			$weight[1] = $weight[0];
			$weight[0] = $byte[0]['bytes'];
			$moving28[27] = $moving28[26];
			$moving28[26] = $moving28[25];
			$moving28[25] = $moving28[24];
			$moving28[24] = $moving28[23];
			$moving28[23] = $moving28[22];
			$moving28[22] = $moving28[21];
			$moving28[21] = $moving28[20];
			$moving28[20] = $moving28[19];
			$moving28[19] = $moving28[18];
			$moving28[18] = $moving28[17];
			$moving28[17] = $moving28[16];
			$moving28[16] = $moving28[15];
			$moving28[15] = $moving28[14];
			$moving28[14] = $moving28[13];
			$moving28[13] = $moving28[12];
			$moving28[12] = $moving28[11];
			$moving28[11] = $moving28[10];
			$moving28[10] = $moving28[9];
			$moving28[9] = $moving28[8];
			$moving28[8] = $moving28[7];
			$moving28[7] = $moving28[6];
			$moving28[6] = $moving28[5];
			$moving28[5] = $moving28[4];
			$moving28[4] = $moving28[3];
			$moving28[3] = $moving28[2];
			$moving28[2] = $moving28[1];
			$moving28[1] = $moving28[0];
			$moving28[0] = $byte[0]['bytes'];
			$new = array('date'=>$byte['HwData']['usage_date'], 'usage'=>round($byte[0]['bytes'],2), 'moving7'=>'{}', 'moving28'=>'{}');
			if ($count >= 6) {
				$new['moving7'] = round(array_sum($weight)/7,2);
			}
			if ($count >= 27) {
				$new['moving28'] = round(array_sum($moving28)/28,2);
			}
			$count++;
			$output[] = $new;
			if (empty($chartMin) || $chartMin > $byte[0]['bytes']) {$chartMin = $byte[0]['bytes'];}
			if (empty($chartMax) || $chartMax < $byte[0]['bytes']) {$chartMax = $byte[0]['bytes'];}
		}
		$this->set('days',$days);
		$this->set('bytes',$output);
		$this->set('chartMin',$chartMin-1500);
		$this->set('chartMax',$chartMax+1500);
		$this->Session->write('Member.id',$member_id);
	}
}
?>