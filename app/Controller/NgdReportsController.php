<?php
class NgdReportsController extends AppController {

	var $name = 'NgdReports';
	var $helpers = array('Html', 'Form');

	function beforeFilter() {
		parent::beforeFilter();
	}


	function index() {
		$this->NgdReport->recursive = 0;
		$this->set('NgdReports', $this->paginate());
		$this->Session->write('ActivePage','allOptions');
	}


	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid NgdReport.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('NgdReport', $this->NgdReport->read(null, $id));
	}


	function billings() {
		$report_date = $this->NgdReport->getRecentReportDate('avg_billings','overall');
		$this->paginate = array('limit' => 20);
		$conds = array("report" => 'avg_billings', 'data_date' => $report_date);
		$reports = $this->paginate($conds);
		$this->set('reports', $reports);
		$this->set('report_date', $report_date);
	}


	function chi($type = 'member') {
		$sub = '';
		if ($type = 'member') {
			$sub = 'member_chi';
		}
		$report_date = $this->NgdReport->getRecentReportDate('chi', $sub);
		$this->NgdReport->bindModel(array('belongsTo' => array("Member","Plan")),false);
		$this->paginate = array('limit' => 20,
								'order' => 'value desc',
								'contain' => array("Member","Plan"));
		$conds = array("report" => 'chi', 'data_date' => $report_date);
		if (!empty($sub)) {
			$conds['subkey'] = $sub;
		}
		$reports = $this->paginate($conds);
		$this->set('reports', $reports);
		$this->set('report_date', $report_date);
	}


	function churn() {
	}


	function ltv() {
		$report_date = $this->NgdReport->getRecentReportDate('ltv','overall');
		$this->paginate = array('limit' => 20);
		$conds = array("report" => 'ltv', 'data_date' => $report_date);
		$reports = $this->paginate($conds);
		$this->set('reports', $reports);
		$this->set('report_date', $report_date);
	}
}
?>