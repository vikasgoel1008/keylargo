<?php

class AffiliatesController extends AppController {

	function beforeFilter() {
		parent::beforeFilter();
		$this->Session->write('ActivePage','allAffiliate');
	}


	function active() {
		$this->Affiliate->recursive = 0;
		$this->paginate = array('limit' => 15, 'order' => array('reg_date' =>  'desc'), 'conditions' => array('status' => 'active'));
		$this->set('affiliates', $this->paginate());
		$this->set('title_for_layout',"NGD Active Affiliates");
		$this->Session->write('ActivePage','activeAffiliate');
	}


	function add() {
		$this->Session->write('ActivePage','addAffiliate');
	}


	function close($id) {
		if (!$this->is_admin_user()) {
			$this->Session->setFlash(__('You do not have the correct privileges to view this information.', true), 'error');
			$this->redirect(array('action'=>'view', $id));
		}
		if (!$id ) {
			$this->Session->setFlash(__('Invalid Affiliate', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Affiliate->closeAffiliate($id)) {
			$this->Session->setFlash(__('This affiliate has been closed', true), 'flash_success');
		}
		else {
			$this->Session->setFlash(__('Failed to close affiliate', true), 'error');
		}
		$this->redirect($this->referer());
	}


	function edit($id) {
		if (!$this->is_admin_user()) {
			$this->Session->setFlash(__('You do not have the correct privileges to view this information.', true), 'error');
			$this->redirect(array('action'=>'view', $id));
		}
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Affiliate', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->Affiliate->save($this->data)) {
				$this->Session->setFlash(__('The Affiliate has been saved', true), 'flash_success');
				$this->redirect(array('action'=>'view',$id));
			} else {
				$this->Session->setFlash(__('The Affiliate could not be saved. Please, try again.', true), 'error');
			}
		}
		if (empty($this->data)) {
			$this->Affiliate->recursive = 0;
			$this->data = $this->Affiliate->read(null, $id);
		}
	}


	function index() {
		$this->Affiliate->recursive = 0;
		$this->paginate = array('limit' => 15, 'order' => array('reg_date' =>  'desc'));
		$this->set('affiliates', $this->paginate());
		$this->set('title_for_layout',"NGD Affiliates");
	}


	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid Affiliate.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Affiliate->recursive = 0;
		$affiliate = $this->Affiliate->read(null, $id);
		$this->set('affiliate',$affiliate);
		$sales30 = $this->Affiliate->AffiliateSalesTotal->find('first',
		array('conditions' => array('affiliate_id' => $id,
																						 'to_days(now())-to_days(sales_date) <=' => 30),
																   'fields' => array("sum(sales_amount) sales"),
																   'group' => 'affiliate_id'));
		$sales60 = $this->Affiliate->AffiliateSalesTotal->find('first',
		array('conditions' => array('affiliate_id' => $id,
																						 'to_days(now())-to_days(sales_date) <=' => 60),
																   'fields' => array("sum(sales_amount) sales"),
																   'group' => 'affiliate_id'));
		$sales90 = $this->Affiliate->AffiliateSalesTotal->find('first',
		array('conditions' => array('affiliate_id' => $id,
																						 'to_days(now())-to_days(sales_date) <=' => 90),
																   'fields' => array("sum(sales_amount) sales"),
																   'group' => 'affiliate_id'));
		$this->set('sales30', $sales30[0]['sales']);
		$this->set('sales60', $sales60[0]['sales']);
		$this->set('sales90', $sales90[0]['sales']);
		$this->set('title_for_layout',"NGD View Affiliate - $id");
	}


	function payment_summary($id) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid Affiliate.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Affiliate->recursive = 0;
		$affiliate = $this->Affiliate->read(null, $id);
		$this->set('affiliate',$affiliate);
		$this->LoadModel("AffiliatePayment");
		$this->paginate = array('limit' => 15, 'order' => 'pay_date desc');
		$this->set('payments',$this->paginate('AffiliatePayment',array('AffiliatePayment.affiliate_id' => $id)));
		$this->set('title_for_layout',"NGD Affiliate Payments - $id");
	}


	function performance_report($days = 30) {
		$this->set('title_for_layout',"$days Day Affiliate Performance");
		$debug = Configure::read('debug');
		$sales30 = $this->Affiliate->AffiliateSalesTotal->find('all',
		array('conditions' => array('to_days(now())-to_days(sales_date) <=' => 30),
																   'fields' => array("sum(sales_amount) sales, affiliate_id"),
																   'group' => 'affiliate_id',
																   'order' => "sum(sales_amount) desc"));
		$sales60 = $this->Affiliate->AffiliateSalesTotal->find('all',
		array('conditions' => array('to_days(now())-to_days(sales_date) <=' => 60),
																   'fields' => array("sum(sales_amount) sales, affiliate_id"),
																   'group' => 'affiliate_id',
																   'order' => "sum(sales_amount) desc"));
		$sales90 = $this->Affiliate->AffiliateSalesTotal->find('all',
		array('conditions' => array('to_days(now())-to_days(sales_date) <=' => 90),
																   'fields' => array("sum(sales_amount) sales, affiliate_id"),
																   'group' => 'affiliate_id',
																   'order' => "sum(sales_amount) desc"));
		$count = 0;
		$affiliateSeries = '';
		$sales30Series = '';
		$sales60Series = '';
		$sales90Series = '';
		$affCheck = '';
		foreach ($sales30 as $data) {
			if ($debug > 0) { print_r($data); echo "<br/><br/>"; }
			if ($data[0]['sales'] == 0) {continue;}
			$affCheck[$count] = $data['AffiliateSalesTotal']['affiliate_id'];
			$affiliateSeries .= '<value xid=\''.$data['AffiliateSalesTotal']['affiliate_id'].'\'>'. $this->_getAffiliateEmail($data['AffiliateSalesTotal']['affiliate_id']) .'</value>';
			$sales30Series .= '<value xid=\''.$data['AffiliateSalesTotal']['affiliate_id'].'\'>'.$data[0]['sales'].'</value>';
			$count++;
		}
		foreach ($sales60 as $data) {
			if ($debug > 0) { print_r($data); echo "<br/><br/>"; }
			if ($data[0]['sales'] == 0) {continue;}
			if (!in_array($data['AffiliateSalesTotal']['affiliate_id'], $affCheck)) {
				$affCheck[$count] = $data['AffiliateSalesTotal']['affiliate_id'];
				$affiliateSeries .= '<value xid=\''.$data['AffiliateSalesTotal']['affiliate_id'].'\'>'. $this->_getAffiliateEmail($data['AffiliateSalesTotal']['affiliate_id']) .'</value>';
				$count++;
			}
			$sales60Series .= '<value xid=\''.$data['AffiliateSalesTotal']['affiliate_id'].'\'>'.$data[0]['sales'].'</value>';
		}
		foreach ($sales90 as $data) {
			if ($debug > 0) { print_r($data); echo "<br/><br/>"; }
			if ($data[0]['sales'] == 0) {continue;}
			if (!in_array($data['AffiliateSalesTotal']['affiliate_id'], $affCheck)) {
				$affCheck[$count] = $data['AffiliateSalesTotal']['affiliate_id'];
				$affiliateSeries .= '<value xid=\''.$data['AffiliateSalesTotal']['affiliate_id'].'\'>'. $this->_getAffiliateEmail($data['AffiliateSalesTotal']['affiliate_id']) .'</value>';
				$count++;
			}
			$sales90Series .= '<value xid=\''.$data['AffiliateSalesTotal']['affiliate_id'].'\'>'.$data[0]['sales'].'</value>';
		}
		$this->set('seriesXML',$affiliateSeries);
		$this->set('valuesXML',"<graph gid='3'>$sales30Series</graph><graph gid='2'>$sales60Series</graph><graph gid='1'>$sales90Series</graph>");
		$this->set('days',$days);
	}


	function _getAffiliateEmail($affiliate_id) {
		$this->Affiliate->recursive = 0;
		$aff = $this->Affiliate->find('first', array('conditions' => array('affiliate_id' => $affiliate_id)));
		return $aff['Affiliate']['email_address'];
	}
}
?>
