<?php
class NotesController extends AppController {
	
	var $name = 'Notes';
	var $helpers = array('Html', 'Form');

	function beforeFilter() {
		parent::beforeFilter();
		$this->Session->write('ActivePage','members');
	}

	
	function index($member_id) {
		$this->Note->recursive = 0;
		$this->paginate = array('order' => 'date_added');
		$this->set('notes', $this->paginate(array('member_id' => $member_id)));
		$this->set('member', $this->Note->Member->getMember($member_id));
		$this->Session->write('Member.id',$member_id);
	}


	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid Note.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('note', $this->Note->read(null, $id));
	}


	function add($member_id = null) {
		if (empty($this->data) && empty($member_id)) {
			$this->Session->setFlash(__('Oops, you have to select a member to create a note for', true));
			$this->redirect($this->referer());
		}
		if (!empty($this->data)) {
			$this->Note->create();
			$this->data['Note']['date_added'] = date("Y-m-d H:i:s");
			if ($this->Note->save($this->data)) {
				$this->Session->setFlash(__('The Note has been saved', true));
				$this->redirect(array('action'=>'index', $this->data['Note']['member_id']));
			} else {
				$this->Session->setFlash(__('The Note could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->set('member', $this->Note->Member->getMember($member_id));
		}
	}


	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Note', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->Note->save($this->data)) {
				$this->Session->setFlash(__('The Note has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Note could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Note->read(null, $id);
		}
	}


	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Note', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Note->del($id)) {
			$this->Session->setFlash(__('Note deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}
}
?>