<?php
class DealsController extends AppController {

	var $name = 'Deals';
	var $helpers = array('Html', 'Form');
	var $unlimited_usenet = array('N' => 'Not Unlimited',
								  'Y' => 'Unlimited Usenet',
								  'AD' => 'Adult Usenet',
								  'B' => 'Best Usenet',
								  'US' => 'Free Usenet Sucks');
	var $block_usenet = array('N' => 'Not Block Special',
							  'Y' => 'Block Usenet');


	function beforeFilter() {
		parent::beforeFilter();
		$this->Session->write('ActivePage','deals');
	}


	function active() {
		$this->Deal->recursive = 0;
		$this->paginate = array('limit' => 15,
								'order' => array('expire_date' => 'DESC'));
		$this->set('deals', $this->paginate(array('start_date <=' => date("Y-m-d H:i:s"), 'expire_date >' => date("Y-m-d H:i:s"))));
		$this->set('title_for_layout',"NGD Active Deal Listing");
		$this->Session->write('ActivePage','activeDeal');
	}


	function add($plan_id = null) {
		$this->Deal->Behaviors->attach('Containable');
		$this->Deal->contain();
		if (!empty($this->data)) {
			$deal = $this->Deal->createDeal($this->data);
			if (is_array($deal) && array_key_exists('error', $deal)) {
				$this->Session->setFlash(__($deal['error']['message'], true), 'error');
			}
			else {
				$redir = array('action'=>'set_deal_text', $deal);
				$this->Session->setFlash(__('Deal created successfully.', true), 'flash_success');
				$this->redirect($redir);
			}
		}
		$args = array('status' => 'current');
		if (!empty($plan_id)) { $args['id'] = $plan_id; }
		$this->loadModel("Plan");
		$this->set('plan', $this->Plan->getPlans($args,true));
		$this->set('unlimited', $this->unlimited_usenet);
		$this->set('block', $this->block_usenet);
		$this->Session->write('ActivePage','addDeal');
	}


	function duplicate($deal_id = null) {
		$this->Deal->Behaviors->attach('Containable');
		$this->Deal->contain();
		if (empty($this->data) && empty($deal_id)) {
			$this->Session->setFlash(__("Sorry, you can't duplicate air.", true), 'error');
			$this->redirect($this->referer());
		}
		if (!empty($this->data)) {
			$deal = $this->Deal->createDeal($this->data, false);
			if (is_array($deal) && array_key_exists('error', $deal)) {
				$this->Session->setFlash(__($deal['error']['message'], true), 'error');
			}
			else {
				$this->Session->setFlash(__('Deal created successfully.', true), 'flash_success');
				$this->redirect(array('action'=>'set_deal_text', $deal));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Deal->read(null, $deal_id);
			unset($this->data['Deal']['id']);
			unset($this->data['Deal']['alternate']);
		}
		$this->loadModel("Plan");
		$this->set('plan', $this->Plan->getPlans(array('status' => 'current'),true));
		$this->set('unlimited', $this->unlimited_usenet);
		$this->set('block', $this->block_usenet);
	}


	function edit($id = null) {
		if (empty($this->data) && empty($id)) {
			$this->Session->setFlash(__("Sorry, you can't edit nothing!", true), 'error');
			$this->redirect($this->referer());
		}
		$this->Deal->Behaviors->attach('Containable');
		$this->Deal->contain();
		if (!empty($this->data)) {
			if (!$this->Deal->save($this->data)) {
				$this->Session->setFlash(__($deal['error']['message'], true), 'error');
			}
			else {
				$this->Session->setFlash(__('Deal edited successfully.', true), 'flash_success');
				$this->redirect(array('action'=>'set_deal_text', $this->data['Deal']['id']));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Deal->read(null, $id);
		}
		$this->loadModel("Plan");
		$this->set('plan', $this->Plan->getPlans(array('status' => 'current'),true));
		$this->set('unlimited', $this->unlimited_usenet);
		$this->set('block', $this->block_usenet);
	}


	function index($filter = 'all') {
		$this->Deal->recursive = 0;
		$this->paginate = array('limit' => 15,
								'order' => array('expire_date' => 'DESC'));
		$this->set('deals', $this->paginate());
		$this->set('title_for_layout',"NGD Deal Listing");
		$this->Session->write('ActivePage','allDeal');
	}


	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid Deal.', true));
			$this->redirect(array('action'=>'index'));
		}
		$deal = $this->Deal->read(null, $id);
		$this->set('deal',$deal);
		$this->set('title_for_layout',"NGD View Deal");
		$this->Session->write('ActivePage','');
	}


	function set_deal_text($deal_id) {
		$this->Deal->Behaviors->attach('Containable');
		$this->Deal->contain();
		if (!empty($this->data)) {
			if (!$this->Deal->save($this->data)) {
				$this->Session->setFlash(__('Deal text could not be set at this time.', true), 'error');
			}
			else {
				$this->Session->setFlash(__('Deal text saved successfully.', true), 'flash_success');
				$this->redirect(array('action'=>'view', $this->Deal->id));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Deal->read(null, $deal_id);
		}
	}


	function performance() {
		$deals = $this->Deal->SoldDeal->find("all", array('conditions' => array('Deal.id' => 'SoldDeal.deal_id')))	;
		print_r($deals);
	}
}
?>