<?php
class LandingPagesController extends AppController {

	var $name = 'LandingPages';
	var $helpers = array('Html', 'Form');

	function beforeFilter() {
		parent::beforeFilter();
		$this->LandingPage->Behaviors->attach('Containable');
	}


	function active() {
		$this->LandingPage->recursive = 0;
		$this->paginate = array('limit' => 20,
								'order' => array('LandingPage.name' => 'asc', 'LandingPage.id' => 'asc'));
		$this->set('title_for_layout',"Landing Page A/B Testing");
		$this->set('lps', $this->paginate(array('LandingPage.expire_date >' => date("Y-m-d H:i:s"),'LandingPage.start_date <' => date("Y-m-d H:i:s"))));
	}


	function add() {
		if (!empty($this->data)) {
			$this->LandingPage->create();
			if ($this->LandingPage->save($this->data)) {
				$this->Session->setFlash(__('The LandingPage has been saved', true), 'flash_success');
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The LandingPage could not be saved. Please, try again.', true), 'error');
			}
		}
		$this->set('deals',$this->LandingPage->Deal->find('list'));
	}


	function duplicate($landing_id = null) {
		if (empty($this->data) && empty($landing_id)) {
			$this->Session->setFlash(__("Sorry, you can't duplicate air.", true), 'error');
			$this->redirect($this->referer());
		}
		if (!empty($this->data)) {
			$this->LandingPage->create();
			if ($this->LandingPage->save($this->data)) {
				$this->Session->setFlash(__('The LandingPage has been saved', true), 'flash_success');
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The LandingPage could not be saved. Please, try again.', true), 'error');
			}
		}
		if (empty($this->data)) {
			$this->data = $this->LandingPage->read(null, $deal_id);
			unset($this->data['LandingPage']['id']);
			unset($this->data['LandingPage']['landings']);
			unset($this->data['LandingPage']['conversions']);
			unset($this->data['LandingPage']['start_date']);
			unset($this->data['LandingPage']['expire_date']);
		}
		$this->set('deals',$this->LandingPage->Deal->find('list'));
	}


	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid LandingPage', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->LandingPage->save($this->data)) {
				$this->Session->setFlash(__('The LandingPage has been saved', true), 'flash_success');
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The LandingPage could not be saved. Please, try again.', true), 'error');
			}
		}
		if (empty($this->data)) {
			$this->data = $this->LandingPage->read(null, $id);
		}
		$this->set("deals", $this->LandingPage->Deal->find('list'));
	}


	function index() {
		$this->LandingPage->recursive = 0;
		$this->paginate = array('limit' => 20,
								'order' => array('LandingPage.name' => 'asc', 'LandingPage.id' => 'asc'));
		$this->set('title_for_layout',"Landing Page A/B Testing");
		$this->set('lps', $this->paginate());
	}


	function new_test($id) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__("You can't do that!", true), 'error');
			$this->redirect($this->referer());
		}
		$this->set("deals", $this->LandingPage->Deal->find('list'));
		if (!empty($this->data)) {	
			$id = $this->data['LandingPage']['parent'];
			$t1 = $this->data;
			$t1['LandingPage']['template'] = $this->data['LandingPage']['template_1'];
			$t1['LandingPage']['description'] = $this->data['LandingPage']['description_1'];
			$t2 = $this->data;
			$t2['LandingPage']['template'] = $this->data['LandingPage']['template_2'];
			$t2['LandingPage']['description'] = $this->data['LandingPage']['description_2'];
			$data = array(0 => $t1['LandingPage'], 1 => $t2['LandingPage']);
			if ($this->LandingPage->saveAll($data)) {
				$this->LandingPage->update($id, array('in_test' => 'Y'));
				$this->Session->setFlash(__('The LandingPage has been saved', true), 'flash_success');
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('Unable to create test.', true), 'error');
			}
		}
		if (empty($this->data)) {
			$this->data = $this->LandingPage->read(null, $id);
			unset($this->data['LandingPage']['id']);
			unset($this->data['LandingPage']['landings']);
			unset($this->data['LandingPage']['conversions']);
			unset($this->data['LandingPage']['default']);
			unset($this->data['LandingPage']['start_date']);
			unset($this->data['LandingPage']['expire_date']);	
			$this->data['LandingPage']['template_1'] = $this->data['LandingPage']['template'];
			$this->data['LandingPage']['description_1'] = $this->data['LandingPage']['description'];
		}
		$this->set('parent', $id);
	}


	function view($id) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid LandingPage.', true), 'error');
			$this->redirect($this->referer());
		}
		$lp = $this->LandingPage->read(null, $id);
		$this->set('lp', $lp);
		$this->set('title_for_layout',"Landing Page View");
	}
}
?>