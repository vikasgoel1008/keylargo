<?php
class NgdOptionsController extends AppController {

	var $name = 'NgdOptions';
	var $helpers = array('Html', 'Form');
	var $signup_layout = array(1=>'Subs & Blocks',2=>'Blocks & Subs',3=>'Subs Only',4=>'Blocks Only');

	function beforeFilter() {
		parent::beforeFilter();
		$this->set('layouts', $this->signup_layout);
	}


	function index() {
		$this->NgdOption->recursive = 0;
		$this->set('ngdOptions', $this->paginate());
		$this->Session->write('ActivePage','allOptions');
	}


	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid NgdOption.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('ngdOption', $this->NgdOption->read(null, $id));
	}


	function add() {
		if (!empty($this->data)) {
			$this->NgdOption->create();
			if ($this->NgdOption->save($this->data)) {
				$this->Session->setFlash(__('The NgdOption has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The NgdOption could not be saved. Please, try again.', true));
			}
		}
		$this->Session->write('ActivePage','addOption');
	}


	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid NgdOption', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->NgdOption->save($this->data)) {
				$this->Session->setFlash(__('The NgdOption has been saved', true));
				$this->redirect(array('action'=>'view', $id));
			} else {
				$this->Session->setFlash(__('The NgdOption could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->NgdOption->read(null, $id);
		}
	}


	function edit_plan_option($id = null) {
		$this->LoadModel("Plan");
		$this->Plan->recursive = 0;
		$plans = $this->Plan->find('list', array('conditions' => array('status'=>array('active','current'))));
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid NgdOption', true));
			$this->redirect($this->referer());
		}
		if (!empty($this->data)) {
			$this->data['NgdOption']['value'] = $this->data['NgdOption']['plan_id'];
			if ($this->NgdOption->save($this->data)) {
				$this->Session->setFlash(__('The option was saved successfully', true), 'flash_success');
				$this->redirect($this->Session->read('myOptionReferer'));
			} else {
				$this->Session->setFlash(__('The option could not be saved. Please, try again.', true), 'error');
			}
		}
		if (empty($this->data)) {
			$this->Session->write('myOptionReferer', $this->referer());
			$this->data = $this->NgdOption->read(null, $id);
		}
		$this->set('plans', $plans);
	}


	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for NgdOption', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->NgdOption->del($id)) {
			$this->Session->setFlash(__('NgdOption deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}


	function compare_conversion_rate($id=null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid NgdOption.', true));
			$this->redirect(array('action'=>'index'));
		}
		$option = $this->NgdOption->read(null, $id);
		if ($option['NgdOption']['key_name'] != 'conversion_rate') {
			$this->Session->setFlash(__('Cannot compare conversion rate changes with this option.', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			$this->set('newRate', $this->data['NgdOption']['newRate']);
			$this->loadModel('Plan');
			$this->Plan->recursive = 0;
			$this->set('plans', $this->Plan->find('all', array('conditions' => array('status' => 'current'))));
		}
		if (empty($this->data)) {
			$this->data = $this->NgdOption->read(null, $id);
		}
	}


	function plans_display($key_name, $type = 'monthly') {
		if (!$key_name) {
			$this->Session->setFlash(__('You must provide the display type key name', true), 'error');
			$this->redirect(array('action'=>'index'));
		}
		$this->paginate = array('order' => 'key_name, subkey asc');
		$cond['name'] = 'ngd';
		$cond['key_name'] = $key_name;
		if ($key_name == 'signup' && $type == 'monthly') {
			$cond['key_name'] .= '_monthly_plans';
		}
		elseif ($key_name == 'signup' && $type == 'blocks') {
			$cond['key_name'] .= '_block_plans';
		}
		$this->NgdOption->recursive = 0;
		$this->set('ngdOptions', $this->paginate($cond));
		$this->Session->write('ActivePage','planDisplay');
		$this->set('key_name',$key_name);
		$this->loadModel("Plan");
		$plans = $this->Plan->find("list");
		$this->set('plans', $plans);
		$this->set('title_for_layout',"Plan Display Order - $key_name");
	}
}
?>