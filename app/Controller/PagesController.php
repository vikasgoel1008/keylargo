<?php
App::uses('AppController', 'Controller');

class PagesController extends AppController {
	
	public $uses = array();

	public function display() {
		$this->layout = "new";
		$this->loadModel('Member');
		
		$last_signup_members = $this->Member->find('all',array('conditions'=>array('Member.status'=>'active'),'order'=>array('date_reg' => 'DESC'),'limit'=>20));
		$this->loadModel('CcTransaction');
		$today = date("Y-m-d");
		$revenues = $this->CcTransaction->find('first', array('fields' => array('SUM(amount) as charges', 'left(trans_date,10) as revenue_date'),
		                                     			   	'group' => array('left(trans_date,10)'),
															'conditions' => array('approved' => 'APPROVED',
																				  'left(trans_date,10)' => $today)));
		$signups = $this->CcTransaction->find('first', array('fields' => array('COUNT(CcTransaction.id) as signups', 'left(trans_date,10) as revenue_date'),
		                                     			   	'group' => array('left(trans_date,10)'),
															'conditions' => array('approved' => 'APPROVED',
																				'trans_purpose' => 'signup',
																				'left(trans_date,10)' => $today)));

		$adds = $this->CcTransaction->find('first', array('fields' => array('COUNT(CcTransaction.id) as adds', 'left(trans_date,10) as revenue_date'),
		                                     			   	'group' => array('left(trans_date,10)'),
															'conditions' => array('approved' => 'APPROVED',
																				'trans_purpose' => 'add block',
																				'left(trans_date,10)' => $today)));
		
		$day_wise = $this->CcTransaction->find('all', array('fields'	=> array('left(trans_date,10) sale_date',
																				'CcTransaction.trans_purpose',
																			  	 'SUM(amount) charges'),
		                                     			   	'order' 	=> array('trans_date ASC',),
														 	'group'	 	=> array('left(trans_date,10) ASC','trans_purpose'),
															'conditions' => array('approved' => 'APPROVED',
																				  'trans_type' => 'charge',
																				  'to_days(now())-to_days(trans_date) <' => 60)));
		$monthly_revenues = $this->CcTransaction->find('all', array('fields'	=> array("CONCAT(monthname(trans_date), ' ', CAST(year(trans_date) AS CHAR)) as sale_date",
																			  	 'CcTransaction.trans_purpose', 
																			  	 'SUM(amount) charges'),
		                                     			   	'order' 	=> array('year(trans_date) ASC', 'month(trans_date) ASC', 'trans_purpose'),
														 	'group'	 	=> array('year(trans_date) DESC', 'monthname(trans_date) DESC', 'trans_purpose'),
															'conditions' => array('approved' => 'APPROVED', 'left(trans_date,7) <>' => '2009-07'  )));
		
		$month_rcd_date = "";
		$month_new = '';
		foreach ($monthly_revenues as $month_sale) {
			if ($month_sale[0]['sale_date'] != $month_rcd_date) {
				if (!empty($month_new)) { $month_output[] = $month_new;}
				$month_new = array('date'=>$month_sale[0]['sale_date'], 'block'=>0.00, 'rebill'=>0.00, 'recycle'=>0.00, 'freetrial'=>0.00, 'signup'=>0.00,'freeupgrade'=>0.00);
				$month_rcd_date = $month_sale[0]['sale_date'];
			}
			switch ($month_sale['CcTransaction']['trans_purpose']) {
				case "add block":
					$month_new['block'] = $month_sale[0]['charges'];
					break;
				case "free trial expire":
					$month_new['freetrial'] = $month_sale[0]['charges'];
					break;
				case "free upgrade":
					$month_new['freeupgrade'] = $month_sale[0]['charges'];
					break;

				case "rebill":
					$month_new['rebill'] = $month_sale[0]['charges'];
					break;
				case "recycle":
					$month_new['recycle'] = $month_sale[0]['charges'];
					break;
				case "signup":
					$month_new['signup'] = $month_sale[0]['charges'];
					break;
			}
		}
		if (!empty($month_new)) { $month_output[] = $month_new;
		//print_r($month_output);die;
		$this->set('month_revenues',$month_output);}
		
		$rcd_date = "";
		$output[] = '';
		$new = '';
		foreach ($day_wise as $sale) {
			if ($sale[0]['sale_date'] != $rcd_date) {
				if (!empty($new)) { $output[] = $new;}
				$new = array('date'=>$sale[0]['sale_date'], 'block'=>0.00, 'rebill'=>0.00, 'recycle'=>0.00, 'freetrial'=>0.00, 'signup'=>0.00,'freeupgrade'=>0.00);
				$rcd_date = $sale[0]['sale_date'];
			}
			switch ($sale['CcTransaction']['trans_purpose']) {
				case "add block":
					$new['block'] = $sale[0]['charges'];
					break;
				case "free trial expire":
					$new['freetrial'] = $sale[0]['charges'];
					break;
				case "free upgrade":
					$new['freeupgrade'] = $sale[0]['charges'];
					break;
				case "rebill":
					$new['rebill'] = $sale[0]['charges'];
					break;
				case "recycle":
					$new['recycle'] = $sale[0]['charges'];
					break;
				case "signup":
					$new['signup'] = $sale[0]['charges'];
					break;
			}
		}
		if (!empty($new)) { $output[] = $new;}
		unset($output['0']);
		$this->set('day_wise_revenue',$output);
		
		$this->loadModel('HwDailyUsageTotal');
		$this->loadModel('HwUsageDetail');
		$bandwidth = $this->HwUsageDetail->find('all',array('conditions'=>array('to_days(now())-to_days(usage_date) <' => 60),'fields'	=> array('left(usage_date,10) usage_date','SUM(x_total_bytes) charges'),'group'=> array('left(usage_date,10) ASC')));
		$weekly_bandwidth = $this->HwUsageDetail->find('all',array(
																	'conditions'=>array('to_days(now())-to_days(usage_date) <' => 60),
																	'fields'	=> array("week(usage_date) as usage_date",'SUM(x_total_bytes) charges'),
																	'group'=> array('week(usage_date) ASC')));
		//echo "<pre>";
		//print_r($weekly_bandwidth);die;
		//$bandwidth = $this->HwDailyUsageTotal->find('all',array('conditions'=>array('to_days(now())-to_days(usage_date) <' => 60),'fields'	=> array('left(usage_date,10) as usage_date','SUM(x_total_bytes)as charges'),'group'=> array('left(usage_date,10) ASC')));
		$this->set(compact('last_signup_members'));
		$this->set(compact('revenues'));
		$this->set(compact('day_wise_total_revenue'));
		$this->set(compact('signups'));
		$this->set(compact('adds'));
		$this->set(compact('bandwidth'));
		$this->set(compact('weekly_bandwidth'));
		$path = func_get_args();
		$count = count($path);
		if (!$count) {
			return $this->redirect('/');
		}
		$page = $subpage = $title_for_layout = null;
		if (!empty($path[0])) {
			$page = $path[0];
		}
		if (!empty($path[1])) {
			$subpage = $path[1];
		}
		if (!empty($path[$count - 1])) {
			$title_for_layout = Inflector::humanize($path[$count - 1]);
		}
		$this->set(compact('page', 'subpage', 'title_for_layout'));
		$this->render(join('/', $path));
	}
}
