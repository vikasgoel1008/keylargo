<?php
class ExtAclComponent extends Object {

	var $components = array('ExtAcl');

	function initaro() {
		$aro = new Aro();
		//$aco = new Aco();

		$groups = array(
			0 => array('alias'=>'role_master','parent_id'=>4)
			//1 => array('alias'=>'role_profile'),
			//2 => array('alias'=>'role_manager'),
			//3 => array('alias'=>'role_admin'),
			//4 => array('alias'=>'role_master')
		);

		foreach ($groups as $data) {
			$aro->create();
			$aro->save($data);
		}
	}
	function addAroType($aname){
		$aro = new Aro();
		$data = array(
			'alias' => $aname
		);
		$aro->create();
		$aro->save($data);
	}
	function initaco() {
		$aco = new Aco();
		//$aco = new Aco();

		$pages = array(
			0 => array('alias' => 'adminedit',
						'parent_id' => 2)
			
		);

		foreach ($pages as $data) {
			$aco->create();
			$aco->save($data);
		}
	}
	function addAcoType($aname,$acomodel){
		$aco = new Aco();
		if ($aco->findByAlias($aname)) return;
		$data = array(
			'alias' => $aname,
			'model' => $acomodel
		);
		$aco->create();
		$aco->save($data);
	}
	function delAco($aname) {
		$aco = new Aco();
		$deleteAco = $aco->findByAlias($aname);
		$deleteId = $deleteAco['Aco']['id'];
		$aco->delete($deleteId);
	}
	function addUser($aname,$uid,$parent=null){
		$aro = new Aro();
		$parentAro = $aro->findByAlias($parent);
		$parentId = $parentAro['Aro']['id'];
		$data = array(
			'alias' => $aname,
			'model' => 'User',
			'foreign_key' => $uid,
			'parent_id' => $parentId
		);
		$aro->create();
		$aro->save($data);
	}
	function addSiteUser($aname,$uid,$pname){
		$aro = new Aro();
		$profileAro = $aro->findByAlias($pname);
		$profileId = $profileAro['Aro']['id'];
		$data = array(
			'alias' => $aname,
			'model' => 'User',
			'foreign_key' => $uid,
			'parent_id' => $profileId
		);
		$aro->create();
		$aro->save($data);
	}
	function hasSiteAccess($aname,$pname) {
		$aro = new Aro();
		$profileAro = $aro->findByAlias($pname);
		$profileId = $profileAro['Aro']['id'];
		$childAro = $aro->findByAlias($aname);
		$parentId = $childAro['Aro']['parent_id'];
		if (($profileId == $parentId) || $this->checkAdminStatus($aname)) return true;
		else return false;
	}
	function isSiteUser($aname,$pname) {
		$aro = new Aro();
		$profileAro = $aro->findByAlias($pname);
		$profileId = $profileAro['Aro']['id'];
		$childAro = $aro->findByAlias($aname);
		$parentId = $childAro['Aro']['parent_id'];
		if (($profileId == $parentId)) return true;
		else return false;
	}
	function delAro($aname) {
		$aro = new Aro();
		$deleteAro = $aro->findByAlias($aname);
		$deleteId = $deleteAro['Aro']['id'];
		$aro->delete($deleteId);
	}

	function updateAroAlias($id,$aname) {
		$aro = new Aro();
		$updateAro = $aro->findByForeignKey($id);
		$updateAro['Aro']['alias'] = $aname;
		$aro->save($updateAro);
	}
	function updateAroParent($aname,$parent) {
		$aro = new Aro();
		$parentAro = $aro->findByAlias($parent);
		$updateAro = $aro->findByAlias($aname);
		$updateAro['Aro']['parent_id'] = $parentAro['Aro']['id'];
		$aro->save($updateAro);
	}

	function delAdminStatus($aname) {
		$aro = new Aro();
		$childAro = $aro->findByAlias($aname);
		$childId = $childAro['Aro']['id'];
		$data = array('id'=>$childId,'parent_id'=>null);
		$aro->save($data);
	}

	function addAdminStatus($aname) {
		$aro = new Aro();
		$adminAro = $aro->findByAlias('admin');
		$adminId = $adminAro['Aro']['id'];
		$childAro = $aro->findByAlias($aname);
		$childId = $childAro['Aro']['id'];
		$data = array('id'=>$childId,'parent_id'=>$adminId);
		$aro->save($data);
	}

	function checkAdminStatus($aname) {
		$aro = new Aro();
		$adminAro = $aro->findByAlias('admin');
		$adminId = $adminAro['Aro']['id'];
		$childAro = $aro->findByAlias($aname);
		$parentId = $childAro['Aro']['parent_id'];
		if ($adminId == $parentId) return true;
		else return false;
	}

	function allowACL($aroIn,$acoIn) {
		$this->Acl->allow($aroIn,$acoIn);
	}
}
?>