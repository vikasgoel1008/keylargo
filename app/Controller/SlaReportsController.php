<?php
class SlaReportsController extends AppController {

	var $name = 'SlaReports';
	var $helpers = array('Html', 'Form');

	
	function beforeFilter() {
		parent::beforeFilter();
	}


	function add() {
		if (!empty($this->data)) {
			$this->SlaReport->create();
			$check = $this->SlaReport->find('first', array('conditions' => array('data_date' => $this->data['SlaReport']['data_date'])));
			if (!empty($check)) {
				$this->Session->setFlash(__('A report for this date already exists.', true), 'error');
			}
			elseif ($this->data['SlaReport']['data_date'] == date("Y-m-d")) {
				$this->Session->setFlash(__('You cannot enter data for the current date', true), 'flash_success');
			}
			elseif ($this->SlaReport->save($this->data)) {
				$this->Session->setFlash(__('The SlaReport has been saved', true), 'flash_success');
			} else {
				$this->Session->setFlash(__('The SlaReport could not be saved. Please, try again.', true), 'error');
			}
		}
		$this->Session->write('ActivePage','addSlaReport');
	}


	function report_daily_sla_stats($days = 30) {
		$this->set('title_for_layout',"$days Day SLA Report");
		$datas = $this->SlaReport->find('all', array('fields'	=> array('data_date','goal', '(success / (failures+success)) sla_rate'),
		                                     			   			'order' 	=> array('data_date'),
														 			'conditions' => array('to_days(now())-to_days(data_date) <=' => $days +1)));
		$dateSeries = "";
		$goalSeries = '';
		$slaSeries = '';
		$count =0 ;
		foreach ($datas as $data) {
			$goal = $data['SlaReport']['goal']*100;
			$slaRate = $data[0]['sla_rate']*100;
			$dateSeries .= '<value xid=\''.$count.'\'>'.$data['SlaReport']['data_date'].'</value>';
			$goalSeries .= '<value xid=\''.$count.'\'>'.$goal.'</value>';
			$slaSeries .= '<value xid=\''.$count.'\'>'.$slaRate.'</value>';
			$count++;
		}
		$this->set('seriesXML',$dateSeries);
		$this->set('valuesXML',"<graph gid='1'>$goalSeries</graph><graph gid='2'>$slaSeries</graph>");
		$this->set('days',$days);
		parent::session_clean_member_id();
		$this->Session->write('ActivePage','slaDailyStats');
	}


	function report_weekly_sla_stats($weeks = 15) {
		$this->set('title_for_layout',"$weeks Week SLA Report");
		$dateStats =getdate();
		$recentSunday = date('Y-m-d', mktime(0,0,0,date('m'),date('d')-$dateStats['wday'],date('Y')));
		$oldestDate = date('Y-m-d', mktime(0,0,0,date('m'),date('d')-$dateStats['wday']-(7 * $weeks)+1,date('Y')));
		$datas = $this->SlaReport->find('all', array('fields'	=> array('data_date','goal', 'failures','success'),
		                                     			   			'order' 	=> array('data_date'),
														 			'conditions' => array('data_date >=' => $oldestDate, 'data_date <=' => $recentSunday)));
		$count = 0;
		$arCount = 0;
		$failures = 0;
		$success = 0;
		$dateSeries = '';
		$goalSeries = '';
		$slaSeries = '';
		foreach ($datas as $data) {
			$failures += $data['SlaReport']['failures'];
			$success  += $data['SlaReport']['success'];
			if ($count <> 6) {
				$count++;
			}
			else {
				$totalTickets = $failures + $success;
				$goal = $data['SlaReport']['goal']*100;
				$slaRate = number_format(($success / $totalTickets) *100,1);
				$dateSeries .= '<value xid=\''.$arCount.'\'>'.$data['SlaReport']['data_date'].'</value>';
				$goalSeries .= '<value xid=\''.$arCount.'\'>'.$goal.'</value>';
				$slaSeries .= '<value xid=\''.$arCount.'\'>'.$slaRate.'</value>';
				$count = 0;
				$failures = 0;
				$success = 0;
				$arCount++;
			}
		}
		$this->set('seriesXML',$dateSeries);
		$this->set('valuesXML',"<graph gid='1'>$goalSeries</graph><graph gid='2'>$slaSeries</graph>");
		$this->set('weeks', $weeks);
		parent::session_clean_member_id();
		$this->Session->write('ActivePage','slaWeeklyStats');
	}
}
?>