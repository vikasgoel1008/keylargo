<?php
class Cancel extends AppModel {

	var $name = 'Cancel';

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $belongsTo = array(
		'Member' => array(
			'className' => 'Member',
			'foreignKey' => 'member_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Plan' => array(
			'className' => 'Plan',
			'foreignKey' => 'plan_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	function logCancel($id, $cancel_date, $expire_date, $reason, $plan_id) {
		
		
		$this->create();
		$myCancel['Cancel']['member_id'] = $id;
		$myCancel['Cancel']['cancel_date'] = $cancel_date;
		$myCancel['Cancel']['effective_date'] = $expire_date;
		$myCancel['Cancel']['reason'] = $reason;		
		$myCancel['Cancel']['plan_id'] = $plan_id;

		if ($this->save($myCancel)) {return true;}
		else { return false;}
		
	}
	
}
?>