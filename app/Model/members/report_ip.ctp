<div class="ccTransactions index">

	<h2>Accounts per IP</h2>
	
	<?php echo $this->element('members_menu');?>
	<?php $paginator->options(array('url' => $this->passedArgs));?>
	
	<div class="box">
		<p>&nbsp;</p>
	
		<table cellpadding="0" cellspacing="0">
			<tr class="highlight">
				<td colspan="4"><?php echo $html->link(__('Blacklist', true), array('action' => 'blacklist_ip', $session->read('Member.ip'), 'free account fraud')); ?></td>				
			</tr>
			<tr class="highlight">
				<td><?php echo $paginator->sort('date_reg');?></td>
				<td><?php echo $paginator->sort('email');?></td>
				<td><?php echo $paginator->sort('ip');?></td>
				<td><?php echo $html->link(__('Close Free Accounts', true), array('action' => 'close_free_by_ip', $session->read('Member.ip'))); ?></td>
			</tr>
		
			<?php foreach ($members as $member): ?>
			<tr  class="highlight">
				<td><?php echo $member['Member']['date_reg']; ?></td>
				<td><?php echo $html->link($member['Member']['email'], 'view/'.$member['Member']['id']); ?></td>
				<td><?php echo $member['Member']['ip']; ?></td>
				<td>
					<?php 
						if (($member['Member']['status'] == 'closed')) {
							echo "Account Closed";	
						}
						elseif ($member['Member']['date_expire'] != '0000-00-00 00:00:00') {
							echo "Account Closed";	
						}
						else {
							echo $html->link('Cancel', 'cancel/'.$member['Member']['id']."/".date("Y-m-d"), null, sprintf(__('Are you sure you want to cancel the account %s?', true), $member['Member']['email']));					
						} 
					?>
				</td>
			</tr>
			<?php endforeach; ?>
			<tr class="highlight">
				<td colspan="4"><?php  echo $paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)));?></td>
			</tr>
			
		</table>
		
		<p id="pagin">
			<?php echo $paginator->first();?>
		 	<?php echo $paginator->prev('< previous', "", "", "");?>
		 	<?php //echo $paginator->prev() ?> 
			<?php echo $paginator->numbers();?>  
		 	<?php //echo $paginator->next() ?>
			<?php //echo $paginator->counter(); ?>
			<?php echo $paginator->next('next >', "", "", "");?>
			<?php echo $paginator->last();?>
		 	
		<p/>
		
		<span class="clear"></span>
	
	</div>
</div>