<h2>Close Customer Account</h2>

<?php echo $this->element('members_menu');?>

<?=$form->create('Member', array('action' => 'close'));?>
	
<fieldset>
	<br/>
	<h3>The account is scheduled to be billed on <?php echo $this->data['Member']['next_bill_date']; ?>.</h3>
	
	<?php echo $form->hidden('id'); ?> 
	
	<?php if ($this->data['Member']['next_bill_date'] == '0000-00-00 00:00:00' || $this->data['Member']['pay_type'] == 'WP') {$myCloseDate = date('Y-m-d');} else {$myCloseDate = $this->data['Member']['next_bill_date'];} ?>
	
	<?=$form->input('my_expire_date',array('type'=>'text',
										'class'=>'small',
										'label'=>'Expire Date', 
										'value'=>$myCloseDate));?><br class="hid" /><p>Format: YYYY-MM-DD</p>
	<?=$form->input('reason',array('type'=>'select',
										'class'=>'small',
										'label'=>'Reason for Canceling',
										'options' => $reasons,
										'empty' => true));?><br class="hid" />
										
	<p></p>
	<?=$form->end(array('label'=>'Close Account','class'=>'button submit'));?>
	<span class="clear"></span>
</fieldset>