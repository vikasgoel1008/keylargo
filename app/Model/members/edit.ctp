<h2>Customer Info</h2>

<?php echo $this->element('members_menu');?>

<?=$form->create('Member');?>
<fieldset>
	<?php echo $form->hidden('id'); ?> 

	<?=$form->input('name',array('type'=>'text','class'=>'small','label'=>'Name'));?><br class="hid" />
	<?=$form->input('email',array('type'=>'text','class'=>'small','label'=>'Email Address'));?><br class="hid" />
	<?=$form->input('login_password',array('type'=>'text','class'=>'tiny','label'=>'Password'));?><br class="hid" />
	<?=$form->input('status',array('type'=>'text','class'=>'tiny','label'=>'Status'));?><br class="hid" />
	<?=$form->input('hw_cust_id',array('type'=>'text','class'=>'tiny','label'=>'HW Customer ID'));?><br class="hid" />
	<?=$form->input('date_cancel',array('type'=>'text','class'=>'tiny','label'=>'Cancel Date'));?><br class="hid" />
	<?=$form->input('date_expire',array('type'=>'text','class'=>'tiny','label'=>'Expiration Date'));?><br class="hid" />
	<p></p>
	<?=$form->end(array('label'=>'Update','class'=>'button submit'));?>
	<span class="clear"></span>
</fieldset>