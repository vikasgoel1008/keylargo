<h2>Create StorageNinja Acct</h2>

<?php echo $this->element('members_menu');?>

<?=$form->create('Member', array('action' => 'create_storageninja_account'));?>
<fieldset>
	
	<?=$form->input('id',array('type'=>'hidden','class'=>'small','label'=>'Email Address'));?><br class="hid" />
	<?=$form->input('name',array('type'=>'text','class'=>'small','label'=>'Name', 'readonly' => 'readonly'));?><br class="hid" />
	<?=$form->input('login_username',array('type'=>'text','class'=>'small','label'=>'User', 'readonly' => 'readonly'));?><br class="hid" />
	<?=$form->input('sn_pwd',array('type'=>'text','class'=>'small','label'=>'StorageNinja Password'));?><br class="hid" />
	
	<p></p>
	<?=$form->end(array('label'=>'Create StorageNinja Account','class'=>'button submit'));?>
	<span class="clear"></span>
</fieldset>