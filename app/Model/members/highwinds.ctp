<div class="Members highwinds">

<h2>Highwinds</h2>

<?php echo $this->element('members_menu');?>

<div class="box">
	<p>&nbsp;</p>

	<table cellpadding="0" cellspacing="0">
	
		<tr class="highlight">
			<td colspan="2">
				<?php echo $html->link('Daily Usage', 'usage/days/12/'. $member['Member']['id']);?> | 
				<?php echo $html->link('Monthly Usage', 'usage/months/12/'. $member['Member']['id']);?> | 
				<?php echo $html->link('Custom Usage', 'hw_date_range/'. $member['Member']['id']);?>  
			</td>
		</tr>
		
		<tr class="highlight"><td colspan="2"><?php echo $member['Member']['name']; ?> | <em><?php echo $member['Member']['email']; ?></em> | <?php echo $member['Member']['login_username']; ?></td></tr>
		
		<?php if ($member['Member']['is_free_trial'] == "Y"): ?>
		<tr class="highlight"><td colspan="2"><span class="notgood">This customer is currently within their free trial period.</span></td></tr>
		<?php endif; ?>
		
		<tr><td colspan="3"</td></tr>
			
		
		
		<?php if (empty($usage['cust_group'])) : ?>
		<tr class="highlight">
			<td colspan="2">
				<?php echo $html->link('Create in HW', 'create_hw_account/'. $member['Member']['id'], array('class' => 'notgood')); ?>
			</td>
		</tr>
		<?php endif; ?>
		
		<?php if ($usage['status'] == 'Cap Reached'): ?>
		<tr class="highlight"><td colspan="2"><?php echo $html->link("Recycle Account",array('action' => 'hw_recycle', $member['Member']['id'])) ?></td></tr>		
		<?php endif; ?>
		
		<tr class="highlight">
			<td>Highwinds Status:</td>
			<td>
				<?php echo $usage['status']; ?>
				
				<?php if ($usage['status'] == 'Cap Reached'): ?>
				 | <?php echo $html->link("Recycle",array('action' => 'hw_recycle', $member['Member']['id'])) ?>
				
				<?php elseif ($usage['status'] == 'Suspended'): ?>
				 | <?php echo $html->link("Activate",array('action' => 'hw_activate', $member['Member']['id'])) ?>
				
				<?php elseif ($usage['status'] == 'Active'): ?>
				 | <?php echo $html->link("Suspend",array('action' => 'hw_suspend_account', $member['Member']['id'])) ?>
				 | <?php echo $html->link('Close', array('action' =>'hw_close_account',$member['Member']['id']), null, sprintf(__('Are you sure you want to close this account %s?', true), $member['Member']['login_username'])); ?>
				
				<?php endif; ?>
			</td>
		</tr>
		<tr class="highlight">
			<td>Highwinds Group:</td>
			<td>
				<?php echo $usage['cust_group']; ?>
				
				
				<?php if ($usage['cust_group'] == 'Unlimited' ) : ?> | 
				<?php echo $html->link('Unlimited Heavy', array('action' =>'hw_unlimited_heavy',$member['Member']['id']), null, sprintf(__('Are you sure you want to throttle this account %s?', true), $member['Member']['login_username'])); ?>
				<?php endif; ?>
			</td>
		</tr>
		<tr class="highlight"><td>Open Date:</td><td><?php echo str_replace("\"", "", $usage['open_date']); ?></td></tr>
		<tr class="highlight"><td>Cycle Date:</td><td><?php echo str_replace("\"", "", $usage['cycle_date']); ?></td></tr>
		<tr class="highlight"><td>Close Date:</td><td><?php echo str_replace("\"", "", $usage['close_date']); ?></td></tr>
		<tr><td colspan="2"></td></tr>
		
		<tr class="highlight"><td>Remaining Usage:</td><td><?php echo $usage['remaining']; ?> GB</td></tr>
		<tr class="highlight"><td>Allowed Usage:</td><td><?php echo $usage['total']; ?> GB</td></tr>
		<tr class="highlight"><td>Usage this Cycle:</td><td><?php echo $usage['used']; ?> GB</td></tr>
		
	</table>
	
	<p id="pagin">
		&nbsp;
	<p/>

	<span class="clear"></span>
</div>
</div>	