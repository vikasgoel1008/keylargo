<div class="ccTransactions index">
	<h2>Bank Reconciles Report</h2>
	
	<div class="box">
		<p>&nbsp;</p>
		<table cellpadding="0" cellspacing="0">
			
			<!--
			<tr class="highlight">	
				<td colspan="6"><?php  echo $html->link('Add Transaction', array('action' => 'add'));?></td>
			</tr>
			-->
			
			<tr><td colspan="3">Transactions From:</td></tr>
			<tr><td colspan="3"><?php echo $dates['start']; ?> -  <?php echo $dates['end']; ?></td></tr>
			<tr>
				<td colspan="3">
					<?php echo $html->link("Prev", array('action' => 'bank_reconciliation', $dates['prev'])); ?>
					 | 
					<?php echo $dates['report']; ?>
					 | 
					<?php if (!empty($dates['next'])) :?>
					<?php echo $html->link("Next", array('action' => 'bank_reconciliation', $dates['next'])); ?>
					<?php else : ?>
					Next 
					<?php endif; ?>
				</td>
			</tr>
			<tr><td colspan="3">&nbsp;</td></tr>
			<tr><td>&nbsp;</td><td>Dollar Amount</td><td>Transactions</td></tr>
		
			<tr class="highlight">	
				<td>Revenues</td><td><?php echo $revenues['dollars']; ?></td><td><?php echo $revenues['trans']; ?></td>
			</tr>
			
			<tr class="highlight">	
				<td>Refunds</td><td><?php echo $refunds['dollars']; ?></td><td><?php echo $refunds['trans']; ?></td>
			</tr>
			
		</table>

		<p id="pagin">
			&nbsp;
		<p/>

		<span class="clear"></span>
	</div>
</div>

