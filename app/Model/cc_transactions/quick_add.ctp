<h2>Add Customer Transaction</h2>

<?php echo $this->element('members_menu');?>

<?=$form->create('CcTransaction');?>
<fieldset>
	
	<?=$form->input('member_id',array('type'=>'text','class'=>'small','label'=>'Member Id', 'value' => $session->read('Member.id')));?><br class="hid" />
	<?=$form->input('order_nbr',array('type'=>'text','class'=>'tiny','label'=>'Order'));?><br class="hid" />
	<?=$form->input('amount',array('type'=>'text','class'=>'small','label'=>'Amount'));?><br class="hid" />
	<?=$form->input('trans_date',array('type'=>'text','class'=>'tiny','label'=>'Date'));?><br class="hid" />
	<?=$form->input('trans_type',array('type'=>'text','class'=>'tiny','label'=>'Type'));?><br class="hid" />
	<?=$form->input('trans_purpose',array('type'=>'text','class'=>'tiny','label'=>'Purpose'));?><br class="hid" />
	
	<p></p>
	<?=$form->end(array('label'=>'Create Account','class'=>'button submit'));?>
	<span class="clear"></span>
</fieldset>