<?php
/* SVN FILE: $Id: app_model.php 7945 2008-12-19 02:16:01Z gwoo $ */
/**
 * Application model for Cake.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) :  Rapid Development Framework (http://www.cakephp.org)
 * Copyright 2005-2008, Cake Software Foundation, Inc. (http://www.cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource
 * @copyright     Copyright 2005-2008, Cake Software Foundation, Inc. (http://www.cakefoundation.org)
 * @link          http://www.cakefoundation.org/projects/info/cakephp CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.cake.libs.model
 * @since         CakePHP(tm) v 0.2.9
 * @version       $Revision: 7945 $
 * @modifiedby    $LastChangedBy: gwoo $
 * @lastmodified  $Date: 2008-12-18 18:16:01 -0800 (Thu, 18 Dec 2008) $
 * @license       http://www.opensource.org/licenses/mit-license.php The MIT License
 */
/**
 * Application model for Cake.
 *
 * This is a placeholder class.
 * Create the same file in app/app_model.php
 * Add your application-wide methods to the class, your models will inherit them.
 *
 * @package       cake
 * @subpackage    cake.cake.libs.model
 */
class AppModel extends Model {

	function unbindModelAll() { 
	    $unbind = array(); 
	    foreach ($this->belongsTo as $model=>$info) 
	    { 
	      $unbind['belongsTo'][] = $model; 
	    } 
	    foreach ($this->hasOne as $model=>$info) 
	    { 
	      $unbind['hasOne'][] = $model; 
	    } 
	    foreach ($this->hasMany as $model=>$info) 
	    { 
	      $unbind['hasMany'][] = $model; 
	    } 
	    foreach ($this->hasAndBelongsToMany as $model=>$info) 
	    { 
	      $unbind['hasAndBelongsToMany'][] = $model; 
	    } 
	    parent::unbindModel($unbind); 
	} 
	
	function get($args, $fields = null) {
		return $this->find('first', array('conditions' => $args));
	}
	
	function getAll($args) {
		return $this->find('all', array('conditions' => $args));
	}
		
	function update($id, $args) {
		
		if (!is_array($args)) {return false;}
		
		// process args to get field list
		$fields = array_keys($args);
		$args['id'] = $id;
		
		// save the data
		if ($this->save($args, false, $fields)) { return true;}
		else {return false;}
	}


}
?>