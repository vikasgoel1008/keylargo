<?php
class Contest extends AppModel {

	var $name = 'Contest';

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $hasMany = array(
		'Contestant' => array(
			'className' => 'Contestant',
			'foreignKey' => 'contest_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
?>