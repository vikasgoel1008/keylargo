<?php
class HwWeatherman extends AppModel {

	var $name = 'HwWeatherman';
	//public $useDbConfig = 'highwinds';
	
	
	private $endpoint = "https://wm.highwinds-media.com/wm/api/http_post.php";
	private $username;
	private $password;
	private $agent;
	
	var $success;
	var $errorMessage;
	
	
	function __construct() {
		
		$this->username=urlencode("jimlastinger@gmail.com");
		$this->password=md5('90x3u47a6');
		$this->agent="NewsgroupDirect Highwinds Integration";
	}
	
	function errorMessage()  {
		return $this->errorMessage;
	}
	
	function addBlock($username, $gigabytes) {
	
		// convert gigabytes to bytes
		$amount = $gigabytes * 1000000000;
		
		$params = array("x_cust_user_id" => $username,
                      "x_byte_increase" => $amount,
                      "x_block_type" => "2"); 
		
		$response = $this->makeCall("add_bytes", $params, 120);
		
		$result['error'] = $response[1];
		$result['new_balance'] = $response[2];
		
		return $result;
	}
	
	function isSuccessful() {
		return $this->success;
	}
	
	function close($username, $expire_date) {
		
		$params = array("x_cust_user_id" => $username,     
                      "x_close_date" => $expire_date);
		
		$this->makeCall("update_customer", $params, 120);
		
		
		return $this->success;
	}
	
	/**
	 * 
	 * @see interfaceUsenetServer::getUser()
	 */
	function getUser($username) {
		
		$params = array("x_cust_user_id" => $username); 
		
		
		$response = $this->makeCall("get_customer", $params, 120);
		
		$result['error'] = $response[1];
		$result['hw_cust_id'] = $response[2];
		$result['acct_status_id'] = $this->translateAccountStatusId($response[3]);
		$result['acct_group_id'] = $response[4];
		$result['x_auth_type_id'] = $response[5]; //(1=IP Address;2=User/Pass;3=IP Block;4=RADIUS;5=Default/None)
		$result['x_svc_class_id'] = $response[6];
		$result['x_email_address'] = $response[7];
		$result['x_company_name'] = $response[8];
		$result['x_first_name'] = $response[9];
		$result['x_last_name'] = $response[10];
		$result['x_address1'] = $response[11];
		$result['x_address2'] = $response[12];
		$result['x_city'] = $response[13];
		$result['x_region'] = $response[14];
		$result['x_postal_id'] = $response[15];
		$result['x_country_id'] = $response[16];
		$result['x_phone_number'] = $response[17];
		$result['x_phone_ext'] = $response[18];
		$result['x_fax_number'] = $response[19];
		$result['x_close_date'] = $response[20];
		$result['x_clear_password'] = $response[21];
		$result['x_open_date'] = $response[22];
		$result['x_bytes_used'] = $response[23];
		$result['x_bytes_allowed'] = $response[24];
    	
    	return $result;

	}

	
	/**
	 * 
	 * @see interfaceUsenetServer::makeCall()
	 */
	function makeCall($command, $params, $timeout=120) {
		
		$x_command=urlencode($command);  
		$x_post_vars="x_username={$this->username}&x_password={$this->password}&x_command={$x_command}&";
		
		// Convert array of parameters to string
		$post_vars = "";
		if( is_array($params) )
		{
			foreach( $params as $k => $v )
			{
				if( trim($post_vars) != "" )
					$post_vars .= "&{$k}=$v";
				else
					$post_vars = "{$k}=$v";
			}
			
			$post_vars = $x_post_vars.$post_vars;
		}	
		else
			$post_vars = $x_post_vars.$post_vars; 
			
		
		// configure the post request
		$hw_config = array('timeout' => $timeout,
						   'User-Agent' => $this->agent,
						   'Referer' => $_SERVER["PHP_SELF"]);
			
			
		App::import('Core', 'HttpSocket');
		
		
		$HttpSocket = new HttpSocket($hw_config);
		$results = $HttpSocket->post($this->endpoint, $post_vars); 
		
		$x_result = explode(',', $results);
		//$response = new SimpleXMLElement($results);
		
		if ($x_result[0] == 1) { $this->success = true; }
		else { 
			$this->success = false; 
			$this->errorMessage = str_replace("'","",$x_result[1]);
		}

		
		return $x_result;
		
		/*
		//Initialize CURL session
		$ch = curl_init("https://wm.highwinds-media.com/wm/api/http_post.php");
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);        	// Don't verify peer's certificate
		curl_setopt($ch, CURLOPT_NOPROGRESS, 1);            	// Disable progress indicator
		curl_setopt($ch, CURLOPT_VERBOSE, 1);               	// Output verbose information
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION,0);         	// Don't follow any 'Location:' calls
		curl_setopt($ch, CURLOPT_POST, 1);                  	// Do a regular HTTP POST
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_vars);		// Defining the POST variables as set in 'post_vars'
		curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);        	// Timeout set to 2 minutes (120 seconds)
		curl_setopt($ch, CURLOPT_USERAGENT, $this->agent);  	// Define calling agent
		curl_setopt($ch, CURLOPT_REFERER, $_SERVER["PHP_SELF"]);	// Define the referer in the header sent
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);        	// Return the transfer in a string after execution
		//echo $post_vars;
		$result = curl_exec($ch);
		return $this->readResults($result);
		*/
	}
	
	function success() {
		return $this->success;
	}
	
	function translateAccountStatusId($status_id) {
		
		$hwStatus = '';
		
		switch ($status_id) {
		
			case 1:
				$hwStatus = 'Active';		
				break;
				
			case 2:
				$hwStatus = 'Suspended';		
				break;
				
			case 3:
				$hwStatus = 'Closed';		
				break;
				
			case 4:
				$hwStatus = 'Pending';		
				break;
				
			case 5:
				$hwStatus = 'Cap Reached';		
				break;
				
			default:
				$hwStatus = $hw_results[3];
		
		}
		
		return $hwStatus;
		
	}
	
	function suspendAccount($username) {
		$params = array("x_cust_user_id" => $username, 
                      "x_acct_status_id" => 2);
		
		$response = $this->makeCall("update_customer", $params, 120);
		
		return $this->success;

	}
	
	function updateCustomerGroupId($username, $hw_customer_group_id) {
		
		$params = array("x_cust_user_id" => $username,
                      "x_acct_group_id" => $hw_customer_group_id); 
                      
        $response = $this->makeCall("update_customer", $params, 120);
		
		return $this->success;
	}

}
?>