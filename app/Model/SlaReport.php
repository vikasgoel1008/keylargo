<?php
class SlaReport extends AppModel {

	var $name = 'SlaReport';
	
	var $validate = array('data_date' => array('rule' => 'date',
        									   'message' => 'Enter a valid date in YYYY-MM-DD format.',
        									   'allowEmpty' => true,
        									   'key' => 'Ymd'));

}
?>