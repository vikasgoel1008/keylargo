<?php
class Blacklist extends AppModel {

	var $name = 'Blacklist';

	function blacklistIP ($ip, $reason) {
		return $this->performBlacklist('', $ip, '', $reason);
	}
	
	function blacklistDomain($domain, $reason) {
		return $this->performBlacklist('', '', $domain, $reason);
	}
	
	function blacklistEmail($email, $reason) {
		//echo "1 - $email";
		return $this->performBlacklist($email, '', '', $reason);
	}
	
	
	private function performBlacklist($email = null, $ip = null, $domain = null, $reason) {
		//echo "2 - $email";
	
		$blacklist = $this->create();
		$blacklist['Blacklist']['email'] = $email;
		$blacklist['Blacklist']['IP'] = $ip;
		$blacklist['Blacklist']['domain'] = $domain;
		$blacklist['Blacklist']['reason'] = $reason;
		
		if ($this->save($blacklist)) { return true;}
		else { return false;}
		
	}
	
	
}
?>