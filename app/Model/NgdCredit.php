<?php
class NgdCredit extends AppModel {

	var $name = 'NgdCredit';

	function getCreditAmount($member_id) {
		
		$fields = array('sum(orig_credit_amount) credit_available');
		$cond = array("member_id" => $member_id,
					  "credit_used" => 'N',
					  "forfeited" => 'N');
		
		$this->recursive = -1;
		$credit = $this->find('first', array('conditions' => $cond,
											 'fields' => $fields));
	}
	
	function forfeitCredit($credit_id) {
		
		//if (!is_array($args)) {return false;}
		
		$fields = array('forfeited');
		$args = array('id' => $credit_id, 'forfeited' => 'Y');
		
		// save the data
		if ($this->save($args, false, $fields)) { return true;}
		else {return false;}
	
	}
}
?>