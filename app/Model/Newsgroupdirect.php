<?php
class Newsgroupdirect extends AppModel {

	var $name = 'Newsgroupdirect';
	//public $useDbConfig = 'highwinds';
	
	
	private $username;
	private $password;
	private $apikey;
	public	$success = FALSE;
	public  $errorMessage;
	
	private $baseUrl = 'https://api.newsgroupdirect.com';
	
	/**
	 * 	set default values
	 */
	function __construct() {
		
		$this->username=urlencode("ngdapi-keylargo");
		$this->password='0qo3uahgvbsmNsU623';
		$this->apikey='8YEFCMVjjsdywu2496';
		//$this->agent="NewsgroupDirect Highwinds Integration";
	}

	
	public function addLineItem($invoice_id, $plan_id, $qty, $desc) {

		$url = $this->baseUrl."/invoices/add_line_item/$invoice_id.json?";
	
		$params = array("plan_id" => $plan_id,
                      "qty" => $qty,
                      "desc" => urlencode($desc)); 
		
		$this->makeCall($url, $params, 120);
	
	}
	
	/**
	 * @member_id - required
	 * @invoice_date - required, format yyyy-mm-dd
	 * @invoice_due_date - optional, format yyyy-mm-dd
	 * @rebill - optional, defaults N
	 * @currency - optional, defaults USD
	 *
	 * /invoices/create_invoice.json?apiuser=00000&apipass=99999&secret=902e94&member_id=19654&invoice_date=2011-07-22
	 */
	public function createInvoice($member_id, $invoice_date, $invoice_due_date = '0000-00-00', $rebill = 'N', $currency = 'USD', $signup = 'N') {
	
		$url = $this->baseUrl."/invoices/create_invoice.json?";
	
		$params = array("member_id" => $member_id,
                      	"invoice_date" => $invoice_date,
                      	"invoice_due_date" => $invoice_due_date,
                      	"currency" => $currency,
                      	"signpu" => $signup,
                      	"rebill" => $rebill); 
		
		return $this->makeCall($url, $params, 120);
	}


	function getMember($member_id = null, $login_username = null) {
		
		$url = $this->baseUrl."/members/lookup.json?";
		
		$params = array("member_id" => $member_id,
                      	"username" => $login_username); 
		
		
		return $this->makeCall($url, $params, 120);
	}
	
	function getMembersActive() {
		$url = $this->baseUrl."/members/get_active_members.json?";
		
		//$params = array("member_id" => $member_id,
        //              	"username" => $login_username); 
		
		
		return $this->makeCall($url, '', 120);
	}
	
	public function makeCall($url, $params, $timeout=120) {
		
		$x_post_vars="apiuser={$this->username}&apipass={$this->password}&secret={$this->apikey}&";
		
		// Convert array of parameters to string
		$post_vars = "";
		if( is_array($params) )
		{
			foreach( $params as $k => $v )
			{
				if( trim($post_vars) != "" )
					$post_vars .= "&{$k}=$v";
				else
					$post_vars = "{$k}=$v";
			}
			
			$post_vars = $x_post_vars.$post_vars;
		}	
		else
			$post_vars = $x_post_vars.$post_vars; 
		
		/*
		//Initialize CURL session
		$ch = curl_init($url.$post_vars);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);        	// Don't verify peer's certificate
		curl_setopt($ch, CURLOPT_NOPROGRESS, 1);            	// Disable progress indicator
		curl_setopt($ch, CURLOPT_VERBOSE, 1);               	// Output verbose information
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION,0);         	// Don't follow any 'Location:' calls
		curl_setopt($ch, CURLOPT_POST, 1);                  	// Do a regular HTTP POST
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_vars);		// Defining the POST variables as set in 'post_vars'
		curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);        	// Timeout set to 2 minutes (120 seconds)
		//curl_setopt($ch, CURLOPT_USERAGENT, $this->agent);  	// Define calling agent
		curl_setopt($ch, CURLOPT_REFERER, $_SERVER["PHP_SELF"]);	// Define the referer in the header sent
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);        	// Return the transfer in a string after execution
		//echo $post_vars;
		$result = curl_exec($ch);
		return $this->readResults($result);
		*/
		$full = $url.$post_vars;
		//echo "$full<br/>";
		
		App::uses('HttpSocket', 'Network/Http');
		$HttpSocket = new HttpSocket();
		//echo $full;die;
		$results = $HttpSocket->post($full, ''); 
		return $this->readResults($results);
		
		
	}
	
	private function readResults($results) {
		
		//print_r($results);
		
		$clean = json_decode($results,true);
		
		if (isset($clean['Error'])) {
			$this->success=FALSE;
			$this->errorMessage = $clean['Error']['errorMessage'];
		}
		else{ $this->success=TRUE; }
		
		
		return $clean;
	}
	
	//**********************************************************
	// DEALS FUNCTIONS
	//**********************************************************
	
	function consumeDeal($alternate) {
		$url = $this->baseUrl."/deals/consume_deal/$alternate.json?";
		
		$params = ''; 

		return $this->makeCall($url, $params, 120);
	}
	
	function activeDealForPlan($plan_id) {
		$url = $this->baseUrl."/deals/is_active_deal_for_plan/$plan_id.json?";
		
		$params = ''; 

		return $this->makeCall($url, $params, 120);
	}
	
	function isDealActive($alternate) {
		$url = $this->baseUrl."/deals/is_deal_active/$alternate.json?";
		
		$params = ''; 

		return $this->makeCall($url, $params, 120);
	}
	
	function getDeal($alternate) {
		$url = $this->baseUrl."/deals/get_deal/$alternate.json?";
		
		$params = ''; 

		return $this->makeCall($url, $params, 120);
	}
	
	function getDeals() {
		$url = $this->baseUrl."/deals/get_deals.json?";
		
		$params = ''; 

		return $this->makeCall($url, $params, 120);
	}
	
	function getDealsBlackFriday($year) {
		$url = $this->baseUrl."/deals/get_black_friday_deals/$year.json?";
		
		$params = ''; 

		return $this->makeCall($url, $params, 120);
	}
	
	function getDealsTBTuesday($active = 'Y') {
		$url = $this->baseUrl."/deals/get_terabyte_tuesday_deals.json?";
		
		$params = array('n_active_deal' => $active); 
				
		return $this->makeCall($url, $params, 120);
	}
	
}

?>