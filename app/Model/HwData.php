<?php
class HwData extends AppModel {

	var $name = 'HwData';

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $belongsTo = array('Member', "Plan");
	
	function getMovingAverage($days, $base_date) {
		
		$this->unbindModelAll();
		// take base date and get x# days back date
		$bday_string = strtotime($base_date);
		$hist_date = date('Y-m-d', mktime(0,0,0,date('m',$bday_string),date('d',$bday_string)-($days),date('Y',$bday_string)));

		// set conditions
		$cond['data_type'] = 'daily';
		$cond['usage_date <='] = $base_date;
		$cond['usage_date >'] = $hist_date;
		//$cond["to_days('$base_date')-to_days(usage_date) between ? and ?"] = array(0,$days-1);
		
		$bytes = $this->find('all', array('fields'	=> array("(((sum(total_bytes))/1000000000)/$days) bytes"),
													'conditions' => $cond));
		
		return $bytes[0][0]['bytes'];
		
	}
	
	function graph_daily_bw_usage($member_id = null, $days = 180) {
		
		$this->unbindModelAll();
		$cond['data_type'] = 'daily';
		//$cond['to_days(now())-to_days(usage_date) <='] = $days;
		$cond['to_days(now())-to_days(usage_date) between ? and ?'] = array(0,$days-1);
		
	
		if (!empty($member_id)) { $cond['member_id'] = $member_id; }
		
		$bytes = $this->find('all', array('fields'	=> array('((sum(total_bytes))/1000000000) bytes', 
																	  	 'usage_date'),
                                     			   	'order' 	=> array('usage_date'),
												 	'group'	 	=> array('usage_date'),
													'conditions' => $cond));
		
		return $bytes;
		
	}
	
	function insertDailyRecord($member_id, $hw_cust_id, $usage_date, $bytes_used) {
		
		$this->insertRecord($member_id, $hw_cust_id, $usage_date, $bytes_used, 'daily');
		
	}
	
	function insertMonthlyRecord($member_id, $hw_cust_id, $usage_date, $bytes_used) {
		
		$this->insertRecord($member_id, $hw_cust_id, $usage_date, $bytes_used, 'monthly');
		
	}
	
	function insertRecord($member_id, $hw_cust_id, $usage_date, $bytes_used, $data_type, $ip = null, $hw_group_id = null) {
		
		$this->create();
		$hwd['HwData']['member_id'] = $member_id;
		$hwd['HwData']['data_type'] = $data_type;
		$hwd['HwData']['hw_cust_id'] = $hw_cust_id;
		$hwd['HwData']['total_bytes'] = $bytes_used;
		$hwd['HwData']['usage_date'] = $usage_date;
		$hwd['HwData']['load_datetime'] = date("Y-m-d H:i:s");
		$hwd['HwData']['ip_address'] = $ip;
		$hwd['HwData']['hw_group_id'] = $hw_group_id;
		
		if ($this->save($hwd)) { return true;}
		else { return false; }
	}

}
?>