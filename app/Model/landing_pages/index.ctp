<div class="LandingPages index">
	<h2>Landing Pages (A/B Testing)</h2>
	<?php echo $this->element('landings_menu');?>
	
	<?php $paginator->options(array('url' => $this->passedArgs));?>
	
	<div class="box">
		<p>&nbsp;</p>
		<table cellpadding="0" cellspacing="0">
			<tr class="highlight">	
				<td colspan="7"><?php  echo $paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)));?></td>
			</tr>
			
			<tr class="highlight">
				<td><?php echo $paginator->sort("Campaign",'name');?></td>
				<td><?php echo $paginator->sort("Template Description",'description');?></td>
				<td><?php echo $paginator->sort('landings');?></td>
				<td><?php echo $paginator->sort('conversions');?></td>
				<td><?php echo $paginator->sort('default');?></td>
				<td>Status</td>
				<td>Actions</td>
			</tr>
			
			<?php foreach ($lps as $lp): ?>
			
			<tr class="highlight">
				<td><?php echo $html->link($lp['LandingPage']['name'], array('action' => 'view', $lp['LandingPage']['id'])); ?></td>
				<td><?php echo $lp['LandingPage']['description']; ?></td>
				<td><?php echo $lp['LandingPage']['landings']; ?></td>
				<td><?php echo $lp['LandingPage']['conversions']; ?></td>
				<td><?php echo $lp['LandingPage']['default']; ?></td>
				
				<td>
					
					<?php 	$status=''; 
						 	if ($lp['LandingPage']['start_date'] < date("Y-m-d H:i:s") && $lp['LandingPage']['expire_date'] > date("Y-m-d H:i:s")) { 
						 		$status = 'active';
						 	}
						 	else {$status = 'ended';}
						 	
						 	if ($lp['LandingPage']['in_test'] == 'Y') {
							 	if (!empty($status)) { $status .= ' | ';}
							 	$status.='test';							 	
						 	}
						 	echo $status;
					?>
				</td>
				
				<td>
					
					<?php // see if active ?>
					<?php if ($lp['LandingPage']['start_date'] < date("Y-m-d H:i:s") && $lp['LandingPage']['expire_date'] > date("Y-m-d H:i:s")): ?>
					
						<?php // see if default ?>
						<?php if ($lp['LandingPage']['default'] == 'Y' && $lp['LandingPage']['in_test'] == 'N'): ?>		
							<?php echo $html->link('New Test', array('action' => 'new_test', $lp['LandingPage']['id'])); ?>
						<?php else : ?>
						
						<?php endif; ?>
						
					<?php else : ?>
					
					<?php endif; ?>
									
					
				</td>
				
			</tr>
			
			<?php endforeach; ?>
		</table>

		<p id="pagin">
			<?php echo $paginator->first();?>
		 	<?php echo $paginator->prev('< previous', "", "", "");?>
		 	<?php //echo $paginator->prev() ?> 
			<?php echo $paginator->numbers();?>  
		 	<?php //echo $paginator->next() ?>
			<?php //echo $paginator->counter(); ?>
			<?php echo $paginator->next('next >', "", "", "");?>
			<?php echo $paginator->last();?>
		<p/>

		<span class="clear"></span>
	</div>
</div>

